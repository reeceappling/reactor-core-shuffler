# Simulate3 Core Shuffler

Graphical core shuffler for Simulate3 output files in quarter-core-symmetric layouts to generate input files for the following cycle.

![Shuffler](images/fullApp.PNG)

Note: this program has not yet been extended to arbitrary reactor geometries

# Usage

## Usage Options

- **Download** this repository and open shuffler.html in a browser
- link to the **[live copy](https://reeceappling.gitlab.io/reactor-core-shuffler/shuffler.html)** of the most current commit.

## Load data

In the file area, either drop a file, select a file, or select the default link

![Input area](images/fileSelect.PNG)

After selecting a useable file, the page should populate the core representations with all necessary data

## Workspace Layout

left=original layout, center=changeable octant sections, right=current changes

![Workspace Layout](images/fullApp.PNG)

## Swapping assemblies
Drag and drop in the subsections to shuffle existing assemblies (vert/diag to vert/diag, triangle to triangle),

![modifiable octant](images/workspace2.png)

## New Assemblies

### Creation of assembly types

In order to start implementing new assemblies with/without Burnable Absorbers, find the assembly creation menu above the right core.

![New Assembly Menu](images/newAssembly.PNG)

### Swapping out assemblies

Doubleclick an assembly in a subsection to alter it based on the selected NEW Assembly

If the clicked assembly is not new it will replace it with the new assembly selected in new assembly fields

If the clicked assembly is already new it will change it to whatever non-new assembly which would be shuffled there

![Core w/ new Asseblies](images/newAssembly2.PNG)

## Output and Logs

Output is generated when the button below the current (right) core is clicked

Lastly, all logs, outputs, and help can be found at the bottom of the page in a "console," which can be toggled to visible or hidden.

![Output console](images/console.PNG)

