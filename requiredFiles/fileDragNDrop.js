var recentFile;
var canreset=false;

// getElementById
function $id(id) {
	return document.getElementById(id);
}

// output information
function Output(msg){
	$id("msgs").innerHTML =  msg + $id("msgs").innerHTML;
}

function showHelp(){
	Output("<b>Instructions:</b>"+
				"<p>Swapping assembly positions: </p>"+
				"<p>---Simply drag one assembly position in the middle peices into another appropriate position</p>"+
				"<p>---(triangular into itself, vertical/diagonal into vertical/diagonal)</p>"+
				"<p>Adding/removing Burnable Absorbers:</p>"+
				"<p>---Assembly types can be added by pressing 'Add a new rod field'</p>"+
				"<p>---# of Integral Fuel Burnable Absorber and Wet Annular Burnable Absorber rods can be changed for each type at any time</p>"+
				"<p>---To add a custom assembly type to the layout, in the central area, double click a cell to rotate through the options</p>"+
				"<b>End of Instructions</b>");
}



function drop_handler(e){
	if (e.preventDefault) {e.preventDefault();}
	//if (e.preventDefault) { e.preventDefault(); }
	// fetch FileList object
	var files = e.target.files || e.dataTransfer.files;
	var fi = 0;
	if(files.length==0){
		//---------------------------impossible?-------------------------------
	}
	else{
		if(files.length>1){
			for(var i=0;i<files.length;i++){
				if(files[i].name.split('.')[1]=='out'){
					fi = i;
					break;
				}
			}
		}
	}
	return ParseFile(files[fi]);
}

function containsFiles(ev){
    if(ev.dataTransfer.types){
        for(var i=0;i<ev.dataTransfer.types.length;i++){
			if(ev.dataTransfer.types[i] != "Files"){
				return false;
			}
		}
		return true;
	}    
	return false;
}

function dragover_handler(e){
	e.preventDefault();
}

function dragend_handler(e){
	if (e.preventDefault) {e.preventDefault();}
}

function ParseFile(file) {
	if(file.name.split('.')[1]=='out'){
		Output(
			"<p>"+now()+"</p>"+
			"<p class=\"indent\" style=\"color:green;\">	<strong>Accepted File: " + file.name +
			"</strong> size: <strong>" + file.size/1000 +
			"</strong> kB</p>"
		);
		recentFile=file;
		canreset=true;
		$('#loadtext').html("Current file: "+file.name);
		$('#filedroptext').html("Drag another .out file here to reset, or");
		$('#loadtext').css("color","green");
		loadFile(file);
	}
	else {
		if($('#loadtext').css("color")!="rgb(0, 255, 0)"){
			$('#loadtext').html("Incorrect File Format");
			$('#loadtext').css("color","red");
			alert("Incorrect file format, files must have .out extension");
		}
		else{alert("Incorrect file format, work has not been reset");}
		Output(
			"<p>"+now()+"</p>"+
			"<p class=\"indent\" style=\"color:red;\">	<strong>Rejected File: " + file.name +
			"</strong> size: <strong>" + file.size/1000 +
			"</strong> kB</p>"
		);
	}
	return false;
}

function loadExample(){
	$('#loadtext').html("Current file: EXAMPLE-OUT.OUT");
	$('#filedroptext').html("Drag another .out file here to reset, or");
	$('#loadtext').css("color","green");
	loadWorkspace(exampleTemplate);
	Output(
		"<p>"+now()+"</p>"+
		"<p class=\"indent\" style=\"color:green;\">	<strong>Loaded File: EXAMPLE-OUT.OUT"+
		"</p>"
	);
}

$( document ).ready(function() {
    document.getElementById('fileselect').addEventListener('change',drop_handler,false);
	Output("<p>"+now()+"</p>"+
			"<p class=\"indent\">Program Started</p>"
		);
});
