const exampleTemplate = `
   This source code has included the following optional modules:
 _____________________________________________________________________________________________________________________________
      1 - MOX Models Present (v4)
      2 - Speed Models Present
      3 - Fortran-90 Code Present
      4 - BIG_ENDIAN File Conversion Flag Active for I/O
      5 - Pin Heavy Metal Loading Model Present
      6 - Integrated Pin Exposure Model Present
 _____________________________________________________________________________________________________________________________

 Installed Dimension Limits:
 Assembly Core Maps . . . .(34,34)            Control Rod Assembly Types . . 20           Core Flow Orificing Zones. . . 10
 Nodal Core Maps. . . . . .(40,40)            Control Rod Axial Zones. . . .  7           Editing Batches. . . . . . . .500
 Axial Nodes. . . . . . . . . . 52            Control Rod Groups . . . . . .150           TAB.TFU/XKS. .  20 Tables (10, 5)
 Fuel Segment Types . . . . . .200            Control Sequence Passes. . . . 50           Axial Buckling Table Points. . 15
 Fuel Assembly Types. . . . . .200            Depletion Arguments. . . . . . 30           Real Detector Locations. . . .250
 Fuel Assembly Axial Zones. . . 19            Depletion Steps per Case . . .501           RES Input Restart Files. . . . 50
 Fuel Mechanical Types. . . . . 20            PRI.INP(20)     PRI.STA(50)                 WRE Restart File Requests. . .501
 Fuel Spacer Locations. . . . . 20            PIN.EDT(50)
 Total Storage (Words). . .DYNAMIC            PRI.ITE(10)     PRI.MAC(10)
                                              PRI.DFS(10)     BAT.EDT(50)

 ----Listing of Input Cards - 288 Columns---- 

 'DIM.PWR' 15 15 15 0/
 'DIM.CAL' 24 2 2 1/
 'DIM.DEP' 'HTMO' 'HTFU' 'SAM' 'HBOR' 'EXP' 'PIN' 'EBP'/
  
  
  
 'FUE.LAB' 6/
  1  1                             TYPE01 TYPE01 TYPE01 TYPE01 TYPE01 TYPE01 TYPE01
  2  1               TYPE01 TYPE01 24E-14 TYPE01 24E-13 TYPE01 24L-13 TYPE01 24L-14 TYPE01 TYPE01
  3  1        TYPE01 TYPE01 24F-14 24G-13 24D-13 TYPE01 24H-14 TYPE01 24M-13 24J-13 24K-14 TYPE01 TYPE01
  4  1        TYPE01 24B-10 TYPE01 24D-10 TYPE01 24G-14 TYPE01 24J-14 TYPE01 24M-10 TYPE01 24P-10 TYPE01
  5  1 TYPE01 24B-11 24C-09 24F-12 TYPE01 24G-11 24E-11 24H-12 24L-11 24J-11 TYPE01 24K-12 24N-09 24P-11 TYPE01
  6  1 TYPE01 TYPE01 24C-12 TYPE01 24E-09 TYPE01 24E-12 TYPE01 24L-12 TYPE01 24L-09 TYPE01 24N-12 TYPE01 TYPE01
  7  1 TYPE01 24G-09 TYPE01 24B-09 24C-13 24F-10 24C-11 24H-10 24N-11 24K-10 24N-13 24P-09 TYPE01 24J-09 TYPE01
  8  1 TYPE01 TYPE01 24B-08 TYPE01 24D-08 TYPE01 24F-08 TYPE01 24K-08 TYPE01 24M-08 TYPE01 24P-08 TYPE01 TYPE01
  9  1 TYPE01 24G-07 TYPE01 24B-07 24C-03 24F-06 24C-05 24H-06 24N-05 24K-06 24N-03 24P-07 TYPE01 24J-07 TYPE01
 10  1 TYPE01 TYPE01 24C-04 TYPE01 24E-07 TYPE01 24E-04 TYPE01 24L-04 TYPE01 24L-07 TYPE01 24N-04 TYPE01 TYPE01
 11  1 TYPE01 24B-05 24C-07 24F-04 TYPE01 24G-05 24E-05 24H-04 24L-05 24J-05 TYPE01 24K-04 24N-07 24P-05 TYPE01
 12  1        TYPE01 24B-06 TYPE01 24D-06 TYPE01 24G-02 TYPE01 24J-02 TYPE01 24M-06 TYPE01 24P-06 TYPE01
 13  1        TYPE01 TYPE01 24F-02 24G-03 24D-03 TYPE01 24H-02 TYPE01 24M-03 24J-03 24K-02 TYPE01 TYPE01
 14  1               TYPE01 TYPE01 24E-02 TYPE01 24E-03 TYPE01 24L-03 TYPE01 24L-02 TYPE01 TYPE01
 15  1                             TYPE01 TYPE01 TYPE01 TYPE01 TYPE01 TYPE01 TYPE01
  0  0     FUE.LAB/SER OR BPR.SER
 'COM'/
  
 'COM'               SERIAL   NUMBER TO    FUEL    BATCH
 'COM'               LABEL     CREATE	  TYPE   NUMBER
 'FUE.NEW' 'TYPE01', 'sil00',	  97,        44,    ,,1/
  
 'RES' 'c1c24eoc.res' 20000.  /
  
 'LIB' './cms.pwr-all2.lib'/
 'SEG.LIB'  44 'sil495p0a0' 0 0/
 'SEG.DAT'  44 4.95, 0,  0,0/
 'FUE.ZON' 44 1 'sil495p0a0' 1 .000 5 15.240 44 22.860 44 30.480 44 335.280 44 342.900 44 350.520 4 365.760 3/
  
  
  
  
 ITE.BOR 1500/
 COR.OPE 100/
  
 'DEP.CYC' 'c1c25' .000 25 20181227 20181129/ * new cycle date, prev. end cyle date
 'ERR.SUP'/
 'DEP.STA' 'AVE' 0.0 -25. 150. -50. 590. /                       * Cycle exposure steps (EFPD)
 'STA'/
 'DEP.STA' 'AVE' 620. /                       * Cycle exposure steps (EFPD)
 'COM' The following performs an automated search on power at a boron of 0 ppmCOM
 'ITE.SRC' 'SET' 'POWER'/
  
 'WRE' 'c1c25eoc.res'/
 'STA'/
 'END'/
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page    1
 Run: RUN NAME  ** Project: PROJECT NAME             ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0                                                                                    0.00 ppm    0.000 GWd/MT  

 CASE   1  --- Entering Input Segment ---
           --- Reading Input Cards ---
 _____________________________________________________________________________________________________________________________
 
 Input Cards:  DIM.PWR, DIM.CAL, DIM.DEP, FUE.LAB

 DIM.PWR and DIM.CAL Specify Calculation Dimensions:

 Full Core Assembly Map Width . IAFULL  15
 Control Rod Map Width  . . . . . IRMX  15                                              Rows     Columns
 Detector Map Width . . . . . . . ILMX  15        FUE.TYP Dimensions(+Reflector) IDR       9      JDR  9
 Offset Assemblies Flag . . . . IOFSET   0        FUE.MAP Dimensions . . . . . . IDA       8      JDA  8
 Axial Nodes (Incl. Refl.). . . . . KD  26        QTR.MAP Dimensions . . . . . . .ID      16      JD  16
 Core Fraction Flag . . . . . . .IHAVE   2        Depletion Arguments  . . . . . .ND      18
 Node Mesh per Assembly . . . . .IF2X2   2
 Reflector Layers . . . . . . . . NREF   1
 Radial Core Fraction . . . . . .    0.250
 

 DIM.DEP - Depletion Argument Assignments:

  1   IOD  - Iodine-135 - atoms/(bn-cm)       
  2   XEN  - Xenon-135 - atoms/(bn-cm)        
  3   PRO  - Promethium-149 - atoms/(bn-cm)   
  4   SAM  - Samarium-149 - atoms/(bn-cm)     
  5   EXP  - Exposure - GWd/t                 
  6   HTMO - Moderator Density History - g/cc 
  7   HTFU - Fuel Temperature History -SQRT(K)
  8   HBOR - Moderator Boron History - ppm    
  9   EBP  - Burnable Poison Exposure - GWd/t 
 10   EY-  - Surface Exposure - Y minus       
 11   EX+  - Surface Exposure - X plus        
 12   EY+  - Surface Exposure - Y plus        
 13   EX-  - Surface Exposure - X minus       
 14   HIS  - Spectral History - Average       
 15   HY-  - Spectral History - Y minus       
 16   HX+  - Spectral History - X plus        
 17   HY+  - Spectral History - Y plus        
 18   HX-  - Spectral History - X minus       
 
 DIM.MEM - Dynamic Memory Allocation

 Allocating 30000000 words to the container                                                          
 
 FUEL LABEL FIXED FORMAT: (I2,1X,I2, 34(1X,A6))         

 FUE.NEW expands list of available serial numbers to  97 assemblies, counting at base 10
 Label TYPE01 Serial sil00   to  
 Label TYPE01 Serial sil96 
 
 Input Cards:  COM    , COM    , COM    , FUE.NEW, RES    
 _____________________________________________________________________________________________________________________________

 RES - Begin to reload the core
 _____________________________________________________________________________________________________________________________

 RES - Opening restart file:c1c24eoc.res
       Looking for exposure 20000.0000

 Reading PWR Restart file created by SIMULATE-3 version:1.00.02 
 TIT.RUN = NOM DEPL TIT.PRO = C1C24 SIM3 FFCD     
 TIT.CAS = c1c24 HFP NOMINAL DEPLETION                                                     
 DEP.CYC = c1c24    Step exposure =     537.0000  Core average exposure =  40.6968
 Run Time = 15.44.35  Run Date = 18/01/12
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page    2
 Run: RUN NAME  ** Project: PROJECT NAME             ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0                                                                                    0.00 ppm    0.000 GWd/MT  

  Parameters on Restart File:
  Parameter  Restart  Installed      Parameter  Restart  Installed      Parameter  Restart  Installed     
  ---------  -------  ---------      ---------  -------  ---------      ---------  -------  ---------     
  LIMSEG         200        200      LIMHYD           5          5      LIMID          40         40
  LIMIDA          34         34      LIMJD           40         40      LIMKD          52         52
  LIMMAP         250        250      LIMMDA          20         20      LIMMDB         30         30
  LIMFUE         200        200      LIMNHT          10         10      LIMPAS         50         50
  LIMRAY         150        150      LIMREG          19         19      LIMBTL        500        500
  LIMTFU           4          4      LIMZON          20         20      LMNHYD         20         20
  LIMBSQ          15         15      LIMGRD          35         35      LIMSPA         20         20
  LCRZON           7          7      LIMCRD          20         20      ICLDIM   30000000   30000000
  LIMBTS         500        500      LIMDEP         501        501      LIMWTR          3          3
  LIMSUP           8          8      LIMRES          50         50      LIMWRE        501        501
  LIMSET          20         20      LIMWLW          10         10      LIMLOK         10         10
  LIMGT           25         25      LMRODS          15         15      LIMELE          8          8
  LIMWLP          10         10      LIMLKG           3          3      LIMCYC        300        300
  LIMBAT          50         50

 Maximum length of segment/fuel type names on restart file: 20 characters

 Restart File DIM.CAL:  KDFUEL = 24, IHAVE = 2, IF2X2 = 2, Core Symmetry = ROTATION
 Restart File DIM.DEP:  HTMO, HTFU, SAM , HBOR, EXP , PIN , EBP 

 DIM.PWR and DIM.CAL Specify Calculation Dimensions:

 Full Core Assembly Map Width . IAFULL  15
 Control Rod Map Width  . . . . . IRMX  15                                              Rows     Columns
 Detector Map Width . . . . . . . ILMX  15        FUE.TYP Dimensions(+Reflector) IDR       9      JDR  9
 Offset Assemblies Flag . . . . IOFSET   0        FUE.MAP Dimensions . . . . . . IDA       8      JDA  8
 Axial Nodes (Incl. Refl.). . . . . KD  26        QTR.MAP Dimensions . . . . . . .ID      16      JD  16
 Core Fraction Flag . . . . . . .IHAVE   2        Depletion Arguments  . . . . . .ND      18
 Node Mesh per Assembly . . . . .IF2X2   2
 Reflector Layers . . . . . . . . NREF   1
 Radial Core Fraction . . . . . .    0.250
 

 DIM.DEP - Depletion Argument Assignments:

  1   IOD  - Iodine-135 - atoms/(bn-cm)       
  2   XEN  - Xenon-135 - atoms/(bn-cm)        
  3   PRO  - Promethium-149 - atoms/(bn-cm)   
  4   SAM  - Samarium-149 - atoms/(bn-cm)     
  5   EXP  - Exposure - GWd/t                 
  6   HTMO - Moderator Density History - g/cc 
  7   HTFU - Fuel Temperature History -SQRT(K)
  8   HBOR - Moderator Boron History - ppm    
  9   EBP  - Burnable Poison Exposure - GWd/t 
 10   EY-  - Surface Exposure - Y minus       
 11   EX+  - Surface Exposure - X plus        
 12   EY+  - Surface Exposure - Y plus        
 13   EX-  - Surface Exposure - X minus       
 14   HIS  - Spectral History - Average       
 15   HY-  - Spectral History - Y minus       
 16   HX+  - Spectral History - X plus        
 17   HY+  - Spectral History - Y plus        
 18   HX-  - Spectral History - X minus       
 

 RES - Logical Flags Controlling the Reloading Process:

 Shuffling is according to new FUE.LAB  - T
 Shuffling is according to new FUE.SER  - F
 Reloading is direct, without shuffle   - F
 All input is taken from this file      - T
 Only depletion arguments are taken     - F
 Rotations are according to new FUE.ROT - F

 _____________________________________________________________________________________________________________________________

 RES - Assemblies from FUE.NEW or Reinserted by RES   (2D exposures are nodal averages --Loading not taken into account)
 Serial  Label   ---------Previous-------------------- -Present--
                 (IAF,JAF) FUE.TYP  FUE.EXP*  Quadrant (IAF,JAF) 
 24H-10  24H-10   (10, 8)     28    28.221           1  ( 7, 8)
 24K-08  24K-08   ( 8, 6)     28    28.274           1  ( 8, 9)
 24H-12  24H-12   (12, 8)     29    28.209           1  ( 5, 8)
 24M-08  24M-08   ( 8, 4)     29    27.659           1  ( 8,11)
 24H-14  24H-14   (14, 8)     39    26.924           1  ( 3, 8)
 24P-08  24P-08   ( 8, 2)     39    23.444           1  ( 8,13)
 24G-09  24G-09   ( 9, 9)     40    27.312           1  ( 7, 2)
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page    3
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    
 24G-07  24G-07   ( 7, 9)     40    27.838           1  ( 9, 2)
 24J-09  24J-09   ( 9, 7)     40    27.843           1  ( 7,14)
 24J-07  24J-07   ( 7, 7)     40    27.859           1  ( 9,14)
 24E-09  24E-09   ( 9,11)     32    27.518           1  ( 6, 5)
 24G-05  24G-05   ( 5, 9)     32    27.509           1  (11, 6)
 24J-11  24J-11   (11, 7)     32    27.666           1  ( 5,10)
 24L-07  24L-07   ( 7, 5)     32    27.843           1  (10,11)
 24C-09  24C-09   ( 9,13)     26    45.661           1  ( 5, 3)
 24G-03  24G-03   ( 3, 9)     26    49.032           1  (13, 5)
 24J-13  24J-13   (13, 7)     26    46.361           1  ( 3,11)
 24N-07  24N-07   ( 7, 3)     26    49.535           1  (11,13)
 24B-09  24B-09   ( 9,14)     38    27.217           1  ( 7, 4)
 24G-02  24G-02   ( 2, 9)     38    23.649           1  (12, 7)
 24J-14  24J-14   (14, 7)     38    27.423           1  ( 4, 9)
 24P-07  24P-07   ( 7, 2)     38    23.666           1  ( 9,12)
 24F-08  24F-08   ( 8,10)     28    28.224           1  ( 8, 7)
 24H-06  24H-06   ( 6, 8)     28    28.281           1  ( 9, 8)
 24F-10  24F-10   (10,10)     23    41.260           1  ( 7, 6)
 24F-06  24F-06   ( 6,10)     23    44.922           1  ( 9, 6)
 24K-10  24K-10   (10, 6)     23    44.929           1  ( 7,10)
 24K-06  24K-06   ( 6, 6)     23    48.767           1  ( 9,10)
 24D-10  24D-10   (10,12)     42    28.171           1  ( 4, 5)
 24F-04  24F-04   ( 4,10)     42    27.614           1  (11, 4)
 24K-12  24K-12   (12, 6)     42    28.680           1  ( 5,12)
 24M-06  24M-06   ( 6, 4)     42    28.351           1  (12,11)
 24B-10  24B-10   (10,14)     38    27.543           1  ( 4, 3)
 24F-02  24F-02   ( 2,10)     38    23.483           1  (13, 4)
 24K-14  24K-14   (14, 6)     38    27.514           1  ( 3,12)
 24P-06  24P-06   ( 6, 2)     38    22.926           1  (12,13)
 24G-11  24G-11   (11, 9)     32    27.533           1  ( 5, 6)
 24E-07  24E-07   ( 7,11)     32    27.675           1  (10, 5)
 24L-09  24L-09   ( 9, 5)     32    27.515           1  ( 6,11)
 24J-05  24J-05   ( 5, 7)     32    27.844           1  (11,10)
 24E-11  24E-11   (11,11)     33    28.389           1  ( 5, 7)
 24E-05  24E-05   ( 5,11)     33    28.749           1  (11, 7)
 24L-11  24L-11   (11, 5)     33    28.737           1  ( 5, 9)
 24L-05  24L-05   ( 5, 5)     33    28.704           1  (11, 9)
 24E-04  24E-04   ( 4,11)     24    50.860           1  (10, 7)
 24L-12  24L-12   (12, 5)     24    45.158           1  ( 6, 9)
 24C-11  24C-11   (11,13)     29    29.083           1  ( 7, 7)
 24E-03  24E-03   ( 3,11)     29    28.350           1  (14, 7)
 24L-13  24L-13   (13, 5)     29    28.606           1  ( 2, 9)
 24N-05  24N-05   ( 5, 3)     29    27.134           1  ( 9, 9)
 24B-11  24B-11   (11,14)     37    27.058           1  ( 5, 2)
 24E-02  24E-02   ( 2,11)     37    21.651           1  (14, 5)
 24L-14  24L-14   (14, 5)     37    24.418           1  ( 2,11)
 24P-05  24P-05   ( 5, 2)     37    18.269           1  (11,14)
 24D-08  24D-08   ( 8,12)     29    28.208           1  ( 8, 5)
 24H-04  24H-04   ( 4, 8)     29    27.652           1  (11, 8)
 24F-12  24F-12   (12,10)     42    28.160           1  ( 5, 4)
 24D-06  24D-06   ( 6,12)     42    28.659           1  (12, 5)
 24M-10  24M-10   (10, 4)     42    27.596           1  ( 4,11)
 24K-04  24K-04   ( 4, 6)     42    28.319           1  (11,12)
 24E-12  24E-12   (12,11)     24    49.371           1  ( 6, 7)
 24L-04  24L-04   ( 4, 5)     24    46.842           1  (10, 9)
 24C-12  24C-12   (12,13)     41    26.359           1  ( 6, 3)
 24D-03  24D-03   ( 3,12)     41    23.614           1  (13, 6)
 24M-13  24M-13   (13, 4)     41    24.015           1  ( 3,10)
 24N-04  24N-04   ( 4, 3)     41    20.147           1  (10,13)
 24G-13  24G-13   (13, 9)     26    45.665           1  ( 3, 5)
 24C-07  24C-07   ( 7,13)     26    46.385           1  (11, 3)
 24N-09  24N-09   ( 9, 3)     26    49.027           1  ( 5,13)
 24J-03  24J-03   ( 3, 7)     26    49.536           1  (13,11)
 24E-13  24E-13   (13,11)     29    29.019           1  ( 2, 7)
 24C-05  24C-05   ( 5,13)     29    28.490           1  ( 9, 7)
 24N-11  24N-11   (11, 3)     29    28.295           1  ( 7, 9)
 24L-03  24L-03   ( 3, 5)     29    27.048           1  (14, 9)
 24D-13  24D-13   (13,12)     41    26.205           1  ( 3, 6)
 24C-04  24C-04   ( 4,13)     41    23.918           1  (10, 3)
 24N-12  24N-12   (12, 3)     41    23.501           1  ( 6,13)
 24M-03  24M-03   ( 3, 4)     41    20.024           1  (13,10)
 24C-13  24C-13   (13,13)     43    16.455           1  ( 7, 5)
 24C-03  24C-03   ( 3,13)     43    12.980           1  ( 9, 5)
 24N-13  24N-13   (13, 3)     43    12.902           1  ( 7,11)
 24N-03  24N-03   ( 3, 3)     43     9.508           1  ( 9,11)
 24B-08  24B-08   ( 8,14)     39    26.926           1  ( 8, 3)
 24H-02  24H-02   ( 2, 8)     39    23.470           1  (13, 8)
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page    4
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    
 24G-14  24G-14   (14, 9)     38    27.223           1  ( 4, 7)
 24B-07  24B-07   ( 7,14)     38    27.425           1  ( 9, 4)
 24P-09  24P-09   ( 9, 2)     38    23.690           1  ( 7,12)
 24J-02  24J-02   ( 2, 7)     38    23.685           1  (12, 9)
 24F-14  24F-14   (14,10)     38    27.533           1  ( 3, 4)
 24B-06  24B-06   ( 6,14)     38    27.489           1  (12, 3)
 24P-10  24P-10   (10, 2)     38    23.482           1  ( 4,13)
 24K-02  24K-02   ( 2, 6)     38    22.908           1  (13,12)
 24E-14  24E-14   (14,11)     37    27.013           1  ( 2, 5)
 24B-05  24B-05   ( 5,14)     37    24.351           1  (11, 2)
 24P-11  24P-11   (11, 2)     37    21.616           1  ( 5,14)
 24L-02  24L-02   ( 2, 5)     37    18.218           1  (14,11)
 _____________________________________________________________________________________________________________________________

 RES - Opening restart file:NEWFUEL
       Looking for exposure     0.0000

 RES - Logical Flags Controlling the Reloading Process:

 Shuffling is according to new FUE.LAB  - T
 Shuffling is according to new FUE.SER  - F
 Reloading is direct, without shuffle   - F
 All input is taken from this file      - F
 Only depletion arguments are taken     - T
 Rotations are according to new FUE.ROT - F

 _____________________________________________________________________________________________________________________________

 RES - Assemblies from FUE.NEW or Reinserted by RES   (2D exposures are nodal averages --Loading not taken into account)
 Serial  Label   ---------Previous-------------------- -Present--
                 (IAF,JAF) FUE.TYP  FUE.EXP*  Quadrant (IAF,JAF) 
 sil00   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 5)
 sil00   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 5)
 sil00   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 5)
 sil00   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 5)
 sil01   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 6)
 sil01   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 6)
 sil01   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 6)
 sil01   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 6)
 sil02   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 7)
 sil02   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 7)
 sil02   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 7)
 sil02   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 7)
 sil03   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 8)
 sil03   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 8)
 sil03   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 8)
 sil03   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 8)
 sil04   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 9)
 sil04   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 9)
 sil04   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 9)
 sil04   TYPE01   ( 0, 0)     44     0.000           0  ( 1, 9)
 sil05   TYPE01   ( 0, 0)     44     0.000           0  ( 1,10)
 sil05   TYPE01   ( 0, 0)     44     0.000           0  ( 1,10)
 sil05   TYPE01   ( 0, 0)     44     0.000           0  ( 1,10)
 sil05   TYPE01   ( 0, 0)     44     0.000           0  ( 1,10)
 sil06   TYPE01   ( 0, 0)     44     0.000           0  ( 1,11)
 sil06   TYPE01   ( 0, 0)     44     0.000           0  ( 1,11)
 sil06   TYPE01   ( 0, 0)     44     0.000           0  ( 1,11)
 sil06   TYPE01   ( 0, 0)     44     0.000           0  ( 1,11)
 sil07   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 3)
 sil07   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 3)
 sil07   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 3)
 sil07   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 3)
 sil08   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 4)
 sil08   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 4)
 sil08   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 4)
 sil08   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 4)
 sil09   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 6)
 sil09   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 6)
 sil09   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 6)
 sil09   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 6)
 sil10   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 8)
 sil10   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 8)
 sil10   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 8)
 sil10   TYPE01   ( 0, 0)     44     0.000           0  ( 2, 8)
 sil11   TYPE01   ( 0, 0)     44     0.000           0  ( 2,10)
 sil11   TYPE01   ( 0, 0)     44     0.000           0  ( 2,10)
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page    5
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    
 sil11   TYPE01   ( 0, 0)     44     0.000           0  ( 2,10)
 sil11   TYPE01   ( 0, 0)     44     0.000           0  ( 2,10)
 sil12   TYPE01   ( 0, 0)     44     0.000           0  ( 2,12)
 sil12   TYPE01   ( 0, 0)     44     0.000           0  ( 2,12)
 sil12   TYPE01   ( 0, 0)     44     0.000           0  ( 2,12)
 sil12   TYPE01   ( 0, 0)     44     0.000           0  ( 2,12)
 sil13   TYPE01   ( 0, 0)     44     0.000           0  ( 2,13)
 sil13   TYPE01   ( 0, 0)     44     0.000           0  ( 2,13)
 sil13   TYPE01   ( 0, 0)     44     0.000           0  ( 2,13)
 sil13   TYPE01   ( 0, 0)     44     0.000           0  ( 2,13)
 sil14   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 2)
 sil14   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 2)
 sil14   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 2)
 sil14   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 2)
 sil15   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 3)
 sil15   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 3)
 sil15   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 3)
 sil15   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 3)
 sil16   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 7)
 sil16   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 7)
 sil16   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 7)
 sil16   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 7)
 sil17   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 9)
 sil17   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 9)
 sil17   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 9)
 sil17   TYPE01   ( 0, 0)     44     0.000           0  ( 3, 9)
 sil18   TYPE01   ( 0, 0)     44     0.000           0  ( 3,13)
 sil18   TYPE01   ( 0, 0)     44     0.000           0  ( 3,13)
 sil18   TYPE01   ( 0, 0)     44     0.000           0  ( 3,13)
 sil18   TYPE01   ( 0, 0)     44     0.000           0  ( 3,13)
 sil19   TYPE01   ( 0, 0)     44     0.000           0  ( 3,14)
 sil19   TYPE01   ( 0, 0)     44     0.000           0  ( 3,14)
 sil19   TYPE01   ( 0, 0)     44     0.000           0  ( 3,14)
 sil19   TYPE01   ( 0, 0)     44     0.000           0  ( 3,14)
 sil20   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 2)
 sil20   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 2)
 sil20   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 2)
 sil20   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 2)
 sil21   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 4)
 sil21   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 4)
 sil21   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 4)
 sil21   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 4)
 sil22   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 6)
 sil22   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 6)
 sil22   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 6)
 sil22   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 6)
 sil23   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 8)
 sil23   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 8)
 sil23   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 8)
 sil23   TYPE01   ( 0, 0)     44     0.000           0  ( 4, 8)
 sil24   TYPE01   ( 0, 0)     44     0.000           0  ( 4,10)
 sil24   TYPE01   ( 0, 0)     44     0.000           0  ( 4,10)
 sil24   TYPE01   ( 0, 0)     44     0.000           0  ( 4,10)
 sil24   TYPE01   ( 0, 0)     44     0.000           0  ( 4,10)
 sil25   TYPE01   ( 0, 0)     44     0.000           0  ( 4,12)
 sil25   TYPE01   ( 0, 0)     44     0.000           0  ( 4,12)
 sil25   TYPE01   ( 0, 0)     44     0.000           0  ( 4,12)
 sil25   TYPE01   ( 0, 0)     44     0.000           0  ( 4,12)
 sil26   TYPE01   ( 0, 0)     44     0.000           0  ( 4,14)
 sil26   TYPE01   ( 0, 0)     44     0.000           0  ( 4,14)
 sil26   TYPE01   ( 0, 0)     44     0.000           0  ( 4,14)
 sil26   TYPE01   ( 0, 0)     44     0.000           0  ( 4,14)
 sil27   TYPE01   ( 0, 0)     44     0.000           0  ( 5, 1)
 sil27   TYPE01   ( 0, 0)     44     0.000           0  ( 5, 1)
 sil27   TYPE01   ( 0, 0)     44     0.000           0  ( 5, 1)
 sil27   TYPE01   ( 0, 0)     44     0.000           0  ( 5, 1)
 sil28   TYPE01   ( 0, 0)     44     0.000           0  ( 5, 5)
 sil28   TYPE01   ( 0, 0)     44     0.000           0  ( 5, 5)
 sil28   TYPE01   ( 0, 0)     44     0.000           0  ( 5, 5)
 sil28   TYPE01   ( 0, 0)     44     0.000           0  ( 5, 5)
 sil29   TYPE01   ( 0, 0)     44     0.000           0  ( 5,11)
 sil29   TYPE01   ( 0, 0)     44     0.000           0  ( 5,11)
 sil29   TYPE01   ( 0, 0)     44     0.000           0  ( 5,11)
 sil29   TYPE01   ( 0, 0)     44     0.000           0  ( 5,11)
 sil30   TYPE01   ( 0, 0)     44     0.000           0  ( 5,15)
 sil30   TYPE01   ( 0, 0)     44     0.000           0  ( 5,15)
 sil30   TYPE01   ( 0, 0)     44     0.000           0  ( 5,15)
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page    6
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    
 sil30   TYPE01   ( 0, 0)     44     0.000           0  ( 5,15)
 sil31   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 1)
 sil31   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 1)
 sil31   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 1)
 sil31   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 1)
 sil32   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 2)
 sil32   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 2)
 sil32   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 2)
 sil32   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 2)
 sil33   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 4)
 sil33   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 4)
 sil33   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 4)
 sil33   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 4)
 sil34   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 6)
 sil34   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 6)
 sil34   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 6)
 sil34   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 6)
 sil35   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 8)
 sil35   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 8)
 sil35   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 8)
 sil35   TYPE01   ( 0, 0)     44     0.000           0  ( 6, 8)
 sil36   TYPE01   ( 0, 0)     44     0.000           0  ( 6,10)
 sil36   TYPE01   ( 0, 0)     44     0.000           0  ( 6,10)
 sil36   TYPE01   ( 0, 0)     44     0.000           0  ( 6,10)
 sil36   TYPE01   ( 0, 0)     44     0.000           0  ( 6,10)
 sil37   TYPE01   ( 0, 0)     44     0.000           0  ( 6,12)
 sil37   TYPE01   ( 0, 0)     44     0.000           0  ( 6,12)
 sil37   TYPE01   ( 0, 0)     44     0.000           0  ( 6,12)
 sil37   TYPE01   ( 0, 0)     44     0.000           0  ( 6,12)
 sil38   TYPE01   ( 0, 0)     44     0.000           0  ( 6,14)
 sil38   TYPE01   ( 0, 0)     44     0.000           0  ( 6,14)
 sil38   TYPE01   ( 0, 0)     44     0.000           0  ( 6,14)
 sil38   TYPE01   ( 0, 0)     44     0.000           0  ( 6,14)
 sil39   TYPE01   ( 0, 0)     44     0.000           0  ( 6,15)
 sil39   TYPE01   ( 0, 0)     44     0.000           0  ( 6,15)
 sil39   TYPE01   ( 0, 0)     44     0.000           0  ( 6,15)
 sil39   TYPE01   ( 0, 0)     44     0.000           0  ( 6,15)
 sil40   TYPE01   ( 0, 0)     44     0.000           0  ( 7, 1)
 sil40   TYPE01   ( 0, 0)     44     0.000           0  ( 7, 1)
 sil40   TYPE01   ( 0, 0)     44     0.000           0  ( 7, 1)
 sil40   TYPE01   ( 0, 0)     44     0.000           0  ( 7, 1)
 sil41   TYPE01   ( 0, 0)     44     0.000           0  ( 7, 3)
 sil41   TYPE01   ( 0, 0)     44     0.000           0  ( 7, 3)
 sil41   TYPE01   ( 0, 0)     44     0.000           0  ( 7, 3)
 sil41   TYPE01   ( 0, 0)     44     0.000           0  ( 7, 3)
 sil42   TYPE01   ( 0, 0)     44     0.000           0  ( 7,13)
 sil42   TYPE01   ( 0, 0)     44     0.000           0  ( 7,13)
 sil42   TYPE01   ( 0, 0)     44     0.000           0  ( 7,13)
 sil42   TYPE01   ( 0, 0)     44     0.000           0  ( 7,13)
 sil43   TYPE01   ( 0, 0)     44     0.000           0  ( 7,15)
 sil43   TYPE01   ( 0, 0)     44     0.000           0  ( 7,15)
 sil43   TYPE01   ( 0, 0)     44     0.000           0  ( 7,15)
 sil43   TYPE01   ( 0, 0)     44     0.000           0  ( 7,15)
 sil44   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 1)
 sil44   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 1)
 sil44   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 1)
 sil44   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 1)
 sil45   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 2)
 sil45   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 2)
 sil45   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 2)
 sil45   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 2)
 sil46   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 4)
 sil46   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 4)
 sil46   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 4)
 sil46   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 4)
 sil47   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 6)
 sil47   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 6)
 sil47   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 6)
 sil47   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 6)
 sil48   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 8)
 sil48   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 8)
 sil48   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 8)
 sil48   TYPE01   ( 0, 0)     44     0.000           0  ( 8, 8)
 sil49   TYPE01   ( 0, 0)     44     0.000           0  ( 8,10)
 sil49   TYPE01   ( 0, 0)     44     0.000           0  ( 8,10)
 sil49   TYPE01   ( 0, 0)     44     0.000           0  ( 8,10)
 sil49   TYPE01   ( 0, 0)     44     0.000           0  ( 8,10)
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page    7
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    
 sil50   TYPE01   ( 0, 0)     44     0.000           0  ( 8,12)
 sil50   TYPE01   ( 0, 0)     44     0.000           0  ( 8,12)
 sil50   TYPE01   ( 0, 0)     44     0.000           0  ( 8,12)
 sil50   TYPE01   ( 0, 0)     44     0.000           0  ( 8,12)
 sil51   TYPE01   ( 0, 0)     44     0.000           0  ( 8,14)
 sil51   TYPE01   ( 0, 0)     44     0.000           0  ( 8,14)
 sil51   TYPE01   ( 0, 0)     44     0.000           0  ( 8,14)
 sil51   TYPE01   ( 0, 0)     44     0.000           0  ( 8,14)
 sil52   TYPE01   ( 0, 0)     44     0.000           0  ( 8,15)
 sil52   TYPE01   ( 0, 0)     44     0.000           0  ( 8,15)
 sil52   TYPE01   ( 0, 0)     44     0.000           0  ( 8,15)
 sil52   TYPE01   ( 0, 0)     44     0.000           0  ( 8,15)
 sil53   TYPE01   ( 0, 0)     44     0.000           0  ( 9, 1)
 sil53   TYPE01   ( 0, 0)     44     0.000           0  ( 9, 1)
 sil53   TYPE01   ( 0, 0)     44     0.000           0  ( 9, 1)
 sil53   TYPE01   ( 0, 0)     44     0.000           0  ( 9, 1)
 sil54   TYPE01   ( 0, 0)     44     0.000           0  ( 9, 3)
 sil54   TYPE01   ( 0, 0)     44     0.000           0  ( 9, 3)
 sil54   TYPE01   ( 0, 0)     44     0.000           0  ( 9, 3)
 sil54   TYPE01   ( 0, 0)     44     0.000           0  ( 9, 3)
 sil55   TYPE01   ( 0, 0)     44     0.000           0  ( 9,13)
 sil55   TYPE01   ( 0, 0)     44     0.000           0  ( 9,13)
 sil55   TYPE01   ( 0, 0)     44     0.000           0  ( 9,13)
 sil55   TYPE01   ( 0, 0)     44     0.000           0  ( 9,13)
 sil56   TYPE01   ( 0, 0)     44     0.000           0  ( 9,15)
 sil56   TYPE01   ( 0, 0)     44     0.000           0  ( 9,15)
 sil56   TYPE01   ( 0, 0)     44     0.000           0  ( 9,15)
 sil56   TYPE01   ( 0, 0)     44     0.000           0  ( 9,15)
 sil57   TYPE01   ( 0, 0)     44     0.000           0  (10, 1)
 sil57   TYPE01   ( 0, 0)     44     0.000           0  (10, 1)
 sil57   TYPE01   ( 0, 0)     44     0.000           0  (10, 1)
 sil57   TYPE01   ( 0, 0)     44     0.000           0  (10, 1)
 sil58   TYPE01   ( 0, 0)     44     0.000           0  (10, 2)
 sil58   TYPE01   ( 0, 0)     44     0.000           0  (10, 2)
 sil58   TYPE01   ( 0, 0)     44     0.000           0  (10, 2)
 sil58   TYPE01   ( 0, 0)     44     0.000           0  (10, 2)
 sil59   TYPE01   ( 0, 0)     44     0.000           0  (10, 4)
 sil59   TYPE01   ( 0, 0)     44     0.000           0  (10, 4)
 sil59   TYPE01   ( 0, 0)     44     0.000           0  (10, 4)
 sil59   TYPE01   ( 0, 0)     44     0.000           0  (10, 4)
 sil60   TYPE01   ( 0, 0)     44     0.000           0  (10, 6)
 sil60   TYPE01   ( 0, 0)     44     0.000           0  (10, 6)
 sil60   TYPE01   ( 0, 0)     44     0.000           0  (10, 6)
 sil60   TYPE01   ( 0, 0)     44     0.000           0  (10, 6)
 sil61   TYPE01   ( 0, 0)     44     0.000           0  (10, 8)
 sil61   TYPE01   ( 0, 0)     44     0.000           0  (10, 8)
 sil61   TYPE01   ( 0, 0)     44     0.000           0  (10, 8)
 sil61   TYPE01   ( 0, 0)     44     0.000           0  (10, 8)
 sil62   TYPE01   ( 0, 0)     44     0.000           0  (10,10)
 sil62   TYPE01   ( 0, 0)     44     0.000           0  (10,10)
 sil62   TYPE01   ( 0, 0)     44     0.000           0  (10,10)
 sil62   TYPE01   ( 0, 0)     44     0.000           0  (10,10)
 sil63   TYPE01   ( 0, 0)     44     0.000           0  (10,12)
 sil63   TYPE01   ( 0, 0)     44     0.000           0  (10,12)
 sil63   TYPE01   ( 0, 0)     44     0.000           0  (10,12)
 sil63   TYPE01   ( 0, 0)     44     0.000           0  (10,12)
 sil64   TYPE01   ( 0, 0)     44     0.000           0  (10,14)
 sil64   TYPE01   ( 0, 0)     44     0.000           0  (10,14)
 sil64   TYPE01   ( 0, 0)     44     0.000           0  (10,14)
 sil64   TYPE01   ( 0, 0)     44     0.000           0  (10,14)
 sil65   TYPE01   ( 0, 0)     44     0.000           0  (10,15)
 sil65   TYPE01   ( 0, 0)     44     0.000           0  (10,15)
 sil65   TYPE01   ( 0, 0)     44     0.000           0  (10,15)
 sil65   TYPE01   ( 0, 0)     44     0.000           0  (10,15)
 sil66   TYPE01   ( 0, 0)     44     0.000           0  (11, 1)
 sil66   TYPE01   ( 0, 0)     44     0.000           0  (11, 1)
 sil66   TYPE01   ( 0, 0)     44     0.000           0  (11, 1)
 sil66   TYPE01   ( 0, 0)     44     0.000           0  (11, 1)
 sil67   TYPE01   ( 0, 0)     44     0.000           0  (11, 5)
 sil67   TYPE01   ( 0, 0)     44     0.000           0  (11, 5)
 sil67   TYPE01   ( 0, 0)     44     0.000           0  (11, 5)
 sil67   TYPE01   ( 0, 0)     44     0.000           0  (11, 5)
 sil68   TYPE01   ( 0, 0)     44     0.000           0  (11,11)
 sil68   TYPE01   ( 0, 0)     44     0.000           0  (11,11)
 sil68   TYPE01   ( 0, 0)     44     0.000           0  (11,11)
 sil68   TYPE01   ( 0, 0)     44     0.000           0  (11,11)
 sil69   TYPE01   ( 0, 0)     44     0.000           0  (11,15)
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page    8
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    
 sil69   TYPE01   ( 0, 0)     44     0.000           0  (11,15)
 sil69   TYPE01   ( 0, 0)     44     0.000           0  (11,15)
 sil69   TYPE01   ( 0, 0)     44     0.000           0  (11,15)
 sil70   TYPE01   ( 0, 0)     44     0.000           0  (12, 2)
 sil70   TYPE01   ( 0, 0)     44     0.000           0  (12, 2)
 sil70   TYPE01   ( 0, 0)     44     0.000           0  (12, 2)
 sil70   TYPE01   ( 0, 0)     44     0.000           0  (12, 2)
 sil71   TYPE01   ( 0, 0)     44     0.000           0  (12, 4)
 sil71   TYPE01   ( 0, 0)     44     0.000           0  (12, 4)
 sil71   TYPE01   ( 0, 0)     44     0.000           0  (12, 4)
 sil71   TYPE01   ( 0, 0)     44     0.000           0  (12, 4)
 sil72   TYPE01   ( 0, 0)     44     0.000           0  (12, 6)
 sil72   TYPE01   ( 0, 0)     44     0.000           0  (12, 6)
 sil72   TYPE01   ( 0, 0)     44     0.000           0  (12, 6)
 sil72   TYPE01   ( 0, 0)     44     0.000           0  (12, 6)
 sil73   TYPE01   ( 0, 0)     44     0.000           0  (12, 8)
 sil73   TYPE01   ( 0, 0)     44     0.000           0  (12, 8)
 sil73   TYPE01   ( 0, 0)     44     0.000           0  (12, 8)
 sil73   TYPE01   ( 0, 0)     44     0.000           0  (12, 8)
 sil74   TYPE01   ( 0, 0)     44     0.000           0  (12,10)
 sil74   TYPE01   ( 0, 0)     44     0.000           0  (12,10)
 sil74   TYPE01   ( 0, 0)     44     0.000           0  (12,10)
 sil74   TYPE01   ( 0, 0)     44     0.000           0  (12,10)
 sil75   TYPE01   ( 0, 0)     44     0.000           0  (12,12)
 sil75   TYPE01   ( 0, 0)     44     0.000           0  (12,12)
 sil75   TYPE01   ( 0, 0)     44     0.000           0  (12,12)
 sil75   TYPE01   ( 0, 0)     44     0.000           0  (12,12)
 sil76   TYPE01   ( 0, 0)     44     0.000           0  (12,14)
 sil76   TYPE01   ( 0, 0)     44     0.000           0  (12,14)
 sil76   TYPE01   ( 0, 0)     44     0.000           0  (12,14)
 sil76   TYPE01   ( 0, 0)     44     0.000           0  (12,14)
 sil77   TYPE01   ( 0, 0)     44     0.000           0  (13, 2)
 sil77   TYPE01   ( 0, 0)     44     0.000           0  (13, 2)
 sil77   TYPE01   ( 0, 0)     44     0.000           0  (13, 2)
 sil77   TYPE01   ( 0, 0)     44     0.000           0  (13, 2)
 sil78   TYPE01   ( 0, 0)     44     0.000           0  (13, 3)
 sil78   TYPE01   ( 0, 0)     44     0.000           0  (13, 3)
 sil78   TYPE01   ( 0, 0)     44     0.000           0  (13, 3)
 sil78   TYPE01   ( 0, 0)     44     0.000           0  (13, 3)
 sil79   TYPE01   ( 0, 0)     44     0.000           0  (13, 7)
 sil79   TYPE01   ( 0, 0)     44     0.000           0  (13, 7)
 sil79   TYPE01   ( 0, 0)     44     0.000           0  (13, 7)
 sil79   TYPE01   ( 0, 0)     44     0.000           0  (13, 7)
 sil80   TYPE01   ( 0, 0)     44     0.000           0  (13, 9)
 sil80   TYPE01   ( 0, 0)     44     0.000           0  (13, 9)
 sil80   TYPE01   ( 0, 0)     44     0.000           0  (13, 9)
 sil80   TYPE01   ( 0, 0)     44     0.000           0  (13, 9)
 sil81   TYPE01   ( 0, 0)     44     0.000           0  (13,13)
 sil81   TYPE01   ( 0, 0)     44     0.000           0  (13,13)
 sil81   TYPE01   ( 0, 0)     44     0.000           0  (13,13)
 sil81   TYPE01   ( 0, 0)     44     0.000           0  (13,13)
 sil82   TYPE01   ( 0, 0)     44     0.000           0  (13,14)
 sil82   TYPE01   ( 0, 0)     44     0.000           0  (13,14)
 sil82   TYPE01   ( 0, 0)     44     0.000           0  (13,14)
 sil82   TYPE01   ( 0, 0)     44     0.000           0  (13,14)
 sil83   TYPE01   ( 0, 0)     44     0.000           0  (14, 3)
 sil83   TYPE01   ( 0, 0)     44     0.000           0  (14, 3)
 sil83   TYPE01   ( 0, 0)     44     0.000           0  (14, 3)
 sil83   TYPE01   ( 0, 0)     44     0.000           0  (14, 3)
 sil84   TYPE01   ( 0, 0)     44     0.000           0  (14, 4)
 sil84   TYPE01   ( 0, 0)     44     0.000           0  (14, 4)
 sil84   TYPE01   ( 0, 0)     44     0.000           0  (14, 4)
 sil84   TYPE01   ( 0, 0)     44     0.000           0  (14, 4)
 sil85   TYPE01   ( 0, 0)     44     0.000           0  (14, 6)
 sil85   TYPE01   ( 0, 0)     44     0.000           0  (14, 6)
 sil85   TYPE01   ( 0, 0)     44     0.000           0  (14, 6)
 sil85   TYPE01   ( 0, 0)     44     0.000           0  (14, 6)
 sil86   TYPE01   ( 0, 0)     44     0.000           0  (14, 8)
 sil86   TYPE01   ( 0, 0)     44     0.000           0  (14, 8)
 sil86   TYPE01   ( 0, 0)     44     0.000           0  (14, 8)
 sil86   TYPE01   ( 0, 0)     44     0.000           0  (14, 8)
 sil87   TYPE01   ( 0, 0)     44     0.000           0  (14,10)
 sil87   TYPE01   ( 0, 0)     44     0.000           0  (14,10)
 sil87   TYPE01   ( 0, 0)     44     0.000           0  (14,10)
 sil87   TYPE01   ( 0, 0)     44     0.000           0  (14,10)
 sil88   TYPE01   ( 0, 0)     44     0.000           0  (14,12)
 sil88   TYPE01   ( 0, 0)     44     0.000           0  (14,12)
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page    9
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    
 sil88   TYPE01   ( 0, 0)     44     0.000           0  (14,12)
 sil88   TYPE01   ( 0, 0)     44     0.000           0  (14,12)
 sil89   TYPE01   ( 0, 0)     44     0.000           0  (14,13)
 sil89   TYPE01   ( 0, 0)     44     0.000           0  (14,13)
 sil89   TYPE01   ( 0, 0)     44     0.000           0  (14,13)
 sil89   TYPE01   ( 0, 0)     44     0.000           0  (14,13)
 sil90   TYPE01   ( 0, 0)     44     0.000           0  (15, 5)
 sil90   TYPE01   ( 0, 0)     44     0.000           0  (15, 5)
 sil90   TYPE01   ( 0, 0)     44     0.000           0  (15, 5)
 sil90   TYPE01   ( 0, 0)     44     0.000           0  (15, 5)
 sil91   TYPE01   ( 0, 0)     44     0.000           0  (15, 6)
 sil91   TYPE01   ( 0, 0)     44     0.000           0  (15, 6)
 sil91   TYPE01   ( 0, 0)     44     0.000           0  (15, 6)
 sil91   TYPE01   ( 0, 0)     44     0.000           0  (15, 6)
 sil92   TYPE01   ( 0, 0)     44     0.000           0  (15, 7)
 sil92   TYPE01   ( 0, 0)     44     0.000           0  (15, 7)
 sil92   TYPE01   ( 0, 0)     44     0.000           0  (15, 7)
 sil92   TYPE01   ( 0, 0)     44     0.000           0  (15, 7)
 sil93   TYPE01   ( 0, 0)     44     0.000           0  (15, 8)
 sil93   TYPE01   ( 0, 0)     44     0.000           0  (15, 8)
 sil93   TYPE01   ( 0, 0)     44     0.000           0  (15, 8)
 sil93   TYPE01   ( 0, 0)     44     0.000           0  (15, 8)
 sil94   TYPE01   ( 0, 0)     44     0.000           0  (15, 9)
 sil94   TYPE01   ( 0, 0)     44     0.000           0  (15, 9)
 sil94   TYPE01   ( 0, 0)     44     0.000           0  (15, 9)
 sil94   TYPE01   ( 0, 0)     44     0.000           0  (15, 9)
 sil95   TYPE01   ( 0, 0)     44     0.000           0  (15,10)
 sil95   TYPE01   ( 0, 0)     44     0.000           0  (15,10)
 sil95   TYPE01   ( 0, 0)     44     0.000           0  (15,10)
 sil95   TYPE01   ( 0, 0)     44     0.000           0  (15,10)
 sil96   TYPE01   ( 0, 0)     44     0.000           0  (15,11)
 sil96   TYPE01   ( 0, 0)     44     0.000           0  (15,11)
 sil96   TYPE01   ( 0, 0)     44     0.000           0  (15,11)
 sil96   TYPE01   ( 0, 0)     44     0.000           0  (15,11)
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   10
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    

 _____________________________________________________________________________________________________________________________

 ERR.CHK - SYMGRP - RES STEP - Check full core loading pattern for symmetry of FUE.TYP, ave Exp and 2X2 exp

 _____________________________________________________________________________________________________________________________
 ** Warning -   SYMGRP A - Assembly Group A exposure (or FUE.TYP) fails quarter core rotational symmetry (relative to A0)
 
                                        A2       
                                 |( 7, 9)=24N-11|
                                 |28.295  29.019|
                                 |27.048  28.490|
                                 |FUE.ROT=     0|
                                 |FUE.TYP=    29|
                                 |AVE EXP=28.213|
 
                       A1               A0       
                |( 9, 7)=24C-05| |( 9, 9)=24N-05|
                |28.490  27.048| |27.134  28.606|
                |29.019  28.295| |28.350  29.083|
                |FUE.ROT=     0| |FUE.ROT=     0|
                |FUE.TYP=    29| |FUE.TYP=    29|
                |AVE EXP=28.213| |AVE EXP=28.293|
 _____________________________________________________________________________________________________________________________
 ** Warning -   SYMGRP B - Assembly Group B exposure (or FUE.TYP) fails quarter core rotational symmetry (relative to B0)
 
                                        B2       
                                 |( 6, 9)=24L-12|
                                 |45.158  49.415|
                                 |46.943  50.860|
                                 |FUE.ROT=     0|
                                 |FUE.TYP=    24|
                                 |AVE EXP=48.094|
 
                                                         B0       
                                                  |( 9,10)=24K-06|
                                                  |48.767  44.929|
                                                  |44.922  41.260|
                                                  |FUE.ROT=     0|
                                                  |FUE.TYP=    23|
                                                  |AVE EXP=44.969|
 
                       B1       
                |(10, 7)=24E-04|
                |50.860  46.943|
                |49.415  45.158|
                |FUE.ROT=     0|
                |FUE.TYP=    24|
                |AVE EXP=48.094|
 _____________________________________________________________________________________________________________________________
 ** Warning -   SYMGRP C - Assembly Group C exposure (or FUE.TYP) fails quarter core rotational symmetry (relative to C0)
 
                                        C2       
                                 |( 5, 9)=24L-11|
                                 |28.737  28.389|
                                 |28.704  28.749|
                                 |FUE.ROT=     0|
                                 |FUE.TYP=    33|
                                 |AVE EXP=28.645|
 
                                                         C0       
                                                  |( 9,11)=24N-03|
                                                  | 9.508  12.902|
                                                  |12.980  16.455|
                                                  |FUE.ROT=     0|
                                                  |FUE.TYP=    43|
                                                  |AVE EXP=12.961|
 
                       C1       
                |(11, 7)=24E-05|
                |28.749  28.704|
                |28.389  28.737|
                |FUE.ROT=     0|
                |FUE.TYP=    33|
                |AVE EXP=28.645|
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   11
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    
 _____________________________________________________________________________________________________________________________
 ** Warning -   SYMGRP D - Assembly Group D exposure (or FUE.TYP) fails quarter core rotational symmetry (relative to D0)
 
                                        D2       
                                 |( 2, 9)=24L-13|
                                 |28.606  29.083|
                                 |27.134  28.350|
                                 |FUE.ROT=     0|
                                 |FUE.TYP=    29|
                                 |AVE EXP=28.293|
 
                                                         D0       
                                                  |( 9,14)=24J-07|
                                                  |27.859  27.843|
                                                  |27.838  27.312|
                                                  |FUE.ROT=     0|
                                                  |FUE.TYP=    40|
                                                  |AVE EXP=27.713|
 
                       D1       
                |(14, 7)=24E-03|
                |28.350  27.134|
                |29.083  28.606|
                |FUE.ROT=     0|
                |FUE.TYP=    29|
                |AVE EXP=28.293|
 _____________________________________________________________________________________________________________________________
 ** Warning -   SYMGRP E - Assembly Group E exposure (or FUE.TYP) fails quarter core rotational symmetry (relative to E0)
 
                                                         E2       
                                                  |( 7,10)=24K-10|
                                                  |44.929  41.260|
                                                  |48.767  44.922|
                                                  |FUE.ROT=     0|
                                                  |FUE.TYP=    23|
                                                  |AVE EXP=44.969|
 
                       E1       
                |( 9, 6)=24F-06|
                |44.922  48.767|
                |41.260  44.929|
                |FUE.ROT=     0|
                |FUE.TYP=    23|
                |AVE EXP=44.969|
 
                                        E0       
                                 |(10, 9)=24L-04|
                                 |46.842  50.803|
                                 |45.100  49.371|
                                 |FUE.ROT=     0|
                                 |FUE.TYP=    24|
                                 |AVE EXP=48.029|
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   12
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    
 _____________________________________________________________________________________________________________________________
 ** Warning -   SYMGRP F - Assembly Group F exposure (or FUE.TYP) fails quarter core rotational symmetry (relative to F0)
 
                                                         F2       
                                                  |( 7,11)=24N-13|
                                                  |12.902  16.455|
                                                  | 9.508  12.980|
                                                  |FUE.ROT=     0|
                                                  |FUE.TYP=    43|
                                                  |AVE EXP=12.961|
 
                       F1       
                |( 9, 5)=24C-03|
                |12.980   9.508|
                |16.455  12.902|
                |FUE.ROT=     0|
                |FUE.TYP=    43|
                |AVE EXP=12.961|
 
                                        F0       
                                 |(11, 9)=24L-05|
                                 |28.704  28.737|
                                 |28.749  28.389|
                                 |FUE.ROT=     0|
                                 |FUE.TYP=    33|
                                 |AVE EXP=28.645|
 _____________________________________________________________________________________________________________________________
 ** Warning -   SYMGRP G - Assembly Group G exposure (or FUE.TYP) fails quarter core rotational symmetry (relative to G0)
 
                                                         G2       
                                                  |( 7,14)=24J-09|
                                                  |27.843  27.312|
                                                  |27.859  27.838|
                                                  |FUE.ROT=     0|
                                                  |FUE.TYP=    40|
                                                  |AVE EXP=27.713|
 
                       G1       
                |( 9, 2)=24G-07|
                |27.838  27.859|
                |27.312  27.843|
                |FUE.ROT=     0|
                |FUE.TYP=    40|
                |AVE EXP=27.713|
 
                                        G0       
                                 |(14, 9)=24L-03|
                                 |27.048  28.295|
                                 |28.490  29.019|
                                 |FUE.ROT=     0|
                                 |FUE.TYP=    29|
                                 |AVE EXP=28.213|
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   13
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    

 _____________________________________________________________________________________________________________________________

 ** ERROR - SYMGRP - Assembly groups failing to have 2x2 exposure symmetry

     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
  1                                                                                           01
  2                                                                                          
  3                                                 D2 D2                                     02
  4                                                 D2 D2                                    
  5                                                                                           03
  6                                                                                          
  7                                                                                           04
  8                                                                                          
  9                                                 C2 C2                                     05
 10                                                 C2 C2                                    
 11                                                 B2 B2                                     06
 12                                                 B2 B2                                    
 13                                                 A2 A2 E2 E2 F2 F2             G2 G2       07
 14                                                 A2 A2 E2 E2 F2 F2             G2 G2      
 15                                                                                           08
 16                                                                                          
 17       G1 G1             F1 F1 E1 E1 A1 A1       A0 A0 B0 B0 C0 C0             D0 D0       09
 18       G1 G1             F1 F1 E1 E1 A1 A1       A0 A0 B0 B0 C0 C0             D0 D0      
 19                                     B1 B1       E0 E0                                     10
 20                                     B1 B1       E0 E0                                    
 21                                     C1 C1       F0 F0                                     11
 22                                     C1 C1       F0 F0                                    
 23                                                                                           12
 24                                                                                          
 25                                                                                           13
 26                                                                                          
 27                                     D1 D1       G0 G0                                     14
 28                                     D1 D1       G0 G0                                    
 29                                                                                           15
 30                                                                                          
    R-    P-    N-    M-    L-    K-    J-    H-    G-    F-    E-    D-    C-    B-    A-   

 _____________________________________________________________________________________________________________________________

 ** ERROR - SYMGRP - Assembly groups failing to have assembly exposure symmetry

     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
  1                                              01
  2                         D2                   02
  3                                              03
  4                                              04
  5                         C2                   05
  6                         B2                   06
  7                         A2 E2 F2       G2    07
  8                                              08
  9    G1       F1 E1 A1    A0 B0 C0       D0    09
 10                   B1    E0                   10
 11                   C1    F0                   11
 12                                              12
 13                                              13
 14                   D1    G0                   14
 15                                              15
    R- P- N- M- L- K- J- H- G- F- E- D- C- B- A-
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   14
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    

 _____________________________________________________________________________________________________________________________

 ** ERROR - SYMGRP - Assembly groups failing to have assembly fuel type symmetry

     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
  1                                              01
  2                         D2                   02
  3                                              03
  4                                              04
  5                         C2                   05
  6                         B2                   06
  7                            E2 F2       G2    07
  8                                              08
  9    G1       F1 E1          B0 C0       D0    09
 10                   B1    E0                   10
 11                   C1    F0                   11
 12                                              12
 13                                              13
 14                   D1    G0                   14
 15                                              15
    R- P- N- M- L- K- J- H- G- F- E- D- C- B- A-

 ERR.CHK - Exposure Differences for Symmetric Sub-Assemblies, Full Core
 I/J =    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15
  1                                                              0.     0.     0.     0.     0.     0.     0.
  2                                                              0.     0.     0.     0.     0.     0.     0.
  3                                  0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
  4                                  0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
  5                    0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
  6                    0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
  7                    0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
  8                    0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
  9      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 10      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 11      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 12      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 13      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 14      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 15      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 16      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 17      0.     0. -0.652  0.811     0.     0.     0.     0. -15.76 -19.19 -0.178  1.925  0.140 -0.086     0.
 18      0.     0. -1.707 -0.452     0.     0.     0.     0. -11.93 -15.83 -8.111 -5.874 -0.064 -0.311     0.
 19      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.  5.938 -1.824     0.
 20      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.  8.154  0.229     0.
 21      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0. 15.769 19.196     0.
 22      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0. 11.934 15.836     0.
 23                    0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 24                    0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 25                    0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 26                    0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 27                                  0.     0.     0.     0.     0.     0.     0.     0.  0.512 -0.724     0.
 28                                  0.     0.     0.     0.     0.     0.     0.     0.  1.771  0.763     0.
 29                                                              0.     0.     0.     0.     0.     0.     0.
 30                                                              0.     0.     0.     0.     0.     0.     0.
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   15
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    

 ERR.CHK - Exposure Differences for Symmetric Sub-Assemblies, Full Core
 I/J =   16     17     18     19     20     21     22     23     24     25     26     27     28     29     30
  1      0.     0.     0.     0.     0.     0.     0.                                                        
  2      0.     0.     0.     0.     0.     0.     0.                                                        
  3      0.  0.763  1.771     0.     0.     0.     0.     0.     0.     0.     0.                            
  4      0. -0.724  0.512     0.     0.     0.     0.     0.     0.     0.     0.                            
  5      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.              
  6      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.              
  7      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.              
  8      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.              
  9      0. 15.836 11.934     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 10      0. 19.196 15.769     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 11      0.  0.229  8.154     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 12      0. -1.824  5.938     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 13      0. -0.311 -0.064 -5.874 -8.111 -15.83 -11.93     0.     0.     0.     0. -0.452 -1.707     0.     0.
 14      0. -0.086  0.140  1.925 -0.178 -19.19 -15.76     0.     0.     0.     0.  0.811 -0.652     0.     0.
 15      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 16      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 17      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 18      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 19      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 20      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 21      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 22      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.
 23      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.              
 24      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.              
 25      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.              
 26      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.              
 27      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.                            
 28      0.     0.     0.     0.     0.     0.     0.     0.     0.     0.     0.                            
 29      0.     0.     0.     0.     0.     0.     0.                                                        
 30      0.     0.     0.     0.     0.     0.     0.                                                        

 ERR.CHK - Sub-Assembly Exposures, Full Core
 I/J =    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15
  1                                                              0.     0.     0.     0.     0.     0.     0.
  2                                                              0.     0.     0.     0.     0.     0.     0.
  3                                  0.     0.     0.     0. 27.013 24.351     0.     0. 29.019 28.490     0.
  4                                  0.     0.     0.     0. 21.616 18.218     0.     0. 28.295 27.048     0.
  5                    0.     0.     0.     0. 27.533 27.489 45.665 46.385 26.205 23.918     0.     0. 26.924
  6                    0.     0.     0.     0. 23.482 22.908 49.027 49.536 23.501 20.024     0.     0. 23.444
  7                    0.     0. 27.543 23.483     0.     0. 28.171 27.614     0.     0. 27.223 27.425     0.
  8                    0.     0. 27.514 22.926     0.     0. 28.680 28.351     0.     0. 23.690 23.685     0.
  9      0.     0. 27.058 21.651 45.661 49.032 28.160 28.659     0.     0. 27.533 27.675 28.389 28.749 28.209
 10      0.     0. 24.418 18.269 46.361 49.535 27.596 28.319     0.     0. 27.515 27.844 28.737 28.704 27.659
 11      0.     0.     0.     0. 26.359 23.614     0.     0. 27.518 27.509     0.     0. 49.371 45.100     0.
 12      0.     0.     0.     0. 24.015 20.147     0.     0. 27.666 27.843     0.     0. 50.803 46.842     0.
 13      0.     0. 27.312 27.838     0.     0. 27.217 23.649 16.455 12.980 41.260 44.922 29.083 28.350 28.221
 14      0.     0. 27.843 27.859     0.     0. 27.423 23.666 12.902  9.508 44.929 48.767 28.606 27.134 28.274
 15      0.     0.     0.     0. 26.926 23.470     0.     0. 28.208 27.652     0.     0. 28.224 28.281     0.
 16      0.     0.     0.     0. 26.924 23.444     0.     0. 28.209 27.659     0.     0. 28.221 28.274     0.
 17      0.     0. 27.838 27.859     0.     0. 27.425 23.685 12.980  9.508 44.922 48.767 28.490 27.048 28.281
 18      0.     0. 27.312 27.843     0.     0. 27.223 23.690 16.455 12.902 41.260 44.929 29.019 28.295 28.224
 19      0.     0.     0.     0. 23.918 20.024     0.     0. 27.675 27.844     0.     0. 50.860 46.943     0.
 20      0.     0.     0.     0. 26.205 23.501     0.     0. 27.533 27.515     0.     0. 49.415 45.158     0.
 21      0.     0. 24.351 18.218 46.385 49.536 27.614 28.351     0.     0. 27.509 27.843 28.749 28.704 27.652
 22      0.     0. 27.013 21.616 45.665 49.027 28.171 28.680     0.     0. 27.518 27.666 28.389 28.737 28.208
 23                    0.     0. 27.489 22.908     0.     0. 28.659 28.319     0.     0. 23.649 23.666     0.
 24                    0.     0. 27.533 23.482     0.     0. 28.160 27.596     0.     0. 27.217 27.423     0.
 25                    0.     0.     0.     0. 23.483 22.926 49.032 49.535 23.614 20.147     0.     0. 23.470
 26                    0.     0.     0.     0. 27.543 27.514 45.661 46.361 26.359 24.015     0.     0. 26.926
 27                                  0.     0.     0.     0. 21.651 18.269     0.     0. 28.350 27.134     0.
 28                                  0.     0.     0.     0. 27.058 24.418     0.     0. 29.083 28.606     0.
 29                                                              0.     0.     0.     0.     0.     0.     0.
 30                                                              0.     0.     0.     0.     0.     0.     0.
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   16
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   80.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                       -9.61 ppm  537.000 EFPD    

 ERR.CHK - Sub-Assembly Exposures, Full Core
 I/J =   16     17     18     19     20     21     22     23     24     25     26     27     28     29     30
  1      0.     0.     0.     0.     0.     0.     0.                                                        
  2      0.     0.     0.     0.     0.     0.     0.                                                        
  3      0. 28.606 29.083     0.     0. 24.418 27.058     0.     0.     0.     0.                            
  4      0. 27.134 28.350     0.     0. 18.269 21.651     0.     0.     0.     0.                            
  5  26.926     0.     0. 24.015 26.359 46.361 45.661 27.514 27.543     0.     0.     0.     0.              
  6  23.470     0.     0. 20.147 23.614 49.535 49.032 22.926 23.483     0.     0.     0.     0.              
  7      0. 27.423 27.217     0.     0. 27.596 28.160     0.     0. 23.482 27.533     0.     0.              
  8      0. 23.666 23.649     0.     0. 28.319 28.659     0.     0. 22.908 27.489     0.     0.              
  9  28.208 28.737 28.389 27.666 27.518     0.     0. 28.680 28.171 49.027 45.665 21.616 27.013     0.     0.
 10  27.652 28.704 28.749 27.843 27.509     0.     0. 28.351 27.614 49.536 46.385 18.218 24.351     0.     0.
 11      0. 45.158 49.415     0.     0. 27.515 27.533     0.     0. 23.501 26.205     0.     0.     0.     0.
 12      0. 46.943 50.860     0.     0. 27.844 27.675     0.     0. 20.024 23.918     0.     0.     0.     0.
 13  28.224 28.295 29.019 44.929 41.260 12.902 16.455 23.690 27.223     0.     0. 27.843 27.312     0.     0.
 14  28.281 27.048 28.490 48.767 44.922  9.508 12.980 23.685 27.425     0.     0. 27.859 27.838     0.     0.
 15      0. 28.274 28.221     0.     0. 27.659 28.209     0.     0. 23.444 26.924     0.     0.     0.     0.
 16      0. 28.281 28.224     0.     0. 27.652 28.208     0.     0. 23.470 26.926     0.     0.     0.     0.
 17  28.274 27.134 28.606 48.767 44.929  9.508 12.902 23.666 27.423     0.     0. 27.859 27.843     0.     0.
 18  28.221 28.350 29.083 44.922 41.260 12.980 16.455 23.649 27.217     0.     0. 27.838 27.312     0.     0.
 19      0. 46.842 50.803     0.     0. 27.843 27.666     0.     0. 20.147 24.015     0.     0.     0.     0.
 20      0. 45.100 49.371     0.     0. 27.509 27.518     0.     0. 23.614 26.359     0.     0.     0.     0.
 21  27.659 28.704 28.737 27.844 27.515     0.     0. 28.319 27.596 49.535 46.361 18.269 24.418     0.     0.
 22  28.209 28.749 28.389 27.675 27.533     0.     0. 28.659 28.160 49.032 45.661 21.651 27.058     0.     0.
 23      0. 23.685 23.690     0.     0. 28.351 28.680     0.     0. 22.926 27.514     0.     0.              
 24      0. 27.425 27.223     0.     0. 27.614 28.171     0.     0. 23.483 27.543     0.     0.              
 25  23.444     0.     0. 20.024 23.501 49.536 49.027 22.908 23.482     0.     0.     0.     0.              
 26  26.924     0.     0. 23.918 26.205 46.385 45.665 27.489 27.533     0.     0.     0.     0.              
 27      0. 27.048 28.295     0.     0. 18.218 21.616     0.     0.     0.     0.                            
 28      0. 28.490 29.019     0.     0. 24.351 27.013     0.     0.     0.     0.                            
 29      0.     0.     0.     0.     0.     0.     0.                                                        
 30      0.     0.     0.     0.     0.     0.     0.                                                        

 ERR.CHK - Assembly Exposures, Full Core
 I/J =    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15
  1                                  0.     0.     0.     0.     0.     0.     0.                            
  2                    0.     0. 22.800     0. 28.213     0. 28.293     0. 22.849     0.     0.              
  3             0.     0. 25.353 47.653 23.412     0. 25.191     0. 23.534 47.647 25.366     0.     0.       
  4             0. 25.366     0. 28.204     0. 25.506     0. 25.489     0. 28.184     0. 25.353     0.       
  5      0. 22.849 47.647 28.184     0. 27.642 28.645 27.932 28.645 27.634     0. 28.204 47.653 22.800     0.
  6      0.     0. 23.534     0. 27.634     0. 48.029     0. 48.094     0. 27.642     0. 23.412     0.     0.
  7      0. 27.713     0. 25.489 12.961 44.969 28.293 28.250 28.213 44.969 12.961 25.506     0. 27.713     0.
  8      0.     0. 25.191     0. 27.932     0. 28.250     0. 28.250     0. 27.932     0. 25.191     0.     0.
  9      0. 27.713     0. 25.506 12.961 44.969 28.213 28.250 28.293 44.969 12.961 25.489     0. 27.713     0.
 10      0.     0. 23.412     0. 27.642     0. 48.094     0. 48.029     0. 27.634     0. 23.534     0.     0.
 11      0. 22.800 47.653 28.204     0. 27.634 28.645 27.932 28.645 27.642     0. 28.184 47.647 22.849     0.
 12             0. 25.353     0. 28.184     0. 25.489     0. 25.506     0. 28.204     0. 25.366     0.       
 13             0.     0. 25.366 47.647 23.534     0. 25.191     0. 23.412 47.653 25.353     0.     0.       
 14                    0.     0. 22.849     0. 28.293     0. 28.213     0. 22.800     0.     0.              
 15                                  0.     0.     0.     0.     0.     0.     0.                            
 ** Warning -   DEPCYC   - DEP.CYC - DEP.CYC card overrides starting or ending date from restart file for cycle: 25
 ** Note    -   XPORST   - DEP.CYC - DEP.CYC card has reset cycle exposure from: 537.000 to: 0.000 for cycle: 25
 Input Cards:  LIB    , SEG.LIB, SEG.DAT, FUE.ZON, ITE.BOR, COR.OPE, DEP.CYC, ERR.SUP, DEP.STA, STA    
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   17
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    
 _____________________________________________________________________________________________________________________________

 SEG.LIB - Assignment of Cross Section Library Members to Segments
 LIB     - ./cms.pwr-all2.lib
 Library                  Major  Minor  Assigned to    ---         Run Title / Date and Time ---
 Name                     Flag   Flag     Segment

 sil400p0a0                   0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p0a32                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p0a64                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p0a96                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p0a128                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p0a156                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p0a200                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p4a0                   0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p4a32                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p4a64                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p4a96                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p4a128                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p4a156                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p4a200                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p8a0                   0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p8a32                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p8a64                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p8a96                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p8a128                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p8a156                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p8a200                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p12a0                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p12a32                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p12a64                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p12a96                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p12a128                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p12a156                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p12a200                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p24a0                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p24a32                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p24a64                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p24a96                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p24a128                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p24a156                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil400p24a200                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   18
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    
 sil440p0a0                   0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p0a32                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p0a64                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p0a96                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p0a128                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p0a156                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p0a200                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p4a0                   0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p4a32                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p4a64                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p4a96                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p4a128                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p4a156                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p4a200                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p8a0                   0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p8a32                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p8a64                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p8a96                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p8a128                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p8a156                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p8a200                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p12a0                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p12a32                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p12a64                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p12a96                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p12a128                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p12a156                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p12a200                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p24a0                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p24a32                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p24a64                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p24a96                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p24a128                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p24a156                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil440p24a200                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p0a0                   0      0      44      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p0a32                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p0a64                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   19
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    
 sil495p0a96                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p0a128                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p0a156                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p0a200                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p4a0                   0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p4a32                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p4a64                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p4a96                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p4a128                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p4a156                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p4a200                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p8a0                   0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p8a32                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p8a64                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p8a96                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p8a128                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p8a156                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p8a200                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p12a0                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p12a32                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p12a64                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p12a96                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p12a128                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p12a156                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p12a200                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p24a0                  0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p24a32                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p24a64                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p24a96                 0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p24a128                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p24a156                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 sil495p24a200                0      0     N/A      GENERIC PWR MODEL - FUEL PWRU200C00 BASE AND BRANCHES
                                                    18/01/07   16:58:33   1.00.02
 RADREF                       0      0     N/A      GENERIC PWR MODEL - RADIAL BAFFLE/REFLECTOR
                                                    18/01/07   16:58:33   1.00.02
 TOPREF                       0      0     N/A      GENERIC PWR MODEL - PWR TOP AXIAL  REFLECTOR
                                                    18/01/07   16:58:33   1.00.02
 BOTREF                       0      0     N/A      GENERIC PWR MODEL - PWR BOTTOM REFLECTOR
                                                    18/01/07   16:58:33   1.00.02
 RFA205000I0.00XNOBP          0      0      43      C4 Case Set for NOBP Fuel 205/RFA/NOBP 000IFBA 0.00X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA455000I0.00XNOBP          0      0      27      C4 Case Set for NOBP Fuel 455/RFA/NOBP 000IFBA 0.00X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA455104I1.50XNOBP          0      0      29      C4 Case Set for NOBP Fuel 455/RFA/NOBP 104IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   20
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    
 RFA455156I1.50XNOBP          0      0      28      C4 Case Set for NOBP Fuel 455/RFA/NOBP 156IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA485000I0.00XNOBP          0      0      30      C4 Case Set for NOBP Fuel 485/RFA/NOBP 000IFBA 0.00X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA485064I1.50XNOBP          0      0     N/A      C4 Case Set for NOBP Fuel 485/RFA/NOBP 064IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA485080I1.50XNOBP          0      0     N/A      C4 Case Set for NOBP Fuel 485/RFA/NOBP 080IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA495000I0.00XNOBP          0      0      23      Uses copy of data for following segment                                  
 RFA495000I0.00XNOBP          0      0      34      C4 Case Set for NOBP Fuel 495/RFA/NOBP 000IFBA 0.00X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA495016I1.50XNOBP          0      0     N/A      C4 Case Set for NOBP Fuel 495/RFA/NOBP 016IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA495048I1.50XNOBP          0      0      37      C4 Case Set for NOBP Fuel 495/RFA/NOBP 048IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA495080I1.50XNOBP          0      0      38      C4 Case Set for NOBP Fuel 495/RFA/NOBP 080IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA495104I1.50XNOBP          0      0      26      Uses copy of data for following segment                                  
 RFA495104I1.50XNOBP          0      0      39      C4 Case Set for NOBP Fuel 495/RFA/NOBP 104IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA410000I0.00XNOBP          0      0     N/A      C4 Case Set for NOBP Fuel 410/RFA/NOBP 000IFBA 0.00X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA410128I1.50XNOBP          0      0     N/A      C4 Case Set for NOBP Fuel 410/RFA/NOBP 128IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA460000I0.00XNOBP          0      0     N/A      C4 Case Set for NOBP Fuel 460/RFA/NOBP 000IFBA 0.00X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA460104I1.50XNOBP          0      0     N/A      C4 Case Set for NOBP Fuel 460/RFA/NOBP 104IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA460128I1.50XNOBP          0      0     N/A      C4 Case Set for NOBP Fuel 460/RFA/NOBP 128IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA495064I1.50XNOBP          0      0      24      C4 Case Set for NOBP Fuel 495/RFA/NOBP 064IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA495128I1.50XNOBP          0      0     N/A      C4 Case Set for NOBP Fuel 495/RFA/NOBP 128IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA260BLANK                  0      0       4      C4 All Temp Case Set for WRFA 2.60w/o Annular Blanket Fuel
                                                    16/11/15   09:30:04   1.16.13
 RFA260BOTBK                  0      0       5      C4 All Temp Case Set for WRFA 2.60w/o Annular Blanket Fuel
                                                    16/11/15   09:30:04   1.16.13
 RFA485000I0.00XW24A          0      0      31      C4 Case Set for BP Fuel 485/RFA/W24A WABA 000IFBA 0.00X NOM
                                                    16/11/15   09:30:04   1.16.13
 RFA485064I1.50XW24A          0      0      32      C4 Case Set for BP Fuel 485/RFA/W24A WABA 064IFBA 1.50X NOM
                                                    16/11/15   09:30:04   1.16.13
 RFA485080I1.50XW24A          0      0      33      C4 Case Set for BP Fuel 485/RFA/W24A WABA 080IFBA 1.50X NOM
                                                    16/11/15   09:30:04   1.16.13
 RFA495000I0.00XW20A          0      0      35      C4 Case Set for BP Fuel 495/RFA/W20A WABA 000IFBA 0.00X NOM
                                                    16/11/15   09:30:04   1.16.13
 RFA495048I1.50XW20A          0      0      40      C4 Case Set for BP Fuel 495/RFA/W20A WABA 048IFBA 1.50X NOM
                                                    16/11/15   09:30:04   1.16.13
 RFA495000I0.00XW24A          0      0      36      C4 Case Set for BP Fuel 495/RFA/W24A WABA 000IFBA 0.00X NOM
                                                    16/11/15   09:30:04   1.16.13
 RFA495016I1.50XW24A          0      0      41      C4 Case Set for BP Fuel 495/RFA/W24A WABA 016IFBA 1.50X NOM
                                                    16/11/15   09:30:04   1.16.13
 RFA495080I1.50XW24A          0      0      42      C4 Case Set for BP Fuel 495/RFA/W24A WABA 080IFBA 1.50X NOM
                                                    16/11/15   09:30:04   1.16.13
 RFA460000I0.00XW16P          0      0     N/A      C4 Case Set for NOBP Fuel 460/RFA/NOBP 000IFBA 0.00X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 RFA460104I1.50XW16P          0      0     N/A      C4 Case Set for NOBP Fuel 460/RFA/NOBP 104IFBA 1.50X NOM DEP
                                                    16/11/15   09:30:04   1.16.13
 CATRFA310RADREF              0      0       2      All Temp Case Set for CAT RFA Fuel Radial Reflector
                                                    16/11/15   09:30:04   1.16.13
 MBWTOPREFL                   0      0     N/A      CASMO4 MNS/CNS REFLECTOR SEGMENTS LIBRARY ALL TEMP CASE SET
                                                    02/11/04   10:12:15   1.16.13
 MBWRADREFL                   0      0     N/A      All Temp Case Set for MBW Fuel Radial Reflector
                                                    02/11/04   10:12:15   1.16.13
 MBWBOTREFL                   0      0     N/A      All Temp Case Set for MBW Fuel Bottom Reflector
                                                    02/11/04   10:12:15   1.16.13
 RFATOPREFL                   0      0       3      All Temp Case Set for RFA Fuel Top Reflector
                                                    02/11/04   10:12:15   1.16.13
 RFARADREFL                   0      0     N/A      All Temp Case Set for RFA Fuel Radial Reflector
                                                    02/11/04   10:12:15   1.16.13
 RFABOTREFL                   0      0       1      All Temp Case Set for RFA Fuel Bottom Reflector
                                                    02/11/04   10:12:15   1.16.13
 
 Library created by CMS-Link version: 1.00.02 at 16:58:33 on 2018/01/07
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   21
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    
 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 

 Summary of Library Parameters
                            Seg=1   Total   Heated   Fission   DFS Group 1   DFS Group 2
 Seg  Name                  Ref=2   Rods     Rods    Product   W  S  E  N    W  S  E  N   MAC+FPD     DFS       Det
 ---- --------------------  -----   -----   ------   -------   ----------    ----------   -------     ---       ---
   1  RFABOTREFL              2       0        0        0      1  1  1  1    1  1  1  1      T         T         T
   2  CATRFA310RADREF         2       0        0        0      1  1  1  1    1  1  1  1      T         T         T
   3  RFATOPREFL              2       0        0        0      1  1  1  1    1  1  1  1      T         T         T
   4  RFA260BLANK             1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
   5  RFA260BOTBK             1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  23  RFA495000I0.00XNOBP     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  24  RFA495064I1.50XNOBP     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  26  RFA495104I1.50XNOBP     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  27  RFA455000I0.00XNOBP     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  28  RFA455156I1.50XNOBP     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  29  RFA455104I1.50XNOBP     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  30  RFA485000I0.00XNOBP     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  31  RFA485000I0.00XW24A     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  32  RFA485064I1.50XW24A     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  33  RFA485080I1.50XW24A     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  34  RFA495000I0.00XNOBP     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  35  RFA495000I0.00XW20A     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  36  RFA495000I0.00XW24A     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  37  RFA495048I1.50XNOBP     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  38  RFA495080I1.50XNOBP     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  39  RFA495104I1.50XNOBP     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  40  RFA495048I1.50XW20A     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  41  RFA495016I1.50XW24A     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  42  RFA495080I1.50XW24A     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  43  RFA205000I0.00XNOBP     1     289      264        4      1  1  1  1    1  1  1  1      T         T         T
  44  sil495p0a0              1     289      264        4      0  1  0  0    0  1  0  0      T         T         T
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   22
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

                              Pressure    Loading    
 Seg  Name                     (psia)      (g/cc)     Steam Table Option      CASMO Version
 ---- --------------------    --------    -------     ------------------    ------------------
   1  RFABOTREFL               2249.6     0.00000       TABLED ASME         CASMO-4   2.05.00   
   2  CATRFA310RADREF          2249.6     0.00000       TABLED ASME         CASMO-4   2.05.00   
   3  RFATOPREFL               2249.6     0.00000       TABLED ASME         CASMO-4   2.05.00   
   4  RFA260BLANK              2249.6     2.14904       TABLED ASME         CASMO-4   2.05.00   
   5  RFA260BOTBK              2249.6     2.14904       TABLED ASME         CASMO-4   2.05.07   
  23  RFA495000I0.00XNOBP      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  24  RFA495064I1.50XNOBP      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  26  RFA495104I1.50XNOBP      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  27  RFA455000I0.00XNOBP      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  28  RFA455156I1.50XNOBP      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  29  RFA455104I1.50XNOBP      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  30  RFA485000I0.00XNOBP      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  31  RFA485000I0.00XW24A      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  32  RFA485064I1.50XW24A      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  33  RFA485080I1.50XW24A      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  34  RFA495000I0.00XNOBP      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  35  RFA495000I0.00XW20A      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  36  RFA495000I0.00XW24A      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  37  RFA495048I1.50XNOBP      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  38  RFA495080I1.50XNOBP      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  39  RFA495104I1.50XNOBP      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  40  RFA495048I1.50XW20A      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  41  RFA495016I1.50XW24A      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  42  RFA495080I1.50XW24A      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  43  RFA205000I0.00XNOBP      2249.6     2.74269       TABLED ASME         CASMO-4   2.05.07   
  44  sil495p0a0               2248.1     3.48830       TABLED ASME         CASMO-4E  1.00.02   
 _____________________________________________________________________________________________________________________________

 Print Options:

 Lines/Page  = 80
 Columns/Page=126

 PRI.INP BMAP    QMAP    CMAP    FMAP  
 PRI.STA 2RPF    2KIN    2EXP    2EBP  
 PRI.FTD Off
 PRI.LIS On 
 PRI.ITE No active edits
 PRI.MAC No active edits
 PRI.DFS No active edits
 AUD.PRI No active edits
 PIN.EDT On      2PIN    2PLO    2XPO    QRPF    QEXP  
 DET.EDT No active edits
 BAT.EDT On      2EXP    QXPO  
 CMS.EDT No active edits
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   23
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    
 _____________________________________________________________________________________________________________________________

 Input Summary

 Relative Power. . . . . . PERCTP     100.0000 %           Fuel Assemblies . . . . . . . .      193.0000
 Relative Flow . . . . . . PERCWT     100.0000 %           Fuel Nodes (Assembly Size). . .     4632.0000
 Thermal Power . . . . . . . .CTP    3469.0854 MWt         Assembly Pitch (Cold) . . DXASSM      21.5040 cm
 Core Flow - Metric. . . . . .CWT   61666.4141 MT/hr       Node Height. . (Cold) . . . . DZ      15.2400 cm
           - English . . . . .CWT     135.9489 Mlb/hr      Core Height. . (Cold) . . .HCORE     365.7600 cm
 Rated Power Density . . . POWDEN     106.2730 kW/liter    Core Volume  . (Cold) . . . . .    32643.1484 liters
 Rated Coolant Mass Flux . FLODEN     690.9600 kg/(cm2-hr) Core Fueled Area. . . . . . . .    22311.8613 cm2
 Core Rated Thermal Power. . . .     3469.0854 MWt         Core Loading. . . . . . . . . .       99.1285 metric tons
 Core Rated Flow - Metric. . . .    61666.4141 MT/hr       Fuel Rods in Core . . . . . . .         50952
                 - English . . .      135.9489 Mlb/hr      Fuel Rods In Average Node . . .      264.0000
 Explicit Core Fraction. . . . .        0.2500             Total Fuel Rod Length in Core .    186362.047 meters
 Boundary Conditions . . . .  3                            Core Symmetry . . . . . . . . .        ROTATIONAL
                            +-+-+                          Number of BP Rods at Midplane .           752
                          3 + + +  0
                            +-+-+
                              0    0 (Diag)

1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   24
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

 Fueled Segments:
                                                                       Burnable     Present      Original
                                                                       Poison       Number       Number
 Seg.                         Loading     Enrichment       PU          Loading      Burnable     Burnable      Equivalent
 Num. Segment Name             (g/cc)       (U235)        (WT%)        (g/cc)       Poison       Poison        Assemblies
 ---- --------------------   ---------   ------------    -------      ----------   ----------   ----------    -----------
   4  RFA260BLANK             2.14904      2.60000        ------        ------              0            0          8.042
   5  RFA260BOTBK             2.14904      2.60000        ------        ------              0            0          8.042
  23  RFA495000I0.00XNOBP     2.74269      4.95000        ------        ------              0            0          4.667
  24  RFA495064I1.50XNOBP     2.74269      4.95000        ------       1.50000              0            0          3.333
  26  RFA495104I1.50XNOBP     2.74269      4.95000        ------       1.50000              0            0          6.667
  27  RFA455000I0.00XNOBP     2.74269      4.55000        ------        ------              0            0          1.333
  28  RFA455156I1.50XNOBP     2.74269      4.55000        ------       1.50000              0            0          3.333
  29  RFA455104I1.50XNOBP     2.74269      4.55000        ------       1.50000              0            0         10.000
  30  RFA485000I0.00XNOBP     2.74269      4.85000        ------        ------              0            0          0.500
  31  RFA485000I0.00XW24A     2.74269      4.85000        ------        ------             24           24          0.500
  32  RFA485064I1.50XW24A     2.74269      4.85000        ------       1.50000             24           24          6.667
  33  RFA485080I1.50XW24A     2.74269      4.85000        ------       1.50000             24           24          3.333
  34  RFA495000I0.00XNOBP     2.74269      4.95000        ------        ------              0            0          3.167
  35  RFA495000I0.00XW20A     2.74269      4.95000        ------        ------             20           20          0.167
  36  RFA495000I0.00XW24A     2.74269      4.95000        ------        ------             24           24          0.667
  37  RFA495048I1.50XNOBP     2.74269      4.95000        ------       1.50000              0            0          6.667
  38  RFA495080I1.50XNOBP     2.74269      4.95000        ------       1.50000              0            0         13.333
  39  RFA495104I1.50XNOBP     2.74269      4.95000        ------       1.50000              0            0          3.333
  40  RFA495048I1.50XW20A     2.74269      4.95000        ------       1.50000             20           20          3.333
  41  RFA495016I1.50XW24A     2.74269      4.95000        ------       1.50000             24           24          6.667
  42  RFA495080I1.50XW24A     2.74269      4.95000        ------       1.50000             24           24          6.667
  43  RFA205000I0.00XNOBP     2.74269      2.05000        ------        ------              0            0          3.667
  44  sil495p0a0              3.48830      4.95000        ------        ------              0            0         88.917

 Reflector Segments:
 Seg.
 Num. Segment Name
 ---- --------------------
  1   RFABOTREFL          
  2   CATRFA310RADREF     
  3   RFATOPREFL          
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   25
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 Fuel Temperature Data (SEG.TFU/TAB.TFU)
 TFU = TMO + A + (B+TABTFU(N))*POWER + C*POWER**2

 Seg
 Num.      Segment Name          A        B        C     TAB.TFU  TAB.XKS  TAB.XK2
 ----  --------------------    -----    -----    -----   -------  -------  -------
   4    RFA260BLANK              0.0      0.0      0.0   Table 1  -------  -------
   5    RFA260BOTBK              0.0      0.0      0.0   Table 1  -------  -------
  10    RFA460000I0.00XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  11    RFA460000I0.00XW16P      0.0      0.0      0.0   Table 1  -------  -------
  12    RFA460104I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  13    RFA460104I1.50XW16P      0.0      0.0      0.0   Table 1  -------  -------
  14    RFA495000I0.00XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  15    RFA495064I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  16    RFA495104I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  17    RFA495128I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  18    RFA410000I0.00XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  19    RFA410128I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  20    RFA460000I0.00XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  21    RFA460104I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  22    RFA460128I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  23    RFA495000I0.00XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  24    RFA495064I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  25    RFA495080I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  26    RFA495104I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  27    RFA455000I0.00XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  28    RFA455156I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  29    RFA455104I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  30    RFA485000I0.00XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  31    RFA485000I0.00XW24A      0.0      0.0      0.0   Table 1  -------  -------
  32    RFA485064I1.50XW24A      0.0      0.0      0.0   Table 1  -------  -------
  33    RFA485080I1.50XW24A      0.0      0.0      0.0   Table 1  -------  -------
  34    RFA495000I0.00XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  35    RFA495000I0.00XW20A      0.0      0.0      0.0   Table 1  -------  -------
  36    RFA495000I0.00XW24A      0.0      0.0      0.0   Table 1  -------  -------
  37    RFA495048I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  38    RFA495080I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  39    RFA495104I1.50XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  40    RFA495048I1.50XW20A      0.0      0.0      0.0   Table 1  -------  -------
  41    RFA495016I1.50XW24A      0.0      0.0      0.0   Table 1  -------  -------
  42    RFA495080I1.50XW24A      0.0      0.0      0.0   Table 1  -------  -------
  43    RFA205000I0.00XNOBP      0.0      0.0      0.0   Table 1  -------  -------
  44    sil495p0a0               0.0      0.0      0.0   Table 1  -------  -------

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 Fuel Temperature Tables (TAB.TFU) (multipliers are extrapolated off ends of tables)

 Table:  1
 Primary Variable (Across):EXP 
 Secondary Variable(Down) :POW 
              0.000     5.000    10.000    15.000    20.000    30.000    40.000    50.000    60.000
          ------------------------------------------------------------------------------------------
   0.492  | 331.000   309.700   278.000   242.400   227.100   240.000   252.900   266.900   283.100
   0.983  | 322.000   306.400   278.200   245.700   248.200   264.300   280.300   297.700   318.300
   1.475  | 316.500   305.400   280.000   262.100   272.100   291.600   310.800   331.800   356.400
   1.967  | 311.500   303.400   280.100   283.600   294.200   314.000   332.000   351.000   371.300
   2.458  | 301.900   293.600   281.200   291.500   300.400   316.900   331.500   346.300   362.400

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 PWR.OPT:       1         2
          --------------------
 Power(%):     0.00    100.00
 Temp(F):    557.00    552.90


1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   26
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

 Assembly Physical Descriptions
 ------------------------------

 Assm. Name    : RADIAL              |   Assm. Name    : RFA495000I0.00XNOBP |   Assm. Name    : RFA495064I1.50XNOBP
 Assm. Sub Type:    2     2          |   Assm. Sub Type:   23    23          |   Assm. Sub Type:   24    24
 Assm. Sub Type:    2     2          |   Assm. Sub Type:   23    23          |   Assm. Sub Type:   24    24
 Assm. Class   : Reflector           |   Assm. Class   : Fuel                |   Assm. Class   : Fuel
 Fuel  Type    : 2                   |   Fuel  Type    : 23                  |   Fuel  Type    : 24
 Mech. Design  : 1                   |   Mech. Design  : 1                   |   Mech. Design  : 1
 Loading (gram): 0.                  |   Loading (gram): 455519.             |   Loading (gram): 455519.
 # Axial Zone  : 3                   |   # Axial Zone  : 9                   |   # Axial Zone  : 9
 # Assm        : 31.000              |   # Assm in Core: 4.000               |   # Assm in Core: 4.000
                                     |                                       |
                                     |   **********************  381.000 cm  |   **********************  381.000 cm
                                     |   *     Segment:3      *              |   *     Segment:3      *
                                     |   *     RFATOPREFL     *              |   *     RFATOPREFL     *
                                     |   *     15.240 cm      *              |   *     15.240 cm      *
                                     |   **********************  365.760 cm  |   **********************  365.760 cm
                                     |   *     Segment:4      *              |   *     Segment:4      *
                                     |   *    RFA260BLANK     *              |   *    RFA260BLANK     *
                                     |   *     15.240 cm      *              |   *     15.240 cm      *
                                     |   **********************  350.520 cm  |   **********************  350.520 cm
                                     |   *     Segment:23     *              |   *     Segment:23     *
                                     |   *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *
                                     |   *      7.620 cm      *              |   *      7.620 cm      *
                                     |   **********************  342.900 cm  |   **********************  342.900 cm
                                     |   *     Segment:23     *              |   *     Segment:23     *
                                     |   *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *
                                     |   *      7.620 cm      *              |   *      7.620 cm      *
                                     |   **********************  335.280 cm  |   **********************  335.280 cm
                                     |   *     Segment:23     *              |   *     Segment:24     *
                                     |   *RFA495000I0.00XNOBP *              |   *RFA495064I1.50XNOBP *
                                     |   *     304.800 cm     *              |   *     304.800 cm     *
                                     |   **********************   30.480 cm  |   **********************   30.480 cm
                                     |   *     Segment:23     *              |   *     Segment:23     *
                                     |   *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *
                                     |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************  381.000 cm  |   **********************   22.860 cm  |   **********************   22.860 cm
 *     Segment:3      *              |   *     Segment:23     *              |   *     Segment:23     *
 *     RFATOPREFL     *              |   *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *
 *     15.240 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************  365.760 cm  |   **********************   15.240 cm  |   **********************   15.240 cm
 *     Segment:2      *              |   *     Segment:5      *              |   *     Segment:5      *
 *  CATRFA310RADREF   *              |   *    RFA260BOTBK     *              |   *    RFA260BOTBK     *
 *     365.760 cm     *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************    0.000 cm  |   **********************    0.000 cm  |   **********************    0.000 cm
 *     Segment:1      *              |   *     Segment:1      *              |   *     Segment:1      *
 *     RFABOTREFL     *              |   *     RFABOTREFL     *              |   *     RFABOTREFL     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************  -15.240 cm  |   **********************  -15.240 cm  |   **********************  -15.240 cm

 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   27
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

 Assm. Name    : RFA495104I1.50XNOBP |   Assm. Name    : RFA455156I1.50XNOBP |   Assm. Name    : RFA455104I1.50XNOBP
 Assm. Sub Type:   26    26          |   Assm. Sub Type:   28    28          |   Assm. Sub Type:   29    29
 Assm. Sub Type:   26    26          |   Assm. Sub Type:   28    28          |   Assm. Sub Type:   29    29
 Assm. Class   : Fuel                |   Assm. Class   : Fuel                |   Assm. Class   : Fuel
 Fuel  Type    : 26                  |   Fuel  Type    : 28                  |   Fuel  Type    : 29
 Mech. Design  : 1                   |   Mech. Design  : 1                   |   Mech. Design  : 1
 Loading (gram): 455519.             |   Loading (gram): 455519.             |   Loading (gram): 455519.
 # Axial Zone  : 9                   |   # Axial Zone  : 9                   |   # Axial Zone  : 9
 # Assm in Core: 8.000               |   # Assm in Core: 4.000               |   # Assm in Core: 12.000
                                     |                                       |
 **********************  381.000 cm  |   **********************  381.000 cm  |   **********************  381.000 cm
 *     Segment:3      *              |   *     Segment:3      *              |   *     Segment:3      *
 *     RFATOPREFL     *              |   *     RFATOPREFL     *              |   *     RFATOPREFL     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************  365.760 cm  |   **********************  365.760 cm  |   **********************  365.760 cm
 *     Segment:4      *              |   *     Segment:4      *              |   *     Segment:4      *
 *    RFA260BLANK     *              |   *    RFA260BLANK     *              |   *    RFA260BLANK     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************  350.520 cm  |   **********************  350.520 cm  |   **********************  350.520 cm
 *     Segment:23     *              |   *     Segment:27     *              |   *     Segment:27     *
 *RFA495000I0.00XNOBP *              |   *RFA455000I0.00XNOBP *              |   *RFA455000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************  342.900 cm  |   **********************  342.900 cm  |   **********************  342.900 cm
 *     Segment:23     *              |   *     Segment:27     *              |   *     Segment:27     *
 *RFA495000I0.00XNOBP *              |   *RFA455000I0.00XNOBP *              |   *RFA455000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************  335.280 cm  |   **********************  335.280 cm  |   **********************  335.280 cm
 *     Segment:26     *              |   *     Segment:28     *              |   *     Segment:29     *
 *RFA495104I1.50XNOBP *              |   *RFA455156I1.50XNOBP *              |   *RFA455104I1.50XNOBP *
 *     304.800 cm     *              |   *     304.800 cm     *              |   *     304.800 cm     *
 **********************   30.480 cm  |   **********************   30.480 cm  |   **********************   30.480 cm
 *     Segment:23     *              |   *     Segment:27     *              |   *     Segment:27     *
 *RFA495000I0.00XNOBP *              |   *RFA455000I0.00XNOBP *              |   *RFA455000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************   22.860 cm  |   **********************   22.860 cm  |   **********************   22.860 cm
 *     Segment:23     *              |   *     Segment:27     *              |   *     Segment:27     *
 *RFA495000I0.00XNOBP *              |   *RFA455000I0.00XNOBP *              |   *RFA455000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************   15.240 cm  |   **********************   15.240 cm  |   **********************   15.240 cm
 *     Segment:5      *              |   *     Segment:5      *              |   *     Segment:5      *
 *    RFA260BOTBK     *              |   *    RFA260BOTBK     *              |   *    RFA260BOTBK     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************    0.000 cm  |   **********************    0.000 cm  |   **********************    0.000 cm
 *     Segment:1      *              |   *     Segment:1      *              |   *     Segment:1      *
 *     RFABOTREFL     *              |   *     RFABOTREFL     *              |   *     RFABOTREFL     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************  -15.240 cm  |   **********************  -15.240 cm  |   **********************  -15.240 cm

 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   28
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

 Assm. Name    : RFA485064I1.50XW24A |   Assm. Name    : RFA485080I1.50XW24A |   Assm. Name    : RFA495048I1.50XNOBP
 Assm. Sub Type:   32    32          |   Assm. Sub Type:   33    33          |   Assm. Sub Type:   37    37
 Assm. Sub Type:   32    32          |   Assm. Sub Type:   33    33          |   Assm. Sub Type:   37    37
 Assm. Class   : Fuel                |   Assm. Class   : Fuel                |   Assm. Class   : Fuel
 Fuel  Type    : 32                  |   Fuel  Type    : 33                  |   Fuel  Type    : 37
 Mech. Design  : 1                   |   Mech. Design  : 1                   |   Mech. Design  : 1
 Loading (gram): 455519.             |   Loading (gram): 455519.             |   Loading (gram): 455519.
 # Axial Zone  : 9                   |   # Axial Zone  : 9                   |   # Axial Zone  : 9
 # Assm in Core: 8.000               |   # Assm in Core: 4.000               |   # Assm in Core: 8.000
                                     |                                       |
 **********************  381.000 cm  |   **********************  381.000 cm  |   **********************  381.000 cm
 *     Segment:3      *              |   *     Segment:3      *              |   *     Segment:3      *
 *     RFATOPREFL     *              |   *     RFATOPREFL     *              |   *     RFATOPREFL     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************  365.760 cm  |   **********************  365.760 cm  |   **********************  365.760 cm
 *     Segment:4      *              |   *     Segment:4      *              |   *     Segment:4      *
 *    RFA260BLANK     *              |   *    RFA260BLANK     *              |   *    RFA260BLANK     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************  350.520 cm  |   **********************  350.520 cm  |   **********************  350.520 cm
 *     Segment:30     *              |   *     Segment:30     *              |   *     Segment:34     *
 *RFA485000I0.00XNOBP *              |   *RFA485000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************  342.900 cm  |   **********************  342.900 cm  |   **********************  342.900 cm
 *     Segment:31     *              |   *     Segment:31     *              |   *     Segment:34     *
 *RFA485000I0.00XW24A *              |   *RFA485000I0.00XW24A *              |   *RFA495000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************  335.280 cm  |   **********************  335.280 cm  |   **********************  335.280 cm
 *     Segment:32     *              |   *     Segment:33     *              |   *     Segment:37     *
 *RFA485064I1.50XW24A *              |   *RFA485080I1.50XW24A *              |   *RFA495048I1.50XNOBP *
 *     304.800 cm     *              |   *     304.800 cm     *              |   *     304.800 cm     *
 **********************   30.480 cm  |   **********************   30.480 cm  |   **********************   30.480 cm
 *     Segment:31     *              |   *     Segment:31     *              |   *     Segment:34     *
 *RFA485000I0.00XW24A *              |   *RFA485000I0.00XW24A *              |   *RFA495000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************   22.860 cm  |   **********************   22.860 cm  |   **********************   22.860 cm
 *     Segment:30     *              |   *     Segment:30     *              |   *     Segment:34     *
 *RFA485000I0.00XNOBP *              |   *RFA485000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************   15.240 cm  |   **********************   15.240 cm  |   **********************   15.240 cm
 *     Segment:5      *              |   *     Segment:5      *              |   *     Segment:5      *
 *    RFA260BOTBK     *              |   *    RFA260BOTBK     *              |   *    RFA260BOTBK     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************    0.000 cm  |   **********************    0.000 cm  |   **********************    0.000 cm
 *     Segment:1      *              |   *     Segment:1      *              |   *     Segment:1      *
 *     RFABOTREFL     *              |   *     RFABOTREFL     *              |   *     RFABOTREFL     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************  -15.240 cm  |   **********************  -15.240 cm  |   **********************  -15.240 cm

 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   29
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

 Assm. Name    : RFA495080I1.50XNOBP |   Assm. Name    : RFA495104I1.50XNOBP |   Assm. Name    : RFA495048I1.50XW20A
 Assm. Sub Type:   38    38          |   Assm. Sub Type:   39    39          |   Assm. Sub Type:   40    40
 Assm. Sub Type:   38    38          |   Assm. Sub Type:   39    39          |   Assm. Sub Type:   40    40
 Assm. Class   : Fuel                |   Assm. Class   : Fuel                |   Assm. Class   : Fuel
 Fuel  Type    : 38                  |   Fuel  Type    : 39                  |   Fuel  Type    : 40
 Mech. Design  : 1                   |   Mech. Design  : 1                   |   Mech. Design  : 1
 Loading (gram): 455519.             |   Loading (gram): 455519.             |   Loading (gram): 455519.
 # Axial Zone  : 9                   |   # Axial Zone  : 9                   |   # Axial Zone  : 9
 # Assm in Core: 16.000              |   # Assm in Core: 4.000               |   # Assm in Core: 4.000
                                     |                                       |
 **********************  381.000 cm  |   **********************  381.000 cm  |   **********************  381.000 cm
 *     Segment:3      *              |   *     Segment:3      *              |   *     Segment:3      *
 *     RFATOPREFL     *              |   *     RFATOPREFL     *              |   *     RFATOPREFL     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************  365.760 cm  |   **********************  365.760 cm  |   **********************  365.760 cm
 *     Segment:4      *              |   *     Segment:4      *              |   *     Segment:4      *
 *    RFA260BLANK     *              |   *    RFA260BLANK     *              |   *    RFA260BLANK     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************  350.520 cm  |   **********************  350.520 cm  |   **********************  350.520 cm
 *     Segment:34     *              |   *     Segment:34     *              |   *     Segment:34     *
 *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************  342.900 cm  |   **********************  342.900 cm  |   **********************  342.900 cm
 *     Segment:34     *              |   *     Segment:34     *              |   *     Segment:35     *
 *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XW20A *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************  335.280 cm  |   **********************  335.280 cm  |   **********************  335.280 cm
 *     Segment:38     *              |   *     Segment:39     *              |   *     Segment:40     *
 *RFA495080I1.50XNOBP *              |   *RFA495104I1.50XNOBP *              |   *RFA495048I1.50XW20A *
 *     304.800 cm     *              |   *     304.800 cm     *              |   *     304.800 cm     *
 **********************   30.480 cm  |   **********************   30.480 cm  |   **********************   30.480 cm
 *     Segment:34     *              |   *     Segment:34     *              |   *     Segment:35     *
 *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XW20A *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************   22.860 cm  |   **********************   22.860 cm  |   **********************   22.860 cm
 *     Segment:34     *              |   *     Segment:34     *              |   *     Segment:34     *
 *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************   15.240 cm  |   **********************   15.240 cm  |   **********************   15.240 cm
 *     Segment:5      *              |   *     Segment:5      *              |   *     Segment:5      *
 *    RFA260BOTBK     *              |   *    RFA260BOTBK     *              |   *    RFA260BOTBK     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************    0.000 cm  |   **********************    0.000 cm  |   **********************    0.000 cm
 *     Segment:1      *              |   *     Segment:1      *              |   *     Segment:1      *
 *     RFABOTREFL     *              |   *     RFABOTREFL     *              |   *     RFABOTREFL     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************  -15.240 cm  |   **********************  -15.240 cm  |   **********************  -15.240 cm

 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   30
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

 Assm. Name    : RFA495016I1.50XW24A |   Assm. Name    : RFA495080I1.50XW24A |   Assm. Name    : RFA205000I0.00XNOBP
 Assm. Sub Type:   41    41          |   Assm. Sub Type:   42    42          |   Assm. Sub Type:   43    43
 Assm. Sub Type:   41    41          |   Assm. Sub Type:   42    42          |   Assm. Sub Type:   43    43
 Assm. Class   : Fuel                |   Assm. Class   : Fuel                |   Assm. Class   : Fuel
 Fuel  Type    : 41                  |   Fuel  Type    : 42                  |   Fuel  Type    : 43
 Mech. Design  : 1                   |   Mech. Design  : 1                   |   Mech. Design  : 1
 Loading (gram): 455519.             |   Loading (gram): 455519.             |   Loading (gram): 455519.
 # Axial Zone  : 9                   |   # Axial Zone  : 9                   |   # Axial Zone  : 9
 # Assm in Core: 8.000               |   # Assm in Core: 8.000               |   # Assm in Core: 4.000
                                     |                                       |
 **********************  381.000 cm  |   **********************  381.000 cm  |   **********************  381.000 cm
 *     Segment:3      *              |   *     Segment:3      *              |   *     Segment:3      *
 *     RFATOPREFL     *              |   *     RFATOPREFL     *              |   *     RFATOPREFL     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************  365.760 cm  |   **********************  365.760 cm  |   **********************  365.760 cm
 *     Segment:4      *              |   *     Segment:4      *              |   *     Segment:4      *
 *    RFA260BLANK     *              |   *    RFA260BLANK     *              |   *    RFA260BLANK     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************  350.520 cm  |   **********************  350.520 cm  |   **********************  350.520 cm
 *     Segment:34     *              |   *     Segment:34     *              |   *     Segment:43     *
 *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *              |   *RFA205000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************  342.900 cm  |   **********************  342.900 cm  |   **********************  342.900 cm
 *     Segment:36     *              |   *     Segment:36     *              |   *     Segment:43     *
 *RFA495000I0.00XW24A *              |   *RFA495000I0.00XW24A *              |   *RFA205000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************  335.280 cm  |   **********************  335.280 cm  |   **********************  335.280 cm
 *     Segment:41     *              |   *     Segment:42     *              |   *     Segment:43     *
 *RFA495016I1.50XW24A *              |   *RFA495080I1.50XW24A *              |   *RFA205000I0.00XNOBP *
 *     304.800 cm     *              |   *     304.800 cm     *              |   *     304.800 cm     *
 **********************   30.480 cm  |   **********************   30.480 cm  |   **********************   30.480 cm
 *     Segment:36     *              |   *     Segment:36     *              |   *     Segment:43     *
 *RFA495000I0.00XW24A *              |   *RFA495000I0.00XW24A *              |   *RFA205000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************   22.860 cm  |   **********************   22.860 cm  |   **********************   22.860 cm
 *     Segment:34     *              |   *     Segment:34     *              |   *     Segment:43     *
 *RFA495000I0.00XNOBP *              |   *RFA495000I0.00XNOBP *              |   *RFA205000I0.00XNOBP *
 *      7.620 cm      *              |   *      7.620 cm      *              |   *      7.620 cm      *
 **********************   15.240 cm  |   **********************   15.240 cm  |   **********************   15.240 cm
 *     Segment:5      *              |   *     Segment:5      *              |   *     Segment:5      *
 *    RFA260BOTBK     *              |   *    RFA260BOTBK     *              |   *    RFA260BOTBK     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************    0.000 cm  |   **********************    0.000 cm  |   **********************    0.000 cm
 *     Segment:1      *              |   *     Segment:1      *              |   *     Segment:1      *
 *     RFABOTREFL     *              |   *     RFABOTREFL     *              |   *     RFABOTREFL     *
 *     15.240 cm      *              |   *     15.240 cm      *              |   *     15.240 cm      *
 **********************  -15.240 cm  |   **********************  -15.240 cm  |   **********************  -15.240 cm

 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   31
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

 Assm. Name    : sil495p0a0
 Assm. Sub Type:   44    44
 Assm. Sub Type:   44    44
 Assm. Class   : Fuel
 Fuel  Type    : 44
 Mech. Design  : 1
 Loading (gram): 571119.
 # Axial Zone  : 9
 # Assm in Core: 97.000
 
 **********************  381.000 cm
 *     Segment:3      *
 *     RFATOPREFL     *
 *     15.240 cm      *
 **********************  365.760 cm
 *     Segment:4      *
 *    RFA260BLANK     *
 *     15.240 cm      *
 **********************  350.520 cm
 *     Segment:44     *
 *     sil495p0a0     *
 *      7.620 cm      *
 **********************  342.900 cm
 *     Segment:44     *
 *     sil495p0a0     *
 *      7.620 cm      *
 **********************  335.280 cm
 *     Segment:44     *
 *     sil495p0a0     *
 *     304.800 cm     *
 **********************   30.480 cm
 *     Segment:44     *
 *     sil495p0a0     *
 *      7.620 cm      *
 **********************   22.860 cm
 *     Segment:44     *
 *     sil495p0a0     *
 *      7.620 cm      *
 **********************   15.240 cm
 *     Segment:5      *
 *    RFA260BOTBK     *
 *     15.240 cm      *
 **********************    0.000 cm
 *     Segment:1      *
 *     RFABOTREFL     *
 *     15.240 cm      *
 **********************  -15.240 cm

 

 Axial Nodal Boundaries (cm)
 ---------------------------
  27   381.0000
  26   365.7600
  25   350.5200
  24   335.2800
  23   320.0400
  22   304.8000
  21   289.5600
  20   274.3200
  19   259.0800
  18   243.8400
  17   228.6000
  16   213.3600
  15   198.1200
  14   182.8800
  13   167.6400
  12   152.4000
  11   137.1600
  10   121.9200
   9   106.6800
   8    91.4400
   7    76.2000
   6    60.9600
   5    45.7200
   4    30.4800
   3    15.2400
   2     0.0000
   1   -15.2400
 
 Grid Type Information
 ---------------------
 Type     Thickness(cm)     SIG12     DSIGA2
 ----     -------------     -----     ------
 BT2              5.700    0.9060     0.9890
 MIX              6.460    0.9060     0.9890
 MID              3.810    0.9060     0.9890
 IFM              1.210    0.9060     0.9890
 MX1              5.700    0.9060     0.9890
 MX2              6.460    0.9060     0.9890
 MX3              1.800    0.9060     0.9890


 Axial Grid Location by Mechanical Design Type
 ---------------------------------------------

 Mechanical Type:  1
 Grid         Axial           Grid 
 Type      Location(cm)    Thickness(cm)
 ------    ------------    -------------
 MID         327.840          3.810
 IFM         301.740          1.210
 MID         275.650          3.810
 IFM         249.540          1.210
 MID         223.450          3.810
 IFM         197.350          1.210
 MID         171.250          3.810
 MID         119.050          3.810
 MID          65.210          3.810


1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   32
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

 Control Rod Descriptions
 ------------------------
 CRD.  Name    : AICTIP/B4C          |   CRD.  Name    : B4C\AICTIP
 CRD.  Type    : 1                   |   CRD.  Type    : 2
 Mech. Descrip : 1: PWR Finger       |   Mech. Descrip : 1: PWR Finger
 # Axial Zone  : 3                   |   # Axial Zone  : 3
 **********************  365.760 cm  |   **********************  365.760 cm
 *      TYPE:20       *              |   *      TYPE:20       *
 *GRAY FACTOR:  1.000 *              |   *GRAY FACTOR:  1.000 *
 *     253.170 cm     *              |   *     253.660 cm     *
 **********************  112.590 cm  |   **********************  112.100 cm
 *      TYPE:10       *              |   *      TYPE:10       *
 *GRAY FACTOR:  1.000 *              |   *GRAY FACTOR:  1.000 *
 *     101.600 cm     *              |   *     101.620 cm     *
 **********************   10.990 cm  |   **********************   10.480 cm
 *       TYPE:0       *              |   *       TYPE:0       *
 *GRAY FACTOR:  1.000 *              |   *GRAY FACTOR:  1.000 *
 *     10.990 cm      *              |   *     10.480 cm      *
 **********************    0.000 cm  |   **********************    0.000 cm


 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 

 Fuel Mechanical Designs           1
 Total Pins                      289
 Fuel Pins                       264
 Non-Fuel Pins                    25
 Number of Assemblies        48.2500

1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   33
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    
 _____________________________________________________________________________________________________________________________

 PRI.INP - FMAP - Loading Map for Full Core
 I / J =    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        FUE.LAB:      :      :      :25L-01:25K-01:25J-01:25H-01:25G-01:25F-01:25E-01:      :      :      :      :           
   1    FUE.SER:      :      :      :sil00 :sil01 :sil02 :sil03 :sil04 :sil05 :sil06 :      :      :      :      :  01       
        TYP,ROT:      :      :      :     0:     0:     0:     0:     0:     0:     0:      :      :      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :      :25N-02:25M-02:24E-14:25K-02:24E-13:25H-02:24L-13:25F-02:24L-14:25D-02:25C-02:      :      :           
   2    :      :      :sil07 :sil08 :24E-14:sil09 :24E-13:sil10 :24L-13:sil11 :24L-14:sil12 :sil13 :      :      :  02       
        :      :      :     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :25P-03:25N-03:24F-14:24G-13:24D-13:25J-03:24H-14:25G-03:24M-13:24J-13:24K-14:25C-03:25B-03:      :           
   3    :      :sil14 :sil15 :24F-14:24G-13:24D-13:sil16 :24H-14:sil17 :24M-13:24J-13:24K-14:sil18 :sil19 :      :  03       
        :      :     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :25P-04:24B-10:25M-04:24D-10:25K-04:24G-14:25H-04:24J-14:25F-04:24M-10:25D-04:24P-10:25B-04:      :           
   4    :      :sil20 :24B-10:sil21 :24D-10:sil22 :24G-14:sil23 :24J-14:sil24 :24M-10:sil25 :24P-10:sil26 :      :  04       
        :      :     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :25R-05:24B-11:24C-09:24F-12:25L-05:24G-11:24E-11:24H-12:24L-11:24J-11:25E-05:24K-12:24N-09:24P-11:25A-05:           
   5    :sil27 :24B-11:24C-09:24F-12:sil28 :24G-11:24E-11:24H-12:24L-11:24J-11:sil29 :24K-12:24N-09:24P-11:sil30 :  05       
        :     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :25R-06:25P-06:24C-12:25M-06:24E-09:25K-06:24E-12:25H-06:24L-12:25F-06:24L-09:25D-06:24N-12:25B-06:25A-06:           
   6    :sil31 :sil32 :24C-12:sil33 :24E-09:sil34 :24E-12:sil35 :24L-12:sil36 :24L-09:sil37 :24N-12:sil38 :sil39 :  06       
        :     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :25R-07:24G-09:25N-07:24B-09:24C-13:24F-10:24C-11:24H-10:24N-11:24K-10:24N-13:24P-09:25C-07:24J-09:25A-07:           
   7    :sil40 :24G-09:sil41 :24B-09:24C-13:24F-10:24C-11:24H-10:24N-11:24K-10:24N-13:24P-09:sil42 :24J-09:sil43 :  07       
        :     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:     0:           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :25R-08:25P-08:24B-08:25M-08:24D-08:25K-08:24F-08:25H-08:24K-08:25F-08:24M-08:25D-08:24P-08:25B-08:25A-08:           
   8    :sil44 :sil45 :24B-08:sil46 :24D-08:sil47 :24F-08:sil48 :24K-08:sil49 :24M-08:sil50 :24P-08:sil51 :sil52 :  08       
        :     0:     0:     0:     0:     0:     0:     0: 44  0: 28  0: 44  0: 29  0: 44  0: 39  0: 44  0: 44  0:           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :25R-09:24G-07:25N-09:24B-07:24C-03:24F-06:24C-05:24H-06:24N-05:24K-06:24N-03:24P-07:25C-09:24J-07:25A-09:           
   9    :sil53 :24G-07:sil54 :24B-07:24C-03:24F-06:24C-05:24H-06:24N-05:24K-06:24N-03:24P-07:sil55 :24J-07:sil56 :  09       
        :     0:     0:     0:     0:     0:     0:     0: 28  0: 29  0: 23  0: 43  0: 38  0: 44  0: 40  0: 44  0:           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :25R-10:25P-10:24C-04:25M-10:24E-07:25K-10:24E-04:25H-10:24L-04:25F-10:24L-07:25D-10:24N-04:25B-10:25A-10:           
  10    :sil57 :sil58 :24C-04:sil59 :24E-07:sil60 :24E-04:sil61 :24L-04:sil62 :24L-07:sil63 :24N-04:sil64 :sil65 :  10       
        :     0:     0:     0:     0:     0:     0:     0: 44  0: 24  0: 44  0: 32  0: 44  0: 41  0: 44  0: 44  0:           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :25R-11:24B-05:24C-07:24F-04:25L-11:24G-05:24E-05:24H-04:24L-05:24J-05:25E-11:24K-04:24N-07:24P-05:25A-11:           
  11    :sil66 :24B-05:24C-07:24F-04:sil67 :24G-05:24E-05:24H-04:24L-05:24J-05:sil68 :24K-04:24N-07:24P-05:sil69 :  11       
        :     0:     0:     0:     0:     0:     0:     0: 29  0: 33  0: 32  0: 44  0: 42  0: 26  0: 37  0: 44  0:           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :25P-12:24B-06:25M-12:24D-06:25K-12:24G-02:25H-12:24J-02:25F-12:24M-06:25D-12:24P-06:25B-12:      :           
  12    :      :sil70 :24B-06:sil71 :24D-06:sil72 :24G-02:sil73 :24J-02:sil74 :24M-06:sil75 :24P-06:sil76 :      :  12       
        :      :     0:     0:     0:     0:     0:     0: 44  0: 38  0: 44  0: 42  0: 44  0: 38  0: 44  0:      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :25P-13:25N-13:24F-02:24G-03:24D-03:25J-13:24H-02:25G-13:24M-03:24J-03:24K-02:25C-13:25B-13:      :           
  13    :      :sil77 :sil78 :24F-02:24G-03:24D-03:sil79 :24H-02:sil80 :24M-03:24J-03:24K-02:sil81 :sil82 :      :  13       
        :      :     0:     0:     0:     0:     0:     0: 39  0: 44  0: 41  0: 26  0: 38  0: 44  0: 44  0:      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :      :25N-14:25M-14:24E-02:25K-14:24E-03:25H-14:24L-03:25F-14:24L-02:25D-14:25C-14:      :      :           
  14    :      :      :sil83 :sil84 :24E-02:sil85 :24E-03:sil86 :24L-03:sil87 :24L-02:sil88 :sil89 :      :      :  14       
        :      :      :     0:     0:     0:     0:     0: 44  0: 29  0: 44  0: 37  0: 44  0: 44  0:      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :      :      :      :25L-15:25K-15:25J-15:25H-15:25G-15:25F-15:25E-15:      :      :      :      :           
  15    :      :      :      :      :sil90 :sil91 :sil92 :sil93 :sil94 :sil95 :sil96 :      :      :      :      :  15       
        :      :      :      :      :     0:     0:     0: 44  0: 44  0: 44  0: 44  0:      :      :      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
            R-     P-     N-     M-     L-     K-     J-     H-     G-     F-     E-     D-     C-     B-     A-
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   34
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    
 _____________________________________________________________________________________________________________________________

 PRI.INP - CMAP - Loading Map for Calculated Core
 I / J =    1      2      3      4      5      6      7      8
        +25H-08+24K-08+25F-08+24M-08+25D-08+24P-08+25B-08+25A-08+                                                            
        : 44  1: 28  6: 44  1: 29  6: 44  1: 39  8: 44  1: 44  1:                                                            
   1    :  4.75:  4.39:  4.75:  4.39:  4.75:  4.75:  4.75:  4.75:  08                                                        
        :  0.00: 28.52:  0.00: 28.21:  0.00: 25.45:  0.00:  0.00:                                                            
        +24H-06+24N-05+24K-06+24N-03+24P-07+25C-09+24J-07+25A-09+                                                            
        : 28  6: 29  6: 23  5: 43  9: 38  8: 44  1: 40  8: 44  1:                                                            
   2    :  4.39:  4.39:  4.75:  2.10:  4.75:  4.75:  4.75:  4.75:  09                                                        
        : 28.52: 28.58: 45.43: 13.06: 25.76:  0.00: 27.98:  0.00:                                                            
        +25H-10+24L-04+25F-10+24L-07+25D-10+24N-04+25B-10+25A-10+                                                            
        : 44  1: 24  5: 44  1: 32  7: 44  1: 41  8: 44  1: 44  1:                                                            
   3    :  4.75:  4.75:  4.75:  4.66:  4.75:  4.75:  4.75:  4.75:  10                                                        
        :  0.00: 48.52:  0.00: 27.90:  0.00: 23.78:  0.00:  0.00:                                                            
        +24H-04+24L-05+24J-05+25E-11+24K-04+24N-07+24P-05+25A-11+                                                            
        : 29  6: 33  7: 32  7: 44  1: 42  8: 26  5: 37  8: 44  1:                                                            
   4    :  4.39:  4.66:  4.66:  4.75:  4.75:  4.75:  4.75:  4.75:  11                                                        
        : 28.21: 28.92: 27.90:  0.00: 28.46: 48.13: 23.10:  0.00:                                                            
        +25H-12+24J-02+25F-12+24M-06+25D-12+24P-06+25B-12+------+                                                            
        : 44  1: 38  8: 44  1: 42  8: 44  1: 38  8: 44  1:      :                                                            
   5    :  4.75:  4.75:  4.75:  4.75:  4.75:  4.75:  4.75:      :  12                                                        
        :  0.00: 25.77:  0.00: 28.48:  0.00: 25.64:  0.00:      :                                                            
        +24H-02+25G-13+24M-03+24J-03+24K-02+25C-13+25B-13+------+                                                            
        : 39  8: 44  1: 41  8: 26  5: 38  8: 44  1: 44  1:      :                                                            
   6    :  4.75:  4.75:  4.75:  4.75:  4.75:  4.75:  4.75:      :  13                                                        
        : 25.45:  0.00: 23.65: 48.13: 25.62:  0.00:  0.00:      :                                                            
        +25H-14+24L-03+25F-14+24L-02+25D-14+25C-14+------+------+                                                            
        : 44  1: 29  6: 44  1: 37  8: 44  1: 44  1:      :      :                                                            
   7    :  4.75:  4.39:  4.75:  4.75:  4.75:  4.75:      :      :  14                                                        
        :  0.00: 28.50:  0.00: 23.05:  0.00:  0.00:      :      :                                                            
        +25H-15+25G-15+25F-15+25E-15+------+------+------+FUE.LAB                                                            
        : 44  1: 44  1: 44  1: 44  1:      :      :      :TYP,BAT                                                            
   8    :  4.75:  4.75:  4.75:  4.75:      :      :      :FUE.ENR  15                                                        
        :  0.00:  0.00:  0.00:  0.00:      :      :      :FUE.EXP                                                            
        +------+------+------+------+------+------+------+------+
            H-     G-     F-     E-     D-     C-     B-     A-
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   35
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    
 _____________________________________________________________________________________________________________________________

 PRI.INP - QMAP - Nodal Map for Calculated Core
 I/J =  1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     16
    +25H-08+24K-08+------+25F-08+------+24M-08+------+25D-08+------+24P-08+------+25B-08+------+25A-08+------+REF---+         
    : 44  4/ 28  1: 28  4/ 44  1: 44  4/ 29  1: 29  4/ 44  1: 44  4/ 39  1: 39  4/ 44  1: 44  4/ 44  1: 44  4/  2  0:         
  1 :  0.00/ 28.56: 28.50/  0.00:  0.00/ 27.93: 28.49/  0.00:  0.00/ 23.72: 27.21/  0.00:  0.00/  0.00:  0.00/  0.00:         
    +24H-06+24N-05+//////+24K-06+//////+24N-03+//////+24P-07+//////+25C-09+//////+24J-07+//////+25A-09+//////+REF///+         
    : 28  3/ 29  2: 29  3/ 23  2: 23  3/ 43  2: 43  3/ 38  2: 38  3/ 44  2: 44  3/ 40  2: 40  3/ 44  2: 44  3/  2  0:         
  2 : 28.55/ 27.41: 28.90/ 49.27: 45.39/  9.58: 13.00/ 23.92: 27.71/  0.00:  0.00/ 28.13: 28.12/  0.00:  0.00/  0.00:         
    +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+         
    : 28  4/ 29  1: 29  4/ 23  1: 23  4/ 43  1: 43  4/ 38  1: 38  4/ 44  1: 44  4/ 40  1: 40  4/ 44  1: 44  4/  2  0:         
  3 : 28.50/ 28.64: 29.38/ 45.38: 41.69/ 13.08: 16.59/ 23.90: 27.50/  0.00:  0.00/ 28.11: 27.58/  0.00:  0.00/  0.00:         
    +25H-10+24L-04+//////+25F-10+//////+24L-07+//////+25D-10+//////+24N-04+//////+25B-10+//////+25A-10+//////+REF///+         
    : 44  3/ 24  2: 24  3/ 44  2: 44  3/ 32  2: 32  3/ 44  2: 44  3/ 41  2: 41  3/ 44  2: 44  3/ 44  2: 44  3/  2  0:         
  4 :  0.00/ 47.32: 51.32/  0.00:  0.00/ 28.11: 27.93/  0.00:  0.00/ 20.35: 24.26/  0.00:  0.00/  0.00:  0.00/  0.00:         
    +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+         
    : 44  4/ 24  1: 24  4/ 44  1: 44  4/ 32  1: 32  4/ 44  1: 44  4/ 41  1: 41  4/ 44  1: 44  4/ 44  1: 44  4/  2  0:         
  5 :  0.00/ 45.56: 49.87/  0.00:  0.00/ 27.77: 27.78/  0.00:  0.00/ 23.85: 26.63/  0.00:  0.00/  0.00:  0.00/  0.00:         
    +24H-04+24L-05+//////+24J-05+//////+25E-11+//////+24K-04+//////+24N-07+//////+24P-05+//////+25A-11+//////+REF///+         
    : 29  3/ 33  2: 33  3/ 32  2: 32  3/ 44  2: 44  3/ 42  2: 42  3/ 26  2: 26  3/ 37  2: 37  3/ 44  2: 44  3/  2  0:         
  6 : 27.93/ 28.98: 29.01/ 28.11: 27.78/  0.00:  0.00/ 28.59: 27.86/ 50.04: 46.83/ 18.47: 24.68/  0.00:  0.00/  0.00:         
    +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+         
    : 29  4/ 33  1: 33  4/ 32  1: 32  4/ 44  1: 44  4/ 42  1: 42  4/ 26  1: 26  4/ 37  1: 37  4/ 44  1: 44  4/  2  0:         
  7 : 28.49/ 29.02: 28.66/ 27.94: 27.79/  0.00:  0.00/ 28.93: 28.43/ 49.53: 46.12/ 21.89: 27.35/  0.00:  0.00/  0.00:         
    +25H-12+24J-02+//////+25F-12+//////+24M-06+//////+25D-12+//////+24P-06+//////+25B-12+//////+REF///+//////+REF///+         
    : 44  3/ 38  2: 38  3/ 44  2: 44  3/ 42  2: 42  3/ 44  2: 44  3/ 38  2: 38  3/ 44  2: 44  3/  2  2:  2  3/  2  0:         
  8 :  0.00/ 23.94: 23.94/  0.00:  0.00/ 28.63: 28.96/  0.00:  0.00/ 23.17: 27.81/  0.00:  0.00/  0.00:  0.00/  0.00:         
    +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+         
    : 44  4/ 38  1: 38  4/ 44  1: 44  4/ 42  1: 42  4/ 44  1: 44  4/ 38  1: 38  4/ 44  1: 44  4/  2  1:      /      :         
  9 :  0.00/ 27.71: 27.51/  0.00:  0.00/ 27.88: 28.44/  0.00:  0.00/ 23.73: 27.83/  0.00:  0.00/  0.00:      /      :         
    +24H-02+25G-13+//////+24M-03+//////+24J-03+//////+24K-02+//////+25C-13+//////+25B-13+//////+REF///+//////+//////+         
    : 39  3/ 44  2: 44  3/ 41  2: 41  3/ 26  2: 26  3/ 38  2: 38  3/ 44  2: 44  3/ 44  2: 44  3/  2  2:      /      :         
 10 : 23.69/  0.00:  0.00/ 20.23: 23.74/ 50.04: 49.52/ 23.15: 23.73/  0.00:  0.00/  0.00:  0.00/  0.00:      /      :         
    +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+         
    : 39  4/ 44  1: 44  4/ 41  1: 41  4/ 26  1: 26  4/ 38  1: 38  4/ 44  1: 44  4/ 44  1: 44  4/  2  1:      /      :         
 11 : 27.20/  0.00:  0.00/ 24.17: 26.47/ 46.85: 46.13/ 27.78: 27.82/  0.00:  0.00/  0.00:  0.00/  0.00:      /      :         
    +25H-14+24L-03+//////+25F-14+//////+24L-02+//////+25D-14+//////+25C-14+//////+REF///+//////+REF///+//////+//////+         
    : 44  3/ 29  2: 29  3/ 44  2: 44  3/ 37  2: 37  3/ 44  2: 44  3/ 44  2: 44  3/  2  2:  2  3/  2  2:      /      :         
 12 :  0.00/ 27.33: 28.58/  0.00:  0.00/ 18.42: 21.85/  0.00:  0.00/  0.00:  0.00/  0.00:  0.00/  0.00:      /      :         
    +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+         
    : 44  4/ 29  1: 29  4/ 44  1: 44  4/ 37  1: 37  4/ 44  1: 44  4/ 44  1: 44  4/  2  1:      /      :      /      :         
 13 :  0.00/ 28.78: 29.31/  0.00:  0.00/ 24.61: 27.30/  0.00:  0.00/  0.00:  0.00/  0.00:      /      :      /      :         
    +25H-15+25G-15+//////+25F-15+//////+25E-15+//////+REF///+//////+REF///+//////+REF///+//////+//////+//////+//////+         
    : 44  3/ 44  2: 44  3/ 44  2: 44  3/ 44  2: 44  3/  2  2:  2  3/  2  2:  2  3/  2  2:      /      :      /      :         
 14 :  0.00/  0.00:  0.00/  0.00:  0.00/  0.00:  0.00/  0.00:  0.00/  0.00:  0.00/  0.00:      /      :      /      :         
    +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+         
    : 44  4/ 44  1: 44  4/ 44  1: 44  4/ 44  1: 44  4/  2  1:      /      :      /      :      /      :      /      :         
 15 :  0.00/  0.00:  0.00/  0.00:  0.00/  0.00:  0.00/  0.00:      /      :      /      :      /      :      /      :         
    +REF///+REF///+//////+REF///+//////+REF///+//////+REF///+//////+//////+//////+//////+//////+//////+//////+FUE.LAB         
    :  2  0/  2  0:  2  0/  2  0:  2  0/  2  0:  2  0/  2  0:      /      :      /      :      /      :      /TYP,ROT         
 16 :  0.00/  0.00:  0.00/  0.00:  0.00/  0.00:  0.00/  0.00:      /      :      /      :      /      :      /EXP             
    +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   36
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    
 _____________________________________________________________________________________________________________________________

 PRI.INP - BMAP - BPS Loading Map for Calculated Core
 I / J =    1      2      3      4      5      6      7      8
        +25H-08+24K-08+25F-08+24M-08+25D-08+24P-08+25B-08+25A-08+                                                            
        :  0  0:  0  0:  0  0:  0  0:  0  0:  0  0:  0  0:  0  0:                                                            
        :  0.00: 28.52:  0.00: 28.21:  0.00: 25.45:  0.00:  0.00:  08                                                        
   1    :  0.00:  0.00:  0.00:  0.00:  0.00:  0.00:  0.00:  0.00:                                                            
        +24H-06+24N-05+24K-06+24N-03+24P-07+25C-09+24J-07+25A-09+                                                            
        :  0  0:  0  0:  0  0:  0  0:  0  0:  0  0: 18 18:  0  0:                                                            
        : 28.52: 28.58: 45.43: 13.06: 25.76:  0.00: 27.98:  0.00:  09                                                        
   2    :  0.00:  0.00:  0.00:  0.00:  0.00:  0.00: 27.13:  0.00:                                                            
        +25H-10+24L-04+25F-10+24L-07+25D-10+24N-04+25B-10+25A-10+                                                            
        :  0  0:  0  0:  0  0: 21 21:  0  0: 21 21:  0  0:  0  0:                                                            
        :  0.00: 48.52:  0.00: 27.90:  0.00: 23.78:  0.00:  0.00:  10                                                        
   3    :  0.00:  0.00:  0.00: 27.01:  0.00: 23.09:  0.00:  0.00:                                                            
        +24H-04+24L-05+24J-05+25E-11+24K-04+24N-07+24P-05+25A-11+                                                            
        :  0  0: 21 21: 21 21:  0  0: 21 21:  0  0:  0  0:  0  0:                                                            
        : 28.21: 28.92: 27.90:  0.00: 28.46: 48.13: 23.10:  0.00:  11                                                        
   4    :  0.00: 28.01: 27.02:  0.00: 27.56:  0.00:  0.00:  0.00:                                                            
        +25H-12+24J-02+25F-12+24M-06+25D-12+24P-06+25B-12+------+                                                            
        :  0  0:  0  0:  0  0: 21 21:  0  0:  0  0:  0  0:      :                                                            
        :  0.00: 25.77:  0.00: 28.48:  0.00: 25.64:  0.00:      :  12                                                        
   5    :  0.00:  0.00:  0.00: 27.58:  0.00:  0.00:  0.00:      :                                                            
        +24H-02+25G-13+24M-03+24J-03+24K-02+25C-13+25B-13+------+                                                            
        :  0  0:  0  0: 21 21:  0  0:  0  0:  0  0:  0  0:      :                                                            
        : 25.45:  0.00: 23.65: 48.13: 25.62:  0.00:  0.00:      :  13                                                        
   6    :  0.00:  0.00: 22.97:  0.00:  0.00:  0.00:  0.00:      :                                                            
        +25H-14+24L-03+25F-14+24L-02+25D-14+25C-14+------+------+                                                            
        :  0  0:  0  0:  0  0:  0  0:  0  0:  0  0:      :      :                                                            
        :  0.00: 28.50:  0.00: 23.05:  0.00:  0.00:      :      :  14                                                        
   7    :  0.00:  0.00:  0.00:  0.00:  0.00:  0.00:      :      :                                                            
        +25H-15+25G-15+25F-15+25E-15+------+------+------+FUE.LAB                                                            
        :  0  0:  0  0:  0  0:  0  0:      :      :      :BAP BAO                                                            
        :  0.00:  0.00:  0.00:  0.00:      :      :      :EXP      15                                                        
   8    :  0.00:  0.00:  0.00:  0.00:      :      :      :EBP                                                                
        +------+------+------+------+------+------+------+------+
            H-     G-     F-     E-     D-     C-     B-     A-
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   37
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

 _____________________________________________________________________________________________________________________________


 ERR.CHK - SYMROT - Expand to full core to check FUE.SUB/ROT for requested quarter core rotational symmetry              

 ERR.CHK - SYMROT - Core Layout Passes Symmetry Check on Full Core FUE.SUB/ROT

1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   38
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

 Option Summary
 -------------------------------------------------- File Information ------------------------------------------------------
 Model                                       Status                    Related Input Card                Defined
 -----                                       ------                    ------------------                -------
 Error Checking                              On                        ERR.CHK                           Restart File            
 Cross Sections                              CMS-Link Library          LIB                               User Input  
 Read  Restart                               Read Restart File         RES                               User Input              
 Read  Restart with Labels                   No                        RES.LAB                           Default                 
 Write Restart File                          No                        WRE                               Default                 
 Write Restart File with Labels              No                        WRE.LAB                           Default                 
 Write ASCII Restart File                    No                        WRI                               Default                 
 Write SUMMARY File                          Yes                       PRI.SUM                           Default                 
 PIN FILE                                    On                        PIN.FIL                           Default                 
 Write KINETICS File                         Off                       KIN.EDT                           Default                 
 Write CMS-View Data File                    OFF                       CMS.EDT                           Restart File            

 -------------------------------------------------- Calculation Information -----------------------------------------------
 Model                                       Status                    Related Input Card                Defined
 -----                                       ------                    ------------------                -------
 Depletion Units                             EFPD                      DEP.UNI                           Restart File            
 Depletion Power Shape                       Ave                       DEP.STA                           User Input              
 Fission Product Option                      EQ. I,XE, DEPL. PM,SM     DEP.FPD (Set by DEP.STA)          Restart File            
 Soluble Boron Depletion Model               Off                       DEP.BOR                           Default                 
 Soluble Boron Search                        Boron Search Active       ITE.BOR                           User Input              
 Eigenvalue Calculation                      No K-effective Calc.      ITE.KEF                           Overridden              
 Automated Searches                          Off                       ITE.SRC                           Default                 
 High Worth Rod Calc.                        Off                       ITE.HWR                           Default                 
 Neutronics Model                            2 Grp QPANDA -Conv. XS    ITE.SOL                           Restart File
 MOX Models                                  OFF                       ITE.MOX                           Restart File            
 Two-In-Row Convergence                      Off                       ITE.TWO                           Restart File            
 Speed Option                                On                        ITE.SPD                           Restart File            
   Speed Cross Sections                      On                        ITE.SPD                           Restart File            
   Speed Flux Iterations                     On                        ITE.SPD                           Restart File            
   Alternate Coupling Coefficients           On                        ITE.SPD                           Restart File            
 Perform Audit of Data Library               Off                       AUD.PRI                           Default                 
 Input Scan Only                             No                        STA                               Default                 
 Adjoint Calculation                         Off                       KIN.EDT                           Default                 
 Reactivity Perturb. Calc.                   Off                       PRT.COE                           Default                 
 Hydraulic Iteration Option                  On                        HYD.ITE/COR.OPE/COR.MWT           Restart File            
 Fuel Temperature Fit                        On                        SEG.TFU                           Restart File            
 Inlet Temperature Versus Power              On                        PWR.OPT                           Restart File            
 Steam Table Option                          Tabled ASME               COR.STM                           Restart File            
 Perturb Base Conditions                     Off                       SAV.BAS                           Default                 
 Use Perturbed Base Conditions               Off                       USE.BAS                           Default                 
 Grid Model by Mech. Type                    On                        GRD.EDT                           Restart File            
 Grid Model by Segment                       Off                       FUE.GRD/SEG.GRD                   Restart File            
 Control Rod Cusping Model                   On                        CRD.CSP                           Restart File            
 Automated Control Rod Calc.                 None                      CRD.ICB/OCB/TRP/                  Default     
 CRD.SET                                     Inactive                  CRD.SET                           Default                 
 Shutdown Cooling Model                      On                        FUE.SDC                           Default                 
 Print Pin Power Reconstruction Data         On                        PIN.EDT                           Restart File            
 Pin Loadings in Exposure Calculation        Off                       PIN.LOA                           Restart File            
 Integrated Pin Exposure Model               Off                       PIN.IPE                           Restart File            
 Print Statepoint Parameters                 On                        PRI.STA                           Restart File            
 Print Isotopics                             Off                       PRI.ISO                           Default                 
 Print File Trail Data                       Off                       PRI.FTD                           Restart File            
 Batch Edits                                 On                        BAT.EDT                           Restart File            

 Pinfile File Summary
 --------------------
 Pinfile Filename: PINFILE
 Pinfile Edit Options: ON                  


 Memory required was 4588030 / 30000000 ( 15.29 % )

1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   39
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

 Case   1  --- Entering Neutronics Iterations ---
 ** Note    -   PINFIL   - PIN.FIL - PIN.FIL ON --Pin edits will reflect axial integration
 ** Note    -   FLATPOW  - ITE.LIM - Flat power distribution used as initial guess
 ** Note    -   ITEBOR   - ITE.BOR - Critical boron search requested by input
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   40
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

                                   Fuel Loading Statistics (at end of last cycle in core)

               Cycle     Cycle  Assemblies   Ave Exposure (GWd/T)  Discharge Date  Loading Date 
               c1c25      25         97             0.00            2018/12/27     2018/12/27*
               c1c24      24         96            29.23            2018/11/29*    2017/05/27*
               _________________________________________________________________________________
                         *Input (DEP.CYC/DEP.DAT)
   

 I / J =    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :Serial:      :      :      :      :      :      :      :      :      :      :      :      :      :      :           
   1    :Exp   :      :      :      : New  : New  : New  : New  : New  : New  : New  :      :      :      :      :  01       
        :Cycle :      :      :      :      :      :      :      :      :      :      :      :      :      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :      :      :      :24E-14:      :24E-13:      :24L-13:      :24L-14:      :      :      :      :           
   2    :      :      : New  : New  : 22.80: New  : 28.21: New  : 28.29: New  : 22.85: New  : New  :      :      :  02       
        :      :      :      :      :  24  :      :  24  :      :  24  :      :  24  :      :      :      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :      :      :24F-14:24G-13:24D-13:      :24H-14:      :24M-13:24J-13:24K-14:      :      :      :           
   3    :      : New  : New  : 25.35: 47.65: 23.41: New  : 25.19: New  : 23.53: 47.65: 25.37: New  : New  :      :  03       
        :      :      :      :  24  :  24  :  24  :      :  24  :      :  24  :  24  :  24  :      :      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :      :24B-10:      :24D-10:      :24G-14:      :24J-14:      :24M-10:      :24P-10:      :      :           
   4    :      : New  : 25.37: New  : 28.20: New  : 25.51: New  : 25.49: New  : 28.18: New  : 25.35: New  :      :  04       
        :      :      :  24  :      :  24  :      :  24  :      :  24  :      :  24  :      :  24  :      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :24B-11:24C-09:24F-12:      :24G-11:24E-11:24H-12:24L-11:24J-11:      :24K-12:24N-09:24P-11:      :           
   5    : New  : 22.85: 47.65: 28.18: New  : 27.64: 28.64: 27.93: 28.64: 27.63: New  : 28.20: 47.65: 22.80: New  :  05       
        :      :  24  :  24  :  24  :      :  24  :  24  :  24  :  24  :  24  :      :  24  :  24  :  24  :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :      :24C-12:      :24E-09:      :24E-12:      :24L-12:      :24L-09:      :24N-12:      :      :           
   6    : New  : New  : 23.53: New  : 27.63: New  : 48.03: New  : 48.09: New  : 27.64: New  : 23.41: New  : New  :  06       
        :      :      :  24  :      :  24  :      :  24  :      :  24  :      :  24  :      :  24  :      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :24G-09:      :24B-09:24C-13:24F-10:24C-11:24H-10:24N-11:24K-10:24N-13:24P-09:      :24J-09:      :           
   7    : New  : 27.71: New  : 25.49: 12.96: 44.97: 28.29: 28.25: 28.21: 44.97: 12.96: 25.51: New  : 27.71: New  :  07       
        :      :  24  :      :  24  :  24  :  24  :  24  :  24  :  24  :  24  :  24  :  24  :      :  24  :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
            R-     P-     N-     M-     L-     K-     J-     H-     G-     F-     E-     D-     C-     B-     A-
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   41
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    
   

 I / J =    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :      :24B-08:      :24D-08:      :24F-08:      :24K-08:      :24M-08:      :24P-08:      :      :           
   8    : New  : New  : 25.19: New  : 27.93: New  : 28.25: New  : 28.25: New  : 27.93: New  : 25.19: New  : New  :  08       
        :      :      :  24  :      :  24  :      :  24  :      :  24  :      :  24  :      :  24  :      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :24G-07:      :24B-07:24C-03:24F-06:24C-05:24H-06:24N-05:24K-06:24N-03:24P-07:      :24J-07:      :           
   9    : New  : 27.71: New  : 25.51: 12.96: 44.97: 28.21: 28.25: 28.29: 44.97: 12.96: 25.49: New  : 27.71: New  :  09       
        :      :  24  :      :  24  :  24  :  24  :  24  :  24  :  24  :  24  :  24  :  24  :      :  24  :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :      :24C-04:      :24E-07:      :24E-04:      :24L-04:      :24L-07:      :24N-04:      :      :           
  10    : New  : New  : 23.41: New  : 27.64: New  : 48.09: New  : 48.03: New  : 27.63: New  : 23.53: New  : New  :  10       
        :      :      :  24  :      :  24  :      :  24  :      :  24  :      :  24  :      :  24  :      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :24B-05:24C-07:24F-04:      :24G-05:24E-05:24H-04:24L-05:24J-05:      :24K-04:24N-07:24P-05:      :           
  11    : New  : 22.80: 47.65: 28.20: New  : 27.63: 28.64: 27.93: 28.64: 27.64: New  : 28.18: 47.65: 22.85: New  :  11       
        :      :  24  :  24  :  24  :      :  24  :  24  :  24  :  24  :  24  :      :  24  :  24  :  24  :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :      :24B-06:      :24D-06:      :24G-02:      :24J-02:      :24M-06:      :24P-06:      :      :           
  12    :      : New  : 25.35: New  : 28.18: New  : 25.49: New  : 25.51: New  : 28.20: New  : 25.37: New  :      :  12       
        :      :      :  24  :      :  24  :      :  24  :      :  24  :      :  24  :      :  24  :      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :      :      :24F-02:24G-03:24D-03:      :24H-02:      :24M-03:24J-03:24K-02:      :      :      :           
  13    :      : New  : New  : 25.37: 47.65: 23.53: New  : 25.19: New  : 23.41: 47.65: 25.35: New  : New  :      :  13       
        :      :      :      :  24  :  24  :  24  :      :  24  :      :  24  :  24  :  24  :      :      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :      :      :      :      :24E-02:      :24E-03:      :24L-03:      :24L-02:      :      :      :      :           
  14    :      :      : New  : New  : 22.85: New  : 28.29: New  : 28.21: New  : 22.80: New  : New  :      :      :  14       
        :      :      :      :      :  24  :      :  24  :      :  24  :      :  24  :      :      :      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
        :Serial:      :      :      :      :      :      :      :      :      :      :      :      :      :      :           
  15    :Exp   :      :      :      : New  : New  : New  : New  : New  : New  : New  :      :      :      :      :  15       
        :Cycle :      :      :      :      :      :      :      :      :      :      :      :      :      :      :           
        +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+           
            R-     P-     N-     M-     L-     K-     J-     H-     G-     F-     E-     D-     C-     B-     A-
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   42
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    
 _____________________________________________________________________________________________________________________________

 Depletion Statepoint List =      0.000    25.000    50.000    75.000   100.000   125.000   150.000   200.000   250.000
                                300.000   350.000   400.000   450.000   500.000   550.000   590.000

 Cycle exposure at end of this step = 0.0000 EFPD                                                    

 Depletion step used = 0.000* (0.5 beginning of step power + 0.5 end of step power)                  


 Active Iteration Levels :          ITE.LIM :       Calculation Options :
 Use Flat Power Initial Guess. . . . . . . .T       Fix Independent Variables . . .F
 Hydraulic Iteration . . . . . . . .NHMAX  40       Use Cross Section Library . . .T
 Source Iterations . . . . . . . . .NSMAX   4       Discontinuity Factors . . . . .T
 Inner Flux Iterations . . . . . . .NSIMAX  8       Isothermal Hydraulic Option . .F
 Coupling Coefficient Iterations . .NQMAX  40       Boron Search Option . . . . . .T
 Convergence - Flux   5.0E-04  Keff   5.0E-05       Pin Reconstruction Option . . .T
 Initial Keff 1.00004         Shift   3.0E-02       Neutronic Option (ITE.SOL). . .0


 _____________________________________________________________________________________________________________________________

 PIN.PIN - Cross Section Library Includes Pin Power Reconstruction Data

     Segment #     44 Data extracted for SIMNAM sil495p0a0           Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     43 Data extracted for SIMNAM RFA205000I0.00XNOBP  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     27 Data extracted for SIMNAM RFA455000I0.00XNOBP  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     29 Data extracted for SIMNAM RFA455104I1.50XNOBP  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     28 Data extracted for SIMNAM RFA455156I1.50XNOBP  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     30 Data extracted for SIMNAM RFA485000I0.00XNOBP  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     23 Data extracted for SIMNAM RFA495000I0.00XNOBP  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     34 Data extracted for SIMNAM RFA495000I0.00XNOBP  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     37 Data extracted for SIMNAM RFA495048I1.50XNOBP  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     38 Data extracted for SIMNAM RFA495080I1.50XNOBP  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     26 Data extracted for SIMNAM RFA495104I1.50XNOBP  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     39 Data extracted for SIMNAM RFA495104I1.50XNOBP  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     24 Data extracted for SIMNAM RFA495064I1.50XNOBP  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #      4 Data extracted for SIMNAM RFA260BLANK          Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #      5 Data extracted for SIMNAM RFA260BOTBK          Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     31 Data extracted for SIMNAM RFA485000I0.00XW24A  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     32 Data extracted for SIMNAM RFA485064I1.50XW24A  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     33 Data extracted for SIMNAM RFA485080I1.50XW24A  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   43
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

     Segment #     35 Data extracted for SIMNAM RFA495000I0.00XW20A  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     40 Data extracted for SIMNAM RFA495048I1.50XW20A  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     36 Data extracted for SIMNAM RFA495000I0.00XW24A  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     41 Data extracted for SIMNAM RFA495016I1.50XW24A  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #     42 Data extracted for SIMNAM RFA495080I1.50XW24A  Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #      2 Data extracted for SIMNAM CATRFA310RADREF      Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #      3 Data extracted for SIMNAM RFATOPREFL           Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     Segment #      1 Data extracted for SIMNAM RFABOTREFL           Major = 0
                      Detector data available for detector isotopes:   92235.  99999.  45103.

     The run-time pin library size (in words) =   524928

 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   44
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    


 Variables Present on XS Library for Active Segments
 NSEG     SEG.LIB     Depletion Arguments

    1     RFABOTREFL                                                      TMO         BOR 
    2     CATRFA310RADREF                                                 TMO         BOR 
    3     RFATOPREFL                                                      TMO         BOR 
    4     RFA260BLANK           XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
    5     RFA260BOTBK           XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
   23     RFA495000I0.00XNOBP   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
   24     RFA495064I1.50XNOBP   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
   26     RFA495104I1.50XNOBP   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
   27     RFA455000I0.00XNOBP   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
   28     RFA455156I1.50XNOBP   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
   29     RFA455104I1.50XNOBP   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
   30     RFA485000I0.00XNOBP   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
   31     RFA485000I0.00XW24A   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO                                     
                                            SDC 
   32     RFA485064I1.50XW24A   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO                                     
                                            SDC 
   33     RFA485080I1.50XW24A   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO                                     
                                            SDC 
   34     RFA495000I0.00XNOBP   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
   35     RFA495000I0.00XW20A   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO                                     
                                            SDC 
   36     RFA495000I0.00XW24A   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO                                     
                                            SDC 
   37     RFA495048I1.50XNOBP   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
   38     RFA495080I1.50XNOBP   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
   39     RFA495104I1.50XNOBP   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
   40     RFA495048I1.50XW20A   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO                                     
                                            SDC 
   41     RFA495016I1.50XW24A   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO                                     
                                            SDC 
   42     RFA495080I1.50XW24A   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO                                     
                                            SDC 
   43     RFA205000I0.00XNOBP   XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO   CRD                               
                                            SDC 
   44     sil495p0a0            XEN   SAM   EXP               TFU   HTF   TMO   HTM   BOR   HBO                                     
                                            SDC 
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   45
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     1500.00 ppm    0.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =     0.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    13.024 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron

           1.0205649      This problem is not converging as expected --Returning to flat flux for next iteration
                          Possibly: Initial XKEFF too low, boron concentration too low or,
                          SHIFTK  3.00000E-02 too small, (Try SHIFTK > SHIFTK+.02)
     1  1  1.0572902    -0.05729023     1.0000E+00     9.6324E-01     2.89078    -0.077    2100.00    
     2  2  1.0301332    -0.03013325     1.0131E-01     5.8376E-03     2.62486    -0.028    2700.00    
     3  3  0.9999593     0.00004069 *   4.1348E-02     3.3827E-03     2.52064     0.030    3055.16    BOR Abnormally high
     4  4  0.9993855     0.00061455     7.5669E-03     1.2573E-04 *   2.53986     0.030    2860.98    BOR Abnormally high
     5  5  1.0001906    -0.00019062     2.6284E-06 *   1.0546E-03     2.53986     0.016    2879.35    BOR Abnormally high
     6  6  0.9999808     0.00001920 *   2.1590E-03     1.9364E-04 *   2.53439     0.020    2881.21    BOR Abnormally high
     7  7  0.9999724 *   0.00002757 *   1.5265E-03     1.0739E-04 *   2.53827     0.018    2873.95    BOR Abnormally high
     8  8  0.9999982 *   0.00000184 *   4.4928E-04 *   4.3347E-05 *   2.53713     0.017    2875.58    BOR Abnormally high
     9  9  1.0000040 *  -0.00000399 *   1.2544E-04 *   2.2214E-05 *   2.53745     0.017    2874.10 *  BOR Abnormally high

    Nodal Solution Time = 0.81 Sec

 Boron (ppm)  = 2874.105
 Axial Offset =    0.017
 Max. Node-Averaged  Peaking Factor = 2.537 in Node ( 8,14, 11)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  0.599  0.429  0.804  0.858  1.743  1.454  1.968  1.775   08
  9  0.429  0.455  0.586  0.661  1.259  1.925  1.394  1.687   09
 10  0.804  0.569  1.205  0.990  1.589  1.276  1.720  1.540   10
 11  0.858  0.906  1.015  1.462  1.021  0.775  1.008  1.213   11
 12  1.743  1.272  1.612  1.031  1.356  0.898  0.963          12
 13  1.454  1.901  1.259  0.769  0.898  1.154  0.934          13
 14  1.968  1.336  1.668  0.987  0.958  0.931                 14
 15  1.775  1.658  1.486  1.180                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8   9,10   9,16  15,13  16,12  15,13  13,14  13, 4  13, 3   08
  9   9,16  17,17  17,17  17,17   5,15   4,13   3, 5   1, 1   09
 10  15,13  17,17  17,17  15,13   5,14   3,13   4, 5   5, 3   10
 11  16,12  17,17  15,13  14, 5   5, 3   1,17   2, 6   1, 1   11
 12  15,13  15, 5  14, 5   3, 5   5, 4  13, 3  13, 3          12
 13  13,14  13, 4   5, 3   1, 1   3,13   4, 5   1, 1          13
 14  13, 4   5, 3   5, 4   6, 2   3,13   1, 1                 14
 15  13, 3   1, 1   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  0.000 29.630  0.000 29.500  0.000 28.350  0.000  0.000   08
  9 29.630 30.488 51.726 17.997 28.923  0.000 29.070  0.000   09
 10  0.000 53.365  0.000 28.991  0.000 29.546  0.000  0.000   10
 11 29.500 30.172 28.993  0.000 29.867 52.123 29.013  0.000   11
 12  0.000 28.924  0.000 29.889  0.000 29.202  0.000          12
 13 28.350  0.000 29.378 52.124 29.175  0.000  0.000          13
 14  0.000 30.420  0.000 28.963  0.000  0.000                 14
 15  0.000  0.000  0.000  0.000                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   46
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     2874.10 ppm    0.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  0.000 28.555 28.499  0.000  0.000 27.925 28.485  0.000  0.000 23.715 27.206  0.000  0.000  0.000  0.000    1
  2 28.548 27.414 28.899 49.267 45.390  9.582 13.002 23.916 27.711  0.000  0.000 28.131 28.115  0.000  0.000    2
  3 28.496 28.641 29.378 45.383 41.685 13.081 16.585 23.899 27.502  0.000  0.000 28.111 27.581  0.000  0.000    3
  4  0.000 47.325 51.318  0.000  0.000 28.108 27.929  0.000  0.000 20.353 24.264  0.000  0.000  0.000  0.000    4
  5  0.000 45.561 49.869  0.000  0.000 27.771 27.779  0.000  0.000 23.855 26.630  0.000  0.000  0.000  0.000    5
  6 27.932 28.983 29.012 28.109 27.777  0.000  0.000 28.594 27.862 50.036 46.828 18.467 24.681  0.000  0.000    6
  7 28.487 29.024 28.659 27.939 27.795  0.000  0.000 28.935 28.430 49.529 46.122 21.886 27.349  0.000  0.000    7
  8  0.000 23.936 23.940  0.000  0.000 28.626 28.956  0.000  0.000 23.171 27.806  0.000  0.000                  8
  9  0.000 27.713 27.509  0.000  0.000 27.879 28.441  0.000  0.000 23.732 27.833  0.000  0.000                  9
 10 23.689  0.000  0.000 20.228 23.740 50.038 49.524 23.153 23.732  0.000  0.000  0.000  0.000                 10
 11 27.204  0.000  0.000 24.166 26.474 46.852 46.125 27.780 27.824  0.000  0.000  0.000  0.000                 11
 12  0.000 27.327 28.585  0.000  0.000 18.417 21.850  0.000  0.000  0.000  0.000                               12
 13  0.000 28.782 29.312  0.000  0.000 24.614 27.303  0.000  0.000  0.000  0.000                               13
 14  0.000  0.000  0.000  0.000  0.000  0.000  0.000                                                           14
 15  0.000  0.000  0.000  0.000  0.000  0.000  0.000                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  0.562  0.378  0.398  0.652  0.737  0.601  0.759  1.381  1.631  1.364  1.386  1.879  1.834  1.620  1.128    1
  2  0.378  0.372  0.382  0.372  0.440  0.464  0.576  1.000  1.177  1.766  1.835  1.328  1.272  1.546  1.095    2
  3  0.400  0.385  0.402  0.418  0.496  0.485  0.580  0.994  1.149  1.709  1.764  1.267  1.216  1.475  1.044    3
  4  0.660  0.390  0.409  0.820  0.937  0.736  0.843  1.363  1.505  1.199  1.197  1.626  1.594  1.408  0.975    4
  5  0.754  0.462  0.488  0.960  1.107  0.867  0.938  1.385  1.394  1.004  0.995  1.432  1.434  1.257  0.859    5
  6  0.612  0.621  0.664  0.769  0.884  1.339  1.380  0.977  0.897  0.681  0.687  0.921  0.877  1.006  0.685    6
  7  0.772  0.778  0.808  0.885  0.963  1.391  1.386  0.952  0.848  0.625  0.603  0.746  0.663  0.708  0.451    7
  8  1.401  1.034  1.037  1.414  1.415  0.986  0.954  1.287  1.205  0.813  0.712  0.904  0.727                  8
  9  1.640  1.195  1.171  1.531  1.411  0.904  0.850  1.205  1.199  0.849  0.747  0.890  0.638                  9
 10  1.363  1.764  1.707  1.200  1.005  0.681  0.624  0.812  0.848  1.093  1.007  0.833  0.548                 10
 11  1.379  1.813  1.735  1.178  0.982  0.679  0.598  0.708  0.745  1.006  0.890  0.655  0.388                 11
 12  1.865  1.278  1.194  1.581  1.399  0.905  0.736  0.896  0.885  0.830  0.654                               12
 13  1.818  1.202  1.125  1.540  1.393  0.857  0.652  0.719  0.634  0.545  0.387                               13
 14  1.605  1.507  1.424  1.360  1.220  0.981  0.693                                                           14
 15  1.118  1.070  1.011  0.943  0.833  0.666  0.440                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 2.675 in Node ( 8,14,12)

    Pin Power Reconstruction Time = 0.38 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  0.562  0.388  0.701  0.686  1.513  1.373  1.849  1.368   08
  9  0.388  0.385  0.432  0.526  1.080  1.768  1.271  1.290   09
 10  0.701  0.437  0.956  0.846  1.412  1.099  1.521  1.125   10
 11  0.686  0.718  0.875  1.374  0.918  0.649  0.802  0.713   11
 12  1.513  1.109  1.443  0.924  1.224  0.780  0.790          12
 13  1.373  1.755  1.091  0.645  0.778  0.999  0.606          13
 14  1.849  1.200  1.478  0.787  0.784  0.604                 14
 15  1.368  1.253  1.089  0.695                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.142  0.909  1.149  0.905  1.136  0.934  1.132  1.138   08
  9  0.909  0.908  0.829  0.799  0.936  1.133  0.923  1.139   09
 10  1.149  0.815  1.144  0.925  1.137  0.947  1.136  1.142   10
 11  0.905  0.922  0.925  1.138  0.926  0.812  0.956  1.149   11
 12  1.136  0.935  1.137  0.926  1.130  0.941  1.147          12
 13  0.934  1.133  0.948  0.812  0.941  1.143  1.151          13
 14  1.132  0.896  1.136  0.956  1.147  1.151                 14
 15  1.138  1.140  1.142  1.149                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   47
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     2874.10 ppm    0.000 EFPD    

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8  0.000 28.524  0.000 28.207  0.000 25.454  0.000  0.000   08
  9 28.524 28.583 45.431 13.063 25.757  0.000 27.985  0.000   09
 10  0.000 48.518  0.000 27.897  0.000 23.776  0.000  0.000   10
 11 28.207 28.920 27.905  0.000 28.455 48.129 23.096  0.000   11
 12  0.000 25.774  0.000 28.476  0.000 25.636  0.000          12
 13 25.454  0.000 23.652 48.135 25.622  0.000  0.000          13
 14  0.000 28.501  0.000 23.046  0.000  0.000                 14
 15  0.000  0.000  0.000  0.000                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  27.13   0.00   09
 10   0.00   0.00   0.00  27.01   0.00  23.09   0.00   0.00   10
 11   0.00  28.01  27.02   0.00  27.56   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  27.58   0.00   0.00   0.00          12
 13   0.00   0.00  22.97   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   48
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     2874.10 ppm    0.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97             0.00 25H-08 sil48  ( 8, 8,  )       0.00  25H-08  sil48  ( 8, 8,  )         0.00
        6    26A        16            28.58 24N-05 24N-05 ( 9, 9,  )      28.21  24H-04  24H-04 (11, 8,  )        28.45
        8    26C        48            28.48 24M-06 24M-06 (12,11,  )      23.05  24L-02  24L-02 (14,11,  )        25.56
        5    25C        16            48.52 24L-04 24L-04 (10, 9,  )      45.43  24K-06  24K-06 ( 9,10,  )        47.55
        9    26D         4            13.06 24N-03 24N-03 ( 9,11,  )      13.06  24N-03  24N-03 ( 9,11,  )        13.06
        7    26B        12            28.92 24L-05 24L-05 (11, 9,  )      27.90  24L-07  24L-07 (10,11,  )        28.24

     CORE              193            48.52 24L-04 24L-04 (10, 9,  )       0.00  25H-08  sil48  ( 8, 8,  )        13.02


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97             0.00 25H-08 sil48  ( 1, 1,  )
        6    26A        16            30.49 24N-05 24N-05 ( 3, 3,  )
        8    26C        48            29.89 24M-06 24M-06 ( 8, 7,  )
        5    25C        16            53.37 24L-04 24L-04 ( 4, 3,  )
        9    26D         4            18.00 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            30.17 24L-05 24L-05 ( 6, 2,  )

     CORE              193            53.37 24L-04 24L-04 ( 4, 3,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   49
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     2874.10 ppm    0.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  13.024 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp.   0.0 EFPD     0.0 EFPH   0.000 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  0.000E+00 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  0.000 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  6.631E-05  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     2.537 ( 8,14,11)
       Fuel    Average . . .TFUAVE  877.31 K      1119.48 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  580.31 K       584.90 F                  F-delta-H       1.968 ( 8,14)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         2.139 (14, 8, 4)   [Off]
 Boron Conc. . . . . . . . . . BOR  2874.1 ppm                               Max-3PIN        2.660 ( 8,14,11)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        2.675 ( 8,14,12)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O   0.017                                 Total Neutron Leakage . . . . . . . . .  0.039
 Edit Ring -     1      2      3      4      5
 RPF            0.406  0.637  1.239  1.084  0.654
 KIN            0.935  0.937  1.014  1.045  1.150
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.733  0.724  0.698  0.704  0.723
 TFU (Deg K)    672.0  753.8  947.0  909.3  787.6
 TMO (Deg K)    569.7  573.9  584.3  581.9  574.6
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.16722   0.87336   6.60264   0.02288   0.66571   646.889   597.031
  23   0.51512   1.07832   9.74102   0.00000   0.66692   744.276   596.628
  22   0.73361   1.05848  11.43890   0.00000   0.66920   808.206   595.874
  21   0.91662   1.04909  12.75295   0.00000   0.67226   863.582   594.851
  20   1.04029   1.04261  13.31150   0.00000   0.67599   900.924   593.593
  19   1.12760   1.03829  13.53614   0.00000   0.68009   926.866   592.176
 
  18   1.20130   1.03698  13.76731   0.00000   0.68443   948.540   590.629
  17   1.25176   1.03494  13.86980   0.00000   0.68894   962.641   588.973
  16   1.29195   1.03399  13.98473   0.00000   0.69354   973.443   587.227
  15   1.30015   1.03201  13.85252   0.00000   0.69817   973.880   585.421
  14   1.32708   1.03220  14.06265   0.00000   0.70279   980.544   583.561
  13   1.33129   1.03086  14.10229   0.00000   0.70740   979.868   581.652
 
  12   1.31427   1.02975  13.99695   0.00000   0.71192   972.448   579.725
  11   1.31373   1.03033  14.19353   0.00000   0.71635   970.522   577.785
  10   1.29338   1.02977  14.26924   0.00000   0.72068   962.336   575.836
   9   1.25847   1.02972  14.27913   0.00000   0.72487   949.652   573.905
   8   1.20488   1.02849  14.18992   0.00000   0.72885   931.164   572.019
   7   1.16130   1.02957  14.39366   0.00000   0.73264   916.223   570.190
 
   6   1.08795   1.03037  14.37222   0.00000   0.73619   891.973   568.435
   5   0.98030   1.03170  14.03154   0.00000   0.73943   857.027   566.806
   4   0.86630   1.03695  13.77999   0.00000   0.74229   820.643   565.342
   3   0.69904   1.04635  12.77589   0.00000   0.74469   768.276   564.093
   2   0.47544   1.06546  10.73113   0.00000   0.74648   700.421   563.150
   1   0.14095   0.83814   6.51653   0.00000   0.74741   605.019   562.653
 
 Ave   1.00000   1.02365  13.02366   0.00095   0.70822   877.307   580.315
 P**2                      8.08692
 A-O   0.01700   0.00466  -0.02109   1.00000  -0.03450     0.017     0.018
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   50
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 c1c24 HFP NOMINAL DEPLETION                                                     2874.10 ppm    0.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  1.19035E-09  9.56722E-10  3.90718E-09  7.79071E-09  6.60264E+00  6.76334E-01  2.90205E+01  6.00844E+02  0.00000E+00
  23  3.66127E-09  3.01202E-09  7.43221E-09  2.06765E-08  9.74102E+00  6.83295E-01  2.89801E+01  5.15478E+02  3.01908E+00
  22  5.21599E-09  3.68850E-09  8.70324E-09  2.20629E-08  1.14389E+01  6.85378E-01  2.93756E+01  5.14936E+02  3.46709E+00
  21  6.51805E-09  4.03481E-09  9.40691E-09  2.15409E-08  1.27529E+01  6.87594E-01  2.96500E+01  5.21073E+02  3.87563E+00
  20  7.39756E-09  4.23787E-09  9.44112E-09  2.14492E-08  1.33115E+01  6.89917E-01  2.97206E+01  5.26738E+02  4.05620E+00
  19  8.01784E-09  4.36785E-09  9.24942E-09  2.14713E-08  1.35361E+01  6.92249E-01  2.97097E+01  5.31481E+02  4.13061E+00
 
  18  8.54156E-09  4.43544E-09  9.10688E-09  2.11536E-08  1.37673E+01  6.94566E-01  2.97102E+01  5.35507E+02  4.20691E+00
  17  8.89927E-09  4.47985E-09  8.92323E-09  2.10017E-08  1.38698E+01  6.96865E-01  2.96876E+01  5.38538E+02  4.24112E+00
  16  9.18416E-09  4.49784E-09  8.78602E-09  2.07746E-08  1.39847E+01  6.99142E-01  2.96764E+01  5.40915E+02  4.27915E+00
  15  9.24122E-09  4.52707E-09  8.52391E-09  2.10626E-08  1.38525E+01  7.01383E-01  2.96105E+01  5.42235E+02  4.23716E+00
  14  9.43194E-09  4.50197E-09  8.49740E-09  2.05862E-08  1.40626E+01  7.03592E-01  2.96364E+01  5.43632E+02  4.30508E+00
  13  9.46066E-09  4.48823E-09  8.37916E-09  2.05059E-08  1.41023E+01  7.05794E-01  2.96239E+01  5.44325E+02  4.31754E+00
 
  12  9.33871E-09  4.48019E-09  8.18634E-09  2.07294E-08  1.39970E+01  7.07964E-01  2.95795E+01  5.44370E+02  4.28315E+00
  11  9.33437E-09  4.42588E-09  8.18240E-09  2.02914E-08  1.41935E+01  7.10113E-01  2.96152E+01  5.44553E+02  4.34587E+00
  10  9.18899E-09  4.37809E-09  8.12357E-09  2.01415E-08  1.42692E+01  7.12260E-01  2.96260E+01  5.44046E+02  4.36931E+00
   9  8.94047E-09  4.32693E-09  8.04912E-09  2.01509E-08  1.42791E+01  7.14396E-01  2.96260E+01  5.42916E+02  4.37199E+00
   8  8.55882E-09  4.27287E-09  7.95116E-09  2.03777E-08  1.41899E+01  7.16514E-01  2.96086E+01  5.40935E+02  4.34204E+00
   7  8.24898E-09  4.16714E-09  8.05599E-09  1.99200E-08  1.43937E+01  7.18630E-01  2.96755E+01  5.38734E+02  4.40699E+00
 
   6  7.72748E-09  4.05540E-09  8.09169E-09  1.98551E-08  1.43722E+01  7.20753E-01  2.96961E+01  5.35240E+02  4.39897E+00
   5  6.96222E-09  3.92060E-09  8.01278E-09  2.02341E-08  1.40315E+01  7.22854E-01  2.96452E+01  5.30269E+02  4.28823E+00
   4  6.15197E-09  3.69664E-09  8.03513E-09  1.98689E-08  1.37800E+01  7.24925E-01  2.96254E+01  5.24910E+02  4.20827E+00
   3  4.96323E-09  3.36117E-09  7.59509E-09  1.98776E-08  1.27759E+01  7.26916E-01  2.93949E+01  5.18936E+02  3.89894E+00
   2  3.37327E-09  2.73563E-09  6.31240E-09  1.92077E-08  1.07311E+01  7.28700E-01  2.88499E+01  5.20930E+02  3.34796E+00
   1  1.00176E-09  8.48661E-10  2.88486E-09  7.95422E-09  6.51653E+00  7.24739E-01  2.86020E+01  6.14790E+02  0.00000E+00
 
 Ave   7.1063E-09   3.8291E-09   7.9099E-09   1.9529E-08   1.3024E+01   7.0604E-01   2.9498E+01   5.3985E+02   3.8666E+00
 A-O        0.017        0.028        0.057        0.024       -0.021       -0.018        0.001       -0.003       -0.024
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  6.44056E+00  6.68367E+00  6.71226E+00  6.47726E+00  9.74462E-01  9.71455E-01  9.75017E-01  9.75048E-01  9.71277E-01
  23  9.54533E+00  9.87276E+00  9.91022E+00  9.59161E+00  9.73454E-01  9.68238E-01  9.73141E-01  9.73122E-01  9.67994E-01
  22  1.12188E+01  1.15984E+01  1.16398E+01  1.12684E+01  1.00813E+00  1.00197E+00  1.00755E+00  1.00753E+00  1.00179E+00
  21  1.25166E+01  1.29387E+01  1.29837E+01  1.25696E+01  9.94457E-01  9.87734E-01  9.93078E-01  9.93067E-01  9.87629E-01
  20  1.30629E+01  1.35006E+01  1.35477E+01  1.31184E+01  9.97756E-01  9.91011E-01  9.96223E-01  9.96210E-01  9.90915E-01
  19  1.32788E+01  1.37220E+01  1.37702E+01  1.33359E+01  1.00440E+00  9.97798E-01  1.00290E+00  1.00288E+00  9.97692E-01
 
  18  1.35062E+01  1.39581E+01  1.40074E+01  1.35643E+01  9.98114E-01  9.91491E-01  9.96278E-01  9.96260E-01  9.91400E-01
  17  1.36046E+01  1.40609E+01  1.41109E+01  1.36636E+01  9.97555E-01  9.91002E-01  9.95549E-01  9.95530E-01  9.90911E-01
  16  1.37165E+01  1.41782E+01  1.42289E+01  1.37762E+01  9.93762E-01  9.87266E-01  9.91546E-01  9.91526E-01  9.87178E-01
  15  1.35781E+01  1.40336E+01  1.40844E+01  1.36386E+01  1.00861E+00  1.00245E+00  1.00670E+00  1.00667E+00  1.00233E+00
  14  1.37881E+01  1.42541E+01  1.43059E+01  1.38492E+01  9.95148E-01  9.88873E-01  9.92726E-01  9.92701E-01  9.88776E-01
  13  1.38250E+01  1.42933E+01  1.43456E+01  1.38868E+01  9.96049E-01  9.89879E-01  9.93524E-01  9.93495E-01  9.89780E-01
 
  12  1.37152E+01  1.41783E+01  1.42308E+01  1.37777E+01  1.00781E+00  1.00194E+00  1.00552E+00  1.00548E+00  1.00181E+00
  11  1.39127E+01  1.43855E+01  1.44390E+01  1.39759E+01  9.94950E-01  9.88935E-01  9.92169E-01  9.92136E-01  9.88832E-01
  10  1.39876E+01  1.44643E+01  1.45184E+01  1.40515E+01  9.92562E-01  9.86561E-01  9.89576E-01  9.89544E-01  9.86463E-01
   9  1.39954E+01  1.44723E+01  1.45269E+01  1.40599E+01  9.95797E-01  9.89941E-01  9.92819E-01  9.92782E-01  9.89832E-01
   8  1.39030E+01  1.43753E+01  1.44300E+01  1.39681E+01  1.00711E+00  1.00152E+00  1.00434E+00  1.00430E+00  1.00138E+00
   7  1.41094E+01  1.45927E+01  1.46484E+01  1.41751E+01  9.92849E-01  9.87060E-01  9.89554E-01  9.89515E-01  9.86956E-01
 
   6  1.40884E+01  1.45725E+01  1.46285E+01  1.41544E+01  9.92906E-01  9.87197E-01  9.89520E-01  9.89478E-01  9.87090E-01
   5  1.37464E+01  1.42180E+01  1.42734E+01  1.38124E+01  1.00898E+00  1.00369E+00  1.00601E+00  1.00596E+00  1.00354E+00
   4  1.35042E+01  1.39745E+01  1.40294E+01  1.35690E+01  9.95633E-01  9.90296E-01  9.92305E-01  9.92257E-01  9.90155E-01
   3  1.25151E+01  1.29559E+01  1.30080E+01  1.25771E+01  9.93537E-01  9.88649E-01  9.90416E-01  9.90364E-01  9.88456E-01
   2  1.04989E+01  1.08683E+01  1.09134E+01  1.05539E+01  9.79571E-01  9.75794E-01  9.77293E-01  9.77257E-01  9.75527E-01
   1  6.34425E+00  6.58201E+00  6.61079E+00  6.37849E+00  1.00424E+00  1.00341E+00  1.00444E+00  1.00447E+00  1.00336E+00
 
 Ave   1.2765E+01   1.3201E+01   1.3250E+01   1.2824E+01   9.9616E-01   9.9059E-01   9.9409E-01   9.9407E-01   9.9046E-01
 A-O       -0.021       -0.021       -0.021       -0.021       -0.001       -0.002       -0.000       -0.000       -0.002
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   51
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  1 c1c24 HFP NOMINAL DEPLETION                                                     2874.10 ppm   25.000 EFPD    

 Cycle exposure at end of this step = 25.000 EFPD                                                    

 Depletion step used = 25.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   52
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  1 c1c24 HFP NOMINAL DEPLETION                                                     2874.10 ppm   25.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =    25.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    13.899 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9974219     0.00257808     7.2351E-02     5.5147E-03     2.36625     0.024    2591.65    
     2  2  1.0006430    -0.00064300     1.5958E-02     1.3045E-03     2.40462    -0.000    2615.49    
     3  3  1.0000321    -0.00003215 *   1.4131E-02     3.0211E-04 *   2.37111     0.009    2634.95    
     4  4  0.9999705     0.00002952 *   4.7005E-03     1.2483E-04 *   2.38231     0.008    2627.81    
     5  5  0.9999976 *   0.00000244 *   2.7133E-03     1.0349E-04 *   2.37586     0.008    2628.89    
     6  6  1.0000008 *  -0.00000077 *   4.5177E-04 *   3.7841E-05 *   2.37694     0.009    2628.85 *  

    Nodal Solution Time = 0.61 Sec

 Boron (ppm)  = 2628.849
 Axial Offset =    0.009
 Max. Node-Averaged  Peaking Factor = 2.377 in Node ( 8,14, 10)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  0.654  0.461  0.839  0.865  1.711  1.419  1.903  1.709   08
  9  0.461  0.480  0.607  0.672  1.244  1.872  1.357  1.633   09
 10  0.839  0.588  1.228  1.003  1.576  1.257  1.682  1.500   10
 11  0.865  0.914  1.026  1.472  1.033  0.775  1.004  1.199   11
 12  1.711  1.258  1.600  1.043  1.373  0.924  0.988          12
 13  1.419  1.851  1.250  0.775  0.923  1.184  0.961          13
 14  1.903  1.303  1.636  0.987  0.984  0.959                 14
 15  1.709  1.607  1.453  1.171                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8   9,10   9,16  14,13  16,12  15,13  13,14  13, 4  13, 3   08
  9   9,16  17,17  17,17  17,17   5,15   4,13   4, 5   1, 1   09
 10  14,13  17,17  17,17  15,13   5,14   3,13   4, 5   5, 3   10
 11  16,12  17,17  15,13  14, 5   5, 3   1,17   2, 6   1, 1   11
 12  15,13  15, 5  14, 5   3, 5   5, 4  13, 3  13, 3          12
 13  13,14  13, 4   5, 3   1, 1   3,13   4, 5   1, 1          13
 14  13, 4   5, 3   5, 4   6, 2   3,13   1, 1                 14
 15  13, 3   1, 1   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  0.491 30.050  0.652 30.325  1.367 29.758  1.535  1.373   08
  9 30.050 30.914 52.116 18.598 30.151  1.504 30.354  1.303   09
 10  0.652 53.791  0.941 29.802  1.253 30.438  1.347  1.199   10
 11 30.325 30.877 29.851  1.163 30.878 52.808 29.669  0.931   11
 12  1.367 30.161  1.272 30.899  1.082 29.922  0.776          12
 13 29.758  1.485 30.257 52.810 29.891  0.926  0.748          13
 14  1.535 31.551  1.309 29.608  0.772  0.746                 14
 15  1.373  1.271  1.159  0.908                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   53
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  1 c1c24 HFP NOMINAL DEPLETION                                                     2628.85 ppm   25.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  0.462 28.945 28.907  0.529  0.594 28.529 29.240  1.083  1.273 25.049 28.557  1.453  1.417  1.251  0.872    1
  2 28.938 27.797 29.290 49.646 45.836 10.050 13.577 24.904 28.866  1.374  1.423 29.424 29.353  1.196  0.848    2
  3 28.906 29.035 29.787 45.807 42.186 13.569 17.164 24.882 28.632  1.331  1.370 29.348 28.765  1.143  0.810    3
  4  0.535 47.721 51.732  0.658  0.748 28.843 28.768  1.073  1.180 21.532 25.437  1.265  1.238  1.094  0.758    4
  5  0.607 46.028 50.360  0.766  0.880 28.635 28.711  1.091  1.096 24.845 27.609  1.118  1.117  0.979  0.670    5
  6 28.547 29.606 29.677 28.877 28.658  1.058  1.089 29.562 28.752 50.712 47.509 19.377 25.545  0.788  0.537    6
  7 29.254 29.799 29.462 28.818 28.750  1.098  1.094 29.880 29.274 50.153 46.723 22.627 28.006  0.558  0.355    7
  8  1.099 24.956 24.965  1.112  1.114 29.604 29.904  1.019  0.955 23.984 28.518  0.719  0.578                  8
  9  1.280 28.886 28.660  1.200  1.109 28.776 29.287  0.956  0.952 24.582 28.581  0.709  0.509                  9
 10 25.022  1.372  1.330 21.408 24.733 50.714 50.147 23.965 24.580  0.871  0.803  0.665  0.438                 10
 11 28.549  1.407  1.350 25.322 27.443 47.526 46.723 28.490 28.570  0.802  0.711  0.524  0.311                 11
 12  1.443 28.572 29.752  1.232  1.094 19.312 22.583  0.713  0.706  0.663  0.523                               12
 13  1.404 29.952 30.411  1.198  1.088 25.459 27.951  0.572  0.506  0.436  0.310                               13
 14  1.240  1.167  1.106  1.058  0.952  0.770  0.547                                                           14
 15  0.865  0.829  0.786  0.734  0.651  0.523  0.347                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  0.612  0.412  0.430  0.692  0.772  0.623  0.771  1.372  1.604  1.339  1.352  1.815  1.767  1.559  1.089    1
  2  0.413  0.405  0.411  0.396  0.464  0.484  0.590  1.003  1.165  1.725  1.783  1.294  1.236  1.494  1.060    2
  3  0.432  0.415  0.429  0.442  0.519  0.504  0.594  0.999  1.141  1.674  1.720  1.239  1.186  1.431  1.014    3
  4  0.700  0.414  0.431  0.851  0.965  0.756  0.857  1.364  1.494  1.190  1.181  1.590  1.553  1.371  0.952    4
  5  0.789  0.485  0.508  0.987  1.130  0.885  0.950  1.388  1.392  1.005  0.991  1.410  1.406  1.232  0.845    5
  6  0.634  0.642  0.684  0.788  0.901  1.352  1.389  0.987  0.908  0.690  0.694  0.924  0.874  0.997  0.679    6
  7  0.783  0.791  0.821  0.898  0.975  1.400  1.396  0.965  0.863  0.639  0.616  0.758  0.671  0.711  0.452    7
  8  1.392  1.036  1.041  1.413  1.417  0.997  0.968  1.303  1.224  0.835  0.732  0.923  0.742                  8
  9  1.613  1.183  1.164  1.519  1.409  0.915  0.865  1.224  1.221  0.873  0.770  0.913  0.656                  9
 10  1.339  1.725  1.674  1.192  1.007  0.691  0.639  0.834  0.872  1.121  1.034  0.857  0.566                 10
 11  1.346  1.765  1.696  1.165  0.981  0.688  0.613  0.729  0.768  1.033  0.916  0.677  0.402                 11
 12  1.803  1.248  1.172  1.550  1.382  0.910  0.750  0.917  0.910  0.855  0.676                               12
 13  1.752  1.171  1.102  1.506  1.372  0.857  0.661  0.735  0.652  0.564  0.402                               13
 14  1.546  1.459  1.386  1.329  1.200  0.976  0.698                                                           14
 15  1.081  1.038  0.985  0.924  0.822  0.664  0.442                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 2.503 in Node (14, 8,10)

    Pin Power Reconstruction Time = 0.28 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  0.612  0.422  0.738  0.703  1.495  1.344  1.784  1.319   08
  9  0.422  0.415  0.455  0.543  1.077  1.726  1.239  1.250   09
 10  0.738  0.460  0.983  0.862  1.409  1.092  1.490  1.100   10
 11  0.703  0.734  0.890  1.384  0.931  0.660  0.807  0.710   11
 12  1.495  1.106  1.440  0.936  1.243  0.802  0.809          12
 13  1.344  1.715  1.086  0.658  0.801  1.026  0.626          13
 14  1.784  1.173  1.453  0.795  0.804  0.624                 14
 15  1.319  1.217  1.069  0.695                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.146  0.915  1.152  0.910  1.134  0.936  1.128  1.138   08
  9  0.915  0.914  0.835  0.807  0.939  1.129  0.926  1.139   09
 10  1.152  0.820  1.145  0.929  1.136  0.951  1.134  1.142   10
 11  0.910  0.927  0.929  1.136  0.930  0.817  0.961  1.153   11
 12  1.134  0.939  1.135  0.930  1.129  0.945  1.150          12
 13  0.936  1.130  0.952  0.817  0.945  1.144  1.156          13
 14  1.128  0.900  1.135  0.961  1.150  1.156                 14
 15  1.138  1.140  1.143  1.153                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8  0.462 28.924  0.566 28.892  1.183 26.794  1.429  1.057   08
  9 28.924 28.978 45.869 13.590 26.821  1.374 29.222  0.999   09
 10  0.566 48.961  0.763 28.739  1.110 24.856  1.184  0.875   10
 11 28.892 29.636 28.776  1.085 29.367 48.774 23.889  0.560   11
 12  1.183 26.867  1.134 29.393  0.971 26.416  0.629          12
 13 26.794  1.365 24.726 48.778 26.401  0.797  0.485          13
 14  1.429 29.672  1.153 23.826  0.624  0.483                 14
 15  1.057  0.972  0.849  0.547                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  28.34   0.00   09
 10   0.00   0.00   0.00  27.84   0.00  24.15   0.00   0.00   10
 11   0.00  28.71  27.88   0.00  28.46   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  28.49   0.00   0.00   0.00          12
 13   0.00   0.00  24.02   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   54
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  1 c1c24 HFP NOMINAL DEPLETION                                                     2628.85 ppm   25.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97             1.43 25B-08 sil51  ( 8,14,  )       0.46  25H-08  sil48  ( 8, 8,  )         0.92
        6    26A        16            29.67 24L-03 24L-03 (14, 9,  )      28.89  24H-04  24H-04 (11, 8,  )        29.12
        8    26C        48            29.39 24M-06 24M-06 (12,11,  )      23.83  24L-02  24L-02 (14,11,  )        26.55
        5    25C        16            48.96 24L-04 24L-04 (10, 9,  )      45.87  24K-06  24K-06 ( 9,10,  )        48.10
        9    26D         4            13.59 24N-03 24N-03 ( 9,11,  )      13.59  24N-03  24N-03 ( 9,11,  )        13.59
        7    26B        12            29.64 24L-05 24L-05 (11, 9,  )      28.74  24L-07  24L-07 (10,11,  )        29.05

     CORE              193            48.96 24L-04 24L-04 (10, 9,  )       0.46  25H-08  sil48  ( 8, 8,  )        13.90


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97             1.53 25B-08 sil51  ( 1,12,  )
        6    26A        16            31.55 24L-03 24L-03 (13, 3,  )
        8    26C        48            30.90 24M-06 24M-06 ( 8, 7,  )
        5    25C        16            53.79 24L-04 24L-04 ( 4, 3,  )
        9    26D         4            18.60 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            30.88 24L-05 24L-05 ( 6, 2,  )

     CORE              193            53.79 24L-04 24L-04 ( 4, 3,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   55
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  1 c1c24 HFP NOMINAL DEPLETION                                                     2628.85 ppm   25.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  13.899 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp.  25.0 EFPD   600.0 EFPH   0.875 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  6.000E+02 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  0.875 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  7.161E-05  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     2.377 ( 8,14,10)
       Fuel    Average . . .TFUAVE  876.51 K      1118.05 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  580.44 K       585.13 F                  F-delta-H       1.903 ( 8,14)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         2.059 ( 8,14, 4)   [Off]
 Boron Conc. . . . . . . . . . BOR  2628.8 ppm                               Max-3PIN        2.493 (14, 8,10)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        2.503 (14, 8,10)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O   0.009                                 Total Neutron Leakage . . . . . . . . .  0.039
 Edit Ring -     1      2      3      4      5
 RPF            0.440  0.659  1.233  1.075  0.664
 KIN            0.940  0.942  1.016  1.047  1.154
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.732  0.723  0.698  0.705  0.722
 TFU (Deg K)    681.7  760.6  944.7  905.2  789.5
 TMO (Deg K)    570.3  574.4  584.4  581.9  574.8
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.17934   0.88558   6.81686   0.02288   0.66566   650.305   597.155
  23   0.54045   1.08219  10.19080   0.00000   0.66698   750.972   596.718
  22   0.75874   1.06084  12.07479   0.00000   0.66943   814.776   595.911
  21   0.93580   1.05055  13.54225   0.00000   0.67268   868.322   594.830
  20   1.04932   1.04357  14.20185   0.00000   0.67646   902.533   593.544
  19   1.12512   1.03898  14.49598   0.00000   0.68054   924.911   592.114
 
  18   1.18742   1.03757  14.78508   0.00000   0.68483   943.094   590.571
  17   1.22787   1.03547  14.92628   0.00000   0.68925   954.139   588.933
  16   1.25985   1.03453  15.07195   0.00000   0.69373   962.482   587.220
  15   1.26299   1.03259  14.94456   0.00000   0.69822   961.450   585.457
  14   1.28616   1.03285  15.17605   0.00000   0.70269   967.066   583.649
  13   1.28983   1.03155  15.21907   0.00000   0.70715   966.350   581.796
 
  12   1.27554   1.03051  15.10041   0.00000   0.71152   959.880   579.925
  11   1.27930   1.03119  15.29840   0.00000   0.71583   959.386   578.039
  10   1.26632   1.03070  15.35996   0.00000   0.72006   953.568   576.136
   9   1.24139   1.03075  15.34441   0.00000   0.72418   944.009   574.239
   8   1.19998   1.02961  15.21479   0.00000   0.72813   929.305   572.371
   7   1.16967   1.03085  15.38712   0.00000   0.73193   918.439   570.540
 
   6   1.11049   1.03187  15.30924   0.00000   0.73554   898.487   568.761
   5   1.01598   1.03352  14.88244   0.00000   0.73887   867.453   567.088
   4   0.91259   1.03934  14.53827   0.00000   0.74187   834.147   565.558
   3   0.74923   1.04965  13.39326   0.00000   0.74443   782.714   564.232
   2   0.51899   1.07043  11.15504   0.00000   0.74636   712.673   563.213
   1   0.15764   0.85056   6.70117   0.00000   0.74739   609.866   562.667
 
 Ave   1.00000   1.02605  13.89856   0.00095   0.70807   876.514   580.444
 P**2                      9.47436
 A-O   0.00857   0.00436  -0.01896   1.00000  -0.03404     0.014     0.018
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   56
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  1 c1c24 HFP NOMINAL DEPLETION                                                     2628.85 ppm   25.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  1.27601E-09  9.92386E-10  2.23348E-09  1.31116E-08  6.81686E+00  6.50342E-01  2.64267E+01  1.66922E+03  0.00000E+00
  23  3.84180E-09  3.10712E-09  6.70721E-09  3.66796E-08  1.01908E+01  6.53137E-01  2.86578E+01  1.66850E+03  3.09629E+00
  22  5.39419E-09  3.77177E-09  9.60146E-09  4.11765E-08  1.20748E+01  6.56676E-01  2.97123E+01  1.68259E+03  3.57425E+00
  21  6.65436E-09  4.10142E-09  1.19888E-08  4.23220E-08  1.35422E+01  6.60794E-01  3.05227E+01  1.69567E+03  4.00665E+00
  20  7.46233E-09  4.28971E-09  1.35193E-08  4.31616E-08  1.42019E+01  6.65342E-01  3.09304E+01  1.70691E+03  4.20273E+00
  19  8.00137E-09  4.40641E-09  1.45354E-08  4.37388E-08  1.44960E+01  6.70087E-01  3.11412E+01  1.71621E+03  4.28770E+00
 
  18  8.44419E-09  4.46213E-09  1.53723E-08  4.37111E-08  1.47851E+01  6.74940E-01  3.13239E+01  1.72326E+03  4.37241E+00
  17  8.73148E-09  4.49737E-09  1.59104E-08  4.37399E-08  1.49263E+01  6.79861E-01  3.14152E+01  1.72851E+03  4.41214E+00
  16  8.95837E-09  4.50843E-09  1.63350E-08  4.35639E-08  1.50720E+01  6.84805E-01  3.14916E+01  1.73223E+03  4.45434E+00
  15  8.98025E-09  4.53331E-09  1.63570E-08  4.39251E-08  1.49446E+01  6.89717E-01  3.14251E+01  1.73481E+03  4.41304E+00
  14  9.14491E-09  4.50624E-09  1.66641E-08  4.33364E-08  1.51760E+01  6.94581E-01  3.15140E+01  1.73572E+03  4.48327E+00
  13  9.17088E-09  4.49293E-09  1.66978E-08  4.31616E-08  1.52191E+01  6.99437E-01  3.15042E+01  1.73593E+03  4.49570E+00
 
  12  9.06933E-09  4.48782E-09  1.64799E-08  4.32942E-08  1.51004E+01  7.04213E-01  3.14083E+01  1.73532E+03  4.45903E+00
  11  9.09638E-09  4.43869E-09  1.65159E-08  4.26145E-08  1.52984E+01  7.08918E-01  3.14508E+01  1.73331E+03  4.52088E+00
  10  9.00472E-09  4.39854E-09  1.63206E-08  4.22400E-08  1.53600E+01  7.13583E-01  3.14169E+01  1.73040E+03  4.54141E+00
   9  8.82790E-09  4.35740E-09  1.59624E-08  4.19871E-08  1.53444E+01  7.18165E-01  3.13346E+01  1.72656E+03  4.53970E+00
   8  8.53403E-09  4.31645E-09  1.53820E-08  4.19409E-08  1.52148E+01  7.22627E-01  3.11835E+01  1.72145E+03  4.50337E+00
   7  8.31930E-09  4.22461E-09  1.49620E-08  4.10506E-08  1.53871E+01  7.26983E-01  3.11561E+01  1.71439E+03  4.56249E+00
 
   6  7.89878E-09  4.12981E-09  1.41537E-08  4.05076E-08  1.53092E+01  7.31232E-01  3.09900E+01  1.70580E+03  4.54547E+00
   5  7.22654E-09  4.01601E-09  1.28768E-08  4.02873E-08  1.48824E+01  7.35284E-01  3.06353E+01  1.69542E+03  4.42193E+00
   4  6.49073E-09  3.81150E-09  1.14985E-08  3.89890E-08  1.45383E+01  7.39087E-01  3.02858E+01  1.68283E+03  4.32764E+00
   3  5.32786E-09  3.49775E-09  9.33630E-09  3.75061E-08  1.33933E+01  7.42542E-01  2.95224E+01  1.66887E+03  3.99725E+00
   2  3.68856E-09  2.88802E-09  6.33187E-09  3.39320E-08  1.11550E+01  7.45402E-01  2.82152E+01  1.65752E+03  3.41699E+00
   1  1.12159E-09  9.05056E-10  1.91822E-09  1.25575E-08  6.70117E+00  7.47133E-01  2.55815E+01  1.67321E+03  0.00000E+00
 
 Ave   7.1111E-09   3.8809E-09   1.2819E-08   3.9106E-08   1.3899E+01   7.0062E-01   3.0385E+01   1.7073E+03   4.0051E+00
 A-O        0.009        0.024        0.014        0.026       -0.019       -0.039        0.004        0.002       -0.022
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  6.66060E+00  6.89033E+00  6.91889E+00  6.69731E+00  1.00386E+00  1.00141E+00  1.03334E+00  1.03336E+00  1.00128E+00
  23  1.00081E+01  1.03082E+01  1.03453E+01  1.00541E+01  9.49369E-01  9.40992E-01  9.40206E-01  9.40225E-01  9.40809E-01
  22  1.18725E+01  1.22145E+01  1.22555E+01  1.19217E+01  1.01506E+00  1.00699E+00  1.00320E+00  1.00326E+00  1.00686E+00
  21  1.33280E+01  1.37031E+01  1.37476E+01  1.33806E+01  9.89822E-01  9.80739E-01  9.78023E-01  9.78098E-01  9.80697E-01
  20  1.39781E+01  1.43630E+01  1.44095E+01  1.40332E+01  9.96006E-01  9.86888E-01  9.83898E-01  9.83977E-01  9.86866E-01
  19  1.42653E+01  1.46519E+01  1.46994E+01  1.43220E+01  1.00747E+00  9.98586E-01  9.95212E-01  9.95291E-01  9.98561E-01
 
  18  1.45523E+01  1.49439E+01  1.49925E+01  1.46100E+01  9.96515E-01  9.87220E-01  9.84534E-01  9.84616E-01  9.87217E-01
  17  1.46906E+01  1.50841E+01  1.51334E+01  1.47490E+01  9.96033E-01  9.86621E-01  9.84120E-01  9.84205E-01  9.86628E-01
  16  1.48342E+01  1.52309E+01  1.52809E+01  1.48934E+01  9.89016E-01  9.79375E-01  9.77447E-01  9.77532E-01  9.79392E-01
  15  1.47006E+01  1.50913E+01  1.51414E+01  1.47605E+01  1.01510E+00  1.00612E+00  1.00333E+00  1.00342E+00  1.00610E+00
  14  1.49328E+01  1.53320E+01  1.53830E+01  1.49934E+01  9.91781E-01  9.82049E-01  9.80529E-01  9.80613E-01  9.82066E-01
  13  1.49732E+01  1.53742E+01  1.54258E+01  1.50344E+01  9.93320E-01  9.83552E-01  9.82198E-01  9.82281E-01  9.83570E-01
 
  12  1.48495E+01  1.52465E+01  1.52983E+01  1.49115E+01  1.01384E+00  1.00458E+00  1.00266E+00  1.00274E+00  1.00457E+00
  11  1.50487E+01  1.54545E+01  1.55073E+01  1.51113E+01  9.91515E-01  9.81491E-01  9.80816E-01  9.80895E-01  9.81509E-01
  10  1.51091E+01  1.55193E+01  1.55728E+01  1.51724E+01  9.87877E-01  9.77599E-01  9.77315E-01  9.77394E-01  9.77624E-01
   9  1.50907E+01  1.55026E+01  1.55565E+01  1.51547E+01  9.93045E-01  9.82851E-01  9.82700E-01  9.82774E-01  9.82865E-01
   8  1.49564E+01  1.53665E+01  1.54206E+01  1.50211E+01  1.01287E+00  1.00315E+00  1.00242E+00  1.00249E+00  1.00314E+00
   7  1.51309E+01  1.55529E+01  1.56080E+01  1.51961E+01  9.88335E-01  9.77758E-01  9.78314E-01  9.78385E-01  9.77776E-01
 
   6  1.50519E+01  1.54779E+01  1.55333E+01  1.51175E+01  9.88389E-01  9.77723E-01  9.78570E-01  9.78637E-01  9.77734E-01
   5  1.46212E+01  1.50403E+01  1.50952E+01  1.46868E+01  1.01623E+00  1.00637E+00  1.00641E+00  1.00647E+00  1.00633E+00
   4  1.42840E+01  1.47068E+01  1.47611E+01  1.43485E+01  9.92731E-01  9.82284E-01  9.83618E-01  9.83675E-01  9.82252E-01
   3  1.31501E+01  1.35521E+01  1.36037E+01  1.32118E+01  9.90236E-01  9.80098E-01  9.81796E-01  9.81841E-01  9.80011E-01
   2  1.09350E+01  1.12776E+01  1.13224E+01  1.09898E+01  9.61082E-01  9.51770E-01  9.54959E-01  9.54989E-01  9.51610E-01
   1  6.53357E+00  6.76002E+00  6.78881E+00  6.56785E+00  1.03835E+00  1.03848E+00  1.07120E+00  1.07122E+00  1.03846E+00
 
 Ave   1.3665E+01   1.4047E+01   1.4096E+01   1.3723E+01   9.9658E-01   9.8769E-01   9.8945E-01   9.8952E-01   9.8766E-01
 A-O       -0.018       -0.019       -0.019       -0.019       -0.001       -0.001       -0.002       -0.002       -0.001
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   57
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  2 c1c24 HFP NOMINAL DEPLETION                                                     2628.85 ppm   50.000 EFPD    

 Cycle exposure at end of this step = 50.000 EFPD                                                    

 Depletion step used = 25.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   58
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  2 c1c24 HFP NOMINAL DEPLETION                                                     2628.85 ppm   50.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =    50.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    14.773 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9984826     0.00151736     6.3467E-02     4.4887E-03     2.23508     0.013    2476.06    
     2  2  1.0003634    -0.00036340     1.3544E-02     5.3013E-04     2.26577     0.001    2489.65    
     3  3  0.9999948     0.00000524 *   8.5154E-03     1.3176E-05 *   2.24664     0.005    2494.75    
     4  4  1.0000039 *  -0.00000387 *   2.6187E-03     4.2513E-05 *   2.25254     0.005    2490.68    
     5  5  1.0000001 *  -0.00000011 *   1.6678E-03     9.9633E-05 *   2.24879     0.004    2490.25 *  
     6  6  0.9999999 *   0.00000012 *   2.3679E-04 *   1.6384E-05 *   2.24932     0.005    2490.39 *  

    Nodal Solution Time = 0.61 Sec

 Boron (ppm)  = 2490.387
 Axial Offset =    0.005
 Max. Node-Averaged  Peaking Factor = 2.249 in Node ( 8,14, 10)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  0.705  0.492  0.872  0.871  1.683  1.386  1.847  1.657   08
  9  0.492  0.505  0.624  0.681  1.229  1.824  1.325  1.585   09
 10  0.872  0.605  1.248  1.013  1.564  1.239  1.649  1.470   10
 11  0.871  0.920  1.034  1.478  1.042  0.775  0.999  1.188   11
 12  1.683  1.243  1.587  1.051  1.384  0.942  1.008          12
 13  1.386  1.806  1.240  0.780  0.942  1.205  0.982          13
 14  1.847  1.273  1.608  0.985  1.005  0.980                 14
 15  1.657  1.562  1.428  1.164                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8   9,10   9,16  14,13  16,12  15,13  13,14  13, 4  13, 3   08
  9   9,16  17,17  17,17  17,17   5,15   4,13   4, 5   1, 1   09
 10  14,13  17,17  17,17  15,13   5,14   3,13   4, 5   5, 3   10
 11  16,12  17,17  15,13  14, 5   5, 3   1, 1   2, 6   1, 1   11
 12  15,13  15, 5  14, 5   3, 5   5, 4  13, 3  13, 3          12
 13  13,14  13, 4   5, 3   1, 1   3,13   4, 5   1, 1          13
 14  13, 4   5, 3   5, 4   6, 2   3,13   1, 1                 14
 15  13, 3   1, 1   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  1.024 30.506  1.330 31.161  2.710 31.132  3.018  2.697   08
  9 30.506 31.369 52.532 19.222 31.365  2.966 31.614  2.565   09
 10  1.330 54.241  1.900 30.771  2.495 31.327  2.663  2.368   10
 11 31.161 31.600 30.814  2.332 31.896 53.503 30.332  1.854   11
 12  2.710 31.385  2.533 31.918  2.175 30.660  1.570          12
 13 31.132  2.931 31.134 53.507 30.636  1.872  1.514          13
 14  3.018 32.660  2.592 30.263  1.564  1.510                 14
 15  2.697  2.505  2.294  1.811                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   59
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  2 c1c24 HFP NOMINAL DEPLETION                                                     2490.39 ppm   50.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  0.963 29.368 29.346  1.088  1.214 29.153 30.006  2.159  2.525 26.357 29.875  2.860  2.785  2.458  1.717    1
  2 29.362 28.213 29.710 50.049 46.304 10.537 14.166 25.893 30.009  2.717  2.809 30.685 30.557  2.355  1.671    2
  3 29.347 29.459 30.224 46.253 42.708 14.075 17.756 25.868 29.753  2.636  2.708 30.557 29.923  2.256  1.599    3
  4  1.101 48.142 52.168  1.339  1.518 29.598 29.619  2.146  2.351 22.701 26.595  2.504  2.447  2.161  1.500    4
  5  1.241 46.517 50.871  1.553  1.778 29.514 29.653  2.184  2.190 25.836 28.584  2.221  2.215  1.941  1.331    5
  6 29.181 30.248 30.361 29.663 29.553  2.126  2.186 30.540 29.652 51.397 48.196 20.289 26.405  1.570  1.070    6
  7 30.032 30.584 30.278 29.708 29.716  2.203  2.196 30.838 30.131 50.789 47.336 23.380 28.671  1.118  0.711    7
  8  2.191 25.978 25.993  2.224  2.230 30.591 30.864  2.049  1.924 24.815 29.248  1.451  1.166                  8
  9  2.539 30.046 29.803  2.391  2.217 29.682 30.147  1.925  1.919 25.451 29.349  1.435  1.031                  9
 10 26.330  2.716  2.636 22.580 25.727 51.400 50.783 24.796 25.450  1.762  1.625  1.346  0.889                 10
 11 29.862  2.779  2.671 26.465 28.408 48.208 47.333 29.217 29.336  1.623  1.439  1.064  0.632                 11
 12  2.841 29.789 30.898  2.442  2.176 20.210 23.329  1.442  1.430  1.343  1.062                               12
 13  2.762 31.094 31.488  2.373  2.160 26.305 28.607  1.155  1.025  0.886  0.631                               13
 14  2.437  2.300  2.184  2.094  1.891  1.536  1.098                                                           14
 15  1.704  1.636  1.553  1.455  1.295  1.045  0.696                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  0.662  0.446  0.461  0.730  0.805  0.642  0.781  1.365  1.580  1.314  1.321  1.762  1.712  1.511  1.060    1
  2  0.446  0.437  0.441  0.420  0.486  0.503  0.603  1.003  1.152  1.690  1.739  1.263  1.205  1.453  1.033    2
  3  0.463  0.444  0.456  0.464  0.540  0.522  0.607  1.001  1.132  1.644  1.682  1.214  1.161  1.396  0.992    3
  4  0.738  0.438  0.452  0.880  0.991  0.773  0.868  1.364  1.483  1.180  1.165  1.559  1.521  1.342  0.935    4
  5  0.822  0.506  0.528  1.012  1.152  0.899  0.960  1.391  1.389  1.004  0.986  1.392  1.384  1.213  0.834    5
  6  0.653  0.660  0.701  0.804  0.914  1.362  1.397  0.995  0.916  0.698  0.699  0.924  0.871  0.990  0.676    6
  7  0.793  0.801  0.832  0.908  0.983  1.408  1.404  0.975  0.875  0.651  0.627  0.767  0.676  0.713  0.453    7
  8  1.384  1.035  1.043  1.412  1.419  1.004  0.978  1.315  1.238  0.850  0.747  0.939  0.754                  8
  9  1.589  1.170  1.154  1.509  1.406  0.922  0.877  1.239  1.238  0.890  0.786  0.931  0.670                  9
 10  1.314  1.690  1.646  1.183  1.007  0.699  0.651  0.850  0.889  1.142  1.054  0.875  0.580                 10
 11  1.316  1.723  1.662  1.152  0.977  0.694  0.624  0.745  0.785  1.054  0.935  0.694  0.414                 11
 12  1.751  1.220  1.151  1.525  1.368  0.912  0.761  0.934  0.928  0.874  0.693                               12
 13  1.698  1.144  1.082  1.479  1.354  0.857  0.668  0.748  0.667  0.579  0.413                               13
 14  1.499  1.421  1.356  1.305  1.185  0.972  0.702                                                           14
 15  1.052  1.013  0.966  0.910  0.814  0.662  0.445                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 2.365 in Node ( 8,14,10)

    Pin Power Reconstruction Time = 0.28 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  0.662  0.454  0.774  0.717  1.479  1.316  1.731  1.281   08
  9  0.454  0.444  0.477  0.559  1.072  1.689  1.211  1.218   09
 10  0.774  0.481  1.009  0.875  1.407  1.084  1.464  1.081   10
 11  0.717  0.749  0.902  1.393  0.940  0.669  0.810  0.708   11
 12  1.479  1.100  1.436  0.945  1.258  0.818  0.824          12
 13  1.316  1.680  1.080  0.667  0.817  1.046  0.641          13
 14  1.731  1.149  1.432  0.799  0.819  0.640                 14
 15  1.281  1.189  1.054  0.695                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.148  0.920  1.154  0.914  1.132  0.938  1.125  1.137   08
  9  0.920  0.919  0.840  0.815  0.942  1.127  0.928  1.139   09
 10  1.154  0.825  1.146  0.932  1.134  0.953  1.133  1.143   10
 11  0.914  0.930  0.931  1.135  0.933  0.822  0.964  1.156   11
 12  1.132  0.941  1.134  0.932  1.128  0.949  1.152          12
 13  0.938  1.127  0.954  0.822  0.949  1.145  1.159          13
 14  1.125  0.902  1.134  0.965  1.152  1.159                 14
 15  1.137  1.140  1.144  1.156                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8  0.963 29.356  1.161 29.593  2.354 28.106  2.812  2.079   08
  9 29.356 29.402 46.329 14.133 27.881  2.717 30.430  1.970   09
 10  1.161 49.425  1.547 29.596  2.218 25.929  2.346  1.733   10
 11 29.593 30.368 29.660  2.178 30.290 49.430 24.686  1.117   11
 12  2.354 27.955  2.265 30.321  1.954 27.216  1.271          12
 13 28.106  2.700 25.795 49.431 27.200  1.612  0.983          13
 14  2.812 30.817  2.288 24.613  1.263  0.981                 14
 15  2.079  1.918  1.684  1.094                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  29.53   0.00   09
 10   0.00   0.00   0.00  28.68   0.00  25.21   0.00   0.00   10
 11   0.00  29.43  28.75   0.00  29.37   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  29.40   0.00   0.00   0.00          12
 13   0.00   0.00  25.07   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   60
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  2 c1c24 HFP NOMINAL DEPLETION                                                     2490.39 ppm   50.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97             2.81 25H-14 sil86  (14, 8,  )       0.96  25H-08  sil48  ( 8, 8,  )         1.83
        6    26A        16            30.82 24L-03 24L-03 (14, 9,  )      29.36  24K-08  24K-08 ( 8, 9,  )        29.79
        8    26C        48            30.43 24J-07 24J-07 ( 9,14,  )      24.61  24L-02  24L-02 (14,11,  )        27.54
        5    25C        16            49.43 24J-03 24J-03 (13,11,  )      46.33  24K-06  24K-06 ( 9,10,  )        48.65
        9    26D         4            14.13 24N-03 24N-03 ( 9,11,  )      14.13  24N-03  24N-03 ( 9,11,  )        14.13
        7    26B        12            30.37 24L-05 24L-05 (11, 9,  )      29.60  24L-07  24L-07 (10,11,  )        29.87

     CORE              193            49.43 24J-03 24J-03 (13,11,  )       0.96  25H-08  sil48  ( 8, 8,  )        14.77


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97             3.02 25B-08 sil51  ( 1,12,  )
        6    26A        16            32.66 24L-03 24L-03 (13, 3,  )
        8    26C        48            31.92 24M-06 24M-06 ( 8, 7,  )
        5    25C        16            54.24 24L-04 24L-04 ( 4, 3,  )
        9    26D         4            19.22 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            31.60 24L-05 24L-05 ( 6, 2,  )

     CORE              193            54.24 24L-04 24L-04 ( 4, 3,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   61
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  2 c1c24 HFP NOMINAL DEPLETION                                                     2490.39 ppm   50.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  14.773 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp.  50.0 EFPD  1200.0 EFPH   1.750 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  6.000E+02 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  0.875 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  7.645E-05  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     2.249 ( 8,14,10)
       Fuel    Average . . .TFUAVE  875.65 K      1116.50 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  580.52 K       585.26 F                  F-delta-H       1.847 ( 8,14)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.994 ( 8,14, 4)   [Off]
 Boron Conc. . . . . . . . . . BOR  2490.4 ppm                               Max-3PIN        2.359 ( 8,14,10)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        2.365 ( 8,14,10)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O   0.005                                 Total Neutron Leakage . . . . . . . . .  0.039
 Edit Ring -     1      2      3      4      5
 RPF            0.473  0.681  1.227  1.067  0.671
 KIN            0.945  0.946  1.016  1.049  1.157
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.731  0.722  0.698  0.705  0.722
 TFU (Deg K)    691.3  766.8  942.3  901.5  790.7
 TMO (Deg K)    571.0  574.8  584.3  581.8  575.0
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.19100   0.89537   7.04578   0.02288   0.66570   653.524   597.236
  23   0.56446   1.08509  10.66163   0.00000   0.66714   757.302   596.764
  22   0.78327   1.06271  12.73188   0.00000   0.66974   821.319   595.908
  21   0.95550   1.05173  14.34816   0.00000   0.67307   873.449   594.791
  20   1.06048   1.04433  15.10087   0.00000   0.67690   905.057   593.479
  19   1.12661   1.03952  15.45547   0.00000   0.68099   924.432   592.034
 
  18   1.17946   1.03805  15.79363   0.00000   0.68525   939.700   590.487
  17   1.21143   1.03592  15.96567   0.00000   0.68960   948.160   588.859
  16   1.23636   1.03500  16.13558   0.00000   0.69400   954.340   587.168
  15   1.23478   1.03307  16.00884   0.00000   0.69839   951.871   585.434
  14   1.25428   1.03340  16.25850   0.00000   0.70274   956.405   583.664
  13   1.25664   1.03213  16.30411   0.00000   0.70708   955.336   581.855
 
  12   1.24358   1.03114  16.17382   0.00000   0.71134   949.271   580.030
  11   1.24981   1.03190  16.37607   0.00000   0.71554   949.582   578.187
  10   1.24169   1.03145  16.42868   0.00000   0.71968   945.282   576.324
   9   1.22383   1.03157  16.39492   0.00000   0.72373   937.898   574.459
   8   1.19144   1.03047  16.23388   0.00000   0.72764   925.938   572.612
   7   1.17140   1.03184  16.38477   0.00000   0.73143   918.198   570.788
 
   6   1.12370   1.03302  16.26137   0.00000   0.73507   901.716   568.999
   5   1.04041   1.03492  15.75883   0.00000   0.73846   874.024   567.298
   4   0.94665   1.04119  15.33064   0.00000   0.74155   843.561   565.723
   3   0.78785   1.05223  14.04834   0.00000   0.74422   793.377   564.340
   2   0.55363   1.07413  11.61217   0.00000   0.74627   722.080   563.263
   1   0.17172   0.86037   6.90479   0.00000   0.74737   613.788   562.678
 
 Ave   1.00000   1.02794  14.77345   0.00095   0.70804   875.650   580.516
 P**2                     10.76252
 A-O   0.00452   0.00414  -0.01744   1.00000  -0.03364     0.013     0.017
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   62
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  2 c1c24 HFP NOMINAL DEPLETION                                                     2490.39 ppm   50.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  1.35895E-09  1.02732E-09  2.39752E-09  1.41706E-08  7.04578E+00  6.50916E-01  2.64189E+01  1.64006E+03  0.00000E+00
  23  4.01286E-09  3.18697E-09  7.06562E-09  4.23913E-08  1.06616E+01  6.54048E-01  2.86299E+01  1.65866E+03  3.17712E+00
  22  5.56930E-09  3.83843E-09  1.00314E-08  4.65646E-08  1.27319E+01  6.57728E-01  2.96735E+01  1.68269E+03  3.68499E+00
  21  6.79555E-09  4.14962E-09  1.24244E-08  4.70432E-08  1.43482E+01  6.61863E-01  3.04734E+01  1.69956E+03  4.14045E+00
  20  7.54302E-09  4.32229E-09  1.38918E-08  4.75603E-08  1.51009E+01  6.66400E-01  3.08710E+01  1.71357E+03  4.35072E+00
  19  8.01349E-09  4.42540E-09  1.48156E-08  4.79547E-08  1.54555E+01  6.71118E-01  3.10712E+01  1.72492E+03  4.44481E+00
 
  18  8.38926E-09  4.46882E-09  1.55584E-08  4.76923E-08  1.57936E+01  6.75923E-01  3.12425E+01  1.73308E+03  4.53656E+00
  17  8.61648E-09  4.49428E-09  1.60055E-08  4.75882E-08  1.59657E+01  6.80782E-01  3.13233E+01  1.73907E+03  4.58060E+00
  16  8.79331E-09  4.49768E-09  1.63543E-08  4.72741E-08  1.61356E+01  6.85650E-01  3.13900E+01  1.74308E+03  4.62600E+00
  15  8.78175E-09  4.51780E-09  1.63177E-08  4.76959E-08  1.60088E+01  6.90483E-01  3.13179E+01  1.74611E+03  4.58477E+00
  14  8.92020E-09  4.48731E-09  1.65908E-08  4.69316E-08  1.62585E+01  6.95256E-01  3.14001E+01  1.74646E+03  4.65692E+00
  13  8.93677E-09  4.47324E-09  1.66128E-08  4.67233E-08  1.63041E+01  7.00021E-01  3.13880E+01  1.74630E+03  4.66926E+00
 
  12  8.84374E-09  4.46993E-09  1.64078E-08  4.69349E-08  1.61738E+01  7.04710E-01  3.12942E+01  1.74541E+03  4.63064E+00
  11  8.88789E-09  4.42414E-09  1.64830E-08  4.61361E-08  1.63761E+01  7.09325E-01  3.13386E+01  1.74224E+03  4.69215E+00
  10  8.83028E-09  4.38971E-09  1.63499E-08  4.57500E-08  1.64287E+01  7.13905E-01  3.13108E+01  1.73823E+03  4.71066E+00
   9  8.70323E-09  4.35671E-09  1.60764E-08  4.55416E-08  1.63949E+01  7.18412E-01  3.12384E+01  1.73323E+03  4.70571E+00
   8  8.47301E-09  4.32672E-09  1.55973E-08  4.56313E-08  1.62339E+01  7.22810E-01  3.11002E+01  1.72685E+03  4.66444E+00
   7  8.33062E-09  4.24635E-09  1.53005E-08  4.46858E-08  1.63848E+01  7.27108E-01  3.10854E+01  1.71742E+03  4.71931E+00
 
   6  7.99121E-09  4.16607E-09  1.46152E-08  4.42409E-08  1.62614E+01  7.31314E-01  3.09350E+01  1.70639E+03  4.69498E+00
   5  7.39850E-09  4.07057E-09  1.34389E-08  4.43107E-08  1.57588E+01  7.35336E-01  3.05977E+01  1.69342E+03  4.56022E+00
   4  6.73092E-09  3.88309E-09  1.21375E-08  4.31542E-08  1.53306E+01  7.39118E-01  3.02627E+01  1.67681E+03  4.45290E+00
   3  5.60048E-09  3.58918E-09  9.96335E-09  4.21143E-08  1.40483E+01  7.42559E-01  2.95128E+01  1.65807E+03  4.10200E+00
   2  3.93315E-09  2.99607E-09  6.82739E-09  3.91233E-08  1.16122E+01  7.45408E-01  2.82185E+01  1.63721E+03  3.49171E+00
   1  1.22116E-09  9.50266E-10  2.10898E-09  1.37359E-08  6.90479E+00  7.47132E-01  2.55879E+01  1.63848E+03  0.00000E+00
 
 Ave   7.1115E-09   3.9066E-09   1.3057E-08   4.2956E-08   1.4773E+01   7.0114E-01   3.0320E+01   1.7078E+03   4.1438E+00
 A-O        0.005        0.022        0.009        0.027       -0.017       -0.038        0.004        0.004       -0.020
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  6.89580E+00  7.11113E+00  7.13965E+00  6.93251E+00  1.00375E+00  1.00134E+00  1.03273E+00  1.03277E+00  1.00125E+00
  23  1.04925E+01  1.07639E+01  1.08008E+01  1.05383E+01  9.49401E-01  9.41768E-01  9.40033E-01  9.40098E-01  9.41654E-01
  22  1.25481E+01  1.28512E+01  1.28917E+01  1.25970E+01  1.01520E+00  1.00797E+00  1.00301E+00  1.00313E+00  1.00793E+00
  21  1.41567E+01  1.44837E+01  1.45277E+01  1.42089E+01  9.89757E-01  9.81594E-01  9.77665E-01  9.77801E-01  9.81647E-01
  20  1.49023E+01  1.52340E+01  1.52799E+01  1.49569E+01  9.95988E-01  9.87821E-01  9.83565E-01  9.83708E-01  9.87895E-01
  19  1.52515E+01  1.55817E+01  1.56286E+01  1.53077E+01  1.00754E+00  9.99613E-01  9.94936E-01  9.95081E-01  9.99688E-01
 
  18  1.55890E+01  1.59211E+01  1.59690E+01  1.56462E+01  9.96479E-01  9.88170E-01  9.84202E-01  9.84349E-01  9.88267E-01
  17  1.57590E+01  1.60911E+01  1.61397E+01  1.58169E+01  9.96017E-01  9.87595E-01  9.83817E-01  9.83966E-01  9.87702E-01
  16  1.59276E+01  1.62612E+01  1.63105E+01  1.59863E+01  9.88928E-01  9.80281E-01  9.77098E-01  9.77246E-01  9.80397E-01
  15  1.57944E+01  1.61226E+01  1.61721E+01  1.58539E+01  1.01522E+00  1.00719E+00  1.00312E+00  1.00327E+00  1.00728E+00
  14  1.60456E+01  1.63804E+01  1.64307E+01  1.61057E+01  9.91727E-01  9.82966E-01  9.80213E-01  9.80359E-01  9.83082E-01
  13  1.60887E+01  1.64250E+01  1.64759E+01  1.61494E+01  9.93279E-01  9.84463E-01  9.81888E-01  9.82033E-01  9.84578E-01
 
  12  1.59528E+01  1.62862E+01  1.63374E+01  1.60142E+01  1.01395E+00  1.00559E+00  1.00244E+00  1.00258E+00  1.00568E+00
  11  1.61566E+01  1.64979E+01  1.65500E+01  1.62187E+01  9.91431E-01  9.82328E-01  9.80472E-01  9.80611E-01  9.82443E-01
  10  1.62079E+01  1.65537E+01  1.66065E+01  1.62707E+01  9.87777E-01  9.78409E-01  9.76966E-01  9.77103E-01  9.78529E-01
   9  1.61707E+01  1.65192E+01  1.65725E+01  1.62342E+01  9.92957E-01  9.83635E-01  9.82340E-01  9.82472E-01  9.83744E-01
   8  1.60039E+01  1.63529E+01  1.64064E+01  1.60681E+01  1.01294E+00  1.00403E+00  1.00214E+00  1.00227E+00  1.00411E+00
   7  1.61568E+01  1.65179E+01  1.65724E+01  1.62215E+01  9.88222E-01  9.78464E-01  9.77933E-01  9.78057E-01  9.78572E-01
 
   6  1.60310E+01  1.63985E+01  1.64534E+01  1.60961E+01  9.88265E-01  9.78379E-01  9.78174E-01  9.78292E-01  9.78479E-01
   5  1.55223E+01  1.58879E+01  1.59422E+01  1.55874E+01  1.01627E+00  1.00710E+00  1.00609E+00  1.00620E+00  1.00714E+00
   4  1.50991E+01  1.54726E+01  1.55264E+01  1.51632E+01  9.92614E-01  9.82828E-01  9.83215E-01  9.83318E-01  9.82880E-01
   3  1.38241E+01  1.41850E+01  1.42362E+01  1.38855E+01  9.90166E-01  9.80617E-01  9.81461E-01  9.81550E-01  9.80610E-01
   2  1.14055E+01  1.17191E+01  1.17636E+01  1.14600E+01  9.60914E-01  9.52065E-01  9.54577E-01  9.54638E-01  9.51973E-01
   1  6.74244E+00  6.95637E+00  6.98515E+00  6.77675E+00  1.03818E+00  1.03814E+00  1.07055E+00  1.07058E+00  1.03814E+00
 
 Ave   1.4564E+01   1.4894E+01   1.4942E+01   1.4622E+01   9.9654E-01   9.8843E-01   9.8911E-01   9.8923E-01   9.8849E-01
 A-O       -0.017       -0.017       -0.017       -0.017       -0.001       -0.001       -0.002       -0.002       -0.001
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   63
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  3 c1c24 HFP NOMINAL DEPLETION                                                     2490.39 ppm   75.000 EFPD    

 Cycle exposure at end of this step = 75.000 EFPD                                                    

 Depletion step used = 25.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   64
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  3 c1c24 HFP NOMINAL DEPLETION                                                     2490.39 ppm   75.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =    75.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    15.648 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9984479     0.00155208     6.8586E-02     5.5835E-03     2.10495     0.010    2335.08    
     2  2  1.0003703    -0.00037026     1.4928E-02     5.1751E-04     2.13685    -0.003    2348.86    
     3  3  1.0000024    -0.00000237 *   7.6220E-03     1.4009E-04 *   2.12069     0.002    2355.10    
     4  4  1.0000107 *  -0.00001067 *   2.5193E-03     3.3733E-05 *   2.12604     0.001    2351.00    
     5  5  0.9999984 *   0.00000164 *   1.5219E-03     8.8457E-05 *   2.12281     0.001    2351.13 *  
     6  6  1.0000007 *  -0.00000071 *   2.2884E-04 *   1.6127E-05 *   2.12330     0.001    2350.97 *  

    Nodal Solution Time = 0.61 Sec

 Boron (ppm)  = 2350.973
 Axial Offset =    0.001
 Max. Node-Averaged  Peaking Factor = 2.123 in Node ( 8,14, 10)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  0.761  0.524  0.907  0.875  1.656  1.353  1.794  1.607   08
  9  0.524  0.530  0.642  0.690  1.213  1.779  1.292  1.538   09
 10  0.907  0.621  1.266  1.021  1.551  1.223  1.617  1.440   10
 11  0.875  0.924  1.041  1.483  1.048  0.778  0.995  1.177   11
 12  1.656  1.227  1.574  1.057  1.395  0.960  1.031          12
 13  1.353  1.763  1.227  0.783  0.960  1.229  1.005          13
 14  1.794  1.243  1.581  0.983  1.029  1.003                 14
 15  1.607  1.518  1.404  1.157                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8   9,10   9,16  14,13  16,12  14,13  14,13  13, 4  13, 3   08
  9   9,16  17,17  17,17  17,17   5,15   4,13   4, 5   1, 1   09
 10  14,13  17,17  17,17  15,13   5,14   3, 5   4, 5   5, 3   10
 11  16,12  17,17  15,13  14, 5   5, 3   1, 1   2, 6   1, 1   11
 12  14,13  15, 5  14, 5   3, 5   5, 4  13, 3  13, 3          12
 13  14,13  13, 4   5, 3   1, 1   3,13   4, 5   1, 1          13
 14  13, 4   5, 3   5, 4   5, 3   3,13   1, 1                 14
 15  13, 3   1, 1   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  1.598 30.995  2.035 32.004  4.030 32.474  4.457  3.980   08
  9 30.995 31.852 52.973 19.858 32.564  4.390 32.847  3.793   09
 10  2.035 54.712  2.875 31.830  3.726 32.212  3.952  3.513   10
 11 32.004 32.337 31.881  3.504 32.922 54.206 31.001  2.770   11
 12  4.030 32.593  3.783 32.944  3.276 31.466  2.381          12
 13 32.474  4.340 32.011 54.212 31.450  2.834  2.296          13
 14  4.457 33.748  3.852 30.924  2.373  2.291                 14
 15  3.980  3.707  3.408  2.710                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   65
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  3 c1c24 HFP NOMINAL DEPLETION                                                     2350.97 ppm   75.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  1.505 29.826 29.817  1.679  1.861 29.796 30.782  3.230  3.758 27.640 31.163  4.226  4.110  3.629  2.539    1
  2 29.820 28.661 30.160 50.476 46.794 11.043 14.767 26.882 31.139  4.031  4.159 31.915 31.730  3.482  2.473    2
  3 29.820 29.912 30.687 46.723 43.251 14.598 18.361 26.857 30.865  3.917  4.016 31.742 31.055  3.340  2.371    3
  4  1.698 48.586 52.625  2.043  2.308 30.369 30.480  3.218  3.513 23.858 27.736  3.718  3.631  3.206  2.229    4
  5  1.901 47.028 51.401  2.359  2.692 30.407 30.604  3.278  3.281 26.826 29.554  3.309  3.295  2.888  1.983    5
  6 29.835 30.907 31.061 30.464 30.461  3.202  3.287 31.524 30.558 52.090 48.888 21.200 27.263  2.347  1.601    6
  7 30.818 31.379 31.103 30.608 30.690  3.312  3.303 31.804 31.000 51.437 47.961 24.142 29.341  1.681  1.068    7
  8  3.276 26.998 27.021  3.333  3.346 31.585 31.833  3.088  2.904 25.662 29.993  2.197  1.765                  8
  9  3.779 31.193 30.936  3.573  3.322 30.595 31.018  2.905  2.900 26.338 30.134  2.176  1.565                  9
 10 27.614  4.031  3.919 23.742 26.720 52.093 51.432 25.643 26.336  2.669  2.463  2.044  1.352                 10
 11 31.144  4.118  3.964 27.595 29.371 48.896 47.954 29.960 30.120  2.461  2.183  1.617  0.963                 11
 12  4.199 30.979 32.023  3.631  3.247 21.112 24.085  2.184  2.168  2.039  1.616                               12
 13  4.077 32.209 32.545  3.526  3.219 27.150 29.270  1.749  1.557  1.348  0.962                               13
 14  3.599  3.403  3.239  3.111  2.817  2.299  1.653                                                           14
 15  2.521  2.424  2.306  2.166  1.933  1.565  1.048                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  0.714  0.482  0.494  0.771  0.840  0.662  0.791  1.356  1.554  1.288  1.289  1.709  1.657  1.464  1.031    1
  2  0.482  0.471  0.471  0.445  0.508  0.522  0.616  1.002  1.138  1.652  1.694  1.232  1.174  1.411  1.007    2
  3  0.496  0.474  0.484  0.487  0.561  0.540  0.619  1.003  1.121  1.612  1.643  1.188  1.135  1.362  0.971    3
  4  0.778  0.463  0.474  0.910  1.017  0.790  0.878  1.362  1.471  1.168  1.149  1.529  1.488  1.314  0.919    4
  5  0.856  0.528  0.547  1.037  1.172  0.911  0.968  1.390  1.385  1.002  0.980  1.374  1.362  1.195  0.825    5
  6  0.672  0.678  0.718  0.819  0.927  1.371  1.402  1.000  0.922  0.705  0.704  0.924  0.868  0.985  0.674    6
  7  0.802  0.811  0.841  0.916  0.990  1.412  1.409  0.983  0.886  0.663  0.638  0.777  0.683  0.718  0.455    7
  8  1.375  1.033  1.042  1.408  1.417  1.009  0.986  1.325  1.252  0.866  0.763  0.956  0.768                  8
  9  1.563  1.156  1.143  1.495  1.401  0.929  0.888  1.253  1.254  0.907  0.804  0.952  0.687                  9
 10  1.289  1.654  1.615  1.172  1.006  0.707  0.663  0.866  0.907  1.164  1.077  0.896  0.597                 10
 11  1.285  1.680  1.626  1.138  0.974  0.700  0.636  0.762  0.803  1.076  0.957  0.714  0.428                 11
 12  1.700  1.192  1.130  1.499  1.354  0.915  0.772  0.952  0.950  0.895  0.713                               12
 13  1.645  1.117  1.062  1.451  1.337  0.856  0.676  0.763  0.684  0.596  0.427                               13
 14  1.453  1.383  1.326  1.281  1.171  0.969  0.708                                                           14
 15  1.025  0.989  0.948  0.897  0.808  0.662  0.448                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 2.238 in Node (14, 8, 9)

    Pin Power Reconstruction Time = 0.28 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  0.714  0.488  0.811  0.732  1.462  1.288  1.678  1.243   08
  9  0.488  0.475  0.500  0.574  1.066  1.650  1.182  1.188   09
 10  0.811  0.503  1.034  0.887  1.402  1.075  1.438  1.063   10
 11  0.732  0.762  0.913  1.399  0.948  0.678  0.813  0.708   11
 12  1.462  1.094  1.430  0.953  1.271  0.835  0.841          12
 13  1.288  1.644  1.072  0.677  0.835  1.069  0.659          13
 14  1.678  1.125  1.410  0.805  0.837  0.658                 14
 15  1.243  1.162  1.039  0.697                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.150  0.925  1.156  0.918  1.131  0.939  1.122  1.138   08
  9  0.925  0.925  0.845  0.822  0.944  1.124  0.930  1.140   09
 10  1.156  0.830  1.147  0.935  1.133  0.956  1.131  1.145   10
 11  0.918  0.934  0.934  1.134  0.935  0.826  0.968  1.160   11
 12  1.131  0.943  1.132  0.935  1.127  0.952  1.154          12
 13  0.939  1.124  0.957  0.826  0.952  1.146  1.162          13
 14  1.122  0.905  1.132  0.968  1.155  1.162                 14
 15  1.138  1.141  1.146  1.160                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8  1.505 29.821  1.785 30.308  3.511 29.390  4.153  3.072   08
  9 29.821 29.855 46.811 14.692 28.936  4.031 31.610  2.916   09
 10  1.785 49.910  2.351 30.465  3.323 26.993  3.488  2.576   10
 11 30.308 31.113 30.556  3.276 31.221 50.094 25.487  1.674   11
 12  3.511 29.037  3.393 31.258  2.949 28.032  1.926          12
 13 29.390  4.008 26.857 50.094 28.015  2.444  1.494          13
 14  4.153 31.939  3.405 25.404  1.915  1.491                 14
 15  3.072  2.843  2.507  1.641                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  30.69   0.00   09
 10   0.00   0.00   0.00  29.54   0.00  26.25   0.00   0.00   10
 11   0.00  30.16  29.62   0.00  30.28   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  30.32   0.00   0.00   0.00          12
 13   0.00   0.00  26.12   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   66
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  3 c1c24 HFP NOMINAL DEPLETION                                                     2350.97 ppm   75.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97             4.15 25H-14 sil86  (14, 8,  )       1.49  25C-14  sil89  (14,13,  )         2.74
        6    26A        16            31.94 24L-03 24L-03 (14, 9,  )      29.82  24H-06  24H-06 ( 9, 8,  )        30.48
        8    26C        48            31.61 24J-07 24J-07 ( 9,14,  )      25.40  24L-02  24L-02 (14,11,  )        28.52
        5    25C        16            50.09 24N-07 24N-07 (11,13,  )      46.81  24K-06  24K-06 ( 9,10,  )        49.23
        9    26D         4            14.69 24N-03 24N-03 ( 9,11,  )      14.69  24N-03  24N-03 ( 9,11,  )        14.69
        7    26B        12            31.11 24L-05 24L-05 (11, 9,  )      30.46  24L-07  24L-07 (10,11,  )        30.71

     CORE              193            50.09 24N-07 24N-07 (11,13,  )       1.49  25C-14  sil89  (14,13,  )        15.65


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97             4.46 25B-08 sil51  ( 1,12,  )
        6    26A        16            33.75 24L-03 24L-03 (13, 3,  )
        8    26C        48            32.94 24M-06 24M-06 ( 8, 7,  )
        5    25C        16            54.71 24L-04 24L-04 ( 4, 3,  )
        9    26D         4            19.86 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            32.34 24L-05 24L-05 ( 6, 2,  )

     CORE              193            54.71 24L-04 24L-04 ( 4, 3,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   67
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  3 c1c24 HFP NOMINAL DEPLETION                                                     2350.97 ppm   75.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  15.648 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp.  75.0 EFPD  1800.0 EFPH   2.625 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  6.000E+02 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  0.875 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  8.186E-05  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     2.123 ( 8,14,10)
       Fuel    Average . . .TFUAVE  874.75 K      1114.87 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  580.58 K       585.37 F                  F-delta-H       1.794 ( 8,14)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.930 ( 8,14, 3)   [Off]
 Boron Conc. . . . . . . . . . BOR  2351.0 ppm                               Max-3PIN        2.230 (14, 8,10)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        2.238 (14, 8, 9)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O   0.001                                 Total Neutron Leakage . . . . . . . . .  0.039
 Edit Ring -     1      2      3      4      5
 RPF            0.508  0.702  1.219  1.059  0.680
 KIN            0.950  0.950  1.017  1.050  1.161
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.729  0.721  0.699  0.705  0.721
 TFU (Deg K)    701.6  773.2  939.2  897.9  792.5
 TMO (Deg K)    571.6  575.2  584.3  581.8  575.2
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.20439   0.90574   7.29018   0.02288   0.66584   657.172   597.292
  23   0.59166   1.08823  11.15426   0.00000   0.66738   764.514   596.788
  22   0.81057   1.06469  13.41103   0.00000   0.67008   828.678   595.893
  21   0.97704   1.05292  15.17164   0.00000   0.67349   879.121   594.739
  20   1.07251   1.04509  16.00975   0.00000   0.67736   907.820   593.399
  19   1.12828   1.04006  16.41627   0.00000   0.68146   923.963   591.938
 
  18   1.17130   1.03853  16.79527   0.00000   0.68569   936.140   590.389
  17   1.19476   1.03638  16.99094   0.00000   0.68999   941.971   588.771
  16   1.21276   1.03549  17.17914   0.00000   0.69430   946.041   587.101
  15   1.20666   1.03362  17.04913   0.00000   0.69858   942.197   585.399
  14   1.22259   1.03401  17.31390   0.00000   0.70282   945.656   583.667
  13   1.22363   1.03276  17.36096   0.00000   0.70704   944.213   581.901
 
  12   1.21153   1.03183  17.21995   0.00000   0.71119   938.499   580.121
  11   1.21971   1.03265  17.42838   0.00000   0.71527   939.451   578.323
  10   1.21580   1.03223  17.47587   0.00000   0.71932   936.495   576.501
   9   1.20425   1.03240  17.42962   0.00000   0.72329   931.107   574.671
   8   1.18025   1.03132  17.24457   0.00000   0.72716   921.786   572.848
   7   1.17002   1.03278  17.38258   0.00000   0.73093   917.082   571.035
 
   6   1.13389   1.03410  17.22348   0.00000   0.73459   904.136   569.240
   5   1.06269   1.03625  16.65512   0.00000   0.73804   880.078   567.515
   4   0.98001   1.04301  16.15174   0.00000   0.74121   852.903   565.897
   3   0.82761   1.05488  14.73682   0.00000   0.74400   804.502   564.456
   2   0.59071   1.07809  12.09987   0.00000   0.74616   732.258   563.318
   1   0.18737   0.87087   7.12679   0.00000   0.74734   618.141   562.691
 
 Ave   1.00000   1.02991  15.64835   0.00095   0.70802   874.747   580.579
 P**2                     11.99647
 A-O   0.00135   0.00393  -0.01629   1.00000  -0.03322     0.011     0.017
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   68
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  3 c1c24 HFP NOMINAL DEPLETION                                                     2350.97 ppm   75.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  1.45394E-09  1.06506E-09  2.58516E-09  1.46135E-08  7.29018E+00  6.51553E-01  2.64151E+01  1.62085E+03  0.00000E+00
  23  4.20595E-09  3.27132E-09  7.47279E-09  4.48443E-08  1.11543E+01  6.54996E-01  2.86149E+01  1.65657E+03  3.26168E+00
  22  5.76338E-09  3.90589E-09  1.05130E-08  4.84884E-08  1.34110E+01  6.58765E-01  2.96496E+01  1.68764E+03  3.79940E+00
  21  6.94874E-09  4.19478E-09  1.29021E-08  4.85838E-08  1.51716E+01  6.62909E-01  3.04371E+01  1.70662E+03  4.27706E+00
  20  7.62875E-09  4.34910E-09  1.42912E-08  4.89699E-08  1.60097E+01  6.67432E-01  3.08206E+01  1.72198E+03  4.50023E+00
  19  8.02570E-09  4.43645E-09  1.51082E-08  4.93059E-08  1.64163E+01  6.72118E-01  3.10061E+01  1.73423E+03  4.60206E+00
 
  18  8.33157E-09  4.46584E-09  1.57448E-08  4.89709E-08  1.67953E+01  6.76871E-01  3.11620E+01  1.74267E+03  4.69953E+00
  17  8.49836E-09  4.48048E-09  1.60938E-08  4.88314E-08  1.69909E+01  6.81662E-01  3.12296E+01  1.74877E+03  4.74677E+00
  16  8.62602E-09  4.47556E-09  1.63622E-08  4.84793E-08  1.71791E+01  6.86452E-01  3.12850E+01  1.75268E+03  4.79445E+00
  15  8.58252E-09  4.49066E-09  1.62654E-08  4.89251E-08  1.70491E+01  6.91202E-01  3.12061E+01  1.75579E+03  4.75271E+00
  14  8.69559E-09  4.45643E-09  1.65010E-08  4.81076E-08  1.73139E+01  6.95883E-01  3.12808E+01  1.75565E+03  4.82634E+00
  13  8.70279E-09  4.44145E-09  1.65080E-08  4.78910E-08  1.73610E+01  7.00555E-01  3.12659E+01  1.75519E+03  4.83847E+00
 
  12  8.61673E-09  4.43993E-09  1.63122E-08  4.81300E-08  1.72200E+01  7.05154E-01  3.11742E+01  1.75413E+03  4.79809E+00
  11  8.67473E-09  4.39724E-09  1.64193E-08  4.72936E-08  1.74284E+01  7.09679E-01  3.12204E+01  1.75030E+03  4.85964E+00
  10  8.64698E-09  4.36842E-09  1.63418E-08  4.69032E-08  1.74759E+01  7.14177E-01  3.11991E+01  1.74572E+03  4.87678E+00
   9  8.56485E-09  4.34356E-09  1.61474E-08  4.67074E-08  1.74296E+01  7.18609E-01  3.11373E+01  1.74018E+03  4.86955E+00
   8  8.39432E-09  4.32469E-09  1.57674E-08  4.68382E-08  1.72446E+01  7.22945E-01  3.10137E+01  1.73327E+03  4.82453E+00
   7  8.32152E-09  4.25612E-09  1.55941E-08  4.58672E-08  1.73826E+01  7.27190E-01  3.10134E+01  1.72261E+03  4.87651E+00
 
   6  8.06434E-09  4.19117E-09  1.50405E-08  4.54462E-08  1.72235E+01  7.31356E-01  3.08814E+01  1.71035E+03  4.84642E+00
   5  7.55754E-09  4.11557E-09  1.39834E-08  4.56116E-08  1.66551E+01  7.35353E-01  3.05649E+01  1.69606E+03  4.70202E+00
   4  6.96857E-09  3.94727E-09  1.27821E-08  4.45075E-08  1.61517E+01  7.39121E-01  3.02482E+01  1.67705E+03  4.58304E+00
   3  5.88336E-09  3.67638E-09  1.06176E-08  4.36947E-08  1.47368E+01  7.42560E-01  2.95144E+01  1.65526E+03  4.21238E+00
   2  4.19660E-09  3.10444E-09  7.35794E-09  4.12625E-08  1.20999E+01  7.45408E-01  2.82322E+01  1.62726E+03  3.57164E+00
   1  1.33230E-09  9.97317E-10  2.31916E-09  1.42138E-08  7.12679E+00  7.47131E-01  2.55979E+01  1.61432E+03  0.00000E+00
 
 Ave   7.1119E-09   3.9248E-09   1.3293E-08   4.4270E-08   1.5648E+01   7.0163E-01   3.0257E+01   1.7110E+03   4.2825E+00
 A-O        0.001        0.020        0.005        0.028       -0.016       -0.038        0.004        0.005       -0.019
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  7.14692E+00  7.34683E+00  7.37531E+00  7.18363E+00  1.00354E+00  1.00116E+00  1.03198E+00  1.03203E+00  1.00109E+00
  23  1.09995E+01  1.12409E+01  1.12774E+01  1.10450E+01  9.49440E-01  9.42510E-01  9.39889E-01  9.39990E-01  9.42456E-01
  22  1.32465E+01  1.35094E+01  1.35494E+01  1.32950E+01  1.01540E+00  1.00896E+00  1.00291E+00  1.00308E+00  1.00900E+00
  21  1.50035E+01  1.52817E+01  1.53250E+01  1.50553E+01  9.89744E-01  9.82426E-01  9.77385E-01  9.77574E-01  9.82551E-01
  20  1.58367E+01  1.61149E+01  1.61602E+01  1.58910E+01  9.96021E-01  9.88716E-01  9.83324E-01  9.83519E-01  9.88864E-01
  19  1.62391E+01  1.65132E+01  1.65594E+01  1.62949E+01  1.00767E+00  1.00060E+00  9.94764E-01  9.94963E-01  1.00074E+00
 
  18  1.66187E+01  1.68920E+01  1.69393E+01  1.66754E+01  9.96500E-01  9.89060E-01  9.83970E-01  9.84169E-01  9.89229E-01
  17  1.68129E+01  1.70850E+01  1.71329E+01  1.68704E+01  9.96044E-01  9.88488E-01  9.83604E-01  9.83804E-01  9.88665E-01
  16  1.70005E+01  1.72727E+01  1.73213E+01  1.70586E+01  9.88883E-01  9.81098E-01  9.76836E-01  9.77034E-01  9.81283E-01
  15  1.68635E+01  1.71312E+01  1.71800E+01  1.69225E+01  1.01540E+00  1.00818E+00  1.00300E+00  1.00320E+00  1.00834E+00
  14  1.71306E+01  1.74031E+01  1.74528E+01  1.71901E+01  9.91715E-01  9.83790E-01  9.79984E-01  9.80179E-01  9.83975E-01
  13  1.71751E+01  1.74491E+01  1.74994E+01  1.72354E+01  9.93272E-01  9.85277E-01  9.81660E-01  9.81853E-01  9.85461E-01
 
  12  1.70279E+01  1.73002E+01  1.73507E+01  1.70889E+01  1.01411E+00  1.00652E+00  1.00231E+00  1.00250E+00  1.00668E+00
  11  1.72384E+01  1.75173E+01  1.75688E+01  1.73000E+01  9.91386E-01  9.83082E-01  9.80211E-01  9.80397E-01  9.83266E-01
  10  1.72845E+01  1.75680E+01  1.76202E+01  1.73468E+01  9.87709E-01  9.79133E-01  9.76692E-01  9.76874E-01  9.79321E-01
   9  1.72344E+01  1.75213E+01  1.75739E+01  1.72974E+01  9.92914E-01  9.84358E-01  9.82066E-01  9.82244E-01  9.84535E-01
   8  1.70427E+01  1.73319E+01  1.73847E+01  1.71064E+01  1.01306E+00  1.00487E+00  1.00196E+00  1.00214E+00  1.00502E+00
   7  1.71827E+01  1.74837E+01  1.75376E+01  1.72469E+01  9.88145E-01  9.79125E-01  9.77629E-01  9.77796E-01  9.79301E-01
 
   6  1.70203E+01  1.73296E+01  1.73838E+01  1.70850E+01  9.88181E-01  9.79008E-01  9.77856E-01  9.78017E-01  9.79176E-01
   5  1.64437E+01  1.67553E+01  1.68091E+01  1.65084E+01  1.01637E+00  1.00784E+00  1.00587E+00  1.00602E+00  1.00795E+00
   4  1.59437E+01  1.62666E+01  1.63199E+01  1.60073E+01  9.92546E-01  9.83390E-01  9.82888E-01  9.83033E-01  9.83509E-01
   3  1.45324E+01  1.48506E+01  1.49014E+01  1.45934E+01  9.90144E-01  9.81167E-01  9.81193E-01  9.81324E-01  9.81228E-01
   2  1.19074E+01  1.21904E+01  1.22346E+01  1.19617E+01  9.60752E-01  9.52364E-01  9.54205E-01  9.54294E-01  9.52332E-01
   1  6.97016E+00  7.17046E+00  7.19924E+00  7.00451E+00  1.03792E+00  1.03771E+00  1.06976E+00  1.06979E+00  1.03771E+00
 
 Ave   1.5463E+01   1.5741E+01   1.5789E+01   1.5521E+01   9.9654E-01   9.8912E-01   9.8883E-01   9.8899E-01   9.8924E-01
 A-O       -0.016       -0.016       -0.016       -0.016       -0.001       -0.001       -0.002       -0.002       -0.001
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   69
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  4 c1c24 HFP NOMINAL DEPLETION                                                     2350.97 ppm  100.000 EFPD    

 Cycle exposure at end of this step = 100.00 EFPD                                                    

 Depletion step used = 25.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   70
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  4 c1c24 HFP NOMINAL DEPLETION                                                     2350.97 ppm  100.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =   100.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    16.523 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9984294     0.00157057     6.2549E-02     6.0046E-03     1.99831     0.007    2194.82    
     2  2  1.0003650    -0.00036499     1.6868E-02     6.7821E-04     2.03259    -0.006    2208.05    
     3  3  1.0000090    -0.00000899 *   7.8025E-03     2.8246E-04 *   2.01686    -0.002    2215.47    
     4  4  1.0000104 *  -0.00001042 *   1.6253E-03     2.4877E-05 *   2.02014    -0.002    2211.18    
     5  5  0.9999979 *   0.00000211 *   6.0404E-04     7.7792E-05 *   2.01892    -0.002    2211.58 *  
     6  6  1.0000010 *  -0.00000098 *   2.7162E-05 *   1.6220E-05 *   2.01887    -0.002    2211.31 *  

    Nodal Solution Time = 0.62 Sec

 Boron (ppm)  = 2211.313
 Axial Offset =   -0.002
 Max. Node-Averaged  Peaking Factor = 2.019 in Node ( 8,14, 7)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  0.819  0.558  0.943  0.881  1.629  1.322  1.740  1.561   08
  9  0.558  0.555  0.659  0.698  1.197  1.732  1.262  1.494   09
 10  0.943  0.637  1.280  1.028  1.538  1.210  1.585  1.413   10
 11  0.881  0.928  1.046  1.486  1.053  0.781  0.992  1.167   11
 12  1.629  1.211  1.560  1.062  1.403  0.976  1.051          12
 13  1.322  1.718  1.214  0.786  0.976  1.249  1.024          13
 14  1.740  1.215  1.554  0.983  1.049  1.023                 14
 15  1.561  1.476  1.380  1.149                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8   9,10  15,13  14,13  16,12  14,13  14,13  13, 4  13, 3   08
  9  15,13  17,17  17,17  17,17   5,15   4,13   5, 4   1, 1   09
 10  14,13  17,17  17,17  15,13   5,14   3, 5   4, 5   5, 3   10
 11  16,12  17,17  13,14  14, 5   5, 3   1, 1   3, 5   1, 1   11
 12  14,13  15, 5  14, 5   3, 5   5, 4  13, 3  13, 3          12
 13  14,13  13, 4   5, 3   1, 1   3,13   4, 5   1, 1          13
 14  13, 4   5, 3   5, 4   5, 3   3,13   1, 1                 14
 15  13, 3   1, 1   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  2.216 31.517  2.767 32.856  5.327 33.782  5.851  5.223   08
  9 31.517 32.363 53.438 20.505 33.749  5.775 34.051  4.987   09
 10  2.767 55.205  3.865 32.890  4.943 33.093  5.213  4.633   10
 11 32.856 33.100 32.948  4.677 33.953 54.916 31.676  3.678   11
 12  5.327 33.787  5.020 33.975  4.382 32.298  3.208          12
 13 33.782  5.712 32.885 54.924 32.282  3.812  3.095          13
 14  5.851 34.815  5.090 31.593  3.199  3.089                 14
 15  5.223  4.879  4.502  3.605                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   71
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  4 c1c24 HFP NOMINAL DEPLETION                                                     2211.31 ppm  100.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  2.088 30.319 30.320  2.301  2.536 30.460 31.567  4.293  4.972 28.899 32.420  5.551  5.393  4.763  3.340    1
  2 30.314 29.143 30.641 50.927 47.307 11.567 15.380 27.871 32.255  5.318  5.475 33.116 32.874  4.577  3.256    2
  3 30.326 30.396 31.178 47.215 43.815 15.139 18.978 27.847 31.966  5.174  5.295 32.902 32.163  4.399  3.127    3
  4  2.327 49.055 53.104  2.771  3.118 31.155 31.351  4.288  4.666 25.005 28.862  4.909  4.789  4.229  2.945    4
  5  2.589 47.560 51.950  3.185  3.621 31.312 31.561  4.372  4.368 27.813 30.518  4.383  4.359  3.821  2.629    5
  6 30.508 31.585 31.777 31.279 31.381  4.282  4.391 32.513 31.470 52.788 49.585 22.112 28.119  3.120  2.130    6
  7 31.614 32.184 31.937 31.515 31.669  4.424  4.412 32.777 31.878 52.097 48.596 24.913 30.018  2.247  1.428    7
  8  4.355 28.017 28.049  4.439  4.460 32.582 32.809  4.134  3.893 26.523 30.753  2.956  2.374                  8
  9  4.999 32.327 32.059  4.744  4.422 31.514 31.898  3.895  3.892 27.240 30.935  2.932  2.111                  9
 10 28.874  5.319  5.178 24.893 27.711 52.794 52.091 26.504 27.238  3.593  3.318  2.756  1.828                 10
 11 32.398  5.424  5.230 28.710 30.329 49.590 48.588 30.719 30.920  3.316  2.944  2.186  1.305                 11
 12  5.517 32.142 33.128  4.800  4.306 22.015 24.851  2.940  2.923  2.751  2.184                               12
 13  5.351 33.299 33.583  4.657  4.264 27.994 29.941  2.355  2.101  1.823  1.303                               13
 14  4.725  4.478  4.271  4.110  3.733  3.061  2.212                                                           14
 15  3.317  3.193  3.045  2.867  2.566  2.086  1.402                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  0.768  0.519  0.527  0.812  0.875  0.683  0.801  1.348  1.530  1.264  1.261  1.661  1.606  1.420  1.005    1
  2  0.519  0.506  0.503  0.470  0.531  0.542  0.628  1.002  1.126  1.618  1.652  1.203  1.146  1.373  0.983    2
  3  0.529  0.506  0.512  0.511  0.583  0.558  0.632  1.004  1.111  1.583  1.607  1.164  1.112  1.330  0.951    3
  4  0.819  0.488  0.497  0.940  1.041  0.805  0.887  1.358  1.458  1.156  1.133  1.499  1.457  1.287  0.904    4
  5  0.891  0.550  0.566  1.061  1.190  0.923  0.974  1.388  1.379  1.000  0.975  1.357  1.342  1.177  0.816    5
  6  0.692  0.696  0.735  0.834  0.937  1.376  1.404  1.004  0.927  0.712  0.709  0.925  0.866  0.980  0.672    6
  7  0.812  0.821  0.851  0.923  0.996  1.414  1.411  0.989  0.895  0.674  0.649  0.786  0.689  0.722  0.458    7
  8  1.366  1.032  1.042  1.403  1.414  1.013  0.992  1.332  1.262  0.880  0.778  0.972  0.781                  8
  9  1.539  1.143  1.133  1.481  1.395  0.934  0.897  1.263  1.267  0.922  0.820  0.970  0.702                  9
 10  1.265  1.620  1.586  1.161  1.004  0.713  0.674  0.880  0.922  1.182  1.096  0.915  0.612                 10
 11  1.257  1.640  1.592  1.124  0.970  0.706  0.647  0.777  0.819  1.095  0.976  0.731  0.441                 11
 12  1.652  1.166  1.110  1.473  1.340  0.917  0.782  0.969  0.968  0.914  0.731                               12
 13  1.596  1.092  1.043  1.425  1.320  0.856  0.684  0.777  0.700  0.611  0.440                               13
 14  1.410  1.348  1.298  1.258  1.157  0.966  0.714                                                           14
 15  0.999  0.967  0.930  0.885  0.802  0.662  0.452                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 2.132 in Node (14, 8, 8)

    Pin Power Reconstruction Time = 0.28 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  0.768  0.524  0.849  0.747  1.446  1.262  1.629  1.209   08
  9  0.524  0.507  0.524  0.590  1.061  1.615  1.156  1.159   09
 10  0.849  0.525  1.058  0.897  1.396  1.066  1.414  1.046   10
 11  0.747  0.775  0.923  1.401  0.954  0.686  0.817  0.708   11
 12  1.446  1.087  1.423  0.959  1.281  0.850  0.856          12
 13  1.262  1.610  1.065  0.685  0.849  1.087  0.675          13
 14  1.629  1.103  1.389  0.810  0.853  0.674                 14
 15  1.209  1.136  1.025  0.699                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.152  0.930  1.158  0.922  1.129  0.941  1.119  1.138   08
  9  0.930  0.930  0.849  0.830  0.946  1.121  0.932  1.141   09
 10  1.158  0.834  1.148  0.938  1.132  0.958  1.130  1.146   10
 11  0.922  0.937  0.937  1.132  0.938  0.830  0.971  1.164   11
 12  1.129  0.945  1.130  0.938  1.126  0.955  1.157          12
 13  0.941  1.121  0.959  0.830  0.955  1.147  1.166          13
 14  1.119  0.907  1.131  0.972  1.157  1.166                 14
 15  1.138  1.142  1.147  1.164                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8  2.088 30.320  2.438 31.037  4.655 30.648  5.453  4.036   08
  9 30.320 30.339 47.316 15.266 29.985  5.316 32.764  3.840   09
 10  2.438 50.417  3.174 31.345  4.423 28.049  4.610  3.406   10
 11 31.037 31.871 31.461  4.378 32.160 50.767 26.290  2.231   11
 12  4.655 30.113  4.516 32.201  3.953 28.863  2.593          12
 13 30.648  5.288 27.911 50.766 28.845  3.293  2.019          13
 14  5.453 33.038  4.507 26.200  2.580  2.015                 14
 15  4.036  3.747  3.319  2.190                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  31.82   0.00   09
 10   0.00   0.00   0.00  30.40   0.00  27.29   0.00   0.00   10
 11   0.00  30.91  30.51   0.00  31.20   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  31.24   0.00   0.00   0.00          12
 13   0.00   0.00  27.15   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   72
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  4 c1c24 HFP NOMINAL DEPLETION                                                     2211.31 ppm  100.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97             5.45 25B-08 sil51  ( 8,14,  )       2.02  25C-14  sil89  (14,13,  )         3.65
        6    26A        16            33.04 24L-03 24L-03 (14, 9,  )      30.32  24H-06  24H-06 ( 9, 8,  )        31.18
        8    26C        48            32.76 24J-07 24J-07 ( 9,14,  )      26.20  24L-02  24L-02 (14,11,  )        29.50
        5    25C        16            50.77 24N-07 24N-07 (11,13,  )      47.32  24K-06  24K-06 ( 9,10,  )        49.82
        9    26D         4            15.27 24N-03 24N-03 ( 9,11,  )      15.27  24N-03  24N-03 ( 9,11,  )        15.27
        7    26B        12            31.87 24L-05 24L-05 (11, 9,  )      31.34  24L-07  24L-07 (10,11,  )        31.56

     CORE              193            50.77 24N-07 24N-07 (11,13,  )       2.02  25C-14  sil89  (14,13,  )        16.52


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97             5.85 25B-08 sil51  ( 1,12,  )
        6    26A        16            34.82 24L-03 24L-03 (13, 3,  )
        8    26C        48            34.05 24J-07 24J-07 ( 2,12,  )
        5    25C        16            55.20 24L-04 24L-04 ( 4, 3,  )
        9    26D         4            20.51 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            33.10 24L-05 24L-05 ( 7, 2,  )

     CORE              193            55.20 24L-04 24L-04 ( 4, 3,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   73
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  4 c1c24 HFP NOMINAL DEPLETION                                                     2211.31 ppm  100.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  16.523 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp. 100.0 EFPD  2400.0 EFPH   3.500 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  6.000E+02 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  0.875 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  8.731E-05  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     2.019 ( 8,14, 7)
       Fuel    Average . . .TFUAVE  873.38 K      1112.42 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  580.64 K       585.48 F                  F-delta-H       1.740 ( 8,14)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.870 ( 8,14, 3)   [Off]
 Boron Conc. . . . . . . . . . BOR  2211.3 ppm                               Max-3PIN        2.117 (14, 8, 7)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        2.132 (14, 8, 8)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O  -0.002                                 Total Neutron Leakage . . . . . . . . .  0.039
 Edit Ring -     1      2      3      4      5
 RPF            0.543  0.724  1.211  1.052  0.689
 KIN            0.955  0.954  1.018  1.052  1.165
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.728  0.720  0.699  0.706  0.721
 TFU (Deg K)    712.2  779.7  935.3  893.8  794.0
 TMO (Deg K)    572.3  575.6  584.2  581.7  575.4
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.21794   0.91632   7.55122   0.02288   0.66599   660.799   597.336
  23   0.61773   1.09137  11.66959   0.00000   0.66761   771.425   596.805
  22   0.83529   1.06655  14.11234   0.00000   0.67040   835.292   595.874
  21   0.99502   1.05392  16.01195   0.00000   0.67388   883.742   594.688
  20   1.08099   1.04568  16.92737   0.00000   0.67778   909.457   593.324
  19   1.12718   1.04047  17.37734   0.00000   0.68188   922.451   591.852
 
  18   1.16165   1.03893  17.78935   0.00000   0.68607   931.737   590.304
  17   1.17815   1.03679  18.00204   0.00000   0.69031   935.202   588.698
  16   1.19075   1.03595  18.20326   0.00000   0.69454   937.422   587.050
  15   1.18140   1.03414  18.06668   0.00000   0.69873   932.579   585.377
  14   1.19479   1.03460  18.34395   0.00000   0.70287   935.134   583.680
  13   1.19507   1.03338  18.39158   0.00000   0.70698   933.463   581.952
 
  12   1.18398   1.03250  18.24071   0.00000   0.71103   928.153   580.211
  11   1.19378   1.03337  18.45681   0.00000   0.71502   929.592   578.451
  10   1.19325   1.03296  18.50243   0.00000   0.71899   927.744   576.665
   9   1.18678   1.03316  18.44852   0.00000   0.72290   924.036   574.865
   8   1.16968   1.03207  18.24598   0.00000   0.72672   917.014   573.065
   7   1.16766   1.03359  18.37880   0.00000   0.73047   914.927   571.262
 
   6   1.14167   1.03501  18.19324   0.00000   0.73414   905.260   569.465
   5   1.08165   1.03737  17.56897   0.00000   0.73764   884.879   567.719
   4   1.00995   1.04458  16.99981   0.00000   0.74089   861.174   566.064
   3   0.86498   1.05731  15.45817   0.00000   0.74378   814.942   564.570
   2   0.62715   1.08199  12.61890   0.00000   0.74606   742.232   563.373
   1   0.20351   0.88165   7.36844   0.00000   0.74732   622.568   562.704
 
 Ave   1.00000   1.03182  16.52324   0.00095   0.70800   873.384   580.640
 P**2                     13.18818
 A-O  -0.00200   0.00374  -0.01544   1.00000  -0.03283     0.010     0.017
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   74
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  4 c1c24 HFP NOMINAL DEPLETION                                                     2211.31 ppm  100.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  1.55014E-09  1.10078E-09  2.78203E-09  1.48581E-08  7.55122E+00  6.52217E-01  2.64133E+01  1.60061E+03  0.00000E+00
  23  4.39113E-09  3.34751E-09  7.88538E-09  4.60775E-08  1.16696E+01  6.55932E-01  2.86054E+01  1.64895E+03  3.35000E+00
  22  5.93931E-09  3.96273E-09  1.09842E-08  4.94739E-08  1.41123E+01  6.59766E-01  2.96311E+01  1.68465E+03  3.91738E+00
  21  7.07690E-09  4.22810E-09  1.33497E-08  4.94343E-08  1.60120E+01  6.63910E-01  3.04039E+01  1.70455E+03  4.41627E+00
  20  7.68940E-09  4.36412E-09  1.46489E-08  4.97902E-08  1.69274E+01  6.68411E-01  3.07709E+01  1.72033E+03  4.65099E+00
  19  8.01844E-09  4.43661E-09  1.53573E-08  5.01138E-08  1.73773E+01  6.73059E-01  3.09393E+01  1.73272E+03  4.75918E+00
 
  18  8.26354E-09  4.45328E-09  1.58930E-08  4.97471E-08  1.77893E+01  6.77755E-01  3.10781E+01  1.74099E+03  4.86112E+00
  17  8.38084E-09  4.45849E-09  1.61539E-08  4.95872E-08  1.80020E+01  6.82476E-01  3.11312E+01  1.74687E+03  4.91052E+00
  16  8.47013E-09  4.44651E-09  1.63532E-08  4.92097E-08  1.82033E+01  6.87187E-01  3.11745E+01  1.75050E+03  4.95969E+00
  15  8.40367E-09  4.45786E-09  1.62084E-08  4.96654E-08  1.80667E+01  6.91855E-01  3.10897E+01  1.75352E+03  4.91695E+00
  14  8.49862E-09  4.42068E-09  1.64142E-08  4.88136E-08  1.83439E+01  6.96448E-01  3.11568E+01  1.75297E+03  4.99169E+00
  13  8.50052E-09  4.40530E-09  1.64117E-08  4.85901E-08  1.83916E+01  7.01032E-01  3.11395E+01  1.75229E+03  5.00353E+00
 
  12  8.42170E-09  4.40573E-09  1.62263E-08  4.88436E-08  1.82407E+01  7.05547E-01  3.10503E+01  1.75115E+03  4.96157E+00
  11  8.49120E-09  4.36565E-09  1.63610E-08  4.79849E-08  1.84568E+01  7.09989E-01  3.10981E+01  1.74695E+03  5.02345E+00
  10  8.48748E-09  4.34167E-09  1.63313E-08  4.75920E-08  1.85024E+01  7.14410E-01  3.10829E+01  1.74213E+03  5.03979E+00
   9  8.44149E-09  4.32388E-09  1.62054E-08  4.74044E-08  1.84485E+01  7.18773E-01  3.10315E+01  1.73643E+03  5.03107E+00
   8  8.32000E-09  4.31489E-09  1.59136E-08  4.75589E-08  1.82460E+01  7.23051E-01  3.09226E+01  1.72941E+03  4.98337E+00
   7  8.30550E-09  4.25657E-09  1.58520E-08  4.65720E-08  1.83788E+01  7.27245E-01  3.09367E+01  1.71825E+03  5.03370E+00
 
   6  8.12037E-09  4.20554E-09  1.54242E-08  4.61581E-08  1.81932E+01  7.31376E-01  3.08242E+01  1.70553E+03  4.99931E+00
   5  7.69305E-09  4.14865E-09  1.44891E-08  4.63601E-08  1.75690E+01  7.35352E-01  3.05311E+01  1.69079E+03  4.84682E+00
   4  7.18191E-09  3.99882E-09  1.33996E-08  4.52531E-08  1.69998E+01  7.39110E-01  3.02355E+01  1.67058E+03  4.71767E+00
   3  6.14928E-09  3.75154E-09  1.12668E-08  4.45054E-08  1.54582E+01  7.42550E-01  2.95202E+01  1.64709E+03  4.32822E+00
   2  4.45546E-09  3.20337E-09  7.90238E-09  4.23220E-08  1.26189E+01  7.45403E-01  2.82504E+01  1.61402E+03  3.65684E+00
   1  1.44688E-09  1.04219E-09  2.54214E-09  1.44584E-08  7.36844E+00  7.47129E-01  2.56099E+01  1.59008E+03  0.00000E+00
 
 Ave   7.1124E-09   3.9350E-09   1.3515E-08   4.5016E-08   1.6523E+01   7.0208E-01   3.0193E+01   1.7055E+03   4.4211E+00
 A-O       -0.002        0.018        0.002        0.028       -0.015       -0.037        0.003        0.006       -0.018
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  7.41518E+00  7.59856E+00  7.62699E+00  7.45189E+00  1.00324E+00  1.00089E+00  1.03112E+00  1.03118E+00  1.00085E+00
  23  1.15298E+01  1.17400E+01  1.17761E+01  1.15752E+01  9.49423E-01  9.43158E-01  9.39688E-01  9.39819E-01  9.43154E-01
  22  1.39676E+01  1.41893E+01  1.42289E+01  1.40159E+01  1.01562E+00  1.00991E+00  1.00282E+00  1.00304E+00  1.01001E+00
  21  1.58677E+01  1.60961E+01  1.61390E+01  1.59191E+01  9.89735E-01  9.83197E-01  9.77127E-01  9.77358E-01  9.83377E-01
  20  1.67802E+01  1.70046E+01  1.70493E+01  1.68340E+01  9.96058E-01  9.89538E-01  9.83109E-01  9.83345E-01  9.89737E-01
  19  1.72271E+01  1.74453E+01  1.74909E+01  1.72823E+01  1.00780E+00  1.00150E+00  9.94629E-01  9.94869E-01  1.00170E+00
 
  18  1.76406E+01  1.78560E+01  1.79027E+01  1.76968E+01  9.96530E-01  9.89864E-01  9.83774E-01  9.84012E-01  9.90083E-01
  17  1.78523E+01  1.80655E+01  1.81129E+01  1.79093E+01  9.96075E-01  9.89285E-01  9.83421E-01  9.83661E-01  9.89512E-01
  16  1.80534E+01  1.82657E+01  1.83137E+01  1.81111E+01  9.88847E-01  9.81820E-01  9.76609E-01  9.76844E-01  9.82053E-01
  15  1.79093E+01  1.81182E+01  1.81664E+01  1.79678E+01  1.01558E+00  1.00909E+00  1.00293E+00  1.00317E+00  1.00930E+00
  14  1.81896E+01  1.84018E+01  1.84509E+01  1.82486E+01  9.91711E-01  9.84520E-01  9.79790E-01  9.80022E-01  9.84752E-01
  13  1.82347E+01  1.84482E+01  1.84979E+01  1.82944E+01  9.93273E-01  9.85996E-01  9.81466E-01  9.81696E-01  9.86227E-01
 
  12  1.80771E+01  1.82900E+01  1.83399E+01  1.81375E+01  1.01427E+00  1.00737E+00  1.00222E+00  1.00245E+00  1.00758E+00
  11  1.82957E+01  1.85141E+01  1.85649E+01  1.83568E+01  9.91352E-01  9.83751E-01  9.79987E-01  9.80210E-01  9.83981E-01
  10  1.83398E+01  1.85628E+01  1.86144E+01  1.84017E+01  9.87649E-01  9.79769E-01  9.76449E-01  9.76668E-01  9.80006E-01
   9  1.82819E+01  1.85086E+01  1.85606E+01  1.83444E+01  9.92884E-01  9.85008E-01  9.81832E-01  9.82047E-01  9.85233E-01
   8  1.80719E+01  1.83024E+01  1.83546E+01  1.81351E+01  1.01319E+00  1.00564E+00  1.00183E+00  1.00205E+00  1.00585E+00
   7  1.82069E+01  1.84486E+01  1.85018E+01  1.82707E+01  9.88076E-01  9.79720E-01  9.77358E-01  9.77562E-01  9.79945E-01
 
   6  1.80175E+01  1.82685E+01  1.83222E+01  1.80817E+01  9.88106E-01  9.79582E-01  9.77571E-01  9.77767E-01  9.79800E-01
   5  1.73832E+01  1.76403E+01  1.76935E+01  1.74474E+01  1.01649E+00  1.00855E+00  1.00570E+00  1.00589E+00  1.00872E+00
   4  1.68160E+01  1.70872E+01  1.71400E+01  1.68792E+01  9.92490E-01  9.83927E-01  9.82592E-01  9.82774E-01  9.84099E-01
   3  1.52745E+01  1.55483E+01  1.55986E+01  1.53351E+01  9.90126E-01  9.81699E-01  9.80939E-01  9.81107E-01  9.81815E-01
   2  1.24415E+01  1.26922E+01  1.27361E+01  1.24955E+01  9.60556E-01  9.52620E-01  9.53804E-01  9.53920E-01  9.52640E-01
   1  7.21805E+00  7.40352E+00  7.43228E+00  7.25242E+00  1.03762E+00  1.03722E+00  1.06888E+00  1.06892E+00  1.03724E+00
 
 Ave   1.6363E+01   1.6589E+01   1.6636E+01   1.6420E+01   9.9653E-01   9.8973E-01   9.8857E-01   9.8877E-01   9.8990E-01
 A-O       -0.015       -0.015       -0.016       -0.015       -0.001       -0.001       -0.002       -0.002       -0.001
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   75
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  5 c1c24 HFP NOMINAL DEPLETION                                                     2211.31 ppm  125.000 EFPD    

 Cycle exposure at end of this step = 125.00 EFPD                                                    

 Depletion step used = 25.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   76
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  5 c1c24 HFP NOMINAL DEPLETION                                                     2211.31 ppm  125.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =   125.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    17.398 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9984340     0.00156599     5.0626E-02     6.0670E-03     1.92158     0.004    2057.00    
     2  2  1.0003534    -0.00035340     2.0793E-02     8.9294E-04     1.96239    -0.010    2069.44    
     3  3  1.0000141    -0.00001412 *   7.5478E-03     4.2661E-04 *   1.94769    -0.005    2077.60    
     4  4  1.0000100 *  -0.00000996 *   1.5781E-03     1.3672E-05 *   1.95076    -0.005    2073.27    
     5  5  0.9999976 *   0.00000240 *   5.6074E-04     6.4470E-05 *   1.94967    -0.005    2073.86 *  
     6  6  1.0000011 *  -0.00000115 *   2.6476E-05 *   1.5363E-05 *   1.94962    -0.005    2073.52 *  

    Nodal Solution Time = 0.61 Sec

 Boron (ppm)  = 2073.518
 Axial Offset =   -0.005
 Max. Node-Averaged  Peaking Factor = 1.950 in Node ( 8,14, 7)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  0.875  0.593  0.978  0.886  1.605  1.295  1.694  1.520   08
  9  0.593  0.580  0.675  0.706  1.183  1.691  1.235  1.457   09
 10  0.978  0.653  1.292  1.033  1.521  1.197  1.554  1.388   10
 11  0.886  0.931  1.051  1.482  1.056  0.783  0.990  1.156   11
 12  1.605  1.197  1.542  1.064  1.405  0.988  1.068          12
 13  1.295  1.679  1.202  0.788  0.988  1.264  1.040          13
 14  1.694  1.190  1.526  0.982  1.066  1.039                 14
 15  1.520  1.441  1.359  1.142                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8   9,10  15,13  14,13  15,13  15,13  14,13  13, 4  13, 3   08
  9  15,13  17,17  17,17  17,17   5,15   4,13   5, 4   1, 1   09
 10  14,13  17,17  17,17  15,13   5,14   3, 5   4, 5   5, 3   10
 11  15,13  17,17  13,14  14, 5   5, 3   1, 1   3, 5   1, 1   11
 12  15,13  15, 5  13, 4   3, 5   5, 4  13, 3  13, 3          12
 13  14,13  13, 4   5, 3   1, 1   3,13   4, 5   1, 1          13
 14  13, 4   3, 5   5, 4   5, 3   3,13   1, 1                 14
 15  13, 3   1, 1   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  2.877 32.074  3.526 33.715  6.603 35.062  7.207  6.430   08
  9 32.074 32.903 53.929 21.164 34.920  7.127 35.230  6.151   09
 10  3.526 55.719  4.869 33.951  6.149 33.971  6.449  5.731   10
 11 33.715 33.917 34.014  5.849 34.987 55.633 32.356  4.581   11
 12  6.603 34.967  6.243 35.009  5.491 33.146  4.050          12
 13 35.062  7.052 33.757 55.642 33.128  4.801  3.907          13
 14  7.207 35.868  6.305 32.268  4.039  3.901                 14
 15  6.430  6.023  5.576  4.495                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   77
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  5 c1c24 HFP NOMINAL DEPLETION                                                     2073.52 ppm  125.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  2.713 30.850 30.857  2.956  3.239 31.143 32.363  5.351  6.167 30.135 33.651  6.841  6.640  5.865  4.121    1
  2 30.845 29.659 31.152 51.403 47.843 12.111 16.007 28.859 33.360  6.579  6.760 34.290 33.991  5.644  4.022    2
  3 30.865 30.910 31.697 47.730 44.400 15.698 19.607 28.838 33.058  6.409  6.547 34.040 33.249  5.434  3.868    3
  4  2.987 49.549 53.605  3.522  3.946 31.957 32.230  5.355  5.808 26.140 29.972  6.079  5.924  5.231  3.651    4
  5  3.303 48.113 52.518  4.028  4.563 32.227 32.525  5.463  5.451 28.798 31.477  5.445  5.407  4.741  3.268    5
  6 31.200 32.281 32.510 32.108 32.310  5.366  5.496 33.505 32.387 53.493 50.287 23.025 28.972  3.889  2.658    6
  7 32.420 32.998 32.781 32.429 32.654  5.536  5.523 33.755 32.764 52.766 49.241 25.693 30.701  2.817  1.789    7
  8  5.426 29.034 29.077  5.540  5.570 33.583 33.789  5.183  4.889 27.397 31.526  3.726  2.993                  8
  9  6.201 33.449 33.172  5.904  5.516 32.437 32.787  4.891  4.892 28.156 31.751  3.701  2.668                  9
 10 30.111  6.582  6.416 26.033 28.700 53.500 52.761 27.378 28.153  4.529  4.186  3.482  2.315                 10
 11 33.625  6.700  6.470 29.812 31.284 50.289 49.231 31.492 31.735  4.183  3.717  2.767  1.656                 11
 12  6.800 33.281 34.214  5.950  5.355 22.920 25.627  3.707  3.691  3.476  2.765                               12
 13  6.590 34.365 34.603  5.768  5.296 28.838 30.619  2.971  2.657  2.309  1.654                               13
 14  5.820  5.526  5.282  5.092  4.638  3.820  2.776                                                           14
 15  4.094  3.947  3.771  3.559  3.195  2.607  1.759                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  0.822  0.556  0.561  0.852  0.910  0.703  0.811  1.341  1.509  1.243  1.235  1.617  1.562  1.381  0.981    1
  2  0.556  0.541  0.534  0.495  0.554  0.561  0.641  1.002  1.115  1.587  1.615  1.177  1.120  1.339  0.962    2
  3  0.563  0.537  0.540  0.534  0.603  0.575  0.644  1.005  1.103  1.555  1.574  1.143  1.090  1.300  0.933    3
  4  0.859  0.513  0.519  0.967  1.064  0.820  0.895  1.355  1.445  1.146  1.119  1.473  1.428  1.262  0.890    4
  5  0.924  0.572  0.585  1.082  1.206  0.932  0.979  1.385  1.372  0.997  0.970  1.341  1.323  1.161  0.809    5
  6  0.711  0.714  0.751  0.847  0.947  1.379  1.404  1.007  0.931  0.718  0.714  0.925  0.864  0.975  0.670    6
  7  0.821  0.830  0.859  0.930  1.000  1.413  1.411  0.994  0.902  0.683  0.659  0.794  0.695  0.726  0.461    7
  8  1.358  1.031  1.042  1.397  1.409  1.015  0.996  1.336  1.269  0.891  0.790  0.985  0.792                  8
  9  1.517  1.131  1.123  1.468  1.388  0.937  0.904  1.270  1.276  0.934  0.833  0.984  0.715                  9
 10  1.244  1.590  1.559  1.151  1.002  0.719  0.684  0.892  0.934  1.196  1.110  0.930  0.626                 10
 11  1.231  1.604  1.562  1.111  0.966  0.712  0.657  0.789  0.833  1.110  0.990  0.746  0.452                 11
 12  1.610  1.143  1.092  1.450  1.326  0.918  0.791  0.983  0.983  0.929  0.746                               12
 13  1.552  1.070  1.025  1.400  1.304  0.855  0.690  0.789  0.713  0.625  0.452                               13
 14  1.372  1.317  1.272  1.237  1.143  0.964  0.720                                                           14
 15  0.976  0.948  0.915  0.874  0.796  0.662  0.456                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 2.051 in Node (14, 8, 8)

    Pin Power Reconstruction Time = 0.28 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  0.822  0.559  0.886  0.762  1.431  1.238  1.585  1.178   08
  9  0.559  0.538  0.547  0.605  1.056  1.583  1.133  1.134   09
 10  0.886  0.547  1.080  0.907  1.389  1.058  1.391  1.030   10
 11  0.762  0.788  0.931  1.402  0.958  0.693  0.819  0.708   11
 12  1.431  1.082  1.415  0.963  1.287  0.862  0.869          12
 13  1.238  1.579  1.057  0.693  0.862  1.102  0.688          13
 14  1.585  1.083  1.370  0.814  0.867  0.688                 14
 15  1.178  1.113  1.012  0.700                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.154  0.935  1.160  0.926  1.127  0.943  1.117  1.138   08
  9  0.935  0.934  0.854  0.837  0.949  1.118  0.934  1.141   09
 10  1.160  0.839  1.149  0.941  1.130  0.960  1.128  1.148   10
 11  0.926  0.940  0.939  1.130  0.940  0.834  0.975  1.167   11
 12  1.127  0.947  1.128  0.940  1.125  0.958  1.159          12
 13  0.943  1.118  0.961  0.834  0.958  1.147  1.170          13
 14  1.117  0.910  1.130  0.976  1.159  1.170                 14
 15  1.138  1.143  1.149  1.168                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8  2.714 30.854  3.121 31.782  5.787 31.880  6.717  4.975   08
  9 30.854 30.855 47.844 15.856 31.029  6.573 33.893  4.742   09
 10  3.121 50.947  4.015 32.235  5.519 29.097  5.713  4.223   10
 11 31.782 32.642 32.375  5.480 33.103 51.447 27.097  2.788   11
 12  5.787 31.183  5.633 33.149  4.964 29.707  3.272          12
 13 31.880  6.542 28.957 51.445 29.690  4.154  2.555          13
 14  6.717 34.116  5.592 27.001  3.257  2.551                 14
 15  4.975  4.631  4.121  2.741                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  32.93   0.00   09
 10   0.00   0.00   0.00  31.27   0.00  28.31   0.00   0.00   10
 11   0.00  31.66  31.41   0.00  32.13   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  32.17   0.00   0.00   0.00          12
 13   0.00   0.00  28.17   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   78
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  5 c1c24 HFP NOMINAL DEPLETION                                                     2073.52 ppm  125.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97             6.72 25H-14 sil86  (14, 8,  )       2.55  25C-14  sil89  (14,13,  )         4.55
        6    26A        16            34.12 24L-03 24L-03 (14, 9,  )      30.85  24K-08  24K-08 ( 8, 9,  )        31.90
        8    26C        48            33.89 24J-07 24J-07 ( 9,14,  )      27.00  24L-02  24L-02 (14,11,  )        30.48
        5    25C        16            51.45 24N-07 24N-07 (11,13,  )      47.84  24K-06  24K-06 ( 9,10,  )        50.42
        9    26D         4            15.86 24N-03 24N-03 ( 9,11,  )      15.86  24N-03  24N-03 ( 9,11,  )        15.86
        7    26B        12            32.64 24L-05 24L-05 (11, 9,  )      32.23  24L-07  24L-07 (10,11,  )        32.42

     CORE              193            51.45 24N-07 24N-07 (11,13,  )       2.55  25C-14  sil89  (14,13,  )        17.40


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97             7.21 25B-08 sil51  ( 1,12,  )
        6    26A        16            35.87 24L-03 24L-03 (13, 3,  )
        8    26C        48            35.23 24J-07 24J-07 ( 2,12,  )
        5    25C        16            55.72 24L-04 24L-04 ( 4, 3,  )
        9    26D         4            21.16 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            34.01 24J-05 24J-05 ( 7, 5,  )

     CORE              193            55.72 24L-04 24L-04 ( 4, 3,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   79
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  5 c1c24 HFP NOMINAL DEPLETION                                                     2073.52 ppm  125.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  17.398 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp. 125.0 EFPD  3000.0 EFPH   4.374 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  6.000E+02 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  0.875 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  9.262E-05  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     1.950 ( 8,14, 7)
       Fuel    Average . . .TFUAVE  871.44 K      1108.92 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  580.70 K       585.58 F                  F-delta-H       1.694 ( 8,14)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.813 ( 8,14, 3)   [Off]
 Boron Conc. . . . . . . . . . BOR  2073.5 ppm                               Max-3PIN        2.045 (14, 8, 7)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        2.051 (14, 8, 8)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O  -0.005                                 Total Neutron Leakage . . . . . . . . .  0.040
 Edit Ring -     1      2      3      4      5
 RPF            0.579  0.745  1.203  1.045  0.696
 KIN            0.959  0.958  1.018  1.053  1.169
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.727  0.720  0.699  0.706  0.721
 TFU (Deg K)    723.0  785.9  930.5  889.1  795.0
 TMO (Deg K)    573.0  576.0  584.2  581.6  575.6
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.23130   0.92685   7.82890   0.02288   0.66613   664.302   597.373
  23   0.64186   1.09432  12.20630   0.00000   0.66781   777.802   596.816
  22   0.85682   1.06812  14.83335   0.00000   0.67068   840.969   595.852
  21   1.00945   1.05469  16.86606   0.00000   0.67421   887.011   594.638
  20   1.08648   1.04609  17.85093   0.00000   0.67814   909.450   593.256
  19   1.12428   1.04076  18.33668   0.00000   0.68223   919.493   591.777
 
  18   1.15161   1.03924  18.77502   0.00000   0.68639   926.267   590.232
  17   1.16259   1.03713  18.99942   0.00000   0.69057   927.880   588.640
  16   1.17110   1.03636  19.20965   0.00000   0.69473   928.746   587.013
  15   1.15953   1.03460  19.06414   0.00000   0.69883   923.264   585.365
  14   1.17117   1.03514  19.35208   0.00000   0.70289   925.196   583.698
  13   1.17111   1.03395  19.39983   0.00000   0.70692   923.427   582.002
 
  12   1.16098   1.03311  19.23993   0.00000   0.71088   918.493   580.294
  11   1.17208   1.03403  19.46494   0.00000   0.71480   920.292   578.566
  10   1.17418   1.03362  19.51126   0.00000   0.71870   919.276   576.810
   9   1.17163   1.03384  19.45352   0.00000   0.72255   916.835   575.036
   8   1.15995   1.03272  19.23874   0.00000   0.72634   911.594   573.256
   7   1.16453   1.03427  19.37268   0.00000   0.73007   911.561   571.464
 
   6   1.14705   1.03575  19.16860   0.00000   0.73374   904.688   569.665
   5   1.09696   1.03825  18.49743   0.00000   0.73727   887.735   567.905
   4   1.03561   1.04586  17.87157   0.00000   0.74059   867.560   566.218
   3   0.89861   1.05943  16.20975   0.00000   0.74357   824.154   564.678
   2   0.66152   1.08564  13.16809   0.00000   0.74596   751.589   563.427
   1   0.21959   0.89243   7.63000   0.00000   0.74729   626.902   562.717
 
 Ave   1.00000   1.03359  17.39814   0.00095   0.70797   871.437   580.696
 P**2                     14.34376
 A-O  -0.00522   0.00356  -0.01483   1.00000  -0.03250     0.009     0.017
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   80
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  5 c1c24 HFP NOMINAL DEPLETION                                                     2073.52 ppm  125.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  1.64498E-09  1.13370E-09  2.98137E-09  1.50269E-08  7.82890E+00  6.52886E-01  2.64126E+01  1.57768E+03  0.00000E+00
  23  4.56252E-09  3.41388E-09  8.28469E-09  4.68153E-08  1.22063E+01  6.56837E-01  2.85990E+01  1.63448E+03  3.44185E+00
  22  6.09267E-09  4.00814E-09  1.14226E-08  5.01382E-08  1.48333E+01  6.60720E-01  2.96150E+01  1.67296E+03  4.03849E+00
  21  7.17990E-09  4.25044E-09  1.37500E-08  5.00513E-08  1.68661E+01  6.64854E-01  3.03697E+01  1.69298E+03  4.55755E+00
  20  7.72902E-09  4.36930E-09  1.49567E-08  5.03980E-08  1.78509E+01  6.69328E-01  3.07162E+01  1.70861E+03  4.80252E+00
  19  7.99852E-09  4.42857E-09  1.55639E-08  5.07157E-08  1.83367E+01  6.73931E-01  3.08662E+01  1.72075E+03  4.91584E+00
 
  18  8.19286E-09  4.43412E-09  1.60096E-08  5.03196E-08  1.87750E+01  6.78568E-01  3.09877E+01  1.72864E+03  5.02120E+00
  17  8.27095E-09  4.43149E-09  1.61969E-08  5.01401E-08  1.89994E+01  6.83221E-01  3.10275E+01  1.73418E+03  5.07194E+00
  16  8.33124E-09  4.41392E-09  1.63416E-08  4.97410E-08  1.92096E+01  6.87856E-01  3.10603E+01  1.73750E+03  5.12198E+00
  15  8.24906E-09  4.42272E-09  1.61612E-08  5.02034E-08  1.90641E+01  6.92446E-01  3.09708E+01  1.74038E+03  5.07788E+00
  14  8.33153E-09  4.38331E-09  1.63465E-08  4.93239E-08  1.93521E+01  6.96956E-01  3.10314E+01  1.73952E+03  5.15349E+00
  13  8.33101E-09  4.36789E-09  1.63394E-08  4.90943E-08  1.93998E+01  7.01460E-01  3.10124E+01  1.73869E+03  5.16500E+00
 
  12  8.25913E-09  4.37029E-09  1.61651E-08  4.93603E-08  1.92399E+01  7.05896E-01  3.09259E+01  1.73752E+03  5.12162E+00
  11  8.33779E-09  4.33223E-09  1.63230E-08  4.84839E-08  1.94649E+01  7.10263E-01  3.09748E+01  1.73317E+03  5.18409E+00
  10  8.35270E-09  4.31214E-09  1.63324E-08  4.80900E-08  1.95113E+01  7.14613E-01  3.09648E+01  1.72829E+03  5.20008E+00
   9  8.33460E-09  4.30011E-09  1.62624E-08  4.79112E-08  1.94535E+01  7.18913E-01  3.09223E+01  1.72262E+03  5.19050E+00
   8  8.25174E-09  4.29931E-09  1.60441E-08  4.80876E-08  1.92387E+01  7.23138E-01  3.08267E+01  1.71573E+03  5.14096E+00
   7  8.28406E-09  4.24927E-09  1.60776E-08  4.70897E-08  1.93727E+01  7.27286E-01  3.08534E+01  1.70447E+03  5.19067E+00
 
   6  8.15945E-09  4.21013E-09  1.57620E-08  4.66844E-08  1.91686E+01  7.31383E-01  3.07594E+01  1.69175E+03  5.15324E+00
   5  7.80270E-09  4.16995E-09  1.49429E-08  4.69125E-08  1.84974E+01  7.35340E-01  3.04906E+01  1.67707E+03  4.99410E+00
   4  7.36498E-09  4.03703E-09  1.39673E-08  4.57912E-08  1.78716E+01  7.39090E-01  3.02182E+01  1.65648E+03  4.85620E+00
   3  6.38868E-09  3.81278E-09  1.18827E-08  4.50551E-08  1.62098E+01  7.42535E-01  2.95257E+01  1.63226E+03  4.44904E+00
   2  4.69968E-09  3.28983E-09  8.43729E-09  4.29550E-08  1.31681E+01  7.45396E-01  2.82701E+01  1.59585E+03  3.74706E+00
   1  1.56098E-09  1.08345E-09  2.77033E-09  1.46190E-08  7.63000E+00  7.47127E-01  2.56230E+01  1.56409E+03  0.00000E+00
 
 Ave   7.1129E-09   3.9385E-09   1.3722E-08   4.5542E-08   1.7398E+01   7.0250E-01   3.0126E+01   1.6911E+03   4.5595E+00
 A-O       -0.005        0.017       -0.002        0.028       -0.015       -0.037        0.003        0.007       -0.017
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  7.70057E+00  7.86629E+00  7.89467E+00  7.73727E+00  1.00284E+00  1.00053E+00  1.03014E+00  1.03022E+00  1.00050E+00
  23  1.20823E+01  1.22598E+01  1.22956E+01  1.21273E+01  9.49333E-01  9.43698E-01  9.39405E-01  9.39563E-01  9.43735E-01
  22  1.47092E+01  1.48885E+01  1.49276E+01  1.47571E+01  1.01583E+00  1.01081E+00  1.00273E+00  1.00299E+00  1.01095E+00
  21  1.67461E+01  1.69243E+01  1.69666E+01  1.67972E+01  9.89719E-01  9.83902E-01  9.76871E-01  9.77136E-01  9.84120E-01
  20  1.77300E+01  1.79004E+01  1.79445E+01  1.77833E+01  9.96093E-01  9.90289E-01  9.82906E-01  9.83175E-01  9.90526E-01
  19  1.82133E+01  1.83761E+01  1.84212E+01  1.82682E+01  1.00794E+00  1.00233E+00  9.94511E-01  9.94783E-01  1.00257E+00
 
  18  1.86540E+01  1.88123E+01  1.88584E+01  1.87098E+01  9.96562E-01  9.90591E-01  9.83596E-01  9.83866E-01  9.90845E-01
  17  1.88777E+01  1.90331E+01  1.90799E+01  1.89343E+01  9.96105E-01  9.89999E-01  9.83255E-01  9.83525E-01  9.90259E-01
  16  1.90881E+01  1.92419E+01  1.92893E+01  1.91454E+01  9.88813E-01  9.82460E-01  9.76398E-01  9.76663E-01  9.82725E-01
  15  1.89346E+01  1.90861E+01  1.91338E+01  1.89926E+01  1.01577E+00  1.00991E+00  1.00288E+00  1.00315E+00  1.01015E+00
  14  1.92261E+01  1.93796E+01  1.94281E+01  1.92847E+01  9.91709E-01  9.85168E-01  9.79612E-01  9.79872E-01  9.85432E-01
  13  1.92713E+01  1.94261E+01  1.94752E+01  1.93306E+01  9.93272E-01  9.86634E-01  9.81287E-01  9.81546E-01  9.86897E-01
 
  12  1.91041E+01  1.92594E+01  1.93087E+01  1.91641E+01  1.01443E+00  1.00815E+00  1.00216E+00  1.00242E+00  1.00839E+00
  11  1.93322E+01  1.94916E+01  1.95419E+01  1.93929E+01  9.91322E-01  9.84344E-01  9.79782E-01  9.80032E-01  9.84606E-01
  10  1.93771E+01  1.95409E+01  1.95919E+01  1.94384E+01  9.87587E-01  9.80331E-01  9.76222E-01  9.76469E-01  9.80600E-01
   9  1.93151E+01  1.94829E+01  1.95344E+01  1.93771E+01  9.92857E-01  9.85592E-01  9.81622E-01  9.81865E-01  9.85850E-01
   8  1.90923E+01  1.92650E+01  1.93167E+01  1.91550E+01  1.01332E+00  1.00636E+00  1.00173E+00  1.00197E+00  1.00660E+00
   7  1.92289E+01  1.94117E+01  1.94644E+01  1.92922E+01  9.88009E-01  9.80257E-01  9.77109E-01  9.77341E-01  9.80516E-01
 
   6  1.90205E+01  1.92135E+01  1.92666E+01  1.90842E+01  9.88032E-01  9.80104E-01  9.77307E-01  9.77533E-01  9.80357E-01
   5  1.83377E+01  1.85399E+01  1.85925E+01  1.84015E+01  1.01661E+00  1.00923E+00  1.00556E+00  1.00578E+00  1.00945E+00
   4  1.77127E+01  1.79311E+01  1.79834E+01  1.77755E+01  9.92438E-01  9.84432E-01  9.82317E-01  9.82529E-01  9.84645E-01
   3  1.60478E+01  1.62756E+01  1.63254E+01  1.61080E+01  9.90101E-01  9.82198E-01  9.80688E-01  9.80888E-01  9.82357E-01
   2  1.30067E+01  1.32233E+01  1.32669E+01  1.30605E+01  9.60317E-01  9.52819E-01  9.53360E-01  9.53500E-01  9.52883E-01
   1  7.48638E+00  7.65576E+00  7.68451E+00  7.52077E+00  1.03728E+00  1.03670E+00  1.06793E+00  1.06798E+00  1.03674E+00
 
 Ave   1.7263E+01   1.7437E+01   1.7484E+01   1.7319E+01   9.9651E-01   9.9029E-01   9.8831E-01   9.8853E-01   9.9049E-01
 A-O       -0.014       -0.015       -0.015       -0.014       -0.001       -0.001       -0.002       -0.002       -0.001
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   81
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  6 c1c24 HFP NOMINAL DEPLETION                                                     2073.52 ppm  150.000 EFPD    

 Cycle exposure at end of this step = 150.00 EFPD                                                    

 Depletion step used = 25.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   82
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  6 c1c24 HFP NOMINAL DEPLETION                                                     2073.52 ppm  150.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =   150.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    18.273 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9984486     0.00155144     4.3432E-02     5.9514E-03     1.86847     0.002    1922.27    
     2  2  1.0003401    -0.00034006     1.6734E-02     1.1364E-03     1.90027    -0.013    1933.85    
     3  3  1.0000182    -0.00001819 *   7.1775E-03     5.7391E-04     1.88673    -0.008    1942.50    
     4  4  1.0000094 *  -0.00000943 *   1.4879E-03     1.5944E-06 *   1.88954    -0.008    1938.21    
     5  5  0.9999974 *   0.00000259 *   5.0952E-04     4.9949E-05 *   1.88857    -0.008    1938.93 *  
     6  6  1.0000012 *  -0.00000124 *   3.3455E-05 *   1.4775E-05 *   1.88851    -0.008    1938.54 *  

    Nodal Solution Time = 0.61 Sec

 Boron (ppm)  = 1938.544
 Axial Offset =   -0.008
 Max. Node-Averaged  Peaking Factor = 1.889 in Node ( 8,14, 7)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  0.928  0.626  1.011  0.893  1.582  1.273  1.655  1.482   08
  9  0.626  0.604  0.690  0.714  1.171  1.657  1.211  1.424   09
 10  1.011  0.667  1.304  1.037  1.508  1.186  1.529  1.365   10
 11  0.893  0.934  1.054  1.478  1.058  0.785  0.989  1.147   11
 12  1.582  1.184  1.528  1.065  1.404  0.998  1.081          12
 13  1.273  1.646  1.191  0.790  0.998  1.273  1.052          13
 14  1.655  1.169  1.504  0.982  1.080  1.051                 14
 15  1.482  1.409  1.340  1.135                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8   9,10  15,13  14,13  15,13  14,13   4,13  13, 4  13, 3   08
  9  15,13  17,17  17,17  17,17   5,15   4,13   5, 4   1, 1   09
 10  14,13  17,17  17,17  15,13   5,14   3, 5   4, 5   5, 3   10
 11  15,13  17,17  13,14  14, 5   5, 4   1, 1   3, 5   1, 1   11
 12  14,13  15, 5  13, 4   4, 5   5, 4  13, 3  13, 3          12
 13   4,13  13, 4   5, 3   1, 1   3,13   4, 5   1, 1          13
 14  13, 4   3, 5   5, 4   5, 3   3,13   1, 1                 14
 15  13, 3   1, 1   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  3.580 32.665  4.310 34.583  7.860 36.315  8.527  7.605   08
  9 32.665 33.471 54.446 21.835 36.078  8.448 36.388  7.287   09
 10  4.310 56.255  5.882 35.011  7.343 34.847  7.662  6.809   10
 11 34.583 34.800 35.079  7.018 36.023 56.355 33.042  5.477   11
 12  7.860 36.134  7.455 36.045  6.600 34.005  4.903          12
 13 36.315  8.363 34.628 56.366 33.988  5.799  4.731          13
 14  8.527 36.906  7.501 32.949  4.892  4.724                 14
 15  7.605  7.141  6.633  5.381                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   83
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  6 c1c24 HFP NOMINAL DEPLETION                                                     1938.54 ppm  150.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  3.380 31.416 31.427  3.641  3.967 31.846 33.168  6.404  7.347 31.352 34.857  8.098  7.853  6.938  4.885    1
  2 31.411 30.210 31.694 51.904 48.400 12.674 16.645 29.848 34.455  7.816  8.018 35.440 35.085  6.685  4.771    2
  3 31.436 31.456 32.244 48.268 45.005 16.274 20.248 29.830 34.142  7.623  7.774 35.158 34.315  6.446  4.595    3
  4  3.678 50.068 54.128  4.293  4.792 32.773 33.117  6.420  6.940 27.265 31.070  7.228  7.037  6.216  4.347    4
  5  4.043 48.688 53.105  4.887  5.517 33.151 33.493  6.551  6.528 29.780 32.431  6.494  6.441  5.648  3.901    5
  6 31.911 32.994 33.258 32.949 33.248  6.452  6.601 34.499 33.307 54.204 50.993 23.937 29.823  4.654  3.184    6
  7 33.235 33.821 33.633 33.350 33.642  6.647  6.632 34.736 33.656 53.444 49.895 26.480 31.389  3.390  2.153    7
  8  6.492 30.051 30.104  6.637  6.677 34.585 34.773  6.234  5.889 28.281 32.311  4.505  3.620                  8
  9  7.387 34.560 34.275  7.054  6.605 33.363 33.681  5.892  5.898 29.082 32.578  4.480  3.235                  9
 10 31.329  7.822  7.633 27.163 29.687 54.213 53.440 28.262 29.079  5.474  5.064  4.218  2.812                 10
 11 34.828  7.950  7.688 30.902 32.235 50.993 49.884 32.276 32.562  5.061  4.501  3.359  2.016                 11
 12  8.052 34.398 35.283  7.083  6.394 23.827 26.411  4.485  4.469  4.212  3.357                               12
 13  7.796 35.411 35.607  6.861  6.316 29.682 31.303  3.596  3.223  2.806  2.013                               13
 14  6.886  6.551  6.274  6.057  5.533  4.577  3.344                                                           14
 15  4.854  4.685  4.485  4.242  3.819  3.127  2.119                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  0.873  0.592  0.594  0.890  0.942  0.722  0.822  1.335  1.490  1.224  1.212  1.579  1.522  1.347  0.961    1
  2  0.592  0.575  0.565  0.520  0.576  0.580  0.654  1.003  1.105  1.559  1.581  1.155  1.098  1.309  0.943    2
  3  0.596  0.568  0.568  0.556  0.624  0.592  0.656  1.006  1.094  1.531  1.545  1.124  1.071  1.274  0.917    3
  4  0.898  0.538  0.540  0.993  1.085  0.834  0.903  1.351  1.433  1.135  1.106  1.449  1.403  1.240  0.878    4
  5  0.956  0.593  0.603  1.102  1.219  0.941  0.983  1.381  1.365  0.995  0.965  1.326  1.305  1.146  0.802    5
  6  0.730  0.731  0.766  0.859  0.954  1.381  1.403  1.009  0.934  0.723  0.718  0.925  0.862  0.970  0.669    6
  7  0.831  0.839  0.867  0.936  1.003  1.411  1.408  0.996  0.907  0.692  0.667  0.802  0.700  0.730  0.464    7
  8  1.352  1.030  1.041  1.391  1.404  1.016  0.999  1.337  1.273  0.901  0.801  0.996  0.802                  8
  9  1.497  1.121  1.114  1.455  1.380  0.940  0.910  1.274  1.281  0.943  0.844  0.996  0.726                  9
 10  1.225  1.562  1.535  1.141  0.999  0.725  0.692  0.901  0.943  1.206  1.121  0.941  0.637                 10
 11  1.209  1.572  1.534  1.099  0.962  0.716  0.666  0.800  0.844  1.121  1.001  0.758  0.462                 11
 12  1.572  1.123  1.076  1.429  1.314  0.920  0.799  0.994  0.995  0.941  0.758                               12
 13  1.514  1.050  1.010  1.378  1.289  0.855  0.696  0.799  0.725  0.636  0.462                               13
 14  1.339  1.289  1.249  1.218  1.131  0.961  0.724                                                           14
 15  0.956  0.930  0.901  0.864  0.791  0.662  0.460                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 1.989 in Node ( 8,14, 6)

    Pin Power Reconstruction Time = 0.28 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  0.873  0.593  0.922  0.776  1.419  1.217  1.547  1.151   08
  9  0.593  0.569  0.569  0.620  1.052  1.554  1.112  1.111   09
 10  0.922  0.569  1.100  0.915  1.383  1.050  1.371  1.016   10
 11  0.776  0.801  0.938  1.401  0.962  0.700  0.822  0.708   11
 12  1.419  1.077  1.407  0.966  1.291  0.872  0.880          12
 13  1.217  1.551  1.050  0.700  0.872  1.113  0.700          13
 14  1.547  1.065  1.352  0.817  0.878  0.699                 14
 15  1.151  1.092  1.001  0.702                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.155  0.939  1.161  0.929  1.125  0.944  1.114  1.139   08
  9  0.939  0.939  0.859  0.844  0.951  1.115  0.935  1.142   09
 10  1.161  0.844  1.148  0.943  1.128  0.963  1.127  1.149   10
 11  0.929  0.943  0.942  1.128  0.942  0.839  0.978  1.171   11
 12  1.125  0.949  1.127  0.942  1.123  0.961  1.161          12
 13  0.944  1.116  0.963  0.839  0.961  1.147  1.173          13
 14  1.114  0.912  1.128  0.979  1.161  1.173                 14
 15  1.139  1.143  1.150  1.171                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8  3.380 31.423  3.832 32.540  6.908 33.091  7.949  5.891   08
  9 31.423 31.401 48.394 16.460 32.069  7.808 34.999  5.624   09
 10  3.832 51.497  4.872 33.133  6.610 30.137  6.800  5.028   10
 11 32.540 33.426 33.297  6.583 34.050 52.134 27.907  3.345   11
 12  6.908 32.248  6.743 34.101  5.978 30.563  3.960          12
 13 33.091  7.773 29.997 52.133 30.545  5.025  3.101          13
 14  7.949 35.175  6.663 27.806  3.943  3.097                 14
 15  5.891  5.499  4.913  3.292                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  34.01   0.00   09
 10   0.00   0.00   0.00  32.15   0.00  29.33   0.00   0.00   10
 11   0.00  32.43  32.31   0.00  33.05   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  33.10   0.00   0.00   0.00          12
 13   0.00   0.00  29.19   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   84
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  6 c1c24 HFP NOMINAL DEPLETION                                                     1938.54 ppm  150.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97             7.95 25H-14 sil86  (14, 8,  )       3.10  25C-14  sil89  (14,13,  )         5.45
        6    26A        16            35.17 24L-03 24L-03 (14, 9,  )      31.40  24N-05  24N-05 ( 9, 9,  )        32.63
        8    26C        48            35.00 24J-07 24J-07 ( 9,14,  )      27.81  24L-02  24L-02 (14,11,  )        31.46
        5    25C        16            52.13 24N-07 24N-07 (11,13,  )      48.39  24K-06  24K-06 ( 9,10,  )        51.04
        9    26D         4            16.46 24N-03 24N-03 ( 9,11,  )      16.46  24N-03  24N-03 ( 9,11,  )        16.46
        7    26B        12            33.43 24L-05 24L-05 (11, 9,  )      33.13  24L-07  24L-07 (10,11,  )        33.29

     CORE              193            52.13 24N-07 24N-07 (11,13,  )       3.10  25C-14  sil89  (14,13,  )        18.27


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97             8.53 25B-08 sil51  ( 1,12,  )
        6    26A        16            36.91 24L-03 24L-03 (13, 3,  )
        8    26C        48            36.39 24J-07 24J-07 ( 2,12,  )
        5    25C        16            56.37 24J-03 24J-03 (10, 6,  )
        9    26D         4            21.84 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            35.08 24J-05 24J-05 ( 7, 5,  )

     CORE              193            56.37 24J-03 24J-03 (10, 6,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   85
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  6 c1c24 HFP NOMINAL DEPLETION                                                     1938.54 ppm  150.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  18.273 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp. 150.0 EFPD  3600.0 EFPH   5.249 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  6.000E+02 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  0.875 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  9.773E-05  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     1.889 ( 8,14, 7)
       Fuel    Average . . .TFUAVE  869.33 K      1105.12 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  580.75 K       585.67 F                  F-delta-H       1.657 ( 9,13)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.766 ( 8,14, 2)   [Off]
 Boron Conc. . . . . . . . . . BOR  1938.5 ppm                               Max-3PIN        1.982 ( 8,14, 7)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        1.989 ( 8,14, 6)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O  -0.008                                 Total Neutron Leakage . . . . . . . . .  0.040
 Edit Ring -     1      2      3      4      5
 RPF            0.613  0.765  1.196  1.038  0.702
 KIN            0.963  0.961  1.019  1.054  1.172
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.725  0.719  0.700  0.706  0.720
 TFU (Deg K)    733.6  791.7  925.7  884.3  795.5
 TMO (Deg K)    573.6  576.4  584.1  581.5  575.7
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.24430   0.93719   8.12287   0.02288   0.66624   667.646   597.403
  23   0.66382   1.09698  12.76265   0.00000   0.66799   783.573   596.822
  22   0.87526   1.06943  15.57139   0.00000   0.67093   845.538   595.829
  21   1.02068   1.05527  17.73108   0.00000   0.67451   888.678   594.592
  20   1.08949   1.04636  18.77811   0.00000   0.67845   908.257   593.195
  19   1.12005   1.04094  19.29298   0.00000   0.68253   915.880   591.713
 
  18   1.14153   1.03947  19.75212   0.00000   0.68666   920.672   590.174
  17   1.14828   1.03740  19.98408   0.00000   0.69078   920.882   588.594
  16   1.15383   1.03672  20.20029   0.00000   0.69488   920.758   586.986
  15   1.14086   1.03501  20.04435   0.00000   0.69891   914.915   585.362
  14   1.15139   1.03563  20.34174   0.00000   0.70289   916.417   583.719
  13   1.15129   1.03446  20.38943   0.00000   0.70685   914.640   582.051
 
  12   1.14206   1.03365  20.22130   0.00000   0.71074   910.061   580.369
  11   1.15415   1.03461  20.45621   0.00000   0.71460   912.099   578.668
  10   1.15823   1.03420  20.50517   0.00000   0.71844   911.692   576.937
   9   1.15865   1.03442  20.44654   0.00000   0.72224   910.198   575.185
   8   1.15117   1.03325  20.22362   0.00000   0.72600   906.350   573.422
   7   1.16095   1.03481  20.36369   0.00000   0.72971   907.892   571.640
 
   6   1.15051   1.03631  20.14774   0.00000   0.73339   903.286   569.843
   5   1.10907   1.03893  19.43756   0.00000   0.73694   889.214   568.071
   4   1.05726   1.04688  18.76349   0.00000   0.74032   872.141   566.358
   3   0.92837   1.06121  16.98836   0.00000   0.74338   831.755   564.778
   2   0.69342   1.08897  13.74552   0.00000   0.74586   760.237   563.478
   1   0.23537   0.90307   7.91126   0.00000   0.74727   631.075   562.730
 
 Ave   1.00000   1.03522  18.27304   0.00095   0.70794   869.327   580.747
 P**2                     15.46890
 A-O  -0.00827   0.00340  -0.01444   1.00000  -0.03221     0.008     0.017
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   86
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  6 c1c24 HFP NOMINAL DEPLETION                                                     1938.54 ppm  150.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  1.73726E-09  1.16354E-09  3.18033E-09  1.51600E-08  8.12287E+00  6.53548E-01  2.64127E+01  1.55182E+03  0.00000E+00
  23  4.71864E-09  3.47040E-09  8.66422E-09  4.73272E-08  1.27626E+01  6.57704E-01  2.85946E+01  1.61349E+03  3.53689E+00
  22  6.22416E-09  4.04318E-09  1.18237E-08  5.06562E-08  1.55714E+01  6.61619E-01  2.95988E+01  1.65331E+03  4.16227E+00
  21  7.26040E-09  4.26326E-09  1.41026E-08  5.05424E-08  1.77311E+01  6.65736E-01  3.03312E+01  1.67300E+03  4.70043E+00
  20  7.75117E-09  4.36632E-09  1.52186E-08  5.08795E-08  1.87781E+01  6.70177E-01  3.06569E+01  1.68819E+03  4.95444E+00
  19  7.96919E-09  4.41389E-09  1.57336E-08  5.11860E-08  1.92930E+01  6.74735E-01  3.07896E+01  1.69991E+03  5.07183E+00
 
  18  8.12198E-09  4.41007E-09  1.61026E-08  5.07617E-08  1.97521E+01  6.79312E-01  3.08955E+01  1.70737E+03  5.17972E+00
  17  8.16996E-09  4.40124E-09  1.62308E-08  5.05635E-08  1.99841E+01  6.83898E-01  3.09236E+01  1.71258E+03  5.23116E+00
  16  8.20923E-09  4.37941E-09  1.63339E-08  5.01433E-08  2.02003E+01  6.88461E-01  3.09474E+01  1.71561E+03  5.28161E+00
  15  8.11721E-09  4.38672E-09  1.61292E-08  5.06117E-08  2.00444E+01  6.92978E-01  3.08545E+01  1.71833E+03  5.23594E+00
  14  8.19179E-09  4.34568E-09  1.63011E-08  4.97056E-08  2.03417E+01  6.97414E-01  3.09095E+01  1.71729E+03  5.31226E+00
  13  8.19096E-09  4.33050E-09  1.62936E-08  4.94710E-08  2.03894E+01  7.01843E-01  3.08894E+01  1.71637E+03  5.32344E+00
 
  12  8.12549E-09  4.33480E-09  1.61302E-08  4.97494E-08  2.02213E+01  7.06209E-01  3.08056E+01  1.71521E+03  5.27879E+00
  11  8.21121E-09  4.29818E-09  1.63073E-08  4.88573E-08  2.04562E+01  7.10507E-01  3.08551E+01  1.71082E+03  5.34203E+00
  10  8.24019E-09  4.28108E-09  1.63480E-08  4.84636E-08  2.05052E+01  7.14793E-01  3.08495E+01  1.70599E+03  5.35801E+00
   9  8.24321E-09  4.27357E-09  1.63225E-08  4.82942E-08  2.04465E+01  7.19035E-01  3.08146E+01  1.70045E+03  5.34807E+00
   8  8.19030E-09  4.27940E-09  1.61636E-08  4.84922E-08  2.02236E+01  7.23210E-01  3.07304E+01  1.69377E+03  5.29736E+00
   7  8.25948E-09  4.23580E-09  1.62761E-08  4.74864E-08  2.03637E+01  7.27317E-01  3.07680E+01  1.68266E+03  5.34727E+00
 
   6  8.18488E-09  4.20655E-09  1.60577E-08  4.70916E-08  2.01477E+01  7.31383E-01  3.06906E+01  1.67018E+03  5.30786E+00
   5  7.88970E-09  4.18108E-09  1.53455E-08  4.73485E-08  1.94376E+01  7.35322E-01  3.04444E+01  1.65583E+03  5.14333E+00
   4  7.51959E-09  4.06338E-09  1.44818E-08  4.62166E-08  1.87635E+01  7.39065E-01  3.01943E+01  1.63535E+03  4.99802E+00
   3  6.60078E-09  3.86086E-09  1.24568E-08  4.54800E-08  1.69884E+01  7.42514E-01  2.95275E+01  1.61106E+03  4.57426E+00
   2  4.92637E-09  3.36367E-09  8.95271E-09  4.33909E-08  1.37455E+01  7.45386E-01  2.82900E+01  1.57270E+03  3.84195E+00
   1  1.67303E-09  1.12086E-09  3.00026E-09  1.47452E-08  7.91126E+00  7.47125E-01  2.56367E+01  1.53598E+03  0.00000E+00
 
 Ave   7.1136E-09   3.9364E-09   1.3915E-08   4.5943E-08   1.8273E+01   7.0289E-01   3.0059E+01   1.6691E+03   4.6977E+00
 A-O       -0.008        0.016       -0.005        0.028       -0.014       -0.036        0.003        0.007       -0.017
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  8.00274E+00  8.14970E+00  8.17802E+00  8.03943E+00  1.00235E+00  1.00006E+00  1.02905E+00  1.02914E+00  1.00006E+00
  23  1.26550E+01  1.27988E+01  1.28343E+01  1.26998E+01  9.49172E-01  9.44134E-01  9.39044E-01  9.39222E-01  9.44202E-01
  22  1.54682E+01  1.56044E+01  1.56430E+01  1.55158E+01  1.01604E+00  1.01166E+00  1.00264E+00  1.00293E+00  1.01183E+00
  21  1.76359E+01  1.77633E+01  1.78051E+01  1.76866E+01  9.89696E-01  9.84544E-01  9.76615E-01  9.76906E-01  9.84790E-01
  20  1.86835E+01  1.88000E+01  1.88436E+01  1.87364E+01  9.96122E-01  9.90975E-01  9.82710E-01  9.83003E-01  9.91239E-01
  19  1.91966E+01  1.93043E+01  1.93488E+01  1.92510E+01  1.00807E+00  1.00310E+00  9.94403E-01  9.94699E-01  1.00336E+00
 
  18  1.96587E+01  1.97606E+01  1.98061E+01  1.97141E+01  9.96592E-01  9.91254E-01  9.83428E-01  9.83720E-01  9.91530E-01
  17  1.98902E+01  1.99888E+01  2.00350E+01  1.99463E+01  9.96130E-01  9.90643E-01  9.83095E-01  9.83385E-01  9.90925E-01
  16  2.01068E+01  2.02032E+01  2.02500E+01  2.01636E+01  9.88776E-01  9.83032E-01  9.76193E-01  9.76480E-01  9.83319E-01
  15  1.99422E+01  2.00377E+01  2.00847E+01  1.99998E+01  1.01594E+00  1.01067E+00  1.00284E+00  1.00313E+00  1.01093E+00
  14  2.02438E+01  2.03399E+01  2.03878E+01  2.03019E+01  9.91705E-01  9.85751E-01  9.79441E-01  9.79721E-01  9.86035E-01
  13  2.02889E+01  2.03863E+01  2.04348E+01  2.03478E+01  9.93269E-01  9.87206E-01  9.81115E-01  9.81394E-01  9.87490E-01
 
  12  2.01130E+01  2.02118E+01  2.02606E+01  2.01725E+01  1.01459E+00  1.00886E+00  1.00210E+00  1.00238E+00  1.00913E+00
  11  2.03515E+01  2.04532E+01  2.05030E+01  2.04117E+01  9.91291E-01  9.84878E-01  9.79587E-01  9.79858E-01  9.85160E-01
  10  2.03991E+01  2.05049E+01  2.05553E+01  2.04600E+01  9.87525E-01  9.80832E-01  9.76004E-01  9.76271E-01  9.81121E-01
   9  2.03361E+01  2.04460E+01  2.04969E+01  2.03977E+01  9.92834E-01  9.86122E-01  9.81425E-01  9.81690E-01  9.86402E-01
   8  2.01047E+01  2.02204E+01  2.02715E+01  2.01670E+01  1.01345E+00  1.00703E+00  1.00164E+00  1.00190E+00  1.00729E+00
   7  2.02480E+01  2.03725E+01  2.04246E+01  2.03108E+01  9.87943E-01  9.80744E-01  9.76874E-01  9.77128E-01  9.81026E-01
 
   6  2.00274E+01  2.01625E+01  2.02150E+01  2.00907E+01  9.87960E-01  9.80581E-01  9.77059E-01  9.77308E-01  9.80859E-01
   5  1.93043E+01  1.94512E+01  1.95033E+01  1.93676E+01  1.01675E+00  1.00989E+00  1.00544E+00  1.00569E+00  1.01013E+00
   4  1.86302E+01  1.87950E+01  1.88467E+01  1.86925E+01  9.92387E-01  9.84905E-01  9.82055E-01  9.82293E-01  9.85148E-01
   3  1.68489E+01  1.70293E+01  1.70787E+01  1.69087E+01  9.90069E-01  9.82666E-01  9.80437E-01  9.80664E-01  9.82858E-01
   2  1.36010E+01  1.37819E+01  1.38251E+01  1.36545E+01  9.60032E-01  9.52957E-01  9.52870E-01  9.53031E-01  9.53058E-01
   1  7.77496E+00  7.92698E+00  7.95569E+00  7.80936E+00  1.03690E+00  1.03615E+00  1.06692E+00  1.06697E+00  1.03619E+00
 
 Ave   1.8162E+01   1.8286E+01   1.8332E+01   1.8218E+01   9.9648E-01   9.9078E-01   9.8804E-01   9.8829E-01   9.9100E-01
 A-O       -0.014       -0.014       -0.014       -0.014       -0.001       -0.001       -0.002       -0.002       -0.001
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   87
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  7 c1c24 HFP NOMINAL DEPLETION                                                     1938.54 ppm  200.000 EFPD    

 Cycle exposure at end of this step = 200.00 EFPD                                                    

 Depletion step used = 50.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   88
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  7 c1c24 HFP NOMINAL DEPLETION                                                     1938.54 ppm  200.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =   200.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    20.023 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9972134     0.00278656     6.9516E-02     1.1756E-02     1.76576     0.005    1647.96    
     2  2  1.0006083    -0.00060835     3.3734E-02     7.9191E-04     1.82741    -0.023    1667.59    
     3  3  1.0001672    -0.00016721     1.9108E-02     1.3853E-03     1.79314    -0.011    1684.24    
     4  4  0.9999897     0.00001034 *   6.1809E-03     5.3323E-04     1.80430    -0.014    1679.75    
     5  5  1.0000060 *  -0.00000597 *   1.4973E-03     3.2092E-04 *   1.80160    -0.013    1677.59 *  
     6  6  0.9999974 *   0.00000262 *   1.5871E-04 *   6.4112E-05 *   1.80189    -0.013    1678.55 *  

    Nodal Solution Time = 0.61 Sec

 Boron (ppm)  = 1678.554
 Axial Offset =   -0.013
 Max. Node-Averaged  Peaking Factor = 1.802 in Node ( 9,13, 6)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  1.023  0.691  1.069  0.907  1.545  1.239  1.588  1.419   08
  9  0.691  0.669  0.720  0.729  1.148  1.597  1.171  1.369   09
 10  1.069  0.694  1.320  1.041  1.484  1.164  1.485  1.324   10
 11  0.907  0.939  1.057  1.471  1.058  0.788  0.986  1.134   11
 12  1.545  1.161  1.503  1.065  1.401  1.011  1.101          12
 13  1.239  1.588  1.169  0.793  1.011  1.285  1.070          13
 14  1.588  1.132  1.465  0.981  1.101  1.070                 14
 15  1.419  1.357  1.304  1.125                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8   9,10   3,13  14,13  15,13  14,13   4,13  13, 4  13, 3   08
  9   3,13   1, 1  16,17  17,17   5,15   4,13   5, 4   5, 3   09
 10  14,13  17,17  17,17  15,13   5,14   3, 5   4, 5   5, 3   10
 11  15,13  17,17  13,14  14, 5   5, 4   1, 1   3, 5   1, 1   11
 12  14,13  15, 5  13, 4   4, 5   5, 4  13, 3  13, 3          12
 13   4,13  13, 4   5, 3   1, 1   3,13   5, 4   1, 1          13
 14  13, 4   3, 5   5, 4   5, 3   3,13   1, 1                 14
 15  13, 3   1, 1   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  5.100 33.940  5.948 36.339 10.322 38.753 11.079  9.874   08
  9 33.940 34.685 55.547 23.208 38.364 11.011 38.670  9.491   09
 10  5.948 57.383  7.934 37.120  9.696 36.590 10.028  8.911   10
 11 36.339 36.578 37.196  9.344 38.093 57.812 34.425  7.253   11
 12 10.322 38.434  9.843 38.116  8.813 35.752  6.635          12
 13 38.753 10.907 36.365 57.828 35.734  7.808  6.400          13
 14 11.079 38.959  9.839 34.326  6.622  6.392                 14
 15  9.874  9.315  8.701  7.141                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   89
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  7 c1c24 HFP NOMINAL DEPLETION                                                     1678.55 ppm  200.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  4.826 32.649 32.659  5.098  5.496 33.307 34.808  8.498  9.665 33.734 37.209 10.532 10.196  9.012  6.371    1
  2 32.646 31.408 32.866 52.975 49.578 13.854 17.959 31.829 36.618 10.232 10.461 37.679 37.213  8.705  6.231    2
  3 32.672 32.633 33.415 49.408 46.272 17.474 21.564 31.818 36.286  9.998 10.166 37.342 36.397  8.417  6.018    3
  4  5.146 51.175 55.233  5.891  6.527 34.441 34.911  8.540  9.178 29.487 33.230  9.475  9.211  8.138  5.713    4
  5  5.592 49.896 54.329  6.646  7.453 35.021 35.439  8.717  8.666 31.738 34.327  8.562  8.471  7.432  5.154    5
  6 33.385 34.467 34.795 34.664 35.143  8.624  8.803 36.489 35.154 55.639 52.418 25.763 31.520  6.175  4.235    6
  7 34.891 35.494 35.358 35.205 35.624  8.862  8.842 36.704 35.455 54.822 51.225 28.073 32.778  4.544  2.887    7
  8  8.611 32.082 32.157  8.816  8.877 36.589 36.745  8.336  7.895 30.072 33.907  6.085  4.895                  8
  9  9.717 36.753 36.458  9.324  8.765 35.221 35.484  7.899  7.918 30.955 34.258  6.061  4.390                  9
 10 33.713 10.243 10.016 29.396 31.654 55.652 54.821 30.054 30.953  7.382  6.839  5.712  3.828                 10
 11 37.174 10.381 10.065 33.050 34.126 52.415 51.214 33.872 34.242  6.836  6.088  4.567  2.756                 11
 12 10.476 36.579 37.378  9.302  8.443 25.644 28.001  6.063  6.048  5.706  4.565                               12
 13 10.127 37.451 37.574  9.000  8.324 31.367 32.686  4.868  4.377  3.821  2.753                               13
 14  8.949  8.542  8.209  7.948  7.296  6.085  4.491                                                           14
 15  6.332  6.127  5.885  5.588  5.057  4.169  2.848                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  0.965  0.659  0.655  0.961  1.001  0.758  0.841  1.326  1.456  1.191  1.172  1.515  1.456  1.290  0.927    1
  2  0.659  0.640  0.623  0.566  0.618  0.616  0.678  1.005  1.088  1.512  1.524  1.116  1.060  1.258  0.912    2
  3  0.657  0.626  0.620  0.598  0.661  0.624  0.678  1.009  1.080  1.488  1.495  1.091  1.039  1.231  0.891    3
  4  0.967  0.584  0.580  1.039  1.121  0.857  0.916  1.343  1.411  1.117  1.083  1.407  1.360  1.203  0.858    4
  5  1.013  0.632  0.637  1.135  1.241  0.954  0.989  1.372  1.352  0.989  0.957  1.301  1.275  1.121  0.790    5
  6  0.764  0.763  0.793  0.879  0.967  1.380  1.396  1.009  0.938  0.732  0.726  0.925  0.859  0.962  0.667    6
  7  0.849  0.856  0.882  0.945  1.007  1.404  1.400  0.998  0.915  0.706  0.682  0.814  0.709  0.737  0.469    7
  8  1.341  1.029  1.040  1.379  1.392  1.016  1.000  1.334  1.276  0.915  0.818  1.012  0.818                  8
  9  1.464  1.103  1.098  1.430  1.365  0.943  0.918  1.277  1.286  0.956  0.860  1.013  0.743                  9
 10  1.192  1.515  1.493  1.122  0.994  0.734  0.707  0.915  0.956  1.219  1.135  0.957  0.654                 10
 11  1.170  1.518  1.487  1.078  0.955  0.725  0.682  0.818  0.860  1.135  1.016  0.777  0.478                 11
 12  1.509  1.088  1.048  1.392  1.292  0.922  0.812  1.011  1.012  0.957  0.777                               12
 13  1.450  1.018  0.984  1.340  1.263  0.854  0.707  0.817  0.742  0.654  0.478                               13
 14  1.283  1.242  1.211  1.185  1.110  0.956  0.733                                                           14
 15  0.923  0.902  0.878  0.847  0.782  0.662  0.467                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 1.908 in Node ( 9,13, 5)

    Pin Power Reconstruction Time = 0.29 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  0.965  0.657  0.986  0.803  1.397  1.181  1.482  1.106   08
  9  0.657  0.627  0.611  0.649  1.045  1.505  1.076  1.073   09
 10  0.986  0.608  1.134  0.929  1.370  1.037  1.336  0.993   10
 11  0.803  0.823  0.949  1.395  0.965  0.711  0.827  0.709   11
 12  1.397  1.067  1.392  0.969  1.293  0.887  0.896          12
 13  1.181  1.503  1.038  0.712  0.887  1.126  0.717          13
 14  1.482  1.034  1.322  0.824  0.896  0.717                 14
 15  1.106  1.058  0.981  0.704                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.155  0.947  1.162  0.936  1.121  0.946  1.110  1.139   08
  9  0.947  0.947  0.867  0.857  0.955  1.110  0.939  1.143   09
 10  1.162  0.852  1.148  0.947  1.125  0.966  1.124  1.151   10
 11  0.936  0.949  0.946  1.124  0.947  0.847  0.984  1.177   11
 12  1.121  0.953  1.123  0.946  1.120  0.967  1.164          12
 13  0.946  1.111  0.967  0.847  0.967  1.146  1.179          13
 14  1.110  0.917  1.126  0.985  1.164  1.179                 14
 15  1.139  1.144  1.152  1.178                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8  4.827 32.657  5.333 34.098  9.123 35.457 10.332  7.665   08
  9 32.657 32.581 49.559 17.713 34.138 10.214 37.157  7.342   09
 10  5.333 52.658  6.630 34.953  8.775 32.195  8.929  6.609   10
 11 34.098 35.029 35.159  8.783 35.951 53.526 29.534  4.460   11
 12  9.123 34.363  8.946 36.010  8.012 32.299  5.358          12
 13 35.457 10.176 32.057 53.525 32.281  6.787  4.216          13
 14 10.332 37.245  8.767 29.425  5.339  4.211                 14
 15  7.665  7.190  6.472  4.398                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  36.12   0.00   09
 10   0.00   0.00   0.00  33.92   0.00  31.34   0.00   0.00   10
 11   0.00  33.99  34.13   0.00  34.91   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  34.97   0.00   0.00   0.00          12
 13   0.00   0.00  31.20   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   90
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  7 c1c24 HFP NOMINAL DEPLETION                                                     1678.55 ppm  200.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97            10.33 25B-08 sil51  ( 8,14,  )       4.21  25C-14  sil89  (14,13,  )         7.23
        6    26A        16            37.25 24L-03 24L-03 (14, 9,  )      32.58  24N-05  24N-05 ( 9, 9,  )        34.15
        8    26C        48            37.16 24J-07 24J-07 ( 9,14,  )      29.42  24L-02  24L-02 (14,11,  )        33.41
        5    25C        16            53.53 24N-07 24N-07 (11,13,  )      49.56  24K-06  24K-06 ( 9,10,  )        52.32
        9    26D         4            17.71 24N-03 24N-03 ( 9,11,  )      17.71  24N-03  24N-03 ( 9,11,  )        17.71
        7    26B        12            35.16 24J-05 24J-05 (11,10,  )      34.95  24L-07  24L-07 (10,11,  )        35.05

     CORE              193            53.53 24N-07 24N-07 (11,13,  )       4.21  25C-14  sil89  (14,13,  )        20.02


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97            11.08 25B-08 sil51  ( 1,12,  )
        6    26A        16            38.96 24L-03 24L-03 (13, 2,  )
        8    26C        48            38.75 24P-08 24P-08 ( 1,11,  )
        5    25C        16            57.83 24J-03 24J-03 (10, 6,  )
        9    26D         4            23.21 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            37.20 24J-05 24J-05 ( 7, 5,  )

     CORE              193            57.83 24J-03 24J-03 (10, 6,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   91
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  7 c1c24 HFP NOMINAL DEPLETION                                                     1678.55 ppm  200.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  20.023 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp. 200.0 EFPD  4800.0 EFPH   6.999 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  1.200E+03 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  1.750 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  1.073E-04  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     1.802 ( 9,13, 6)
       Fuel    Average . . .TFUAVE  865.38 K      1098.02 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  580.83 K       585.83 F                  F-delta-H       1.597 ( 9,13)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.689 ( 9,13, 2)   [Off]
 Boron Conc. . . . . . . . . . BOR  1678.6 ppm                               Max-3PIN        1.891 ( 9,13, 6)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        1.908 ( 9,13, 5)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O  -0.013                                 Total Neutron Leakage . . . . . . . . .  0.040
 Edit Ring -     1      2      3      4      5
 RPF            0.678  0.802  1.183  1.026  0.712
 KIN            0.970  0.967  1.019  1.057  1.178
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.722  0.717  0.700  0.707  0.720
 TFU (Deg K)    753.8  801.8  917.5  875.2  794.9
 TMO (Deg K)    574.9  577.2  584.0  581.4  575.9
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.26939   0.95707   8.75806   0.02288   0.66644   673.950   597.447
  23   0.70249   1.10152  13.92728   0.00000   0.66830   793.550   596.823
  22   0.90508   1.07138  17.08892   0.00000   0.67136   851.745   595.780
  21   1.03656   1.05596  19.48461   0.00000   0.67501   889.704   594.506
  20   1.09118   1.04661  20.63679   0.00000   0.67897   904.365   593.090
  19   1.11015   1.04111  21.19384   0.00000   0.68302   908.236   591.605
 
  18   1.12284   1.03980  21.68201   0.00000   0.68708   910.232   590.079
  17   1.12371   1.03786  21.92038   0.00000   0.69111   908.641   588.523
  16   1.12544   1.03734  22.14272   0.00000   0.69510   907.383   586.946
  15   1.11112   1.03573  21.96345   0.00000   0.69902   901.171   585.359
  14   1.12045   1.03651  22.27770   0.00000   0.70289   902.352   583.757
  13   1.12061   1.03538  22.32540   0.00000   0.70673   900.703   582.130
 
  12   1.11287   1.03461  22.14279   0.00000   0.71052   896.604   580.492
  11   1.12625   1.03565  22.39940   0.00000   0.71428   899.094   578.832
  10   1.13292   1.03522  22.45754   0.00000   0.71803   899.524   577.141
   9   1.13727   1.03542  22.40303   0.00000   0.72176   899.256   575.425
   8   1.13564   1.03412  22.17243   0.00000   0.72545   897.182   573.691
   7   1.15276   1.03567  22.33548   0.00000   0.72913   900.997   571.929
 
   6   1.15302   1.03714  22.11094   0.00000   0.73280   899.638   570.139
   5   1.12576   1.03983  21.34229   0.00000   0.73639   889.871   568.353
   4   1.09062   1.04831  20.59418   0.00000   0.73984   877.951   566.602
   3   0.97784   1.06397  18.61311   0.00000   0.74304   843.087   564.957
   2   0.75021   1.09472  14.97603   0.00000   0.74568   775.086   563.573
   1   0.26583   0.92362   8.53100   0.00000   0.74722   638.914   562.754
 
 Ave   1.00000   1.03811  20.02283   0.00095   0.70788   865.385   580.830
 P**2                     17.64400
 A-O  -0.01342   0.00313  -0.01410   1.00000  -0.03173     0.006     0.017
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   92
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  7 c1c24 HFP NOMINAL DEPLETION                                                     1678.55 ppm  200.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  1.91646E-09  1.21542E-09  3.57919E-09  1.53868E-08  8.75806E+00  6.54855E-01  2.64216E+01  1.49583E+03  0.00000E+00
  23  4.99717E-09  3.56126E-09  9.37392E-09  4.81081E-08  1.39273E+01  6.59313E-01  2.85982E+01  1.56240E+03  3.73536E+00
  22  6.44241E-09  4.09110E-09  1.25390E-08  5.15214E-08  1.70889E+01  6.63254E-01  2.95649E+01  1.60209E+03  4.41623E+00
  21  7.38029E-09  4.26956E-09  1.47062E-08  5.13359E-08  1.94846E+01  6.67332E-01  3.02461E+01  1.62057E+03  4.98944E+00
  20  7.77048E-09  4.34537E-09  1.56547E-08  5.16367E-08  2.06368E+01  6.71702E-01  3.05284E+01  1.63468E+03  5.25839E+00
  19  7.90608E-09  4.37396E-09  1.60140E-08  5.19134E-08  2.11938E+01  6.76165E-01  3.06281E+01  1.64550E+03  5.38132E+00
 
  18  7.99563E-09  4.35520E-09  1.62607E-08  5.14273E-08  2.16820E+01  6.80627E-01  3.07064E+01  1.65225E+03  5.49230E+00
  17  8.00108E-09  4.33710E-09  1.63014E-08  5.11906E-08  2.19204E+01  6.85087E-01  3.07155E+01  1.65692E+03  5.54380E+00
  16  8.01239E-09  4.30910E-09  1.63469E-08  5.07293E-08  2.21427E+01  6.89516E-01  3.07255E+01  1.65957E+03  5.59419E+00
  15  7.91055E-09  4.31564E-09  1.61144E-08  5.12196E-08  2.19635E+01  6.93900E-01  3.06282E+01  1.66202E+03  5.54503E+00
  14  7.97560E-09  4.27196E-09  1.62714E-08  5.02606E-08  2.22777E+01  6.98200E-01  3.06757E+01  1.66086E+03  5.62251E+00
  13  7.97611E-09  4.25742E-09  1.62674E-08  5.00184E-08  2.23254E+01  7.02496E-01  3.06548E+01  1.65990E+03  5.63312E+00
 
  12  7.92127E-09  4.26522E-09  1.61235E-08  5.03267E-08  2.21428E+01  7.06736E-01  3.05757E+01  1.65878E+03  5.58631E+00
  11  8.01548E-09  4.22971E-09  1.63267E-08  4.93977E-08  2.23994E+01  7.10912E-01  3.06269E+01  1.65455E+03  5.65149E+00
  10  8.06276E-09  4.21643E-09  1.64113E-08  4.90019E-08  2.24575E+01  7.15083E-01  3.06284E+01  1.64998E+03  5.66812E+00
   9  8.09407E-09  4.21521E-09  1.64491E-08  4.88510E-08  2.24030E+01  7.19220E-01  3.06058E+01  1.64479E+03  5.65843E+00
   8  8.08333E-09  4.23097E-09  1.63787E-08  4.90948E-08  2.21724E+01  7.23305E-01  3.05400E+01  1.63860E+03  5.60680E+00
   7  8.20461E-09  4.19584E-09  1.66098E-08  4.80677E-08  2.23355E+01  7.27335E-01  3.05960E+01  1.62809E+03  5.65884E+00
 
   6  8.20661E-09  4.18162E-09  1.65486E-08  4.76956E-08  2.21109E+01  7.31343E-01  3.05475E+01  1.61636E+03  5.61791E+00
   5  8.01311E-09  4.18061E-09  1.60241E-08  4.80246E-08  2.13423E+01  7.35246E-01  3.03419E+01  1.60293E+03  5.44570E+00
   4  7.76108E-09  4.08822E-09  1.53696E-08  4.68817E-08  2.05942E+01  7.38981E-01  3.01356E+01  1.58338E+03  5.28916E+00
   3  6.95604E-09  3.92537E-09  1.34856E-08  4.61614E-08  1.86131E+01  7.42447E-01  2.95245E+01  1.55997E+03  4.83560E+00
   2  5.33169E-09  3.47937E-09  9.92065E-09  4.40389E-08  1.49760E+01  7.45351E-01  2.83377E+01  1.52071E+03  4.04413E+00
   1  1.88934E-09  1.18454E-09  3.46040E-09  1.49616E-08  8.53100E+00  7.47116E-01  2.56728E+01  1.47719E+03  0.00000E+00
 
 Ave   7.1177E-09   3.9207E-09   1.4272E-08   4.6552E-08   2.0023E+01   7.0356E-01   2.9926E+01   1.6145E+03   4.9735E+00
 A-O       -0.013        0.014       -0.011        0.029       -0.014       -0.036        0.003        0.007       -0.016
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  8.65563E+00  8.76175E+00  8.78994E+00  8.69226E+00  1.00087E+00  9.98621E-01  1.02623E+00  1.02633E+00  9.98645E-01
  23  1.38538E+01  1.39272E+01  1.39621E+01  1.38981E+01  9.48492E-01  9.44524E-01  9.37819E-01  9.38026E-01  9.44630E-01
  22  1.70290E+01  1.70769E+01  1.71147E+01  1.70759E+01  1.01633E+00  1.01311E+00  1.00222E+00  1.00255E+00  1.01332E+00
  21  1.94397E+01  1.94648E+01  1.95056E+01  1.94896E+01  9.89532E-01  9.85544E-01  9.75875E-01  9.76196E-01  9.85815E-01
  20  2.05951E+01  2.06044E+01  2.06469E+01  2.06472E+01  9.96058E-01  9.92049E-01  9.82094E-01  9.82415E-01  9.92333E-01
  19  2.11512E+01  2.11502E+01  2.11938E+01  2.12048E+01  1.00821E+00  1.00434E+00  9.93981E-01  9.94304E-01  1.00462E+00
 
  18  2.16435E+01  2.16346E+01  2.16790E+01  2.16980E+01  9.96531E-01  9.92272E-01  9.82873E-01  9.83189E-01  9.92565E-01
  17  2.18816E+01  2.18691E+01  2.19143E+01  2.19369E+01  9.96046E-01  9.91614E-01  9.82540E-01  9.82855E-01  9.91911E-01
  16  2.21050E+01  2.20892E+01  2.21350E+01  2.21609E+01  9.88574E-01  9.83864E-01  9.75555E-01  9.75864E-01  9.84165E-01
  15  2.19157E+01  2.19019E+01  2.19479E+01  2.19724E+01  1.01618E+00  1.01190E+00  1.00254E+00  1.00286E+00  1.01218E+00
  14  2.22354E+01  2.22196E+01  2.22665E+01  2.22927E+01  9.91569E-01  9.86612E-01  9.78869E-01  9.79174E-01  9.86910E-01
  13  2.22807E+01  2.22661E+01  2.23135E+01  2.23386E+01  9.93134E-01  9.88052E-01  9.80542E-01  9.80843E-01  9.88350E-01
 
  12  2.20892E+01  2.20781E+01  2.21258E+01  2.21479E+01  1.01479E+00  1.01001E+00  1.00178E+00  1.00209E+00  1.01029E+00
  11  2.23508E+01  2.23397E+01  2.23884E+01  2.24101E+01  9.91108E-01  9.85656E-01  9.78974E-01  9.79269E-01  9.85954E-01
  10  2.24080E+01  2.24001E+01  2.24494E+01  2.24679E+01  9.87271E-01  9.81537E-01  9.75336E-01  9.75628E-01  9.81841E-01
   9  2.23490E+01  2.23450E+01  2.23949E+01  2.24097E+01  9.92674E-01  9.86911E-01  9.80822E-01  9.81111E-01  9.87206E-01
   8  2.21091E+01  2.21123E+01  2.21624E+01  2.21704E+01  1.01361E+00  1.00812E+00  1.00128E+00  1.00157E+00  1.00839E+00
   7  2.22768E+01  2.22856E+01  2.23366E+01  2.23387E+01  9.87693E-01  9.81445E-01  9.76191E-01  9.76471E-01  9.81746E-01
 
   6  2.20475E+01  2.20667E+01  2.21182E+01  2.21098E+01  9.87706E-01  9.81278E-01  9.76360E-01  9.76636E-01  9.81577E-01
   5  2.12636E+01  2.12990E+01  2.13500E+01  2.13259E+01  1.01694E+00  1.01100E+00  1.00505E+00  1.00534E+00  1.01126E+00
   4  2.05142E+01  2.05693E+01  2.06200E+01  2.05757E+01  9.92191E-01  9.85638E-01  9.81341E-01  9.81613E-01  9.85911E-01
   3  1.85212E+01  1.86031E+01  1.86515E+01  1.85802E+01  9.89891E-01  9.83380E-01  9.79706E-01  9.79973E-01  9.83612E-01
   2  1.48679E+01  1.49727E+01  1.50152E+01  1.49208E+01  9.59198E-01  9.52897E-01  9.51503E-01  9.51694E-01  9.53046E-01
   1  8.41102E+00  8.52437E+00  8.55300E+00  8.44542E+00  1.03603E+00  1.03494E+00  1.06461E+00  1.06468E+00  1.03500E+00
 
 Ave   1.9962E+01   1.9984E+01   2.0029E+01   2.0018E+01   9.9628E-01   9.9147E-01   9.8725E-01   9.8753E-01   9.9172E-01
 A-O       -0.014       -0.014       -0.014       -0.014       -0.001       -0.000       -0.002       -0.002       -0.000
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   93
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  8 c1c24 HFP NOMINAL DEPLETION                                                     1678.55 ppm  250.000 EFPD    

 Cycle exposure at end of this step = 250.00 EFPD                                                    

 Depletion step used = 50.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   94
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  8 c1c24 HFP NOMINAL DEPLETION                                                     1678.55 ppm  250.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =   250.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    21.773 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9972716     0.00272842     6.1299E-02     9.7991E-03     1.69781     0.002    1402.05    
     2  2  1.0005735    -0.00057351     3.5359E-02     4.2249E-04 *   1.76004    -0.028    1419.62    
     3  3  1.0001760    -0.00017596     1.9721E-02     1.4193E-03     1.72601    -0.015    1436.42    
     4  4  0.9999994     0.00000059 *   6.6423E-03     5.8920E-04     1.73755    -0.018    1432.31    
     5  5  1.0000042 *  -0.00000423 *   1.7685E-03     3.3595E-04 *   1.73448    -0.017    1430.66 *  
     6  6  0.9999980 *   0.00000196 *   2.9696E-04 *   6.9104E-05 *   1.73500    -0.017    1431.37 *  

    Nodal Solution Time = 0.62 Sec

 Boron (ppm)  = 1431.373
 Axial Offset =   -0.017
 Max. Node-Averaged  Peaking Factor = 1.735 in Node ( 9,13, 6)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  1.102  0.752  1.117  0.918  1.512  1.211  1.533  1.370   08
  9  0.752  0.727  0.746  0.743  1.129  1.547  1.137  1.327   09
 10  1.117  0.717  1.330  1.042  1.461  1.146  1.448  1.292   10
 11  0.918  0.941  1.057  1.458  1.055  0.791  0.984  1.123   11
 12  1.512  1.142  1.478  1.061  1.393  1.018  1.113          12
 13  1.211  1.543  1.151  0.795  1.018  1.292  1.082          13
 14  1.533  1.103  1.432  0.981  1.113  1.082                 14
 15  1.370  1.315  1.276  1.117                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8   9,10   3,13  14,13  15,13  14,13   4,13  13, 4  13, 3   08
  9   3,13   1, 1  16,17  17,17   5,15   4,13   5, 4   5, 3   09
 10  14,13  17,17  17,17  15,13   5,14   4, 5   4, 5   5, 3   10
 11  15,13  17,17  13,14  14, 5   5, 4   1, 1   3, 5   1, 1   11
 12  14,13  14, 5  13, 4   4, 5   5, 4  13, 3  13, 3          12
 13   4,13   5, 4   5, 4   1, 1   3,13   5, 4   1, 1          13
 14  13, 4   3, 5   5, 4   5, 3   3,13   1, 1                 14
 15  13, 3   1, 1   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  6.753 35.335  7.667 38.124 12.725 41.112 13.531 12.052   08
  9 35.335 35.999 56.738 24.629 40.612 13.484 40.936 11.617   09
 10  7.667 58.586 10.010 39.222 12.011 38.357 12.326 10.953   10
 11 38.124 38.371 39.302 11.651 40.160 59.288 35.825  9.012   11
 12 12.725 40.695 12.188 40.183 11.015 37.527  8.390          12
 13 41.112 13.365 38.136 59.308 37.510  9.828  8.089          13
 14 13.531 40.984 12.117 35.723  8.378  8.081                 14
 15 12.052 11.417 10.716  8.888                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   95
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  8 c1c24 HFP NOMINAL DEPLETION                                                     1431.37 ppm  250.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  6.407 34.007 34.005  6.657  7.111 34.835 36.484 10.579 11.935 36.055 39.490 12.875 12.447 11.007  7.810    1
  2 34.004 32.726 34.146 54.133 50.834 15.101 19.319 33.813 38.750 12.580 12.824 39.849 39.274 10.654  7.648    2
  3 34.021 33.919 34.683 50.626 47.609 18.735 22.922 33.810 38.405 12.313 12.486 39.467 38.421 10.327  7.404    3
  4  6.715 52.368 56.414  7.556  8.313 36.151 36.728 10.648 11.383 31.675 35.348 11.663 11.323 10.007  7.052    4
  5  7.225 51.177 55.614  8.453  9.418 36.913 37.393 10.868 10.783 33.685 36.209 10.593 10.459  9.180  6.391    5
  6 34.923 35.999 36.382 36.414 37.058 10.791 10.994 38.478 37.006 57.091 53.858 27.589 33.212  7.685  5.285    6
  7 36.581 37.197 37.110 37.076 37.611 11.063 11.037 38.673 37.265 56.227 52.584 29.689 34.185  5.709  3.631    7
  8 10.713 34.111 34.205 10.976 11.058 38.591 38.718 10.430  9.902 31.887 35.534  7.688  6.192                  8
  9 11.998 38.913 38.611 11.558 10.901 37.084 37.299  9.908  9.942 32.850 35.965  7.663  5.571                  9
 10 36.037 12.595 12.338 31.594 33.610 57.108 56.227 31.870 32.848  9.305  8.631  7.227  4.868                 10
 11 39.451 12.734 12.373 35.160 36.006 53.853 52.572 35.499 35.950  8.628  7.694  5.800  3.519                 11
 12 12.811 38.697 39.424 11.468 10.462 27.464 29.614  7.665  7.650  7.221  5.798                               12
 13 12.368 39.433 39.495 11.085 10.295 33.051 34.089  6.164  5.557  4.862  3.516                               13
 14 10.934 10.467 10.090  9.793  9.028  7.586  5.652                                                           14
 15  7.766  7.530  7.253  6.911  6.283  5.212  3.588                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  1.043  0.717  0.709  1.020  1.051  0.790  0.858  1.319  1.429  1.163  1.140  1.463  1.405  1.246  0.902    1
  2  0.718  0.696  0.675  0.608  0.655  0.649  0.700  1.007  1.074  1.473  1.479  1.084  1.030  1.219  0.890    2
  3  0.710  0.677  0.666  0.636  0.694  0.653  0.698  1.011  1.068  1.453  1.454  1.064  1.013  1.197  0.872    3
  4  1.026  0.625  0.616  1.077  1.149  0.877  0.926  1.335  1.391  1.101  1.064  1.374  1.325  1.173  0.844    4
  5  1.062  0.667  0.666  1.161  1.256  0.964  0.992  1.361  1.339  0.984  0.950  1.281  1.252  1.101  0.783    5
  6  0.794  0.790  0.815  0.895  0.974  1.375  1.387  1.007  0.940  0.740  0.733  0.926  0.857  0.957  0.667    6
  7  0.864  0.870  0.893  0.951  1.007  1.393  1.389  0.997  0.920  0.718  0.695  0.824  0.717  0.744  0.476    7
  8  1.331  1.027  1.037  1.366  1.379  1.013  0.999  1.328  1.275  0.924  0.831  1.024  0.831                  8
  9  1.435  1.087  1.084  1.408  1.350  0.944  0.922  1.276  1.286  0.964  0.870  1.024  0.757                  9
 10  1.165  1.476  1.458  1.106  0.989  0.742  0.719  0.925  0.964  1.224  1.142  0.968  0.668                 10
 11  1.138  1.473  1.448  1.061  0.950  0.733  0.695  0.831  0.871  1.142  1.024  0.790  0.492                 11
 12  1.459  1.060  1.026  1.362  1.274  0.924  0.823  1.024  1.024  0.968  0.790                               12
 13  1.399  0.992  0.964  1.310  1.243  0.853  0.715  0.831  0.756  0.668  0.492                               13
 14  1.240  1.206  1.180  1.160  1.093  0.952  0.742                                                           14
 15  0.899  0.881  0.861  0.835  0.777  0.664  0.474                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 1.843 in Node ( 9,13, 5)

    Pin Power Reconstruction Time = 0.29 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  1.043  0.714  1.040  0.827  1.379  1.152  1.432  1.072   08
  9  0.714  0.679  0.648  0.675  1.040  1.465  1.048  1.044   09
 10  1.040  0.643  1.161  0.939  1.357  1.025  1.308  0.975   10
 11  0.827  0.842  0.957  1.386  0.966  0.721  0.831  0.711   11
 12  1.379  1.059  1.376  0.969  1.291  0.897  0.909          12
 13  1.152  1.464  1.026  0.722  0.898  1.133  0.729          13
 14  1.432  1.010  1.297  0.829  0.909  0.729                 14
 15  1.072  1.032  0.966  0.708                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.154  0.953  1.162  0.941  1.118  0.948  1.107  1.140   08
  9  0.953  0.954  0.875  0.869  0.958  1.106  0.942  1.144   09
 10  1.162  0.860  1.146  0.951  1.121  0.969  1.121  1.153   10
 11  0.941  0.953  0.949  1.120  0.950  0.854  0.990  1.183   11
 12  1.118  0.956  1.119  0.950  1.117  0.971  1.166          12
 13  0.948  1.107  0.970  0.854  0.971  1.145  1.184          13
 14  1.107  0.921  1.123  0.991  1.167  1.184                 14
 15  1.140  1.146  1.154  1.184                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8  6.408 34.010  6.927 35.706 11.306 37.758 12.624  9.378   08
  9 34.010 33.869 50.801 19.020 36.195 12.550 39.252  9.008   09
 10  6.927 53.894  8.436 36.797 10.920 34.229 11.009  8.157   10
 11 35.706 36.672 37.040 10.972 37.856 54.940 31.169  5.577   11
 12 11.306 36.460 11.123 37.923 10.046 34.059  6.778          12
 13 37.758 12.510 34.092 54.940 34.042  8.565  5.354          13
 14 12.624 39.262 10.827 31.055  6.759  5.349                 14
 15  9.378  8.834  8.004  5.509                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  38.16   0.00   09
 10   0.00   0.00   0.00  35.72   0.00  33.32   0.00   0.00   10
 11   0.00  35.59  35.96   0.00  36.77   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  36.83   0.00   0.00   0.00          12
 13   0.00   0.00  33.18   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   96
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  8 c1c24 HFP NOMINAL DEPLETION                                                     1431.37 ppm  250.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97            12.62 25B-08 sil51  ( 8,14,  )       5.35  25C-14  sil89  (14,13,  )         8.99
        6    26A        16            39.26 24L-03 24L-03 (14, 9,  )      33.87  24N-05  24N-05 ( 9, 9,  )        35.71
        8    26C        48            39.25 24J-07 24J-07 ( 9,14,  )      31.05  24L-02  24L-02 (14,11,  )        35.34
        5    25C        16            54.94 24J-03 24J-03 (13,11,  )      50.80  24K-06  24K-06 ( 9,10,  )        53.64
        9    26D         4            19.02 24N-03 24N-03 ( 9,11,  )      19.02  24N-03  24N-03 ( 9,11,  )        19.02
        7    26B        12            37.04 24J-05 24J-05 (11,10,  )      36.67  24L-05  24L-05 (11, 9,  )        36.84

     CORE              193            54.94 24J-03 24J-03 (13,11,  )       5.35  25C-14  sil89  (14,13,  )        21.77


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97            13.53 25B-08 sil51  ( 1,12,  )
        6    26A        16            40.98 24L-03 24L-03 (13, 2,  )
        8    26C        48            41.11 24P-08 24P-08 ( 1,11,  )
        5    25C        16            59.31 24J-03 24J-03 (10, 6,  )
        9    26D         4            24.63 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            39.30 24J-05 24J-05 ( 7, 5,  )

     CORE              193            59.31 24J-03 24J-03 (10, 6,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   97
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  8 c1c24 HFP NOMINAL DEPLETION                                                     1431.37 ppm  250.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  21.773 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp. 250.0 EFPD  6000.0 EFPH   8.749 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  1.200E+03 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  1.750 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  1.161E-04  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     1.735 ( 9,13, 6)
       Fuel    Average . . .TFUAVE  862.02 K      1091.97 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  580.90 K       585.94 F                  F-delta-H       1.547 ( 9,13)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.627 ( 9,13, 2)   [Off]
 Boron Conc. . . . . . . . . . BOR  1431.4 ppm                               Max-3PIN        1.818 ( 9,13, 6)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        1.843 ( 9,13, 5)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O  -0.017                                 Total Neutron Leakage . . . . . . . . .  0.041
 Edit Ring -     1      2      3      4      5
 RPF            0.735  0.834  1.171  1.016  0.719
 KIN            0.976  0.972  1.019  1.059  1.184
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.720  0.715  0.700  0.707  0.719
 TFU (Deg K)    771.6  810.0  911.2  867.4  792.8
 TMO (Deg K)    576.0  577.8  583.8  581.3  576.1
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.29345   0.97558   9.45404   0.02288   0.66659   679.742   597.477
  23   0.73540   1.10503  15.15298   0.00000   0.66856   801.180   596.812
  22   0.92761   1.07255  18.65115   0.00000   0.67171   855.275   595.730
  21   1.04602   1.05613  21.25983   0.00000   0.67541   888.740   594.429
  20   1.08896   1.04652  22.49510   0.00000   0.67937   899.936   593.003
  19   1.09940   1.04104  23.07714   0.00000   0.68338   901.233   591.522
 
  18   1.10622   1.03994  23.58187   0.00000   0.68738   901.461   590.009
  17   1.10373   1.03815  23.81877   0.00000   0.69134   898.845   588.475
  16   1.10366   1.03779  24.04243   0.00000   0.69525   897.035   586.923
  15   1.08929   1.03627  23.83865   0.00000   0.69909   890.798   585.364
  14   1.09836   1.03719  24.16850   0.00000   0.70287   891.929   583.791
  13   1.09913   1.03609  24.21694   0.00000   0.70664   890.491   582.194
 
  12   1.09258   1.03534  24.02212   0.00000   0.71035   886.771   580.585
  11   1.10661   1.03645  24.30208   0.00000   0.71404   889.506   578.955
  10   1.11462   1.03598  24.37274   0.00000   0.71772   890.399   577.292
   9   1.12102   1.03615  24.32742   0.00000   0.72140   890.815   575.602
   8   1.12271   1.03469  24.09690   0.00000   0.72504   889.794   573.891
   7   1.14405   1.03619  24.29278   0.00000   0.72869   895.061   572.146
 
   6   1.15139   1.03756  24.07480   0.00000   0.73235   895.930   570.365
   5   1.13466   1.04021  23.26873   0.00000   0.73595   889.232   568.575
   4   1.11304   1.04902  22.47229   0.00000   0.73945   881.222   566.801
   3   1.01548   1.06574  20.31202   0.00000   0.74274   850.598   565.109
   2   0.79810   1.09923  16.29568   0.00000   0.74552   786.554   563.658
   1   0.29451   0.94284   9.22382   0.00000   0.74718   645.990   562.777
 
 Ave   1.00000   1.04049  21.77262   0.00095   0.70783   862.022   580.895
 P**2                     19.74056
 A-O  -0.01740   0.00292  -0.01419   1.00000  -0.03137     0.005     0.016
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   98
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  8 c1c24 HFP NOMINAL DEPLETION                                                     1431.37 ppm  250.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  2.08735E-09  1.25751E-09  3.97701E-09  1.55540E-08  9.45404E+00  6.56063E-01  2.64312E+01  1.42653E+03  0.00000E+00
  23  5.23190E-09  3.62532E-09  1.00154E-08  4.85919E-08  1.51530E+01  6.60713E-01  2.85988E+01  1.48713E+03  3.94355E+00
  22  6.60480E-09  4.11256E-09  1.31414E-08  5.20509E-08  1.86511E+01  6.64645E-01  2.95259E+01  1.52495E+03  4.67691E+00
  21  7.45003E-09  4.25351E-09  1.51823E-08  5.17623E-08  2.12598E+01  6.68673E-01  3.01608E+01  1.54243E+03  5.28115E+00
  20  7.75723E-09  4.30665E-09  1.59784E-08  5.20012E-08  2.24951E+01  6.72971E-01  3.04085E+01  1.55582E+03  5.56142E+00
  19  7.83216E-09  4.32106E-09  1.62128E-08  5.22358E-08  2.30771E+01  6.77345E-01  3.04824E+01  1.56608E+03  5.68715E+00
 
  18  7.87984E-09  4.29185E-09  1.63713E-08  5.16882E-08  2.35819E+01  6.81706E-01  3.05401E+01  1.57254E+03  5.79923E+00
  17  7.86133E-09  4.26823E-09  1.63577E-08  5.14159E-08  2.38188E+01  6.86059E-01  3.05356E+01  1.57700E+03  5.84958E+00
  16  7.85966E-09  4.23701E-09  1.63735E-08  5.09207E-08  2.40424E+01  6.90377E-01  3.05363E+01  1.57955E+03  5.89921E+00
  15  7.75760E-09  4.24483E-09  1.61332E-08  5.14315E-08  2.38387E+01  6.94651E-01  3.04370E+01  1.58184E+03  5.84642E+00
  14  7.82064E-09  4.19964E-09  1.62890E-08  5.04291E-08  2.41685E+01  6.98842E-01  3.04798E+01  1.58079E+03  5.92491E+00
  13  7.82541E-09  4.18606E-09  1.62945E-08  5.01816E-08  2.42169E+01  7.03031E-01  3.04590E+01  1.57989E+03  5.93513E+00
 
  12  7.77913E-09  4.19687E-09  1.61673E-08  5.05171E-08  2.40221E+01  7.07169E-01  3.03839E+01  1.57881E+03  5.88657E+00
  11  7.87772E-09  4.16110E-09  1.63878E-08  4.95575E-08  2.43021E+01  7.11247E-01  3.04361E+01  1.57485E+03  5.95400E+00
  10  7.93437E-09  4.14945E-09  1.64987E-08  4.91603E-08  2.43727E+01  7.15325E-01  3.04424E+01  1.57056E+03  5.97188E+00
   9  7.98022E-09  4.15153E-09  1.65739E-08  4.90274E-08  2.43274E+01  7.19378E-01  3.04282E+01  1.56566E+03  5.96330E+00
   8  7.99314E-09  4.17325E-09  1.65587E-08  4.93131E-08  2.40969E+01  7.23387E-01  3.03755E+01  1.55979E+03  5.91203E+00
   7  8.14422E-09  4.14195E-09  1.68673E-08  4.82710E-08  2.42928E+01  7.27351E-01  3.04450E+01  1.54989E+03  5.96779E+00
 
   6  8.19646E-09  4.13734E-09  1.69199E-08  4.79279E-08  2.40748E+01  7.31308E-01  3.04186E+01  1.53878E+03  5.92776E+00
   5  8.07810E-09  4.15448E-09  1.65463E-08  4.83351E-08  2.32687E+01  7.35177E-01  3.02444E+01  1.52601E+03  5.75126E+00
   4  7.92200E-09  4.08168E-09  1.60792E-08  4.72111E-08  2.24723E+01  7.38901E-01  3.00732E+01  1.50745E+03  5.58758E+00
   3  7.22473E-09  3.95328E-09  1.43541E-08  4.65466E-08  2.03120E+01  7.42379E-01  2.95069E+01  1.48516E+03  5.10862E+00
   2  5.67190E-09  3.55707E-09  1.07942E-08  4.44282E-08  1.62957E+01  7.45315E-01  2.83705E+01  1.44702E+03  4.26071E+00
   1  2.09276E-09  1.23497E-09  3.91587E-09  1.51350E-08  9.22382E+00  7.47107E-01  2.57059E+01  1.40741E+03  0.00000E+00
 
 Ave   7.1193E-09   3.8915E-09   1.4583E-08   4.6821E-08   2.1773E+01   7.0413E-01   2.9809E+01   1.5369E+03   5.2482E+00
 A-O       -0.017        0.013       -0.015        0.029       -0.014       -0.035        0.002        0.007       -0.015
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  9.37112E+00  9.43224E+00  9.46029E+00  9.40767E+00  9.99048E-01  9.96856E-01  1.02310E+00  1.02320E+00  9.96901E-01
  23  1.51155E+01  1.51155E+01  1.51497E+01  1.51592E+01  9.47617E-01  9.44624E-01  9.36433E-01  9.36654E-01  9.44749E-01
  22  1.86357E+01  1.85939E+01  1.86308E+01  1.86819E+01  1.01663E+00  1.01445E+00  1.00188E+00  1.00223E+00  1.01467E+00
  21  2.12658E+01  2.11888E+01  2.12286E+01  2.13149E+01  9.89407E-01  9.86463E-01  9.75256E-01  9.75590E-01  9.86737E-01
  20  2.25064E+01  2.24098E+01  2.24514E+01  2.25577E+01  9.96035E-01  9.93042E-01  9.81605E-01  9.81937E-01  9.93326E-01
  19  2.30879E+01  2.29807E+01  2.30233E+01  2.31406E+01  1.00839E+00  1.00550E+00  9.93685E-01  9.94016E-01  1.00578E+00
 
  18  2.35975E+01  2.34810E+01  2.35245E+01  2.36512E+01  9.96525E-01  9.93222E-01  9.82450E-01  9.82775E-01  9.93512E-01
  17  2.38343E+01  2.37142E+01  2.37584E+01  2.38887E+01  9.96009E-01  9.92510E-01  9.82107E-01  9.82428E-01  9.92803E-01
  16  2.40594E+01  2.39353E+01  2.39802E+01  2.41145E+01  9.88429E-01  9.84634E-01  9.75039E-01  9.75356E-01  9.84930E-01
  15  2.38444E+01  2.37249E+01  2.37699E+01  2.39002E+01  1.01645E+00  1.01306E+00  1.00237E+00  1.00269E+00  1.01333E+00
  14  2.41810E+01  2.40570E+01  2.41029E+01  2.42374E+01  9.91490E-01  9.87417E-01  9.78420E-01  9.78730E-01  9.87710E-01
  13  2.42271E+01  2.41041E+01  2.41506E+01  2.42841E+01  9.93049E-01  9.88838E-01  9.80086E-01  9.80395E-01  9.89131E-01
 
  12  2.40226E+01  2.39048E+01  2.39515E+01  2.40804E+01  1.01503E+00  1.01110E+00  1.00159E+00  1.00190E+00  1.01138E+00
  11  2.43088E+01  2.41883E+01  2.42360E+01  2.43673E+01  9.90985E-01  9.86390E-01  9.78490E-01  9.78793E-01  9.86683E-01
  10  2.43791E+01  2.42606E+01  2.43089E+01  2.44382E+01  9.87070E-01  9.82192E-01  9.74792E-01  9.75092E-01  9.82491E-01
   9  2.43295E+01  2.42144E+01  2.42632E+01  2.43893E+01  9.92577E-01  9.87662E-01  9.80361E-01  9.80659E-01  9.87954E-01
   8  2.40891E+01  2.39822E+01  2.40312E+01  2.41495E+01  1.01382E+00  1.00916E+00  1.00106E+00  1.00136E+00  1.00944E+00
   7  2.42914E+01  2.41862E+01  2.42362E+01  2.43524E+01  9.87501E-01  9.82110E-01  9.75655E-01  9.75946E-01  9.82409E-01
 
   6  2.40690E+01  2.39732E+01  2.40236E+01  2.41303E+01  9.87511E-01  9.81946E-01  9.75815E-01  9.76103E-01  9.82244E-01
   5  2.32458E+01  2.31694E+01  2.32193E+01  2.33073E+01  1.01720E+00  1.01209E+00  1.00484E+00  1.00514E+00  1.01236E+00
   4  2.24478E+01  2.23911E+01  2.24407E+01  2.25083E+01  9.92057E-01  9.86361E-01  9.80792E-01  9.81080E-01  9.86640E-01
   3  2.02705E+01  2.02500E+01  2.02974E+01  2.03287E+01  9.89739E-01  9.84063E-01  9.79095E-01  9.79383E-01  9.84309E-01
   2  1.62271E+01  1.62505E+01  1.62923E+01  1.62793E+01  9.58250E-01  9.52678E-01  9.50081E-01  9.50291E-01  9.52854E-01
   1  9.12245E+00  9.19204E+00  9.22058E+00  9.15682E+00  1.03506E+00  1.03367E+00  1.06220E+00  1.06228E+00  1.03375E+00
 
 Ave   2.1763E+01   2.1683E+01   2.1727E+01   2.1817E+01   9.9608E-01   9.9209E-01   9.8655E-01   9.8683E-01   9.9234E-01
 A-O       -0.014       -0.014       -0.014       -0.014       -0.001       -0.000       -0.002       -0.002       -0.000
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page   99
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  9 c1c24 HFP NOMINAL DEPLETION                                                     1431.37 ppm  300.000 EFPD    

 Cycle exposure at end of this step = 300.00 EFPD                                                    

 Depletion step used = 50.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  100
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  9 c1c24 HFP NOMINAL DEPLETION                                                     1431.37 ppm  300.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =   300.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    23.522 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9973209     0.00267908     5.9919E-02     9.5295E-03     1.63691     0.001    1167.62    
     2  2  1.0005529    -0.00055295     3.7425E-02     1.0727E-04 *   1.70056    -0.031    1183.98    
     3  3  1.0001764    -0.00017642     1.8653E-02     1.3667E-03     1.66942    -0.018    1200.27    
     4  4  0.9999979     0.00000209 *   4.6305E-03     6.5411E-04     1.67718    -0.021    1196.61    
     5  5  1.0000039 *  -0.00000386 *   1.4208E-03     3.6046E-04 *   1.67480    -0.020    1195.19 *  
     6  6  0.9999981 *   0.00000193 *   3.3144E-04 *   7.6547E-05 *   1.67536    -0.020    1195.87 *  

    Nodal Solution Time = 0.61 Sec

 Boron (ppm)  = 1195.869
 Axial Offset =   -0.020
 Max. Node-Averaged  Peaking Factor = 1.675 in Node (13, 9, 6)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  1.165  0.804  1.158  0.927  1.484  1.185  1.487  1.330   08
  9  0.804  0.777  0.769  0.755  1.113  1.504  1.109  1.293   09
 10  1.158  0.737  1.338  1.041  1.441  1.130  1.417  1.266   10
 11  0.927  0.943  1.054  1.444  1.050  0.793  0.982  1.115   11
 12  1.484  1.125  1.455  1.055  1.383  1.021  1.122          12
 13  1.185  1.506  1.135  0.796  1.022  1.293  1.091          13
 14  1.487  1.078  1.404  0.980  1.122  1.091                 14
 15  1.330  1.281  1.254  1.111                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8   9,10   4,13  14,13  15,13  14,13   4,13  13, 4  13, 3   08
  9   4,13   1, 1  16,17  17,17   5,14   4,13   5, 4   5, 3   09
 10  14,13  17,17  14,13  14,13   5,14   4, 5   4, 5   5, 3   10
 11  15,13  15,13  13,14  14, 5   5, 4   1, 1   3, 5   1, 1   11
 12  14,13  14, 5  13, 4   4, 5   5, 4  13, 3  13, 3          12
 13   4,13   5, 4   5, 4   1, 1   3,13   5, 4   1, 1          13
 14  13, 4   3, 5   5, 4   5, 3   3,13   1, 1                 14
 15  13, 3   1, 1   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  8.515 36.830  9.456 39.933 15.080 43.405 15.902 14.160   08
  9 36.830 37.400 58.007 26.134 42.828 15.884 43.140 13.683   09
 10  9.456 59.854 12.104 41.317 14.289 40.504 14.569 12.948   10
 11 39.933 40.176 41.399 13.935 42.223 60.778 37.285 10.757   11
 12 15.080 42.919 14.495 42.246 13.200 39.321 10.163          12
 13 43.405 15.752 40.266 60.803 39.305 11.850  9.791          13
 14 15.902 42.983 14.346 37.182 10.150  9.784                 14
 15 14.160 13.464 12.689 10.626                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  101
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  9 c1c24 HFP NOMINAL DEPLETION                                                     1195.87 ppm  300.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  8.100 35.473 35.449  8.303  8.799 36.420 38.191 12.649 14.165 38.327 41.713 15.143 14.623 12.939  9.214    1
  2 35.470 34.150 35.522 55.369 52.158 16.410 20.721 35.800 40.855 14.871 15.119 41.962 41.280 12.548  9.034    2
  3 35.469 35.300 36.038 51.913 49.006 20.050 24.318 35.805 40.501 14.576 14.746 41.542 40.399 12.188  8.765    3
  4  8.369 53.637 57.661  9.276 10.141 37.897 38.562 12.743 13.558 33.831 37.432 13.803 13.385 11.835  8.371    4
  5  8.928 52.522 56.955 10.297 11.404 38.821 39.350 13.002 12.879 35.622 38.079 12.595 12.414 10.901  7.619    5
  6 36.515 37.580 38.009 38.193 38.986 12.950 13.169 40.461 38.861 58.557 55.311 29.417 34.901  9.188  6.337    6
  7 38.299 38.926 38.881 38.957 39.597 13.247 13.214 40.638 39.083 57.653 53.966 31.323 35.606  6.886  4.385    7
  8 12.801 36.136 36.249 13.116 13.217 40.584 40.686 12.514 11.906 33.718 37.183  9.308  7.510                  8
  9 14.237 41.044 40.736 13.758 13.014 38.946 39.121 11.913 11.964 34.756 37.691  9.280  6.770                  9
 10 38.311 14.890 14.607 33.761 35.556 58.578 57.655 33.703 34.755 11.234 10.432  8.756  5.929                 10
 11 41.670 15.022 14.625 37.238 37.875 55.306 53.954 37.150 37.676 10.429  9.310  7.052  4.302                 11
 12 15.074 40.765 41.430 13.592 12.455 29.289 31.248  9.285  9.268  8.749  7.049                               12
 13 14.537 41.369 41.381 13.126 12.238 34.736 35.509  7.481  6.756  5.922  4.300                               13
 14 12.858 12.342 11.928 11.601 10.738  9.083  6.826                                                           14
 15  9.166  8.903  8.598  8.218  7.503  6.258  4.340                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  1.107  0.768  0.755  1.071  1.093  0.817  0.873  1.312  1.405  1.139  1.113  1.421  1.362  1.211  0.883    1
  2  0.768  0.746  0.720  0.644  0.687  0.678  0.720  1.008  1.061  1.439  1.439  1.058  1.005  1.188  0.872    2
  3  0.757  0.722  0.707  0.669  0.723  0.679  0.717  1.012  1.057  1.423  1.419  1.041  0.992  1.170  0.858    3
  4  1.076  0.661  0.648  1.109  1.173  0.892  0.934  1.327  1.374  1.086  1.048  1.346  1.297  1.150  0.833    4
  5  1.102  0.697  0.692  1.182  1.267  0.970  0.992  1.351  1.326  0.980  0.945  1.264  1.233  1.086  0.778    5
  6  0.820  0.813  0.835  0.907  0.979  1.368  1.377  1.003  0.940  0.747  0.740  0.927  0.856  0.954  0.669    6
  7  0.877  0.882  0.902  0.955  1.006  1.382  1.377  0.994  0.923  0.728  0.706  0.833  0.724  0.752  0.483    7
  8  1.323  1.025  1.034  1.354  1.366  1.008  0.996  1.320  1.272  0.932  0.841  1.034  0.843                  8
  9  1.411  1.073  1.071  1.388  1.336  0.944  0.924  1.273  1.283  0.969  0.879  1.032  0.768                  9
 10  1.141  1.442  1.427  1.091  0.984  0.749  0.729  0.932  0.969  1.227  1.146  0.975  0.680                 10
 11  1.111  1.436  1.415  1.045  0.945  0.740  0.707  0.842  0.879  1.146  1.029  0.801  0.504                 11
 12  1.418  1.037  1.008  1.337  1.259  0.926  0.833  1.035  1.032  0.975  0.801                               12
 13  1.358  0.971  0.948  1.285  1.226  0.854  0.724  0.843  0.768  0.680  0.504                               13
 14  1.206  1.178  1.157  1.140  1.080  0.951  0.750                                                           14
 15  0.881  0.866  0.849  0.827  0.774  0.667  0.482                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 1.783 in Node ( 9,13, 5)

    Pin Power Reconstruction Time = 0.29 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  1.107  0.762  1.085  0.847  1.363  1.126  1.390  1.045   08
  9  0.762  0.724  0.681  0.699  1.034  1.430  1.024  1.022   09
 10  1.085  0.674  1.183  0.947  1.345  1.015  1.285  0.962   10
 11  0.847  0.858  0.962  1.376  0.965  0.730  0.835  0.714   11
 12  1.363  1.051  1.361  0.968  1.287  0.905  0.919          12
 13  1.126  1.430  1.016  0.731  0.906  1.137  0.740          13
 14  1.390  0.991  1.277  0.834  0.920  0.740                 14
 15  1.045  1.012  0.955  0.712                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.151  0.959  1.160  0.946  1.114  0.950  1.103  1.141   08
  9  0.959  0.960  0.882  0.880  0.960  1.103  0.944  1.145   09
 10  1.160  0.867  1.144  0.954  1.118  0.972  1.119  1.154   10
 11  0.946  0.957  0.952  1.116  0.953  0.861  0.995  1.188   11
 12  1.114  0.958  1.116  0.952  1.114  0.975  1.168          12
 13  0.950  1.103  0.972  0.861  0.975  1.144  1.189          13
 14  1.103  0.924  1.121  0.995  1.168  1.189                 14
 15  1.141  1.147  1.156  1.189                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8  8.100 35.466  8.600 37.356 13.463 40.005 14.844 11.044   08
  9 35.466 35.253 52.112 20.375 38.240 14.828 41.296 10.633   09
 10  8.600 55.194 10.280 38.658 13.046 36.241 13.049  9.681   10
 11 37.356 38.349 38.933 13.145 39.761 56.372 32.812  6.699   11
 12 13.463 38.541 13.276 39.834 12.074 35.837  8.217          12
 13 40.005 14.786 36.107 56.374 35.821 10.351  6.510          13
 14 14.844 41.236 12.852 32.695  8.198  6.505                 14
 15 11.044 10.443  9.515  6.627                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  40.14   0.00   09
 10   0.00   0.00   0.00  37.53   0.00  35.27   0.00   0.00   10
 11   0.00  37.22  37.80   0.00  38.62   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  38.69   0.00   0.00   0.00          12
 13   0.00   0.00  35.14   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  102
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  9 c1c24 HFP NOMINAL DEPLETION                                                     1195.87 ppm  300.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97            14.84 25H-14 sil86  (14, 8,  )       6.51  25C-14  sil89  (14,13,  )        10.75
        6    26A        16            41.24 24L-03 24L-03 (14, 9,  )      35.25  24N-05  24N-05 ( 9, 9,  )        37.33
        8    26C        48            41.30 24J-07 24J-07 ( 9,14,  )      32.70  24L-02  24L-02 (14,11,  )        37.27
        5    25C        16            56.37 24J-03 24J-03 (13,11,  )      52.11  24K-06  24K-06 ( 9,10,  )        55.01
        9    26D         4            20.38 24N-03 24N-03 ( 9,11,  )      20.38  24N-03  24N-03 ( 9,11,  )        20.38
        7    26B        12            38.93 24J-05 24J-05 (11,10,  )      38.35  24L-05  24L-05 (11, 9,  )        38.65

     CORE              193            56.37 24J-03 24J-03 (13,11,  )       6.51  25C-14  sil89  (14,13,  )        23.52


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97            15.90 25B-08 sil51  ( 1,12,  )
        6    26A        16            42.98 24L-03 24L-03 (12, 3,  )
        8    26C        48            43.41 24P-08 24P-08 ( 1,11,  )
        5    25C        16            60.80 24J-03 24J-03 (10, 6,  )
        9    26D         4            26.13 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            41.40 24J-05 24J-05 ( 7, 5,  )

     CORE              193            60.80 24J-03 24J-03 (10, 6,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  103
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  9 c1c24 HFP NOMINAL DEPLETION                                                     1195.87 ppm  300.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  23.522 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp. 300.0 EFPD  7200.0 EFPH  10.499 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  1.200E+03 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  1.750 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  1.244E-04  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     1.675 (13, 9, 6)
       Fuel    Average . . .TFUAVE  859.17 K      1086.84 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  580.94 K       586.03 F                  F-delta-H       1.506 (13, 9)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.575 (13, 9, 2)   [Off]
 Boron Conc. . . . . . . . . . BOR  1195.9 ppm                               Max-3PIN        1.752 ( 9,13, 6)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        1.783 ( 9,13, 5)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O  -0.020                                 Total Neutron Leakage . . . . . . . . .  0.041
 Edit Ring -     1      2      3      4      5
 RPF            0.783  0.861  1.159  1.008  0.727
 KIN            0.981  0.976  1.020  1.061  1.188
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.718  0.714  0.701  0.707  0.719
 TFU (Deg K)    787.2  816.9  906.0  860.7  790.3
 TMO (Deg K)    577.0  578.4  583.7  581.2  576.3
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.31721   0.99289  10.20919   0.02288   0.66672   685.240   597.495
  23   0.76485   1.10772  16.43190   0.00000   0.66878   807.512   596.794
  22   0.94603   1.07317  20.24836   0.00000   0.67201   857.544   595.677
  21   1.05247   1.05603  23.04867   0.00000   0.67575   887.235   594.356
  20   1.08566   1.04628  24.34877   0.00000   0.67971   895.422   592.923
  19   1.08948   1.04088  24.94288   0.00000   0.68369   894.796   591.447
 
  18   1.09203   1.04000  25.45549   0.00000   0.68763   893.889   589.947
  17   1.08742   1.03836  25.68625   0.00000   0.69153   890.726   588.431
  16   1.08637   1.03815  25.90887   0.00000   0.69537   888.749   586.900
  15   1.07240   1.03670  25.68082   0.00000   0.69915   882.605   585.363
  14   1.08146   1.03776  26.02606   0.00000   0.70287   883.913   583.812
  13   1.08279   1.03667  26.07623   0.00000   0.70658   882.721   582.239
 
  12   1.07712   1.03593  25.87094   0.00000   0.71023   879.233   580.652
  11   1.09133   1.03711  26.17496   0.00000   0.71387   882.199   579.044
  10   1.09995   1.03661  26.25979   0.00000   0.71750   883.365   577.403
   9   1.10737   1.03674  26.22628   0.00000   0.72113   884.123   575.734
   8   1.11105   1.03513  26.00035   0.00000   0.72473   883.648   574.043
   7   1.13489   1.03655  26.23481   0.00000   0.72835   889.882   572.316
 
   6   1.14718   1.03779  26.03362   0.00000   0.73198   892.274   570.545
   5   1.13867   1.04032  25.20612   0.00000   0.73559   887.814   568.757
   4   1.12831   1.04933  24.38250   0.00000   0.73913   882.981   566.969
   3   1.04488   1.06686  22.06802   0.00000   0.74249   856.025   565.241
   2   0.83933   1.10279  17.69128   0.00000   0.74538   795.762   563.735
   1   0.32176   0.96086   9.98579   0.00000   0.74714   652.430   562.799
 
 Ave   1.00000   1.04253  23.52241   0.00095   0.70780   859.170   580.942
 P**2                     21.77217
 A-O  -0.02015   0.00274  -0.01451   1.00000  -0.03107     0.004     0.016
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  104
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  9 c1c24 HFP NOMINAL DEPLETION                                                     1195.87 ppm  300.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  2.25615E-09  1.29215E-09  4.38422E-09  1.56837E-08  1.02092E+01  6.57173E-01  2.64439E+01  1.34625E+03  0.00000E+00
  23  5.44216E-09  3.67179E-09  1.06161E-08  4.89063E-08  1.64319E+01  6.61930E-01  2.86011E+01  1.39505E+03  4.16010E+00
  22  6.73802E-09  4.11765E-09  1.36736E-08  5.23626E-08  2.02484E+01  6.65832E-01  2.94881E+01  1.43054E+03  4.94265E+00
  21  7.49840E-09  4.22433E-09  1.55818E-08  5.19303E-08  2.30487E+01  6.69810E-01  3.00811E+01  1.44764E+03  5.57422E+00
  20  7.73626E-09  4.25847E-09  1.62420E-08  5.20972E-08  2.43488E+01  6.74042E-01  3.02980E+01  1.46083E+03  5.86280E+00
  19  7.76408E-09  4.26209E-09  1.63749E-08  5.22961E-08  2.49429E+01  6.78336E-01  3.03505E+01  1.47096E+03  5.98926E+00
 
  18  7.78120E-09  4.22502E-09  1.64664E-08  5.16961E-08  2.54555E+01  6.82609E-01  3.03926E+01  1.47749E+03  6.10107E+00
  17  7.74735E-09  4.19777E-09  1.64161E-08  5.13965E-08  2.56862E+01  6.86869E-01  3.03783E+01  1.48200E+03  6.14954E+00
  16  7.73866E-09  4.16460E-09  1.64151E-08  5.08756E-08  2.59089E+01  6.91093E-01  3.03730E+01  1.48462E+03  6.19805E+00
  15  7.63953E-09  4.17457E-09  1.61755E-08  5.14121E-08  2.56808E+01  6.95276E-01  3.02728E+01  1.48681E+03  6.14173E+00
  14  7.70230E-09  4.12795E-09  1.63341E-08  5.03721E-08  2.60261E+01  6.99376E-01  3.03132E+01  1.48600E+03  6.22121E+00
  13  7.71100E-09  4.11512E-09  1.63494E-08  5.01216E-08  2.60762E+01  7.03477E-01  3.02932E+01  1.48520E+03  6.23123E+00
 
  12  7.67109E-09  4.12848E-09  1.62352E-08  5.04842E-08  2.58709E+01  7.07531E-01  3.02211E+01  1.48413E+03  6.18126E+00
  11  7.77081E-09  4.09139E-09  1.64657E-08  4.94948E-08  2.61750E+01  7.11528E-01  3.02744E+01  1.48050E+03  6.25107E+00
  10  7.83175E-09  4.08009E-09  1.65919E-08  4.90938E-08  2.62598E+01  7.15528E-01  3.02843E+01  1.47646E+03  6.27048E+00
   9  7.88503E-09  4.08382E-09  1.66894E-08  4.89751E-08  2.62263E+01  7.19510E-01  3.02759E+01  1.47177E+03  6.26345E+00
   8  7.91226E-09  4.10937E-09  1.67101E-08  4.92964E-08  2.60004E+01  7.23452E-01  3.02324E+01  1.46607E+03  6.21328E+00
   7  8.08099E-09  4.07912E-09  1.70710E-08  4.82338E-08  2.62348E+01  7.27361E-01  3.03125E+01  1.45667E+03  6.27369E+00
 
   6  8.16854E-09  4.08075E-09  1.72089E-08  4.79130E-08  2.60336E+01  7.31268E-01  3.03034E+01  1.44593E+03  6.23619E+00
   5  8.10906E-09  4.11195E-09  1.69604E-08  4.83943E-08  2.52061E+01  7.35106E-01  3.01545E+01  1.43342E+03  6.05797E+00
   4  8.03284E-09  4.05420E-09  1.66590E-08  4.72907E-08  2.43825E+01  7.38818E-01  3.00134E+01  1.41554E+03  5.89051E+00
   3  7.43561E-09  3.95578E-09  1.50999E-08  4.67093E-08  2.20680E+01  7.42307E-01  2.94859E+01  1.39403E+03  5.39030E+00
   2  5.96547E-09  3.60709E-09  1.15938E-08  4.46529E-08  1.76913E+01  7.45274E-01  2.83968E+01  1.35774E+03  4.48930E+00
   1  2.28624E-09  1.27449E-09  4.36974E-09  1.52781E-08  9.98579E+00  7.47098E-01  2.57384E+01  1.32813E+03  0.00000E+00
 
 Ave   7.1210E-09   3.8537E-09   1.4862E-08   4.6874E-08   2.3522E+01   7.0461E-01   2.9707E+01   1.4443E+03   5.5217E+00
 A-O       -0.020        0.013       -0.019        0.030       -0.015       -0.034        0.002        0.007       -0.015
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  1.01476E+01  1.01596E+01  1.01875E+01  1.01840E+01  9.96791E-01  9.94650E-01  1.01960E+00  1.01972E+00  9.94710E-01
  23  1.64320E+01  1.63561E+01  1.63897E+01  1.64752E+01  9.46571E-01  9.44464E-01  9.34913E-01  9.35138E-01  9.44593E-01
  22  2.02783E+01  2.01462E+01  2.01822E+01  2.03238E+01  1.01692E+00  1.01570E+00  1.00160E+00  1.00195E+00  1.01591E+00
  21  2.31059E+01  2.29275E+01  2.29664E+01  2.31543E+01  9.89306E-01  9.87298E-01  9.74725E-01  9.75059E-01  9.87562E-01
  20  2.44129E+01  2.42124E+01  2.42530E+01  2.44634E+01  9.96034E-01  9.93949E-01  9.81205E-01  9.81534E-01  9.94221E-01
  19  2.50064E+01  2.47958E+01  2.48374E+01  2.50584E+01  1.00858E+00  1.00657E+00  9.93472E-01  9.93800E-01  1.00684E+00
 
  18  2.55246E+01  2.53035E+01  2.53461E+01  2.55775E+01  9.96542E-01  9.94095E-01  9.82109E-01  9.82430E-01  9.94370E-01
  17  2.57552E+01  2.55308E+01  2.55741E+01  2.58089E+01  9.95986E-01  9.93321E-01  9.81739E-01  9.82057E-01  9.93599E-01
  16  2.59797E+01  2.57507E+01  2.57946E+01  2.60339E+01  9.88306E-01  9.85327E-01  9.74592E-01  9.74905E-01  9.85608E-01
  15  2.57391E+01  2.55174E+01  2.55616E+01  2.57942E+01  1.01674E+00  1.01415E+00  1.00227E+00  1.00259E+00  1.01441E+00
  14  2.60925E+01  2.58636E+01  2.59086E+01  2.61481E+01  9.91432E-01  9.88151E-01  9.78037E-01  9.78345E-01  9.88429E-01
  13  2.61405E+01  2.59124E+01  2.59579E+01  2.61968E+01  9.92981E-01  9.89554E-01  9.79695E-01  9.80002E-01  9.89832E-01
 
  12  2.59247E+01  2.57034E+01  2.57493E+01  2.59817E+01  1.01529E+00  1.01214E+00  1.00147E+00  1.00178E+00  1.01240E+00
  11  2.62366E+01  2.60095E+01  2.60562E+01  2.62941E+01  9.90887E-01  9.87063E-01  9.78079E-01  9.78380E-01  9.87341E-01
  10  2.63216E+01  2.60953E+01  2.61427E+01  2.63798E+01  9.86885E-01  9.82777E-01  9.74315E-01  9.74613E-01  9.83062E-01
   9  2.62841E+01  2.60605E+01  2.61084E+01  2.63430E+01  9.92507E-01  9.88360E-01  9.79982E-01  9.80280E-01  9.88638E-01
   8  2.60478E+01  2.58332E+01  2.58813E+01  2.61074E+01  1.01405E+00  1.01016E+00  1.00093E+00  1.00123E+00  1.01042E+00
   7  2.62908E+01  2.60736E+01  2.61225E+01  2.63508E+01  9.87333E-01  9.82719E-01  9.75205E-01  9.75495E-01  9.83003E-01
 
   6  2.60858E+01  2.58765E+01  2.59258E+01  2.61462E+01  9.87343E-01  9.82561E-01  9.75365E-01  9.75653E-01  9.82845E-01
   5  2.52398E+01  2.50520E+01  2.51009E+01  2.53004E+01  1.01749E+00  1.01317E+00  1.00476E+00  1.00506E+00  1.01343E+00
   4  2.44149E+01  2.42455E+01  2.42941E+01  2.44745E+01  9.91965E-01  9.87058E-01  9.80368E-01  9.80661E-01  9.87328E-01
   3  2.20792E+01  2.19535E+01  2.19999E+01  2.21365E+01  9.89606E-01  9.84703E-01  9.78581E-01  9.78877E-01  9.84946E-01
   2  1.76651E+01  1.76026E+01  1.76437E+01  1.77166E+01  9.57204E-01  9.52315E-01  9.48622E-01  9.48839E-01  9.52498E-01
   1  9.90523E+00  9.92612E+00  9.95453E+00  9.93955E+00  1.03400E+00  1.03235E+00  1.05978E+00  1.05986E+00  1.03244E+00
 
 Ave   2.3563E+01   2.3383E+01   2.3426E+01   2.3617E+01   9.9586E-01   9.9261E-01   9.8589E-01   9.8618E-01   9.9285E-01
 A-O       -0.014       -0.014       -0.014       -0.014       -0.001       -0.000       -0.002       -0.002       -0.000
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  105
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 10 c1c24 HFP NOMINAL DEPLETION                                                     1195.87 ppm  350.000 EFPD    

 Cycle exposure at end of this step = 350.00 EFPD                                                    

 Depletion step used = 50.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  106
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 10 c1c24 HFP NOMINAL DEPLETION                                                     1195.87 ppm  350.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =   350.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    25.272 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9973486     0.00265135     4.9869E-02     9.3686E-03     1.59578    -0.000     942.03    
     2  2  1.0005385    -0.00053850     4.4713E-02     8.6800E-04     1.67047    -0.033     957.38    
     3  3  1.0001798    -0.00017984     2.2164E-02     1.4865E-03     1.63425    -0.019     973.48    
     4  4  0.9999967     0.00000332 *   4.4697E-03     7.6576E-04     1.64159    -0.023     970.18    
     5  5  1.0000037 *  -0.00000366 *   8.7887E-04     4.1357E-04 *   1.64015    -0.022     968.90 *  
     6  6  0.9999980 *   0.00000196 *   1.4600E-04 *   8.9437E-05 *   1.64039    -0.022     969.56 *  

    Nodal Solution Time = 0.62 Sec

 Boron (ppm)  =  969.564
 Axial Offset =   -0.022
 Max. Node-Averaged  Peaking Factor = 1.640 in Node (13, 9, 4)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  1.215  0.849  1.192  0.935  1.458  1.162  1.449  1.297   08
  9  0.849  0.819  0.789  0.770  1.098  1.470  1.084  1.265   09
 10  1.192  0.755  1.344  1.038  1.421  1.114  1.390  1.245   10
 11  0.935  0.947  1.049  1.429  1.044  0.794  0.981  1.109   11
 12  1.458  1.108  1.433  1.047  1.372  1.024  1.130          12
 13  1.162  1.473  1.118  0.797  1.024  1.295  1.098          13
 14  1.449  1.056  1.380  0.981  1.130  1.099                 14
 15  1.297  1.254  1.236  1.107                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8   9,10   4,13  14,13  15,13  14,13   4,13  13, 4  13, 3   08
  9   4,13   1, 1  16,17   5,15   5,14   4, 5   5, 4   5, 3   09
 10  14,13  17,17  14,13  14,13   5,14   4, 5   4, 5   5, 3   10
 11  15,13  15,13  13,14  14, 5   5, 4   1, 1   3, 5   1, 1   11
 12  14,13  14, 5  13, 4   4, 5   5, 4  13, 3  13, 3          12
 13   4,13   5, 4   5, 4   1, 1   3,13   5, 4   1, 1          13
 14  13, 4   3, 5   5, 4   5, 3   3,13   1, 1                 14
 15  13, 3   3, 5   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8 10.376 38.413 11.301 41.760 17.390 45.641 18.205 16.209   08
  9 38.413 38.877 59.346 27.664 45.013 18.221 45.291 15.698   09
 10 11.301 61.177 14.210 43.404 16.536 42.615 16.765 14.906   10
 11 41.760 41.990 43.485 16.196 44.279 62.281 38.815 12.494   11
 12 17.390 45.109 16.767 44.309 15.369 41.130 11.948          12
 13 45.641 18.078 42.362 62.310 41.114 13.871 11.505          13
 14 18.205 45.002 16.532 38.664 11.937 11.498                 14
 15 16.209 15.465 14.630 12.358                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  107
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 10 c1c24 HFP NOMINAL DEPLETION                                                      969.56 ppm  350.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  9.884 37.031 36.980 10.022 10.548 38.056 39.925 14.708 16.359 40.554 43.885 17.350 16.739 14.820 10.593    1
  2 37.029 35.664 36.982 56.673 53.543 17.775 22.161 37.789 42.937 17.111 17.357 44.026 43.241 14.397 10.397    2
  3 37.002 36.765 37.469 53.263 50.458 21.414 25.749 37.801 42.575 16.794 16.954 43.576 42.338 14.011 10.106    3
  4 10.095 54.973 58.967 11.042 12.001 39.670 40.410 14.825 15.706 35.960 39.484 15.902 15.407 13.630  9.677    4
  5 10.689 53.923 58.343 12.171 13.405 40.739 41.307 15.118 14.957 37.550 39.937 14.573 14.341 12.601  8.841    5
  6 38.154 39.204 39.672 39.993 40.921 15.097 15.326 42.435 40.714 60.037 56.778 31.247 36.589 10.687  7.392    6
  7 40.040 40.675 40.668 40.843 41.579 15.412 15.370 42.595 40.905 59.098 55.371 32.975 37.043  8.076  5.151    7
  8 14.876 38.156 38.285 15.237 15.355 42.567 42.646 14.584 13.905 35.562 38.853 10.943  8.846                  8
  9 16.439 43.147 42.837 15.927 15.105 40.807 40.946 13.912 13.981 36.672 39.432 10.910  7.988                  9
 10 40.540 17.136 16.831 35.898 37.491 60.062 59.102 35.549 36.671 13.166 12.238 10.295  7.007                 10
 11 43.839 17.255 16.827 39.286 39.734 56.773 55.360 38.820 39.418 12.235 10.934  8.320  5.104                 11
 12 17.277 42.791 43.402 15.679 14.427 31.118 32.900 10.921 10.899 10.290  8.318                               12
 13 16.647 43.267 43.237 15.133 14.157 36.422 36.945  8.818  7.974  7.001  5.102                               13
 14 14.733 14.177 13.734 13.382 12.430 10.580  8.015                                                           14
 15 10.541 10.256  9.928  9.515  8.719  7.311  5.105                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  1.160  0.811  0.796  1.113  1.129  0.841  0.885  1.305  1.383  1.118  1.089  1.384  1.327  1.182  0.869    1
  2  0.811  0.789  0.760  0.677  0.716  0.705  0.739  1.008  1.049  1.409  1.405  1.035  0.984  1.163  0.860    2
  3  0.797  0.762  0.743  0.699  0.748  0.703  0.734  1.012  1.046  1.396  1.388  1.021  0.974  1.148  0.847    3
  4  1.118  0.693  0.676  1.135  1.192  0.905  0.939  1.319  1.356  1.072  1.033  1.322  1.273  1.132  0.826    4
  5  1.136  0.723  0.715  1.199  1.274  0.974  0.991  1.339  1.314  0.975  0.940  1.250  1.217  1.075  0.776    5
  6  0.842  0.833  0.850  0.917  0.981  1.359  1.365  0.998  0.939  0.753  0.747  0.928  0.856  0.952  0.672    6
  7  0.888  0.891  0.909  0.957  1.002  1.369  1.364  0.990  0.924  0.737  0.717  0.841  0.732  0.761  0.491    7
  8  1.314  1.022  1.030  1.341  1.351  1.002  0.991  1.311  1.268  0.938  0.851  1.044  0.855                  8
  9  1.388  1.060  1.058  1.369  1.321  0.942  0.925  1.268  1.280  0.973  0.886  1.040  0.779                  9
 10  1.119  1.412  1.400  1.076  0.978  0.755  0.738  0.938  0.973  1.229  1.150  0.982  0.691                 10
 11  1.088  1.403  1.385  1.031  0.940  0.747  0.718  0.852  0.886  1.150  1.034  0.811  0.515                 11
 12  1.382  1.017  0.992  1.316  1.247  0.928  0.842  1.045  1.040  0.982  0.811                               12
 13  1.324  0.954  0.934  1.265  1.213  0.855  0.732  0.856  0.780  0.691  0.516                               13
 14  1.178  1.155  1.138  1.124  1.071  0.951  0.760                                                           14
 15  0.867  0.854  0.841  0.822  0.773  0.671  0.491                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 1.734 in Node (13, 9, 5)

    Pin Power Reconstruction Time = 0.29 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  1.160  0.804  1.124  0.864  1.348  1.104  1.354  1.024   08
  9  0.804  0.763  0.710  0.720  1.029  1.400  1.003  1.004   09
 10  1.124  0.702  1.200  0.952  1.332  1.005  1.266  0.952   10
 11  0.864  0.871  0.964  1.364  0.963  0.739  0.839  0.719   11
 12  1.348  1.043  1.346  0.965  1.282  0.912  0.930          12
 13  1.104  1.400  1.007  0.739  0.912  1.141  0.750          13
 14  1.354  0.974  1.260  0.839  0.930  0.750                 14
 15  1.024  0.997  0.947  0.718                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.148  0.963  1.158  0.951  1.111  0.952  1.101  1.142   08
  9  0.963  0.965  0.888  0.890  0.963  1.099  0.946  1.146   09
 10  1.158  0.873  1.141  0.957  1.115  0.974  1.117  1.156   10
 11  0.951  0.961  0.955  1.113  0.955  0.868  0.999  1.192   11
 12  1.111  0.960  1.113  0.955  1.111  0.979  1.169          12
 13  0.952  1.100  0.974  0.868  0.979  1.143  1.193          13
 14  1.101  0.927  1.119  1.000  1.169  1.193                 14
 15  1.142  1.148  1.158  1.193                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8  9.884 37.011 10.339 39.044 15.596 42.204 17.003 12.671   08
  9 37.011 36.720 53.484 21.775 40.275 17.054 43.295 12.227   09
 10 10.339 56.552 12.155 40.532 15.152 38.233 15.056 11.187   10
 11 39.044 40.055 40.834 15.301 41.663 57.821 34.463  7.826   11
 12 15.596 40.606 15.406 41.741 14.096 37.630  9.672          12
 13 42.204 17.012 38.102 57.824 37.615 12.143  7.682          13
 14 17.003 43.174 14.848 34.346  9.653  7.678                 14
 15 12.671 12.023 11.011  7.752                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  42.08   0.00   09
 10   0.00   0.00   0.00  39.34   0.00  37.20   0.00   0.00   10
 11   0.00  38.87  39.64   0.00  40.46   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  40.54   0.00   0.00   0.00          12
 13   0.00   0.00  37.07   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  108
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 10 c1c24 HFP NOMINAL DEPLETION                                                      969.56 ppm  350.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97            17.05 25C-09 sil55  ( 9,13,  )       7.68  25C-14  sil89  (14,13,  )        12.50
        6    26A        16            43.17 24L-03 24L-03 (14, 9,  )      36.72  24N-05  24N-05 ( 9, 9,  )        38.99
        8    26C        48            43.29 24J-07 24J-07 ( 9,14,  )      34.35  24L-02  24L-02 (14,11,  )        39.18
        5    25C        16            57.82 24J-03 24J-03 (13,11,  )      53.48  24K-06  24K-06 ( 9,10,  )        56.42
        9    26D         4            21.77 24N-03 24N-03 ( 9,11,  )      21.77  24N-03  24N-03 ( 9,11,  )        21.77
        7    26B        12            40.83 24J-05 24J-05 (11,10,  )      40.06  24L-05  24L-05 (11, 9,  )        40.47

     CORE              193            57.82 24J-03 24J-03 (13,11,  )       7.68  25C-14  sil89  (14,13,  )        25.27


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97            18.22 25C-09 sil55  ( 2,11,  )
        6    26A        16            45.00 24L-03 24L-03 (12, 3,  )
        8    26C        48            45.64 24P-08 24P-08 ( 1,11,  )
        5    25C        16            62.31 24J-03 24J-03 (10, 6,  )
        9    26D         4            27.66 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            43.48 24J-05 24J-05 ( 7, 5,  )

     CORE              193            62.31 24J-03 24J-03 (10, 6,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  109
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 10 c1c24 HFP NOMINAL DEPLETION                                                      969.56 ppm  350.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  25.272 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp. 350.0 EFPD  8400.0 EFPH  12.249 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  1.200E+03 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  1.750 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  1.324E-04  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     1.640 (13, 9, 4)
       Fuel    Average . . .TFUAVE  857.88 K      1084.52 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  580.98 K       586.09 F                  F-delta-H       1.473 (13, 9)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.534 (13, 9, 2)   [Off]
 Boron Conc. . . . . . . . . . BOR   969.6 ppm                               Max-3PIN        1.713 (13, 9, 4)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        1.734 (13, 9, 5)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O  -0.022                                 Total Neutron Leakage . . . . . . . . .  0.041
 Edit Ring -     1      2      3      4      5
 RPF            0.825  0.884  1.148  1.002  0.734
 KIN            0.985  0.980  1.020  1.062  1.193
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.716  0.713  0.701  0.708  0.719
 TFU (Deg K)    801.0  822.8  904.0  856.0  787.5
 TMO (Deg K)    577.8  578.8  583.5  581.1  576.4
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.34161   1.00922  11.02394   0.02288   0.66683   690.638   597.506
  23   0.79287   1.10981  17.75987   0.00000   0.66899   813.206   596.770
  22   0.96255   1.07348  21.87541   0.00000   0.67230   859.248   595.621
  21   1.05755   1.05576  24.84739   0.00000   0.67607   885.936   594.281
  20   1.08192   1.04593  26.19647   0.00000   0.68002   892.423   592.844
  19   1.08013   1.04064  26.79221   0.00000   0.68397   890.476   591.373
 
  18   1.07929   1.04001  27.30619   0.00000   0.68786   888.912   589.886
  17   1.07323   1.03852  27.52772   0.00000   0.69171   885.495   588.385
  16   1.07168   1.03846  27.74801   0.00000   0.69549   883.563   586.873
  15   1.05833   1.03707  27.49660   0.00000   0.69921   877.466   585.354
  14   1.06753   1.03825  27.85731   0.00000   0.70288   879.123   583.822
  13   1.06939   1.03717  27.91014   0.00000   0.70654   878.180   582.268
 
  12   1.06442   1.03643  27.69576   0.00000   0.71015   874.795   580.699
  11   1.07859   1.03767  28.02392   0.00000   0.71374   878.021   579.109
  10   1.08744   1.03715  28.12366   0.00000   0.71733   879.356   577.487
   9   1.09530   1.03724  28.10319   0.00000   0.72092   880.272   575.838
   8   1.10013   1.03547  27.88456   0.00000   0.72449   880.046   574.164
   7   1.12541   1.03681  28.16094   0.00000   0.72807   886.960   572.453
 
   6   1.14125   1.03789  27.98379   0.00000   0.73168   890.404   570.695
   5   1.13940   1.04024  27.14754   0.00000   0.73528   887.402   568.911
   4   1.13877   1.04935  26.31464   0.00000   0.73884   884.978   567.115
   3   1.06878   1.06758  23.86948   0.00000   0.74225   860.422   565.361
   2   0.87615   1.10566  19.15340   0.00000   0.74524   803.483   563.807
   1   0.34832   0.97788  10.81428   0.00000   0.74710   658.364   562.821
 
 Ave   1.00000   1.04432  25.27221   0.00095   0.70779   857.882   580.977
 P**2                     23.74894
 A-O  -0.02199   0.00259  -0.01495   1.00000  -0.03080     0.003     0.016
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  110
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 10 c1c24 HFP NOMINAL DEPLETION                                                      969.56 ppm  350.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  2.42940E-09  1.32099E-09  4.81289E-09  1.57721E-08  1.10239E+01  6.58192E-01  2.64610E+01  1.25629E+03  0.00000E+00
  23  5.64229E-09  3.70648E-09  1.12008E-08  4.90736E-08  1.77599E+01  6.62988E-01  2.86077E+01  1.29031E+03  4.38428E+00
  22  6.85782E-09  4.11183E-09  1.41644E-08  5.24730E-08  2.18754E+01  6.66852E-01  2.94543E+01  1.32380E+03  5.21256E+00
  21  7.53700E-09  4.18641E-09  1.59354E-08  5.18874E-08  2.48474E+01  6.70785E-01  3.00106E+01  1.34128E+03  5.86798E+00
  20  7.71213E-09  4.20394E-09  1.64683E-08  5.19915E-08  2.61965E+01  6.74956E-01  3.02038E+01  1.35484E+03  6.16224E+00
  19  7.69991E-09  4.19883E-09  1.65119E-08  5.21631E-08  2.67922E+01  6.79180E-01  3.02391E+01  1.36525E+03  6.28773E+00
 
  18  7.69266E-09  4.15546E-09  1.65478E-08  5.15179E-08  2.73062E+01  6.83376E-01  3.02693E+01  1.37214E+03  6.39820E+00
  17  7.64832E-09  4.12572E-09  1.64708E-08  5.11961E-08  2.75277E+01  6.87557E-01  3.02481E+01  1.37688E+03  6.44433E+00
  16  7.63592E-09  4.09130E-09  1.64600E-08  5.06533E-08  2.77480E+01  6.91699E-01  3.02391E+01  1.37970E+03  6.49153E+00
  15  7.54136E-09  4.10395E-09  1.62259E-08  5.12197E-08  2.74966E+01  6.95804E-01  3.01382E+01  1.38181E+03  6.43184E+00
  14  7.60487E-09  4.05585E-09  1.63890E-08  5.01422E-08  2.78573E+01  6.99827E-01  3.01783E+01  1.38129E+03  6.51233E+00
  13  7.61740E-09  4.04360E-09  1.64140E-08  4.98886E-08  2.79101E+01  7.03854E-01  3.01595E+01  1.38058E+03  6.52232E+00
 
  12  7.58271E-09  4.05932E-09  1.63109E-08  5.02792E-08  2.76958E+01  7.07835E-01  3.00896E+01  1.37948E+03  6.47117E+00
  11  7.68201E-09  4.02026E-09  1.65469E-08  4.92568E-08  2.80239E+01  7.11764E-01  3.01444E+01  1.37619E+03  6.54338E+00
  10  7.74457E-09  4.00854E-09  1.66820E-08  4.88492E-08  2.81237E+01  7.15698E-01  3.01570E+01  1.37238E+03  6.56444E+00
   9  7.80115E-09  4.01296E-09  1.67925E-08  4.87421E-08  2.81032E+01  7.19616E-01  3.01528E+01  1.36784E+03  6.55916E+00
   8  7.83693E-09  4.04105E-09  1.68373E-08  4.90957E-08  2.78846E+01  7.23501E-01  3.01157E+01  1.36216E+03  6.51054E+00
   7  8.01570E-09  4.01000E-09  1.72334E-08  4.80067E-08  2.81609E+01  7.27359E-01  3.02045E+01  1.35315E+03  6.57611E+00
 
   6  8.12873E-09  4.01562E-09  1.74377E-08  4.77033E-08  2.79838E+01  7.31223E-01  3.02093E+01  1.34254E+03  6.54229E+00
   5  8.11711E-09  4.05815E-09  1.72970E-08  4.82575E-08  2.71475E+01  7.35030E-01  3.00803E+01  1.32987E+03  6.36441E+00
   4  8.10992E-09  4.01217E-09  1.71467E-08  4.71713E-08  2.63146E+01  7.38730E-01  2.99647E+01  1.31218E+03  6.19604E+00
   3  7.60791E-09  3.94014E-09  1.57565E-08  4.66736E-08  2.38695E+01  7.42227E-01  2.94678E+01  1.29080E+03  5.67849E+00
   2  6.22796E-09  3.63678E-09  1.23423E-08  4.47269E-08  1.91534E+01  7.45227E-01  2.84211E+01  1.25627E+03  4.72816E+00
   1  2.47502E-09  1.30545E-09  4.83061E-09  1.53883E-08  1.08143E+01  7.47086E-01  2.57716E+01  1.24015E+03  0.00000E+00
 
 Ave   7.1229E-09   3.8094E-09   1.5117E-08   4.6755E-08   2.5272E+01   7.0502E-01   2.9624E+01   1.3411E+03   5.7937E+00
 A-O       -0.022        0.013       -0.021        0.030       -0.015       -0.034        0.002        0.007       -0.015
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  1.09854E+01  1.09443E+01  1.09720E+01  1.10216E+01  9.94036E-01  9.91943E-01  1.01563E+00  1.01575E+00  9.92012E-01
  23  1.77989E+01  1.76453E+01  1.76783E+01  1.78415E+01  9.45322E-01  9.44025E-01  9.33166E-01  9.33387E-01  9.44148E-01
  22  2.19514E+01  2.17289E+01  2.17642E+01  2.19962E+01  1.01718E+00  1.01683E+00  1.00129E+00  1.00164E+00  1.01703E+00
  21  2.49559E+01  2.46777E+01  2.47157E+01  2.50035E+01  9.89186E-01  9.88028E-01  9.74174E-01  9.74498E-01  9.88274E-01
  20  2.63130E+01  2.60112E+01  2.60509E+01  2.63628E+01  9.96005E-01  9.94746E-01  9.80773E-01  9.81091E-01  9.94998E-01
  19  2.69079E+01  2.65968E+01  2.66376E+01  2.69590E+01  1.00875E+00  1.00754E+00  9.93230E-01  9.93546E-01  1.00779E+00
 
  18  2.74279E+01  2.71057E+01  2.71474E+01  2.74800E+01  9.96537E-01  9.94866E-01  9.81731E-01  9.82040E-01  9.95121E-01
  17  2.76493E+01  2.73241E+01  2.73665E+01  2.77021E+01  9.95934E-01  9.94026E-01  9.81326E-01  9.81634E-01  9.94284E-01
  16  2.78718E+01  2.75414E+01  2.75844E+01  2.79253E+01  9.88157E-01  9.85919E-01  9.74099E-01  9.74402E-01  9.86180E-01
  15  2.76067E+01  2.72861E+01  2.73294E+01  2.76609E+01  1.01701E+00  1.01516E+00  1.00214E+00  1.00245E+00  1.01540E+00
  14  2.79769E+01  2.76465E+01  2.76907E+01  2.80318E+01  9.91350E-01  9.88793E-01  9.77610E-01  9.77909E-01  9.89051E-01
  13  2.80279E+01  2.76978E+01  2.77425E+01  2.80833E+01  9.92887E-01  9.90176E-01  9.79259E-01  9.79557E-01  9.90434E-01
 
  12  2.78022E+01  2.74805E+01  2.75254E+01  2.78584E+01  1.01554E+00  1.01310E+00  1.00133E+00  1.00163E+00  1.01334E+00
  11  2.81398E+01  2.78092E+01  2.78550E+01  2.81965E+01  9.90767E-01  9.87649E-01  9.77627E-01  9.77920E-01  9.87907E-01
  10  2.82403E+01  2.79093E+01  2.79558E+01  2.82977E+01  9.86673E-01  9.83267E-01  9.73789E-01  9.74078E-01  9.83532E-01
   9  2.82162E+01  2.78871E+01  2.79340E+01  2.82742E+01  9.92420E-01  9.88976E-01  9.79569E-01  9.79857E-01  9.89232E-01
   8  2.79869E+01  2.76673E+01  2.77144E+01  2.80456E+01  1.01428E+00  1.01109E+00  1.00079E+00  1.00108E+00  1.01133E+00
   7  2.82740E+01  2.79473E+01  2.79953E+01  2.83332E+01  9.87141E-01  9.83236E-01  9.74715E-01  9.74997E-01  9.83499E-01
 
   6  2.80939E+01  2.77731E+01  2.78215E+01  2.81535E+01  9.87153E-01  9.83088E-01  9.74881E-01  9.75162E-01  9.83352E-01
   5  2.72382E+01  2.69403E+01  2.69883E+01  2.72978E+01  1.01779E+00  1.01419E+00  1.00468E+00  1.00497E+00  1.01442E+00
   4  2.64049E+01  2.61230E+01  2.61706E+01  2.64636E+01  9.91867E-01  9.87689E-01  9.79934E-01  9.80220E-01  9.87939E-01
   3  2.39350E+01  2.37025E+01  2.37480E+01  2.39915E+01  9.89457E-01  9.85270E-01  9.78048E-01  9.78341E-01  9.85500E-01
   2  1.91719E+01  1.90200E+01  1.90604E+01  1.92227E+01  9.56027E-01  9.51773E-01  9.47015E-01  9.47231E-01  9.51952E-01
   1  1.07567E+01  1.07241E+01  1.07524E+01  1.07909E+01  1.03285E+00  1.03099E+00  1.05729E+00  1.05738E+00  1.03109E+00
 
 Ave   2.5364E+01   2.5085E+01   2.5128E+01   2.5417E+01   9.9560E-01   9.9301E-01   9.8517E-01   9.8545E-01   9.9324E-01
 A-O       -0.015       -0.015       -0.015       -0.015       -0.001       -0.000       -0.002       -0.002       -0.000
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  111
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 11 c1c24 HFP NOMINAL DEPLETION                                                      969.56 ppm  400.000 EFPD    

 Cycle exposure at end of this step = 400.00 EFPD                                                    

 Depletion step used = 50.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  112
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 11 c1c24 HFP NOMINAL DEPLETION                                                      969.56 ppm  400.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =   400.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    27.022 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9973758     0.00262416     5.0509E-02     8.9464E-03     1.56152    -0.001     725.28    
     2  2  1.0005290    -0.00052903     4.9636E-02     2.0130E-03     1.64307    -0.035     740.02    
     3  3  1.0001817    -0.00018168     2.2707E-02     8.2046E-04     1.60659    -0.021     755.81    
     4  4  0.9999957     0.00000426 *   4.6939E-03     1.0730E-04 *   1.61417    -0.024     752.84    
     5  5  1.0000035 *  -0.00000347 *   9.4058E-04     4.4692E-04 *   1.61265    -0.023     751.67 *  
     6  6  0.9999980 *   0.00000201 *   1.3474E-04 *   7.3433E-05 *   1.61287    -0.023     752.34 *  

    Nodal Solution Time = 0.61 Sec

 Boron (ppm)  =  752.336
 Axial Offset =   -0.023
 Max. Node-Averaged  Peaking Factor = 1.613 in Node (13, 9, 4)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  1.256  0.886  1.219  0.941  1.434  1.141  1.416  1.269   08
  9  0.886  0.855  0.806  0.785  1.084  1.442  1.062  1.241   09
 10  1.219  0.771  1.345  1.033  1.402  1.099  1.366  1.227   10
 11  0.941  0.950  1.042  1.412  1.035  0.795  0.981  1.105   11
 12  1.434  1.093  1.413  1.038  1.359  1.024  1.137          12
 13  1.141  1.444  1.103  0.797  1.025  1.295  1.105          13
 14  1.416  1.038  1.360  0.981  1.138  1.106                 14
 15  1.269  1.233  1.220  1.104                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8   9,10   4,13  14,13  15,13  14,13   4,13  13, 4  13, 3   08
  9   4,13   1, 1  16,17   5,15   5,14   4, 5   5, 4   5, 3   09
 10  14,13  13, 3  14,13  14,13   5,14   4, 5   4, 5   5, 3   10
 11  15,13  15,13  13,14  13, 4   5, 4   1, 1   3, 5   1, 1   11
 12  14,13  14, 5  13, 4   4, 5   5, 4  13, 3  13, 3          12
 13   4,13   5, 4   4, 5   1, 1   3,13   5, 4   1, 1          13
 14  13, 4   3, 5   5, 4   5, 3   3,13   1, 1                 14
 15  13, 3   3, 5   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8 12.314 40.108 13.194 43.603 19.660 47.826 20.450 18.210   08
  9 40.108 40.421 60.746 29.216 47.170 20.504 47.393 17.672   09
 10 13.194 62.551 16.320 45.482 18.752 44.693 18.919 16.831   10
 11 43.603 43.811 45.558 18.431 46.324 63.796 40.440 14.225   11
 12 19.660 47.268 19.004 46.389 17.519 42.950 13.747          12
 13 47.826 20.352 44.430 63.828 42.935 15.890 13.229          13
 14 20.450 46.987 18.681 40.287 13.736 13.223                 14
 15 18.210 17.430 16.544 14.087                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  113
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 11 c1c24 HFP NOMINAL DEPLETION                                                      752.34 ppm  400.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1 11.742 38.667 38.584 11.802 12.348 39.735 41.683 16.757 18.520 42.741 46.014 19.504 18.803 16.661 11.953    1
  2 38.665 37.257 38.515 58.038 54.982 19.190 23.635 39.778 44.997 19.309 19.545 46.047 45.164 16.210 11.743    2
  3 38.609 38.303 38.967 54.668 51.957 22.822 27.212 39.796 44.629 18.971 19.118 45.572 44.243 15.803 11.433    3
  4 11.882 56.368 60.325 12.847 13.889 41.466 42.267 16.894 17.828 38.062 41.509 17.966 17.395 15.399 10.974    4
  5 12.499 55.373 59.773 14.067 15.413 42.661 43.259 17.216 17.014 39.467 41.787 16.531 16.246 14.284 10.061    5
  6 39.833 40.864 41.363 41.808 42.857 17.227 17.463 44.398 42.565 61.529 58.257 33.081 38.279 12.186  8.454    6
  7 41.802 42.442 42.466 42.730 43.551 17.555 17.505 44.542 42.728 60.561 56.796 34.643 38.495  9.280  5.932    7
  8 16.937 40.169 40.313 17.336 17.469 44.536 44.594 16.638 15.895 37.417 40.541 12.593 10.202                  8
  9 18.608 45.226 44.913 18.067 17.174 42.663 42.771 15.904 15.991 38.594 41.186 12.552  9.224                  9
 10 42.729 19.337 19.014 38.008 39.416 61.557 60.567 37.404 38.593 15.100 14.049 11.845  8.104                 10
 11 45.966 19.441 18.988 41.309 41.585 58.254 56.786 40.510 41.173 14.047 12.566  9.605  5.925                 11
 12 19.429 44.781 45.346 17.735 16.381 32.952 34.570 12.573 12.541 11.840  9.603                               12
 13 18.707 45.136 45.069 17.109 16.057 38.111 38.399 10.175  9.211  8.098  5.923                               13
 14 16.568 15.979 15.512 15.140 14.110 12.077  9.220                                                           14
 15 11.897 11.594 11.247 10.806  9.936  8.371  5.885                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  1.202  0.847  0.830  1.149  1.159  0.861  0.896  1.299  1.364  1.099  1.069  1.354  1.297  1.158  0.859    1
  2  0.847  0.826  0.794  0.706  0.742  0.729  0.756  1.008  1.039  1.383  1.376  1.015  0.966  1.142  0.851    2
  3  0.831  0.796  0.775  0.725  0.771  0.724  0.749  1.011  1.036  1.372  1.361  1.003  0.958  1.130  0.840    3
  4  1.153  0.721  0.701  1.158  1.207  0.915  0.943  1.310  1.340  1.059  1.019  1.301  1.253  1.116  0.822    4
  5  1.164  0.746  0.735  1.211  1.278  0.975  0.988  1.327  1.301  0.969  0.935  1.238  1.204  1.066  0.776    5
  6  0.860  0.850  0.863  0.923  0.981  1.349  1.351  0.991  0.937  0.759  0.753  0.930  0.857  0.953  0.677    6
  7  0.897  0.899  0.914  0.956  0.997  1.355  1.349  0.984  0.924  0.746  0.728  0.850  0.740  0.771  0.501    7
  8  1.306  1.019  1.025  1.328  1.336  0.994  0.985  1.300  1.262  0.942  0.860  1.053  0.868                  8
  9  1.368  1.048  1.046  1.350  1.307  0.939  0.925  1.262  1.275  0.975  0.892  1.047  0.791                  9
 10  1.100  1.386  1.375  1.063  0.973  0.761  0.746  0.943  0.975  1.229  1.152  0.988  0.702                 10
 11  1.068  1.375  1.360  1.019  0.936  0.754  0.728  0.861  0.893  1.153  1.039  0.822  0.528                 11
 12  1.353  1.001  0.979  1.297  1.237  0.931  0.851  1.054  1.048  0.989  0.822                               12
 13  1.295  0.940  0.924  1.248  1.202  0.857  0.741  0.869  0.792  0.703  0.528                               13
 14  1.155  1.136  1.123  1.111  1.064  0.953  0.771                                                           14
 15  0.858  0.847  0.836  0.819  0.774  0.677  0.501                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 1.695 in Node (13, 9, 4)

    Pin Power Reconstruction Time = 0.29 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  1.202  0.839  1.156  0.879  1.334  1.084  1.325  1.008   08
  9  0.839  0.798  0.736  0.740  1.023  1.373  0.985  0.991   09
 10  1.156  0.726  1.213  0.955  1.319  0.996  1.249  0.945   10
 11  0.879  0.881  0.964  1.351  0.959  0.746  0.844  0.725   11
 12  1.334  1.034  1.330  0.961  1.275  0.917  0.940          12
 13  1.084  1.374  0.998  0.747  0.918  1.143  0.760          13
 14  1.325  0.961  1.246  0.845  0.941  0.760                 14
 15  1.008  0.985  0.942  0.725                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.144  0.967  1.156  0.955  1.108  0.953  1.098  1.143   08
  9  0.967  0.970  0.894  0.899  0.965  1.096  0.948  1.147   09
 10  1.156  0.880  1.139  0.959  1.112  0.976  1.115  1.158   10
 11  0.955  0.964  0.957  1.109  0.957  0.874  1.004  1.197   11
 12  1.108  0.962  1.109  0.957  1.108  0.982  1.170          12
 13  0.953  1.096  0.976  0.874  0.982  1.142  1.196          13
 14  1.098  0.931  1.117  1.004  1.171  1.196                 14
 15  1.143  1.149  1.159  1.197                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8 11.743 38.632 12.133 40.763 17.706 44.362 19.110 14.269   08
  9 38.632 38.261 54.911 23.215 42.300 19.235 45.256 13.797   09
 10 12.133 57.960 14.054 42.414 17.238 40.206 17.034 12.679   10
 11 40.763 41.784 42.737 17.438 43.558 59.286 36.125  8.963   11
 12 17.706 42.655 17.511 43.641 16.107 39.434 11.143          12
 13 44.362 19.195 40.079 59.291 39.420 13.941  8.870          13
 14 19.110 45.082 16.820 36.008 11.125  8.866                 14
 15 14.270 13.583 12.498  8.888                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  43.98   0.00   09
 10   0.00   0.00   0.00  41.16   0.00  39.11   0.00   0.00   10
 11   0.00  40.54  41.48   0.00  42.30   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  42.38   0.00   0.00   0.00          12
 13   0.00   0.00  38.98   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  114
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 11 c1c24 HFP NOMINAL DEPLETION                                                      752.34 ppm  400.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97            19.24 25C-09 sil55  ( 9,13,  )       8.87  25C-14  sil89  (14,13,  )        14.23
        6    26A        16            45.08 24L-03 24L-03 (14, 9,  )      38.26  24N-05  24N-05 ( 9, 9,  )        40.68
        8    26C        48            45.26 24J-07 24J-07 ( 9,14,  )      36.01  24L-02  24L-02 (14,11,  )        41.09
        5    25C        16            59.29 24J-03 24J-03 (13,11,  )      54.91  24K-06  24K-06 ( 9,10,  )        57.86
        9    26D         4            23.22 24N-03 24N-03 ( 9,11,  )      23.22  24N-03  24N-03 ( 9,11,  )        23.22
        7    26B        12            42.74 24J-05 24J-05 (11,10,  )      41.78  24L-05  24L-05 (11, 9,  )        42.31

     CORE              193            59.29 24J-03 24J-03 (13,11,  )       8.87  25C-14  sil89  (14,13,  )        27.02


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97            20.50 25C-09 sil55  ( 2,11,  )
        6    26A        16            46.99 24L-03 24L-03 (12, 3,  )
        8    26C        48            47.83 24P-08 24P-08 ( 1,11,  )
        5    25C        16            63.83 24J-03 24J-03 (10, 6,  )
        9    26D         4            29.22 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            45.56 24J-05 24J-05 ( 7, 5,  )

     CORE              193            63.83 24J-03 24J-03 (10, 6,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  115
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 11 c1c24 HFP NOMINAL DEPLETION                                                      752.34 ppm  400.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  27.022 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp. 400.0 EFPD  9600.0 EFPH  13.998 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  1.200E+03 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  1.750 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  1.401E-04  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     1.613 (13, 9, 4)
       Fuel    Average . . .TFUAVE  857.65 K      1084.10 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  581.00 K       586.14 F                  F-delta-H       1.444 (13, 9)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.499 (13, 9, 2)   [Off]
 Boron Conc. . . . . . . . . . BOR   752.3 ppm                               Max-3PIN        1.683 (13, 9, 4)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        1.695 (13, 9, 4)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O  -0.023                                 Total Neutron Leakage . . . . . . . . .  0.042
 Edit Ring -     1      2      3      4      5
 RPF            0.861  0.904  1.137  0.998  0.743
 KIN            0.988  0.984  1.019  1.064  1.197
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.714  0.712  0.702  0.708  0.718
 TFU (Deg K)    813.3  828.1  903.1  853.5  784.7
 TMO (Deg K)    578.5  579.2  583.4  581.0  576.6
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.36642   1.02460  11.89958   0.02288   0.66693   695.880   597.512
  23   0.81890   1.11138  19.13396   0.00000   0.66918   818.098   596.741
  22   0.97641   1.07349  23.52840   0.00000   0.67256   860.508   595.563
  21   1.06037   1.05528  26.65289   0.00000   0.67636   886.167   594.210
  20   1.07698   1.04546  28.03682   0.00000   0.68030   891.050   592.771
  19   1.07070   1.04033  28.62555   0.00000   0.68421   887.885   591.307
 
  18   1.06745   1.03996  29.13592   0.00000   0.68806   885.664   589.831
  17   1.06071   1.03862  29.34642   0.00000   0.69186   882.056   588.346
  16   1.05921   1.03871  29.56400   0.00000   0.69559   880.183   586.849
  15   1.04681   1.03736  29.29053   0.00000   0.69927   874.306   585.346
  14   1.05637   1.03866  29.66714   0.00000   0.70290   876.197   583.830
  13   1.05886   1.03757  29.72362   0.00000   0.70651   875.488   582.290
 
  12   1.05454   1.03684  29.50129   0.00000   0.71009   872.278   580.736
  11   1.06859   1.03814  29.85347   0.00000   0.71365   875.593   579.160
  10   1.07744   1.03759  29.96830   0.00000   0.71720   876.994   577.553
   9   1.08530   1.03766  29.96126   0.00000   0.72075   877.961   575.918
   8   1.09063   1.03572  29.75135   0.00000   0.72429   877.904   574.260
   7   1.11643   1.03700  30.07133   0.00000   0.72785   885.153   572.564
 
   6   1.13453   1.03790  29.92317   0.00000   0.73144   889.405   570.818
   5   1.13773   1.04000  29.08816   0.00000   0.73502   887.962   569.042
   4   1.14515   1.04913  28.26112   0.00000   0.73859   887.954   567.243
   3   1.08763   1.06789  25.70737   0.00000   0.74204   865.267   565.470
   2   0.90878   1.10796  20.67475   0.00000   0.74512   809.876   563.875
   1   0.37406   0.99395  11.70748   0.00000   0.74706   663.766   562.841
 
 Ave   1.00000   1.04588  27.02200   0.00095   0.70778   857.650   581.003
 P**2                     25.67986
 A-O  -0.02340   0.00245  -0.01544   1.00000  -0.03057     0.002     0.016
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  116
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 11 c1c24 HFP NOMINAL DEPLETION                                                      752.34 ppm  400.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  2.60552E-09  1.34375E-09  5.26195E-09  1.58235E-08  1.18996E+01  6.59120E-01  2.64825E+01  1.15778E+03  0.00000E+00
  23  5.82840E-09  3.72924E-09  1.17621E-08  4.91132E-08  1.91340E+01  6.63909E-01  2.86181E+01  1.17591E+03  4.61552E+00
  22  6.95895E-09  4.09534E-09  1.46067E-08  5.24214E-08  2.35284E+01  6.67732E-01  2.94253E+01  1.20815E+03  5.48590E+00
  21  7.55958E-09  4.14022E-09  1.62375E-08  5.16865E-08  2.66529E+01  6.71625E-01  2.99565E+01  1.22678E+03  6.16178E+00
  20  7.67941E-09  4.14357E-09  1.66519E-08  5.17268E-08  2.80368E+01  6.75742E-01  3.01291E+01  1.24124E+03  6.45934E+00
  19  7.63518E-09  4.13186E-09  1.66201E-08  5.18738E-08  2.86256E+01  6.79902E-01  3.01489E+01  1.25226E+03  6.58244E+00
 
  18  7.61055E-09  4.08384E-09  1.66136E-08  5.11866E-08  2.91359E+01  6.84030E-01  3.01687E+01  1.25974E+03  6.69076E+00
  17  7.56117E-09  4.05283E-09  1.65212E-08  5.08447E-08  2.93464E+01  6.88142E-01  3.01418E+01  1.26481E+03  6.73426E+00
  16  7.54899E-09  4.01794E-09  1.65089E-08  5.02818E-08  2.95640E+01  6.92214E-01  3.01302E+01  1.26787E+03  6.78008E+00
  15  7.46134E-09  4.03387E-09  1.62863E-08  5.08806E-08  2.92905E+01  6.96251E-01  3.00295E+01  1.26987E+03  6.71728E+00
  14  7.52724E-09  3.98437E-09  1.64571E-08  4.97646E-08  2.96671E+01  7.00209E-01  3.00698E+01  1.26966E+03  6.79884E+00
  13  7.54420E-09  3.97273E-09  1.64931E-08  4.95078E-08  2.97236E+01  7.04171E-01  3.00524E+01  1.26901E+03  6.80897E+00
 
  12  7.51444E-09  3.99076E-09  1.64011E-08  4.99263E-08  2.95013E+01  7.08092E-01  2.99846E+01  1.26783E+03  6.75685E+00
  11  7.61272E-09  3.94931E-09  1.66401E-08  4.88688E-08  2.98535E+01  7.11962E-01  3.00406E+01  1.26488E+03  6.83140E+00
  10  7.67524E-09  3.93665E-09  1.67798E-08  4.84522E-08  2.99683E+01  7.15838E-01  3.00551E+01  1.26127E+03  6.85415E+00
   9  7.73208E-09  3.94106E-09  1.68961E-08  4.83545E-08  2.99613E+01  7.19701E-01  3.00540E+01  1.25683E+03  6.85067E+00
   8  7.77171E-09  3.97069E-09  1.69549E-08  4.87384E-08  2.97513E+01  7.23536E-01  3.00220E+01  1.25110E+03  6.80384E+00
   7  7.95402E-09  3.93729E-09  1.73711E-08  4.76179E-08  3.00713E+01  7.27348E-01  3.01175E+01  1.24238E+03  6.87478E+00
 
   6  8.08330E-09  3.94490E-09  1.76229E-08  4.73286E-08  2.99232E+01  7.31173E-01  3.01342E+01  1.23170E+03  6.84542E+00
   5  8.10823E-09  3.99615E-09  1.75704E-08  4.79544E-08  2.90882E+01  7.34951E-01  3.00238E+01  1.21851E+03  6.66950E+00
   4  8.15811E-09  3.95877E-09  1.75530E-08  4.68843E-08  2.82611E+01  7.38638E-01  2.99331E+01  1.20055E+03  6.50265E+00
   3  7.74453E-09  3.90979E-09  1.63321E-08  4.64806E-08  2.57074E+01  7.42143E-01  2.94610E+01  1.17856E+03  5.97147E+00
   2  6.46099E-09  3.64896E-09  1.30384E-08  4.46601E-08  2.06748E+01  7.45176E-01  2.84450E+01  1.14521E+03  4.97586E+00
   1  2.65820E-09  1.32854E-09  5.29665E-09  1.54659E-08  1.17075E+01  7.47073E-01  2.58055E+01  1.14433E+03  0.00000E+00
 
 Ave   7.1248E-09   3.7601E-09   1.5353E-08   4.6493E-08   2.7022E+01   7.0536E-01   2.9560E+01   1.2303E+03   6.0641E+00
 A-O       -0.023        0.013       -0.023        0.031       -0.015       -0.033        0.002        0.007       -0.015
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  1.18858E+01  1.17877E+01  1.18152E+01  1.19219E+01  9.90717E-01  9.88670E-01  1.01106E+00  1.01117E+00  9.88744E-01
  23  1.92131E+01  1.89806E+01  1.90130E+01  1.92551E+01  9.43863E-01  9.43318E-01  9.31153E-01  9.31366E-01  9.43430E-01
  22  2.36507E+01  2.33388E+01  2.33733E+01  2.36948E+01  1.01739E+00  1.01785E+00  1.00090E+00  1.00123E+00  1.01804E+00
  21  2.68124E+01  2.64365E+01  2.64737E+01  2.68593E+01  9.89017E-01  9.88646E-01  9.73529E-01  9.73839E-01  9.88871E-01
  20  2.82051E+01  2.78049E+01  2.78438E+01  2.82541E+01  9.95927E-01  9.95433E-01  9.80247E-01  9.80550E-01  9.95662E-01
  19  2.87924E+01  2.83845E+01  2.84244E+01  2.88428E+01  1.00887E+00  1.00841E+00  9.92897E-01  9.93198E-01  1.00863E+00
 
  18  2.93093E+01  2.88897E+01  2.89305E+01  2.93606E+01  9.96487E-01  9.95538E-01  9.81258E-01  9.81553E-01  9.95770E-01
  17  2.95196E+01  2.90973E+01  2.91389E+01  2.95717E+01  9.95830E-01  9.94628E-01  9.80810E-01  9.81104E-01  9.94863E-01
  16  2.97398E+01  2.93116E+01  2.93538E+01  2.97926E+01  9.87962E-01  9.86414E-01  9.73506E-01  9.73795E-01  9.86652E-01
  15  2.94515E+01  2.90356E+01  2.90780E+01  2.95050E+01  1.01725E+00  1.01609E+00  1.00192E+00  1.00222E+00  1.01630E+00
  14  2.98392E+01  2.94106E+01  2.94539E+01  2.98932E+01  9.91225E-01  9.89343E-01  9.77084E-01  9.77369E-01  9.89578E-01
  13  2.98941E+01  2.94654E+01  2.95092E+01  2.99487E+01  9.92748E-01  9.90707E-01  9.78723E-01  9.79008E-01  9.90942E-01
 
  12  2.96597E+01  2.92408E+01  2.92849E+01  2.97151E+01  1.01575E+00  1.01398E+00  1.00110E+00  1.00139E+00  1.01420E+00
  11  3.00229E+01  2.95921E+01  2.96370E+01  3.00789E+01  9.90606E-01  9.88149E-01  9.77079E-01  9.77359E-01  9.88383E-01
  10  3.01393E+01  2.97066E+01  2.97522E+01  3.01959E+01  9.86409E-01  9.83661E-01  9.73157E-01  9.73434E-01  9.83902E-01
   9  3.01289E+01  2.96974E+01  2.97434E+01  3.01861E+01  9.92294E-01  9.89508E-01  9.79063E-01  9.79339E-01  9.89741E-01
   8  2.99080E+01  2.94865E+01  2.95327E+01  2.99658E+01  1.01446E+00  1.01194E+00  1.00056E+00  1.00084E+00  1.01216E+00
   7  3.02410E+01  2.98078E+01  2.98549E+01  3.02993E+01  9.86902E-01  9.83660E-01  9.74126E-01  9.74395E-01  9.83899E-01
 
   6  3.00909E+01  2.96614E+01  2.97088E+01  3.01496E+01  9.86919E-01  9.83526E-01  9.74301E-01  9.74569E-01  9.83763E-01
   5  2.92357E+01  2.88301E+01  2.88770E+01  2.92944E+01  1.01806E+00  1.01515E+00  1.00454E+00  1.00482E+00  1.01536E+00
   4  2.84098E+01  2.80164E+01  2.80630E+01  2.84676E+01  9.91738E-01  9.88244E-01  9.79421E-01  9.79695E-01  9.88470E-01
   3  2.58285E+01  2.54887E+01  2.55333E+01  2.58841E+01  9.89261E-01  9.85752E-01  9.77421E-01  9.77703E-01  9.85960E-01
   2  2.07400E+01  2.04960E+01  2.05356E+01  2.07901E+01  9.54712E-01  9.51059E-01  9.45215E-01  9.45424E-01  9.51225E-01
   1  1.16749E+01  1.15843E+01  1.16124E+01  1.17090E+01  1.03162E+00  1.02957E+00  1.05467E+00  1.05477E+00  1.02968E+00
 
 Ave   2.7165E+01   2.6789E+01   2.6831E+01   2.7217E+01   9.9525E-01   9.9330E-01   9.8432E-01   9.8459E-01   9.9351E-01
 A-O       -0.015       -0.015       -0.015       -0.015       -0.001       -0.000       -0.002       -0.002       -0.000
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  117
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 12 c1c24 HFP NOMINAL DEPLETION                                                      752.34 ppm  450.000 EFPD    

 Cycle exposure at end of this step = 450.00 EFPD                                                    

 Depletion step used = 50.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  118
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 12 c1c24 HFP NOMINAL DEPLETION                                                      752.34 ppm  450.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =   450.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    28.772 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9974028     0.00259715     5.3824E-02     8.3356E-03     1.53049    -0.002     517.17    
     2  2  1.0005231    -0.00052315     5.2712E-02     3.6042E-03     1.61565    -0.037     531.53    
     3  3  1.0001817    -0.00018173     2.3055E-02     3.1489E-04 *   1.57924    -0.022     546.90    
     4  4  0.9999851     0.00001494 *   4.8391E-03     4.8994E-04 *   1.58692    -0.025     544.17    
     5  5  1.0000039 *  -0.00000388 *   8.8562E-04     9.2849E-05 *   1.58552    -0.024     543.12 *  
     6  6  0.9999977 *   0.00000229 *   5.2026E-05 *   3.7849E-05 *   1.58560    -0.024     543.85 *  

    Nodal Solution Time = 0.62 Sec

 Boron (ppm)  =  543.853
 Axial Offset =   -0.024
 Max. Node-Averaged  Peaking Factor = 1.586 in Node (13, 9, 4)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  1.287  0.916  1.241  0.945  1.413  1.122  1.387  1.246   08
  9  0.916  0.884  0.820  0.799  1.071  1.416  1.042  1.221   09
 10  1.241  0.792  1.344  1.026  1.385  1.085  1.346  1.212   10
 11  0.945  0.951  1.034  1.396  1.026  0.796  0.981  1.102   11
 12  1.413  1.079  1.393  1.028  1.346  1.024  1.143          12
 13  1.122  1.418  1.089  0.798  1.024  1.294  1.111          13
 14  1.387  1.022  1.343  0.983  1.144  1.111                 14
 15  1.246  1.217  1.208  1.103                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8   9,10   4,13  14,13  13,15  14,13   4,13  13, 4  13, 3   08
  9   4,13   1, 1  16,17   5,15   5,14   4, 5   5, 4   5, 3   09
 10  14,13  13, 3  14,13  14,13   4,13   5, 4   4, 5   5, 3   10
 11  13,15  15,13  13,14  13, 4   5, 4   1, 1   3, 5   1, 1   11
 12  14,13  14, 5  13, 4   4, 5   5, 4  13, 3  13, 3          12
 13   4,13   5, 4   4, 5   1, 1   3,13   5, 4   1, 1          13
 14  13, 4   3, 5   5, 4   5, 3   3,13   1, 1                 14
 15  13, 3   3, 5   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8 14.310 41.867 15.126 45.457 21.896 49.969 22.646 20.172   08
  9 41.867 42.024 62.201 30.788 49.302 22.739 49.450 19.612   09
 10 15.126 63.968 18.430 47.546 20.939 46.744 21.038 18.730   10
 11 45.457 45.635 47.613 20.640 48.352 65.320 42.108 15.953   11
 12 21.896 49.399 21.210 48.449 19.648 44.781 15.557          12
 13 49.969 22.581 46.473 65.356 44.767 17.906 14.961          13
 14 22.646 48.947 20.799 41.935 15.548 14.956                 14
 15 20.172 19.366 18.436 15.815                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  119
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 12 c1c24 HFP NOMINAL DEPLETION                                                      543.85 ppm  450.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1 13.661 40.368 40.251 13.634 14.191 41.451 43.459 18.795 20.653 44.893 48.105 21.613 20.824 18.467 13.300    1
  2 40.367 38.917 40.112 59.458 56.468 20.650 25.142 41.767 47.037 21.467 21.690 48.031 47.053 17.993 13.077    2
  3 40.278 39.903 40.523 56.121 53.497 24.270 28.704 41.789 46.664 21.113 21.241 47.536 46.121 17.569 12.753    3
  4 13.719 57.816 61.731 14.682 15.796 43.279 44.129 18.948 19.925 40.139 43.508 19.999 19.353 17.145 12.267    4
  5 14.348 56.866 61.240 15.980 17.425 44.583 45.204 19.294 19.052 41.375 43.627 18.471 18.133 15.956 11.284    5
  6 41.546 42.554 43.076 43.633 44.790 19.340 19.578 46.347 44.411 63.033 59.750 34.918 39.972 13.686  9.525    6
  7 43.580 44.221 44.271 44.614 45.512 19.675 19.616 46.476 44.550 62.039 58.242 36.328 39.964 10.502  6.728    7
  8 18.986 42.175 42.330 19.415 19.561 46.490 46.530 18.676 17.876 39.279 42.245 14.258 11.579                  8
  9 20.746 47.281 46.966 20.178 19.220 44.514 44.594 17.885 17.994 40.519 42.952 14.204 10.479                  9
 10 44.883 21.499 21.161 40.093 41.330 63.063 62.047 39.268 40.519 17.035 15.865 13.405  9.218                 10
 11 48.055 21.585 21.110 43.307 43.428 59.748 58.233 42.216 42.939 15.863 14.205 10.908  6.766                 11
 12 21.537 46.741 47.265 19.764 18.320 34.792 36.257 14.239 14.195 13.401 10.906                               12
 13 20.726 46.979 46.883 19.061 17.941 39.806 39.870 11.553 10.467  9.213  6.763                               13
 14 18.372 17.755 17.270 16.881 15.779 13.579 10.443                                                           14
 15 13.243 12.924 12.560 12.095 11.158  9.443  6.682                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  1.235  0.877  0.859  1.178  1.183  0.878  0.905  1.292  1.346  1.083  1.051  1.327  1.272  1.139  0.853    1
  2  0.877  0.857  0.824  0.732  0.764  0.751  0.771  1.008  1.029  1.360  1.350  0.997  0.950  1.125  0.846    2
  3  0.860  0.826  0.802  0.748  0.790  0.743  0.763  1.010  1.027  1.350  1.338  0.988  0.945  1.115  0.837    3
  4  1.181  0.746  0.723  1.175  1.217  0.922  0.945  1.301  1.325  1.047  1.007  1.283  1.236  1.104  0.821    4
  5  1.187  0.766  0.752  1.220  1.278  0.973  0.983  1.314  1.289  0.964  0.931  1.228  1.194  1.059  0.778    5
  6  0.876  0.863  0.873  0.927  0.979  1.337  1.337  0.984  0.934  0.765  0.760  0.932  0.859  0.955  0.684    6
  7  0.905  0.904  0.916  0.954  0.990  1.340  1.334  0.977  0.923  0.753  0.738  0.858  0.749  0.782  0.512    7
  8  1.297  1.015  1.020  1.314  1.322  0.986  0.978  1.289  1.256  0.946  0.868  1.062  0.881                  8
  9  1.350  1.036  1.035  1.333  1.294  0.936  0.923  1.256  1.270  0.977  0.898  1.054  0.803                  9
 10  1.083  1.362  1.353  1.050  0.967  0.766  0.754  0.946  0.977  1.229  1.155  0.994  0.714                 10
 11  1.051  1.351  1.338  1.007  0.932  0.761  0.738  0.869  0.898  1.155  1.044  0.833  0.541                 11
 12  1.327  0.987  0.968  1.282  1.228  0.934  0.859  1.063  1.054  0.995  0.833                               12
 13  1.272  0.929  0.915  1.234  1.193  0.860  0.750  0.883  0.804  0.715  0.541                               13
 14  1.137  1.122  1.112  1.101  1.059  0.956  0.784                                                           14
 15  0.852  0.843  0.834  0.820  0.778  0.685  0.512                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 1.661 in Node (13, 9, 4)

    Pin Power Reconstruction Time = 0.30 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  1.235  0.868  1.182  0.891  1.321  1.067  1.300  0.995   08
  9  0.868  0.827  0.759  0.757  1.018  1.350  0.970  0.981   09
 10  1.182  0.747  1.223  0.956  1.307  0.987  1.235  0.941   10
 11  0.891  0.889  0.963  1.337  0.954  0.754  0.850  0.733   11
 12  1.321  1.027  1.316  0.956  1.268  0.922  0.950          12
 13  1.067  1.351  0.989  0.755  0.922  1.146  0.771          13
 14  1.300  0.950  1.234  0.851  0.951  0.771                 14
 15  0.995  0.978  0.940  0.734                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.140  0.971  1.153  0.958  1.105  0.955  1.096  1.144   08
  9  0.971  0.974  0.900  0.908  0.967  1.093  0.949  1.148   09
 10  1.153  0.886  1.136  0.960  1.109  0.977  1.113  1.159   10
 11  0.958  0.966  0.958  1.106  0.959  0.880  1.007  1.201   11
 12  1.105  0.964  1.106  0.958  1.105  0.985  1.171          12
 13  0.955  1.093  0.978  0.880  0.985  1.140  1.200          13
 14  1.096  0.934  1.115  1.008  1.171  1.200                 14
 15  1.144  1.150  1.160  1.201                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8 13.661 40.316 13.973 42.509 19.795 46.484 21.175 15.845   08
  9 40.316 39.864 56.386 24.692 44.314 21.377 47.185 15.348   09
 10 13.973 59.413 15.971 44.299 19.304 42.162 18.989 14.163   10
 11 42.509 43.531 44.638 19.553 45.446 60.766 37.796 10.110   11
 12 19.795 44.688 19.593 45.532 18.108 41.249 12.630          12
 13 46.484 21.338 42.039 60.773 41.236 15.742 10.074          13
 14 21.175 46.967 18.771 37.681 12.613 10.071                 14
 15 15.845 15.127 13.978 10.037                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  45.84   0.00   09
 10   0.00   0.00   0.00  42.98   0.00  40.99   0.00   0.00   10
 11   0.00  42.22  43.31   0.00  44.12   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  44.20   0.00   0.00   0.00          12
 13   0.00   0.00  40.87   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  120
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 12 c1c24 HFP NOMINAL DEPLETION                                                      543.85 ppm  450.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97            21.38 25C-09 sil55  ( 9,13,  )      10.04  25E-15  sil96  (15,11,  )        15.96
        6    26A        16            46.97 24L-03 24L-03 (14, 9,  )      39.86  24N-05  24N-05 ( 9, 9,  )        42.41
        8    26C        48            47.19 24J-07 24J-07 ( 9,14,  )      37.68  24L-02  24L-02 (14,11,  )        42.98
        5    25C        16            60.77 24J-03 24J-03 (13,11,  )      56.39  24K-06  24K-06 ( 9,10,  )        59.33
        9    26D         4            24.69 24N-03 24N-03 ( 9,11,  )      24.69  24N-03  24N-03 ( 9,11,  )        24.69
        7    26B        12            44.64 24J-05 24J-05 (11,10,  )      43.53  24L-05  24L-05 (11, 9,  )        44.16

     CORE              193            60.77 24J-03 24J-03 (13,11,  )      10.04  25E-15  sil96  (15,11,  )        28.77


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97            22.74 25C-09 sil55  ( 2,11,  )
        6    26A        16            48.95 24L-03 24L-03 (12, 3,  )
        8    26C        48            49.97 24P-08 24P-08 ( 1,11,  )
        5    25C        16            65.36 24J-03 24J-03 (10, 6,  )
        9    26D         4            30.79 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            47.61 24J-05 24J-05 ( 7, 5,  )

     CORE              193            65.36 24J-03 24J-03 (10, 6,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  121
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 12 c1c24 HFP NOMINAL DEPLETION                                                      543.85 ppm  450.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  28.772 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp. 450.0 EFPD 10800.0 EFPH  15.748 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  1.200E+03 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  1.750 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  1.476E-04  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     1.586 (13, 9, 4)
       Fuel    Average . . .TFUAVE  857.98 K      1084.70 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  581.02 K       586.17 F                  F-delta-H       1.418 (13, 9)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.466 (13, 9, 2)   [Off]
 Boron Conc. . . . . . . . . . BOR   543.9 ppm                               Max-3PIN        1.652 (13, 9, 4)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        1.661 (13, 9, 4)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O  -0.024                                 Total Neutron Leakage . . . . . . . . .  0.043
 Edit Ring -     1      2      3      4      5
 RPF            0.891  0.921  1.127  0.995  0.752
 KIN            0.991  0.987  1.019  1.065  1.200
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.713  0.711  0.702  0.708  0.718
 TFU (Deg K)    823.9  833.6  902.4  852.2  782.0
 TMO (Deg K)    579.1  579.6  583.2  581.0  576.8
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.39200   1.03905  12.83755   0.02288   0.66702   701.169   597.514
  23   0.84362   1.11252  20.55134   0.00000   0.66937   822.418   596.709
  22   0.98831   1.07317  25.20337   0.00000   0.67281   863.004   595.504
  21   1.06171   1.05463  28.46194   0.00000   0.67662   887.174   594.140
  20   1.07150   1.04491  29.86826   0.00000   0.68055   890.199   592.701
  19   1.06161   1.03997  30.44308   0.00000   0.68443   885.898   591.245
 
  18   1.05662   1.03986  30.94630   0.00000   0.68824   883.200   589.780
  17   1.04965   1.03867  31.14499   0.00000   0.69199   879.518   588.309
  16   1.04845   1.03888  31.36011   0.00000   0.69569   877.791   586.825
  15   1.03709   1.03757  31.06629   0.00000   0.69932   872.139   585.336
  14   1.04708   1.03898  31.45947   0.00000   0.70292   874.308   583.833
  13   1.05017   1.03789  31.52065   0.00000   0.70650   873.832   582.305
 
  12   1.04644   1.03714  31.29143   0.00000   0.71005   870.750   580.763
  11   1.06029   1.03852  31.66739   0.00000   0.71357   874.174   579.199
  10   1.06900   1.03795  31.79722   0.00000   0.71710   875.625   577.604
   9   1.07664   1.03798  31.80344   0.00000   0.72062   876.576   575.983
   8   1.08210   1.03590  31.60279   0.00000   0.72413   876.544   574.337
   7   1.10785   1.03711  31.96675   0.00000   0.72766   884.040   572.654
 
   6   1.12737   1.03783  31.85077   0.00000   0.73123   888.838   570.922
   5   1.13451   1.03967  31.02466   0.00000   0.73480   888.518   569.154
   4   1.14859   1.04874  30.21606   0.00000   0.73837   890.643   567.356
   3   1.10247   1.06785  27.57406   0.00000   0.74185   870.853   565.568
   2   0.93787   1.10976  22.24879   0.00000   0.74500   815.678   563.939
   1   0.39906   1.00911  12.66345   0.00000   0.74702   668.744   562.862
 
 Ave   1.00000   1.04724  28.77180   0.00095   0.70779   857.985   581.023
 P**2                     27.56978
 A-O  -0.02435   0.00233  -0.01593   1.00000  -0.03037     0.001     0.016
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  122
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 12 c1c24 HFP NOMINAL DEPLETION                                                      543.85 ppm  450.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  2.78699E-09  1.36083E-09  5.73677E-09  1.58335E-08  1.28375E+01  6.59964E-01  2.65089E+01  1.05190E+03  0.00000E+00
  23  6.00495E-09  3.74195E-09  1.23043E-08  4.90176E-08  2.05513E+01  6.64711E-01  2.86318E+01  1.05437E+03  4.85324E+00
  22  7.04587E-09  4.07046E-09  1.50106E-08  5.22328E-08  2.52034E+01  6.68494E-01  2.94083E+01  1.08630E+03  5.76190E+00
  21  7.57141E-09  4.08788E-09  1.64985E-08  5.13432E-08  2.84619E+01  6.72351E-01  2.99178E+01  1.10680E+03  6.45494E+00
  20  7.64245E-09  4.07918E-09  1.68036E-08  5.13229E-08  2.98683E+01  6.76420E-01  3.00700E+01  1.12257E+03  6.75368E+00
  19  7.57246E-09  4.06255E-09  1.67080E-08  5.14491E-08  3.04431E+01  6.80525E-01  3.00750E+01  1.13443E+03  6.87328E+00
 
  18  7.53521E-09  4.01106E-09  1.66689E-08  5.07252E-08  3.09463E+01  6.84592E-01  3.00857E+01  1.14263E+03  6.97886E+00
  17  7.48398E-09  3.97956E-09  1.65687E-08  5.03688E-08  3.11450E+01  6.88643E-01  3.00543E+01  1.14807E+03  7.01962E+00
  16  7.47388E-09  3.94462E-09  1.65598E-08  4.97908E-08  3.13601E+01  6.92654E-01  3.00411E+01  1.15137E+03  7.06411E+00
  15  7.39394E-09  3.96403E-09  1.63517E-08  5.04226E-08  3.10663E+01  6.96632E-01  2.99409E+01  1.15323E+03  6.99851E+00
  14  7.46265E-09  3.91308E-09  1.65313E-08  4.92714E-08  3.14595E+01  7.00534E-01  2.99819E+01  1.15329E+03  7.08121E+00
  13  7.48397E-09  3.90195E-09  1.65786E-08  4.90117E-08  3.15207E+01  7.04440E-01  2.99663E+01  1.15267E+03  7.09164E+00
 
  12  7.45868E-09  3.92216E-09  1.64970E-08  4.94572E-08  3.12914E+01  7.08308E-01  2.99003E+01  1.15137E+03  7.03874E+00
  11  7.55546E-09  3.87809E-09  1.67374E-08  4.83650E-08  3.16674E+01  7.12127E-01  2.99574E+01  1.14875E+03  7.11556E+00
  10  7.61706E-09  3.86421E-09  1.68791E-08  4.79380E-08  3.17972E+01  7.15954E-01  2.99735E+01  1.14532E+03  7.13996E+00
   9  7.67260E-09  3.86824E-09  1.69971E-08  4.78478E-08  3.18034E+01  7.19769E-01  2.99745E+01  1.14095E+03  7.13824E+00
   8  7.71355E-09  3.89881E-09  1.70638E-08  4.82573E-08  3.16028E+01  7.23558E-01  2.99464E+01  1.13511E+03  7.09329E+00
   7  7.89549E-09  3.86215E-09  1.74900E-08  4.71022E-08  3.19668E+01  7.27330E-01  3.00473E+01  1.12665E+03  7.16961E+00
 
   6  8.03517E-09  3.87041E-09  1.77760E-08  4.68206E-08  3.18508E+01  7.31119E-01  3.00741E+01  1.11576E+03  7.14516E+00
   5  8.08877E-09  3.92845E-09  1.77964E-08  4.75098E-08  3.10247E+01  7.34869E-01  2.99804E+01  1.10178E+03  6.97245E+00
   4  8.18606E-09  3.89705E-09  1.78948E-08  4.64488E-08  3.02161E+01  7.38543E-01  2.99138E+01  1.08314E+03  6.80911E+00
   3  7.85337E-09  3.86791E-09  1.68393E-08  4.61424E-08  2.75741E+01  7.42054E-01  2.94692E+01  1.05988E+03  6.26775E+00
   2  6.66944E-09  3.64666E-09  1.36888E-08  4.44621E-08  2.22488E+01  7.45120E-01  2.84715E+01  1.02686E+03  5.23112E+00
   1  2.83643E-09  1.34452E-09  5.76879E-09  1.55089E-08  1.26635E+01  7.47058E-01  2.58402E+01  1.04173E+03  0.00000E+00
 
 Ave   7.1267E-09   3.7069E-09   1.5573E-08   4.6110E-08   2.8772E+01   7.0566E-01   2.9510E+01   1.1140E+03   6.3328E+00
 A-O       -0.024        0.014       -0.024        0.032       -0.016       -0.033        0.002        0.007       -0.015
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  1.28503E+01  1.26913E+01  1.27186E+01  1.28862E+01  9.86767E-01  9.84767E-01  1.00578E+00  1.00589E+00  9.84841E-01
  23  2.06714E+01  2.03596E+01  2.03914E+01  2.07128E+01  9.42181E-01  9.42345E-01  9.28843E-01  9.29044E-01  9.42444E-01
  22  2.53720E+01  2.49724E+01  2.50062E+01  2.54155E+01  1.01754E+00  1.01878E+00  1.00038E+00  1.00069E+00  1.01894E+00
  21  2.86720E+01  2.82015E+01  2.82379E+01  2.87182E+01  9.88782E-01  9.89155E-01  9.72750E-01  9.73042E-01  9.89358E-01
  20  3.00874E+01  2.95927E+01  2.96308E+01  3.01357E+01  9.95780E-01  9.96011E-01  9.79583E-01  9.79868E-01  9.96217E-01
  19  3.06601E+01  3.01595E+01  3.01986E+01  3.07098E+01  1.00893E+00  1.00918E+00  9.92434E-01  9.92719E-01  1.00938E+00
 
  18  3.11702E+01  3.06574E+01  3.06974E+01  3.12208E+01  9.96377E-01  9.96112E-01  9.80649E-01  9.80927E-01  9.96321E-01
  17  3.13687E+01  3.08535E+01  3.08943E+01  3.14201E+01  9.95659E-01  9.95128E-01  9.80151E-01  9.80428E-01  9.95340E-01
  16  3.15870E+01  3.10651E+01  3.11065E+01  3.16391E+01  9.87701E-01  9.86810E-01  9.72768E-01  9.73041E-01  9.87025E-01
  15  3.12772E+01  3.07700E+01  3.08116E+01  3.13299E+01  1.01744E+00  1.01694E+00  1.00159E+00  1.00186E+00  1.01713E+00
  14  3.16830E+01  3.11602E+01  3.12027E+01  3.17363E+01  9.91036E-01  9.89801E-01  9.76416E-01  9.76686E-01  9.90012E-01
  13  3.17430E+01  3.12195E+01  3.12625E+01  3.17969E+01  9.92544E-01  9.91146E-01  9.78045E-01  9.78312E-01  9.91358E-01
 
  12  3.15011E+01  3.09887E+01  3.10319E+01  3.15556E+01  1.01591E+00  1.01479E+00  1.00075E+00  1.00102E+00  1.01498E+00
  11  3.18896E+01  3.13622E+01  3.14063E+01  3.19448E+01  9.90384E-01  9.88560E-01  9.76390E-01  9.76654E-01  9.88770E-01
  10  3.20217E+01  3.14910E+01  3.15358E+01  3.20775E+01  9.86076E-01  9.83958E-01  9.72375E-01  9.72636E-01  9.84174E-01
   9  3.20250E+01  3.14946E+01  3.15398E+01  3.20813E+01  9.92110E-01  9.89955E-01  9.78419E-01  9.78679E-01  9.90162E-01
   8  3.18130E+01  3.12932E+01  3.13385E+01  3.18699E+01  1.01461E+00  1.01272E+00  1.00021E+00  1.00047E+00  1.01291E+00
   7  3.21923E+01  3.16562E+01  3.17023E+01  3.22497E+01  9.86597E-01  9.83989E-01  9.73392E-01  9.73645E-01  9.84203E-01
 
   6  3.20755E+01  3.15406E+01  3.15871E+01  3.21333E+01  9.86621E-01  9.83868E-01  9.73579E-01  9.73831E-01  9.84079E-01
   5  3.12286E+01  3.07183E+01  3.07643E+01  3.12864E+01  1.01830E+00  1.01604E+00  1.00429E+00  1.00455E+00  1.01623E+00
   4  3.04230E+01  2.99205E+01  2.99662E+01  3.04799E+01  9.91559E-01  9.88722E-01  9.78785E-01  9.79042E-01  9.88922E-01
   3  2.77515E+01  2.73051E+01  2.73487E+01  2.78061E+01  9.89001E-01  9.86142E-01  9.76651E-01  9.76919E-01  9.86326E-01
   2  2.23624E+01  2.20245E+01  2.20635E+01  2.24117E+01  9.53242E-01  9.50165E-01  9.43177E-01  9.43376E-01  9.50315E-01
   1  1.26579E+01  1.25050E+01  1.25329E+01  1.26918E+01  1.03031E+00  1.02811E+00  1.05190E+00  1.05199E+00  1.02822E+00
 
 Ave   2.8965E+01   2.8495E+01   2.8536E+01   2.9016E+01   9.9481E-01   9.9347E-01   9.8330E-01   9.8356E-01   9.9365E-01
 A-O       -0.016       -0.016       -0.016       -0.016       -0.001       -0.000       -0.003       -0.003       -0.000
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  123
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 13 c1c24 HFP NOMINAL DEPLETION                                                      543.85 ppm  500.000 EFPD    

 Cycle exposure at end of this step = 500.00 EFPD                                                    

 Depletion step used = 50.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  124
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 13 c1c24 HFP NOMINAL DEPLETION                                                      543.85 ppm  500.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =   500.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    30.522 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9974291     0.00257091     5.2703E-02     8.4949E-03     1.50622    -0.001     317.24    
     2  2  1.0005222    -0.00052224     5.1634E-02     5.7320E-03     1.58823    -0.037     331.55    
     3  3  1.0001800    -0.00018001     2.3347E-02     1.9917E-03     1.55199    -0.022     346.40    
     4  4  0.9999850     0.00001500 *   5.0442E-03     5.3671E-04     1.55986    -0.026     343.92    
     5  5  1.0000036 *  -0.00000361 *   9.1761E-04     1.1066E-04 *   1.55843    -0.025     342.99 *  
     6  6  0.9999977 *   0.00000233 *   1.6599E-05 *   5.2005E-05 *   1.55846    -0.025     343.71 *  

    Nodal Solution Time = 0.62 Sec

 Boron (ppm)  =  343.712
 Axial Offset =   -0.025
 Max. Node-Averaged  Peaking Factor = 1.558 in Node (13, 9, 4)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  1.308  0.939  1.256  0.950  1.394  1.105  1.363  1.227   08
  9  0.939  0.907  0.834  0.812  1.059  1.394  1.024  1.205   09
 10  1.256  0.809  1.340  1.018  1.369  1.072  1.328  1.200   10
 11  0.950  0.950  1.024  1.379  1.016  0.797  0.982  1.100   11
 12  1.394  1.066  1.375  1.018  1.333  1.024  1.149          12
 13  1.105  1.396  1.076  0.798  1.024  1.293  1.116          13
 14  1.363  1.009  1.328  0.984  1.150  1.117                 14
 15  1.227  1.204  1.199  1.103                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  14,13   4,13  14,13  13,15  14,13   4,13   4,13  13, 3   08
  9   4,13   5, 3  15,13   5,15   5,14   4, 5   5, 4   5, 3   09
 10  14,13  13, 3  14,13  14,13   4,13   5, 4   4, 5   5, 3   10
 11  13,15  14,13  13,14  13, 4   5, 4   1, 1   3, 5   1, 1   11
 12  14,13  14, 5  13, 4   4, 5   5, 4  13, 3  13, 3          12
 13   4,13   5, 4   4, 5   1, 1   3,13   5, 4   1, 1          13
 14   4,13   3, 5   5, 4   5, 3   3,13   1, 1                 14
 15  13, 3   3, 5   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8 16.350 43.676 17.088 47.319 24.100 52.074 24.799 22.102   08
  9 43.676 43.675 63.701 32.376 51.412 24.933 51.467 21.524   09
 10 17.088 65.422 20.535 49.591 23.104 48.768 23.126 20.608   10
 11 47.319 47.500 49.646 22.821 50.360 66.854 43.830 17.679   11
 12 24.100 51.504 23.386 50.484 21.757 46.620 17.378          12
 13 52.074 24.770 48.493 66.892 46.607 19.917 16.702          13
 14 24.799 50.885 22.891 43.663 17.371 16.698                 14
 15 22.102 21.278 20.311 17.544                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  125
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 13 c1c24 HFP NOMINAL DEPLETION                                                      343.71 ppm  500.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1 15.624 42.122 41.970 15.505 16.068 43.198 45.252 20.823 22.758 47.014 50.163 23.683 22.809 20.246 14.640    1
  2 42.122 40.632 41.761 60.924 57.995 22.150 26.677 43.754 49.059 23.591 23.796 49.982 48.915 19.752 14.406    2
  3 41.998 41.556 42.129 57.616 55.073 25.754 30.223 43.779 48.681 23.222 23.329 49.471 47.974 19.314 14.069    3
  4 15.594 59.308 63.176 16.542 17.718 45.104 45.993 20.987 21.998 42.193 45.483 22.005 21.286 18.875 13.561    4
  5 16.229 58.394 62.738 17.904 19.435 46.501 47.138 21.352 21.071 43.273 45.459 20.397 20.005 17.618 12.512    5
  6 43.287 44.268 44.807 45.464 46.717 21.434 21.671 48.281 46.250 64.547 61.255 36.761 41.670 15.191 10.608    6
  7 45.370 46.008 46.080 46.494 47.458 21.771 21.704 48.397 46.368 63.533 59.707 38.029 41.450 11.744  7.543    7
  8 21.021 44.173 44.336 21.473 21.629 48.428 48.451 20.696 19.847 41.148 43.964 15.936 12.977                  8
  9 22.857 49.316 48.998 22.262 21.245 46.356 46.414 19.857 19.987 42.448 44.728 15.867 11.753                  9
 10 47.006 23.626 23.274 42.153 43.233 64.580 63.543 41.138 42.448 18.969 17.683 14.975 10.352                 10
 11 50.113 23.693 23.200 45.284 45.264 61.255 59.700 43.937 44.716 17.682 15.850 12.228  7.628                 11
 12 23.608 48.677 49.166 21.770 20.246 36.637 37.962 15.919 15.860 14.971 12.227                               12
 13 22.711 48.803 48.682 20.994 19.814 41.507 41.359 12.953 11.743 10.347  7.626                               13
 14 20.149 19.511 19.012 18.608 17.443 15.086 11.688                                                           14
 15 14.581 14.249 13.873 13.388 12.387 10.528  7.499                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  1.259  0.901  0.882  1.200  1.202  0.892  0.912  1.286  1.330  1.068  1.036  1.304  1.251  1.123  0.850    1
  2  0.901  0.882  0.848  0.754  0.784  0.770  0.785  1.007  1.020  1.340  1.327  0.981  0.937  1.111  0.843    2
  3  0.883  0.850  0.825  0.768  0.807  0.761  0.776  1.008  1.018  1.330  1.316  0.974  0.934  1.103  0.836    3
  4  1.203  0.766  0.742  1.188  1.225  0.927  0.945  1.291  1.310  1.035  0.996  1.267  1.222  1.095  0.823    4
  5  1.204  0.783  0.766  1.225  1.276  0.971  0.977  1.301  1.277  0.959  0.927  1.220  1.185  1.054  0.783    5
  6  0.888  0.874  0.881  0.929  0.974  1.324  1.323  0.976  0.931  0.770  0.766  0.935  0.862  0.958  0.693    6
  7  0.910  0.908  0.917  0.951  0.983  1.324  1.319  0.970  0.921  0.761  0.747  0.866  0.758  0.796  0.525    7
  8  1.289  1.010  1.014  1.301  1.307  0.978  0.970  1.278  1.249  0.949  0.875  1.070  0.896                  8
  9  1.333  1.026  1.025  1.316  1.280  0.932  0.921  1.250  1.264  0.978  0.903  1.060  0.817                  9
 10  1.068  1.342  1.333  1.038  0.962  0.771  0.762  0.949  0.978  1.229  1.157  1.001  0.727                 10
 11  1.036  1.329  1.319  0.997  0.929  0.767  0.748  0.876  0.903  1.157  1.048  0.845  0.555                 11
 12  1.306  0.975  0.959  1.269  1.221  0.937  0.868  1.072  1.061  1.001  0.845                               12
 13  1.252  0.920  0.909  1.222  1.187  0.864  0.760  0.897  0.818  0.727  0.555                               13
 14  1.122  1.110  1.103  1.094  1.056  0.960  0.798                                                           14
 15  0.850  0.842  0.835  0.823  0.784  0.695  0.526                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 1.631 in Node ( 8,12, 4)

    Pin Power Reconstruction Time = 0.30 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  1.259  0.892  1.202  0.901  1.309  1.052  1.278  0.986   08
  9  0.892  0.851  0.778  0.773  1.013  1.328  0.957  0.973   09
 10  1.202  0.764  1.229  0.955  1.295  0.979  1.223  0.939   10
 11  0.901  0.895  0.959  1.322  0.949  0.761  0.855  0.743   11
 12  1.309  1.019  1.301  0.950  1.260  0.926  0.961          12
 13  1.052  1.331  0.982  0.762  0.927  1.148  0.782          13
 14  1.278  0.941  1.225  0.857  0.962  0.782                 14
 15  0.986  0.973  0.939  0.745                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.136  0.974  1.150  0.961  1.102  0.956  1.094  1.145   08
  9  0.974  0.978  0.905  0.917  0.968  1.090  0.950  1.149   09
 10  1.150  0.891  1.133  0.962  1.106  0.978  1.111  1.160   10
 11  0.961  0.968  0.959  1.103  0.960  0.886  1.011  1.204   11
 12  1.102  0.966  1.104  0.960  1.102  0.988  1.172          12
 13  0.956  1.090  0.979  0.886  0.988  1.138  1.202          13
 14  1.094  0.937  1.113  1.011  1.172  1.202                 14
 15  1.145  1.151  1.162  1.205                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8 15.624 42.053 15.850 44.277 21.865 48.574 23.203 17.404   08
  9 42.053 41.520 57.902 26.201 46.318 23.484 49.085 16.885   09
 10 15.850 60.904 17.900 46.184 21.352 44.102 20.923 15.641   10
 11 44.277 45.291 46.533 21.645 47.324 62.261 39.477 11.272   11
 12 21.865 46.706 21.652 47.413 20.097 43.072 14.133          12
 13 48.574 23.448 43.984 62.269 43.060 17.546 11.296          13
 14 23.203 48.832 20.706 39.366 14.119 11.293                 14
 15 17.404 16.661 15.456 11.200                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  47.67   0.00   09
 10   0.00   0.00   0.00  44.79   0.00  42.86   0.00   0.00   10
 11   0.00  43.92  45.13   0.00  45.92   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  46.01   0.00   0.00   0.00          12
 13   0.00   0.00  42.74   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  126
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 13 c1c24 HFP NOMINAL DEPLETION                                                      343.71 ppm  500.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97            23.48 25C-09 sil55  ( 9,13,  )      11.20  25E-15  sil96  (15,11,  )        17.69
        6    26A        16            48.83 24L-03 24L-03 (14, 9,  )      41.52  24N-05  24N-05 ( 9, 9,  )        44.17
        8    26C        48            49.09 24J-07 24J-07 ( 9,14,  )      39.37  24L-02  24L-02 (14,11,  )        44.87
        5    25C        16            62.27 24J-03 24J-03 (13,11,  )      57.90  24K-06  24K-06 ( 9,10,  )        60.83
        9    26D         4            26.20 24N-03 24N-03 ( 9,11,  )      26.20  24N-03  24N-03 ( 9,11,  )        26.20
        7    26B        12            46.53 24J-05 24J-05 (11,10,  )      45.29  24L-05  24L-05 (11, 9,  )        46.00

     CORE              193            62.27 24J-03 24J-03 (13,11,  )      11.20  25E-15  sil96  (15,11,  )        30.52


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97            24.93 25C-09 sil55  ( 2,11,  )
        6    26A        16            50.89 24L-03 24L-03 (12, 3,  )
        8    26C        48            52.07 24P-08 24P-08 ( 1,11,  )
        5    25C        16            66.89 24J-03 24J-03 (10, 6,  )
        9    26D         4            32.38 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            49.65 24J-05 24J-05 ( 7, 5,  )

     CORE              193            66.89 24J-03 24J-03 (10, 6,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  127
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 13 c1c24 HFP NOMINAL DEPLETION                                                      343.71 ppm  500.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  30.522 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp. 500.0 EFPD 12000.0 EFPH  17.498 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  1.200E+03 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  1.750 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  1.549E-04  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     1.558 (13, 9, 4)
       Fuel    Average . . .TFUAVE  859.15 K      1086.79 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  581.03 K       586.19 F                  F-delta-H       1.396 (13, 9)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.437 (13, 9, 2)   [Off]
 Boron Conc. . . . . . . . . . BOR   343.7 ppm                               Max-3PIN        1.624 ( 8,12, 4)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        1.631 ( 8,12, 4)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O  -0.025                                 Total Neutron Leakage . . . . . . . . .  0.043
 Edit Ring -     1      2      3      4      5
 RPF            0.915  0.935  1.116  0.993  0.763
 KIN            0.994  0.989  1.019  1.066  1.203
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.712  0.711  0.703  0.708  0.718
 TFU (Deg K)    833.9  839.8  902.0  852.2  781.0
 TMO (Deg K)    579.6  579.8  583.0  581.0  577.0
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.41885   1.05259  13.84042   0.02288   0.66711   706.841   597.513
  23   0.86767   1.11319  22.01035   0.00000   0.66955   826.821   596.673
  22   0.99900   1.07257  26.89765   0.00000   0.67305   866.441   595.443
  21   1.06229   1.05384  30.27267   0.00000   0.67688   888.735   594.068
  20   1.06595   1.04426  31.69033   0.00000   0.68079   890.095   592.630
  19   1.05307   1.03953  32.24561   0.00000   0.68464   884.919   591.181
 
  18   1.04680   1.03970  32.73907   0.00000   0.68842   881.882   589.729
  17   1.03985   1.03864  32.92574   0.00000   0.69213   878.238   588.268
  16   1.03909   1.03898  33.13904   0.00000   0.69578   876.710   586.797
  15   1.02879   1.03768  32.82667   0.00000   0.69939   871.306   585.320
  14   1.03918   1.03919  33.23710   0.00000   0.70295   873.759   583.827
  13   1.04281   1.03809  33.30394   0.00000   0.70651   873.509   582.310
 
  12   1.03956   1.03733  33.06877   0.00000   0.71003   870.530   580.778
  11   1.05313   1.03878  33.46810   0.00000   0.71353   874.026   579.224
  10   1.06160   1.03820  33.61259   0.00000   0.71703   875.478   577.640
   9   1.06884   1.03821  33.63154   0.00000   0.72053   876.352   576.030
   8   1.07417   1.03598  33.44017   0.00000   0.72401   876.281   574.396
   7   1.09945   1.03714  33.84772   0.00000   0.72752   883.874   572.726
 
   6   1.11975   1.03768  33.76578   0.00000   0.73106   889.065   571.007
   5   1.12990   1.03923  32.95451   0.00000   0.73461   889.642   569.248
   4   1.14951   1.04820  32.17473   0.00000   0.73818   893.480   567.454
   3   1.11383   1.06756  29.46312   0.00000   0.74168   875.976   565.657
   2   0.96364   1.11107  23.86961   0.00000   0.74488   822.078   563.998
   1   0.42327   1.02342  13.68032   0.00000   0.74698   673.459   562.881
 
 Ave   1.00000   1.04838  30.52159   0.00095   0.70780   859.146   581.033
 P**2                     29.42129
 A-O  -0.02472   0.00220  -0.01641   1.00000  -0.03019     0.001     0.016
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  128
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 13 c1c24 HFP NOMINAL DEPLETION                                                      343.71 ppm  500.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  2.97742E-09  1.37249E-09  6.24486E-09  1.57961E-08  1.38404E+01  6.60729E-01  2.65411E+01  9.39734E+02  0.00000E+00
  23  6.17708E-09  3.74667E-09  1.28393E-08  4.88041E-08  2.20103E+01  6.65409E-01  2.86506E+01  9.27684E+02  5.09705E+00
  22  7.12471E-09  4.03938E-09  1.53885E-08  5.19208E-08  2.68976E+01  6.69157E-01  2.94048E+01  9.60334E+02  6.03996E+00
  21  7.57811E-09  4.03118E-09  1.67317E-08  5.08737E-08  3.02727E+01  6.72984E-01  2.98926E+01  9.83293E+02  6.74701E+00
  20  7.60531E-09  4.01219E-09  1.69351E-08  5.07995E-08  3.16903E+01  6.77011E-01  3.00247E+01  1.00071E+03  7.04506E+00
  19  7.51391E-09  3.99194E-09  1.67842E-08  5.09105E-08  3.22456E+01  6.81065E-01  3.00162E+01  1.01359E+03  7.16024E+00
 
  18  7.46718E-09  3.93782E-09  1.67194E-08  5.01555E-08  3.27391E+01  6.85081E-01  3.00191E+01  1.02256E+03  7.26266E+00
  17  7.41597E-09  3.90624E-09  1.66160E-08  4.97870E-08  3.29257E+01  6.89077E-01  2.99842E+01  1.02836E+03  7.30065E+00
  16  7.40886E-09  3.87143E-09  1.66127E-08  4.91956E-08  3.31390E+01  6.93035E-01  2.99701E+01  1.03188E+03  7.34391E+00
  15  7.33664E-09  3.89431E-09  1.64202E-08  4.98612E-08  3.28267E+01  6.96961E-01  2.98710E+01  1.03354E+03  7.27583E+00
  14  7.40798E-09  3.84176E-09  1.66081E-08  4.86757E-08  3.32371E+01  7.00814E-01  2.99133E+01  1.03384E+03  7.35975E+00
  13  7.43316E-09  3.83098E-09  1.66659E-08  4.84134E-08  3.33039E+01  7.04672E-01  2.98994E+01  1.03320E+03  7.37065E+00
 
  12  7.41161E-09  3.85319E-09  1.65934E-08  4.88852E-08  3.30688E+01  7.08493E-01  2.98351E+01  1.03175E+03  7.31713E+00
  11  7.50624E-09  3.80633E-09  1.68332E-08  4.77592E-08  3.34681E+01  7.12268E-01  2.98932E+01  1.02945E+03  7.39611E+00
  10  7.56607E-09  3.79103E-09  1.69746E-08  4.73213E-08  3.36126E+01  7.16052E-01  2.99105E+01  1.02619E+03  7.42207E+00
   9  7.61903E-09  3.79443E-09  1.70908E-08  4.72375E-08  3.36315E+01  7.19824E-01  2.99131E+01  1.02190E+03  7.42203E+00
   8  7.65936E-09  3.82552E-09  1.71606E-08  4.76699E-08  3.34402E+01  7.23573E-01  2.98878E+01  1.01594E+03  7.37895E+00
   7  7.83788E-09  3.78500E-09  1.75889E-08  4.64789E-08  3.38477E+01  7.27308E-01  2.99932E+01  1.00770E+03  7.46051E+00
 
   6  7.98327E-09  3.79291E-09  1.78982E-08  4.62008E-08  3.37658E+01  7.31063E-01  3.00287E+01  9.96538E+02  7.44122E+00
   5  8.05906E-09  3.85614E-09  1.79785E-08  4.69467E-08  3.29545E+01  7.34786E-01  2.99499E+01  9.81583E+02  7.27265E+00
   4  8.19564E-09  3.82874E-09  1.81780E-08  4.58911E-08  3.21747E+01  7.38448E-01  2.99058E+01  9.61963E+02  7.11443E+00
   3  7.93714E-09  3.81651E-09  1.72820E-08  4.56753E-08  2.94631E+01  7.41963E-01  2.94880E+01  9.36863E+02  6.56606E+00
   2  6.85399E-09  3.63166E-09  1.42939E-08  4.41473E-08  2.38696E+01  7.45061E-01  2.85067E+01  9.03286E+02  5.49277E+00
   1  3.00916E-09  1.35386E-09  6.24461E-09  1.55138E-08  1.36803E+01  7.47043E-01  2.58759E+01  9.33532E+02  0.00000E+00
 
 Ave   7.1285E-09   3.6505E-09   1.5778E-08   4.5622E-08   3.0522E+01   7.0591E-01   2.9474E+01   9.9398E+02   6.5994E+00
 A-O       -0.025        0.015       -0.025        0.032       -0.016       -0.032        0.001        0.007       -0.015
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  1.38814E+01  1.36577E+01  1.36847E+01  1.39171E+01  9.82096E-01  9.80144E-01  9.99668E-01  9.99775E-01  9.80217E-01
  23  2.21721E+01  2.17810E+01  2.18123E+01  2.22129E+01  9.40241E-01  9.41088E-01  9.26183E-01  9.26370E-01  9.41172E-01
  22  2.71125E+01  2.66276E+01  2.66607E+01  2.71552E+01  1.01760E+00  1.01959E+00  9.99690E-01  9.99979E-01  1.01974E+00
  21  3.05324E+01  2.99711E+01  3.00067E+01  3.05779E+01  9.88458E-01  9.89552E-01  9.71795E-01  9.72068E-01  9.89731E-01
  20  3.19591E+01  3.13745E+01  3.14118E+01  3.20068E+01  9.95544E-01  9.96476E-01  9.78740E-01  9.79007E-01  9.96661E-01
  19  3.25116E+01  3.19229E+01  3.19613E+01  3.25605E+01  1.00891E+00  1.00985E+00  9.91799E-01  9.92064E-01  1.01003E+00
 
  18  3.30123E+01  3.24110E+01  3.24503E+01  3.30622E+01  9.96182E-01  9.96584E-01  9.79862E-01  9.80122E-01  9.96771E-01
  17  3.31988E+01  3.25954E+01  3.26354E+01  3.32494E+01  9.95398E-01  9.95521E-01  9.79306E-01  9.79565E-01  9.95712E-01
  16  3.34159E+01  3.28048E+01  3.28454E+01  3.34672E+01  9.87353E-01  9.87104E-01  9.71848E-01  9.72103E-01  9.87297E-01
  15  3.30864E+01  3.24923E+01  3.25332E+01  3.31384E+01  1.01755E+00  1.01771E+00  1.00108E+00  1.00134E+00  1.01788E+00
  14  3.35112E+01  3.28985E+01  3.29402E+01  3.35637E+01  9.90765E-01  9.90162E-01  9.75567E-01  9.75819E-01  9.90351E-01
  13  3.35772E+01  3.29631E+01  3.30053E+01  3.36303E+01  9.92257E-01  9.91488E-01  9.77182E-01  9.77433E-01  9.91676E-01
 
  12  3.33286E+01  3.27270E+01  3.27695E+01  3.33824E+01  1.01600E+00  1.01552E+00  1.00023E+00  1.00048E+00  1.01569E+00
  11  3.37422E+01  3.31224E+01  3.31657E+01  3.37966E+01  9.90081E-01  9.88878E-01  9.75519E-01  9.75765E-01  9.89065E-01
  10  3.38897E+01  3.32652E+01  3.33091E+01  3.39447E+01  9.85654E-01  9.84152E-01  9.71403E-01  9.71647E-01  9.84344E-01
   9  3.39060E+01  3.32811E+01  3.33254E+01  3.39615E+01  9.91848E-01  9.90310E-01  9.77596E-01  9.77838E-01  9.90495E-01
   8  3.37029E+01  3.30892E+01  3.31336E+01  3.37590E+01  1.01467E+00  1.01342E+00  9.99692E-01  9.99937E-01  1.01359E+00
   7  3.41282E+01  3.34935E+01  3.35388E+01  3.41847E+01  9.86207E-01  9.84217E-01  9.72469E-01  9.72705E-01  9.84406E-01
 
   6  3.40466E+01  3.34107E+01  3.34563E+01  3.41035E+01  9.86240E-01  9.84109E-01  9.72669E-01  9.72903E-01  9.84296E-01
   5  3.32141E+01  3.26031E+01  3.26482E+01  3.32710E+01  1.01848E+00  1.01687E+00  1.00388E+00  1.00412E+00  1.01703E+00
   4  3.24395E+01  3.18313E+01  3.18761E+01  3.24955E+01  9.91307E-01  9.89109E-01  9.77974E-01  9.78213E-01  9.89283E-01
   3  2.96970E+01  2.91461E+01  2.91888E+01  2.97508E+01  9.88654E-01  9.86432E-01  9.75698E-01  9.75947E-01  9.86590E-01
   2  2.40329E+01  2.36006E+01  2.36389E+01  2.40814E+01  9.51583E-01  9.49071E-01  9.40849E-01  9.41032E-01  9.49201E-01
   1  1.37034E+01  1.34845E+01  1.35122E+01  1.37372E+01  1.02890E+00  1.02660E+00  1.04891E+00  1.04901E+00  1.02671E+00
 
 Ave   3.0764E+01   3.0204E+01   3.0244E+01   3.0815E+01   9.9425E-01   9.9350E-01   9.8207E-01   9.8230E-01   9.9366E-01
 A-O       -0.016       -0.016       -0.016       -0.016       -0.002       -0.001       -0.003       -0.003       -0.001
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  129
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 14 c1c24 HFP NOMINAL DEPLETION                                                      343.71 ppm  550.000 EFPD    

 Cycle exposure at end of this step = 550.00 EFPD                                                    

 Depletion step used = 50.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  130
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 14 c1c24 HFP NOMINAL DEPLETION                                                      343.71 ppm  550.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =   550.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    32.271 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9974593     0.00254072     5.2067E-02     8.8013E-03     1.48133    -0.001     125.47    
     2  2  1.0005271    -0.00052709     5.1945E-02     8.7153E-03     1.56249    -0.037     140.16    
     3  3  1.0001761    -0.00017610     2.2948E-02     4.2463E-03     1.52744    -0.021     154.38    
     4  4  0.9999854     0.00001463 *   4.9260E-03     6.0232E-04     1.53500    -0.026     152.07    
     5  5  1.0000033 *  -0.00000327 *   8.1074E-04     1.3645E-04 *   1.53376    -0.024     151.28 *  
     6  6  0.9999976 *   0.00000236 *   1.9404E-04 *   6.8948E-05 *   1.53346    -0.025     151.98 *  

    Nodal Solution Time = 0.62 Sec

 Boron (ppm)  =  151.977
 Axial Offset =   -0.025
 Max. Node-Averaged  Peaking Factor = 1.533 in Node (11,11, 4)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  1.323  0.956  1.269  0.955  1.376  1.089  1.343  1.211   08
  9  0.956  0.926  0.848  0.824  1.048  1.373  1.008  1.192   09
 10  1.269  0.824  1.334  1.009  1.353  1.059  1.312  1.190   10
 11  0.955  0.948  1.013  1.362  1.006  0.797  0.982  1.100   11
 12  1.376  1.053  1.357  1.006  1.320  1.022  1.155          12
 13  1.089  1.374  1.062  0.799  1.023  1.292  1.122          13
 14  1.343  0.998  1.315  0.985  1.156  1.123                 14
 15  1.211  1.195  1.191  1.104                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  14,13   4,13   4,13  13,15  14,13   3,13   4,13  13, 3   08
  9   4,13   3, 5  15,13   5,15   5,14   4, 5   5, 4   5, 3   09
 10   4,13  13, 3  14,13  14,13   4,13   5, 4   4, 5   5, 3   10
 11  13,15  14,13  13,14  13, 4   5, 4   1, 1   3, 5   1, 1   11
 12  14,13  14, 5  13, 4   4, 5   5, 4  13, 3  13, 3          12
 13   3,13   5, 4   4, 5   1, 1   3,13   5, 4   1, 1          13
 14   4,13   3, 5   5, 4   5, 3   3,13   1, 1                 14
 15  13, 3   1, 9   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8 18.421 45.523 19.071 49.186 26.275 54.148 26.914 24.005   08
  9 45.523 45.367 65.242 33.981 53.500 27.089 53.457 23.414   09
 10 19.071 66.908 22.633 51.612 25.244 50.762 25.186 22.470   10
 11 49.186 49.366 51.652 24.974 52.345 68.397 45.594 19.407   11
 12 26.275 53.585 25.534 52.493 23.846 48.482 19.211          12
 13 54.148 26.928 50.488 68.437 48.473 21.924 18.452          13
 14 26.914 52.806 24.961 45.432 19.205 18.449                 14
 15 24.005 23.173 22.173 19.277                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  131
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 14 c1c24 HFP NOMINAL DEPLETION                                                      151.98 ppm  550.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1 17.619 43.918 43.729 17.408 17.972 44.970 47.059 22.841 24.840 49.107 52.192 25.720 24.763 22.003 15.977    1
  2 43.918 42.392 43.455 62.430 59.558 23.687 28.240 45.738 51.064 25.684 25.868 51.905 50.752 21.491 15.733    2
  3 43.758 43.253 43.777 59.147 56.679 27.270 31.767 45.765 50.682 25.301 25.385 51.381 49.806 21.043 15.386    3
  4 17.500 60.837 64.657 18.420 19.649 46.936 47.857 23.011 24.049 44.224 47.438 23.988 23.199 20.592 14.859    4
  5 18.133 59.953 64.263 19.833 21.439 48.411 49.060 23.390 23.071 45.160 47.283 22.310 21.865 19.275 13.750    5
  6 45.049 46.000 46.549 47.296 48.633 23.506 23.741 50.198 48.083 66.070 62.773 38.608 43.374 16.702 11.707    6
  7 47.171 47.802 47.889 48.364 49.388 23.843 23.767 50.302 48.183 65.042 61.190 39.746 42.954 13.009  8.381    7
  8 23.042 46.162 46.331 23.509 23.674 50.347 50.356 22.698 21.807 43.022 45.697 17.627 14.400                  8
  9 24.941 51.330 51.010 24.321 23.250 48.191 48.229 21.818 21.972 44.378 46.513 17.541 13.051                  9
 10 49.100 25.722 25.357 44.190 45.125 66.106 65.052 43.014 44.379 20.903 19.505 16.555 11.507                 10
 11 52.143 25.769 25.261 47.242 47.093 62.776 61.185 45.672 46.503 19.504 17.504 13.569  8.514                 11
 12 25.648 50.591 51.049 23.757 22.163 38.490 39.683 17.613 17.535 16.552 13.568                               12
 13 24.667 50.612 50.470 22.910 21.677 43.216 42.867 14.378 13.042 11.503  8.513                               13
 14 21.904 21.251 20.742 20.325 19.103 16.602 12.956                                                           14
 15 15.918 15.576 15.190 14.687 13.628 11.631  8.340                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  1.276  0.919  0.901  1.218  1.217  0.904  0.918  1.279  1.315  1.054  1.022  1.284  1.232  1.110  0.850    1
  2  0.920  0.902  0.868  0.773  0.800  0.787  0.798  1.005  1.012  1.320  1.306  0.967  0.925  1.100  0.844    2
  3  0.902  0.870  0.845  0.784  0.821  0.776  0.788  1.005  1.010  1.312  1.297  0.961  0.924  1.094  0.838    3
  4  1.219  0.784  0.758  1.198  1.230  0.930  0.944  1.281  1.296  1.024  0.985  1.253  1.209  1.087  0.827    4
  5  1.216  0.797  0.779  1.227  1.272  0.966  0.971  1.288  1.265  0.954  0.923  1.212  1.178  1.052  0.790    5
  6  0.898  0.882  0.886  0.928  0.968  1.310  1.308  0.968  0.927  0.775  0.773  0.938  0.865  0.963  0.704    6
  7  0.915  0.910  0.917  0.946  0.974  1.308  1.303  0.961  0.918  0.768  0.757  0.874  0.767  0.812  0.540    7
  8  1.280  1.006  1.008  1.287  1.292  0.968  0.961  1.267  1.242  0.951  0.882  1.079  0.912                  8
  9  1.317  1.016  1.015  1.300  1.267  0.927  0.918  1.243  1.258  0.979  0.907  1.067  0.832                  9
 10  1.055  1.322  1.314  1.027  0.956  0.776  0.769  0.952  0.979  1.229  1.159  1.007  0.741                 10
 11  1.022  1.310  1.301  0.987  0.925  0.774  0.758  0.883  0.908  1.159  1.054  0.859  0.571                 11
 12  1.287  0.966  0.951  1.257  1.215  0.941  0.877  1.081  1.068  1.008  0.859                               12
 13  1.234  0.913  0.904  1.213  1.181  0.868  0.770  0.914  0.833  0.742  0.572                               13
 14  1.110  1.102  1.096  1.089  1.055  0.966  0.815                                                           14
 15  0.850  0.844  0.839  0.829  0.793  0.706  0.542                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 1.606 in Node ( 8,12, 3)

    Pin Power Reconstruction Time = 0.30 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  1.276  0.910  1.217  0.909  1.298  1.038  1.259  0.980   08
  9  0.910  0.871  0.795  0.787  1.008  1.309  0.944  0.969   09
 10  1.217  0.780  1.232  0.953  1.283  0.971  1.213  0.939   10
 11  0.909  0.899  0.954  1.307  0.943  0.768  0.861  0.755   11
 12  1.298  1.011  1.287  0.944  1.252  0.930  0.973          12
 13  1.038  1.312  0.974  0.769  0.931  1.150  0.795          13
 14  1.259  0.933  1.217  0.864  0.974  0.795                 14
 15  0.980  0.970  0.941  0.757                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.131  0.977  1.146  0.964  1.099  0.958  1.091  1.146   08
  9  0.977  0.981  0.910  0.925  0.970  1.088  0.951  1.150   09
 10  1.146  0.897  1.130  0.963  1.103  0.979  1.109  1.161   10
 11  0.964  0.970  0.960  1.100  0.961  0.892  1.014  1.207   11
 12  1.099  0.968  1.101  0.961  1.099  0.990  1.172          12
 13  0.958  1.088  0.979  0.892  0.990  1.136  1.205          13
 14  1.091  0.940  1.110  1.015  1.172  1.205                 14
 15  1.146  1.152  1.162  1.208                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8 17.619 43.831 17.753 46.062 23.916 50.635 25.199 18.951   08
  9 43.831 43.219 59.454 27.741 48.313 25.559 50.961 18.413   09
 10 17.753 62.428 19.835 48.066 23.380 46.026 22.840 17.119   10
 11 46.062 47.060 48.420 23.714 49.191 63.769 41.171 12.450   11
 12 23.916 48.708 23.688 49.281 22.074 44.903 15.655          12
 13 50.635 25.527 45.912 63.780 44.892 19.354 12.536          13
 14 25.199 50.680 22.626 41.064 15.642 12.534                 14
 15 18.951 18.189 16.936 12.382                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  49.47   0.00   09
 10   0.00   0.00   0.00  46.59   0.00  44.70   0.00   0.00   10
 11   0.00  45.61  46.94   0.00  47.72   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  47.80   0.00   0.00   0.00          12
 13   0.00   0.00  44.59   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  132
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 14 c1c24 HFP NOMINAL DEPLETION                                                      151.98 ppm  550.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97            25.56 25C-09 sil55  ( 9,13,  )      12.38  25E-15  sil96  (15,11,  )        19.41
        6    26A        16            50.68 24L-03 24L-03 (14, 9,  )      43.22  24N-05  24N-05 ( 9, 9,  )        45.95
        8    26C        48            50.96 24J-07 24J-07 ( 9,14,  )      41.06  24L-02  24L-02 (14,11,  )        46.75
        5    25C        16            63.78 24J-03 24J-03 (13,11,  )      59.45  24K-06  24K-06 ( 9,10,  )        62.36
        9    26D         4            27.74 24N-03 24N-03 ( 9,11,  )      27.74  24N-03  24N-03 ( 9,11,  )        27.74
        7    26B        12            48.42 24J-05 24J-05 (11,10,  )      47.06  24L-05  24L-05 (11, 9,  )        47.85

     CORE              193            63.78 24J-03 24J-03 (13,11,  )      12.38  25E-15  sil96  (15,11,  )        32.27


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97            27.09 25C-09 sil55  ( 2,11,  )
        6    26A        16            52.81 24L-03 24L-03 (12, 3,  )
        8    26C        48            54.15 24P-08 24P-08 ( 1,11,  )
        5    25C        16            68.44 24J-03 24J-03 (10, 6,  )
        9    26D         4            33.98 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            51.65 24J-05 24J-05 ( 7, 5,  )

     CORE              193            68.44 24J-03 24J-03 (10, 6,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  133
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 14 c1c24 HFP NOMINAL DEPLETION                                                      151.98 ppm  550.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  32.271 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp. 550.0 EFPD 13200.0 EFPH  19.248 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  1.200E+03 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  1.750 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  1.619E-04  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     1.533 (11,11, 4)
       Fuel    Average . . .TFUAVE  860.72 K      1089.62 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  581.04 K       586.20 F                  F-delta-H       1.376 (12, 8)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.413 (12, 8, 2)   [Off]
 Boron Conc. . . . . . . . . . BOR   152.0 ppm                               Max-3PIN        1.597 ( 8,12, 4)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        1.606 ( 8,12, 3)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O  -0.025                                 Total Neutron Leakage . . . . . . . . .  0.044
 Edit Ring -     1      2      3      4      5
 RPF            0.934  0.945  1.106  0.992  0.775
 KIN            0.996  0.992  1.018  1.067  1.206
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.711  0.710  0.703  0.708  0.717
 TFU (Deg K)    842.8  845.6  901.8  853.2  780.8
 TMO (Deg K)    579.9  580.0  582.8  580.9  577.2
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.44726   1.06520  14.91166   0.02288   0.66720   712.802   597.509
  23   0.89130   1.11338  23.51006   0.00000   0.66973   832.545   596.634
  22   1.00874   1.07168  28.60939   0.00000   0.67329   870.463   595.379
  21   1.06233   1.05288  32.08396   0.00000   0.67714   890.673   593.996
  20   1.06050   1.04351  33.50303   0.00000   0.68103   890.491   592.560
  19   1.04515   1.03901  34.03409   0.00000   0.68485   884.496   591.117
 
  18   1.03792   1.03945  34.51587   0.00000   0.68859   881.158   589.674
  17   1.03118   1.03853  34.69072   0.00000   0.69227   877.565   588.225
  16   1.03089   1.03898  34.90297   0.00000   0.69589   876.217   586.765
  15   1.02161   1.03768  34.57380   0.00000   0.69947   871.086   585.297
  14   1.03232   1.03929  35.00214   0.00000   0.70301   873.747   583.814
  13   1.03641   1.03818  35.07549   0.00000   0.70653   873.691   582.305
 
  12   1.03358   1.03739  34.83510   0.00000   0.71003   870.823   580.782
  11   1.04679   1.03893  35.25726   0.00000   0.71351   874.337   579.237
  10   1.05494   1.03834  35.41596   0.00000   0.71700   875.765   577.662
   9   1.06169   1.03833  35.44688   0.00000   0.72047   876.534   576.063
   8   1.06671   1.03597  35.26443   0.00000   0.72393   876.394   574.440
   7   1.09125   1.03709  35.71453   0.00000   0.72741   883.946   572.782
 
   6   1.11180   1.03744  35.66751   0.00000   0.73092   889.395   571.075
   5   1.12423   1.03869  34.87563   0.00000   0.73445   890.750   569.328
   4   1.14832   1.04752  34.13321   0.00000   0.73801   896.216   567.539
   3   1.12211   1.06700  31.36897   0.00000   0.74153   881.117   565.737
   2   0.98634   1.11193  25.53179   0.00000   0.74478   829.054   564.054
   1   0.44664   1.03693  14.75606   0.00000   0.74695   677.898   562.900
 
 Ave   1.00000   1.04931  32.27138   0.00095   0.70783   860.715   581.036
 P**2                     31.23368
 A-O  -0.02453   0.00207  -0.01683   1.00000  -0.03002     0.001     0.016
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  134
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 14 c1c24 HFP NOMINAL DEPLETION                                                      151.98 ppm  550.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  3.17875E-09  1.37871E-09  6.79324E-09  1.57130E-08  1.49117E+01  6.61420E-01  2.65796E+01  8.22332E+02  0.00000E+00
  23  6.34620E-09  3.74407E-09  1.33703E-08  4.84805E-08  2.35101E+01  6.66022E-01  2.86805E+01  7.97530E+02  5.34667E+00
  22  7.19678E-09  4.00310E-09  1.57445E-08  5.15022E-08  2.86094E+01  6.69739E-01  2.94136E+01  8.31900E+02  6.31965E+00
  21  7.58085E-09  3.97118E-09  1.69422E-08  5.03012E-08  3.20840E+01  6.73542E-01  2.98786E+01  8.57807E+02  7.03765E+00
  20  7.56876E-09  3.94346E-09  1.70506E-08  5.01759E-08  3.35030E+01  6.77531E-01  2.99909E+01  8.77107E+02  7.33334E+00
  19  7.45964E-09  3.92068E-09  1.68522E-08  5.02775E-08  3.40341E+01  6.81540E-01  2.99698E+01  8.91091E+02  7.44335E+00
 
  18  7.40577E-09  3.86462E-09  1.67667E-08  4.94969E-08  3.45159E+01  6.85509E-01  2.99656E+01  9.00863E+02  7.54230E+00
  17  7.35581E-09  3.83325E-09  1.66635E-08  4.91196E-08  3.46907E+01  6.89457E-01  2.99280E+01  9.06999E+02  7.57757E+00
  16  7.35200E-09  3.79863E-09  1.66671E-08  4.85180E-08  3.49030E+01  6.93367E-01  2.99135E+01  9.10689E+02  7.61971E+00
  15  7.28725E-09  3.82488E-09  1.64903E-08  4.92160E-08  3.45738E+01  6.97248E-01  2.98159E+01  9.12090E+02  7.54945E+00
  14  7.36068E-09  3.77058E-09  1.66856E-08  4.79988E-08  3.50021E+01  7.01058E-01  2.98595E+01  9.12594E+02  7.63465E+00
  13  7.38914E-09  3.75996E-09  1.67526E-08  4.77339E-08  3.50755E+01  7.04873E-01  2.98476E+01  9.11906E+02  7.64613E+00
 
  12  7.37079E-09  3.78404E-09  1.66882E-08  4.82303E-08  3.48351E+01  7.08654E-01  2.97849E+01  9.10276E+02  7.59214E+00
  11  7.46275E-09  3.73428E-09  1.69257E-08  4.70718E-08  3.52573E+01  7.12390E-01  2.98438E+01  9.08291E+02  7.67313E+00
  10  7.52033E-09  3.71746E-09  1.70652E-08  4.66224E-08  3.54160E+01  7.16136E-01  2.98620E+01  9.05201E+02  7.70057E+00
   9  7.57003E-09  3.72013E-09  1.71775E-08  4.65441E-08  3.54469E+01  7.19870E-01  2.98655E+01  9.01002E+02  7.70206E+00
   8  7.60858E-09  3.75149E-09  1.72472E-08  4.69965E-08  3.52644E+01  7.23581E-01  2.98424E+01  8.94920E+02  7.66080E+00
   7  7.78162E-09  3.70677E-09  1.76715E-08  4.57689E-08  3.57145E+01  7.27283E-01  2.99511E+01  8.86926E+02  7.74733E+00
 
   6  7.92912E-09  3.71361E-09  1.79953E-08  4.54909E-08  3.56675E+01  7.31007E-01  2.99941E+01  8.75463E+02  7.73330E+00
   5  8.02171E-09  3.78073E-09  1.81242E-08  4.62865E-08  3.48756E+01  7.34705E-01  2.99290E+01  8.59442E+02  7.56959E+00
   4  8.19022E-09  3.75556E-09  1.84104E-08  4.52306E-08  3.41332E+01  7.38353E-01  2.99066E+01  8.38629E+02  7.41778E+00
   3  7.99903E-09  3.75755E-09  1.76664E-08  4.50992E-08  3.13690E+01  7.41871E-01  2.95165E+01  8.11245E+02  6.86527E+00
   2  7.01678E-09  3.60570E-09  1.48546E-08  4.37211E-08  2.55318E+01  7.45000E-01  2.85531E+01  7.76262E+02  5.75972E+00
   1  3.17608E-09  1.35709E-09  6.72306E-09  1.54810E-08  1.47561E+01  7.47025E-01  2.59124E+01  8.20931E+02  0.00000E+00
 
 Ave   7.1304E-09   3.5916E-09   1.5972E-08   4.5045E-08   3.2271E+01   7.0613E-01   2.9450E+01   8.7173E+02   6.8640E+00
 A-O       -0.024        0.017       -0.025        0.033       -0.017       -0.032        0.001        0.007       -0.015
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  1.49827E+01  1.46904E+01  1.47172E+01  1.50181E+01  9.76589E-01  9.74691E-01  9.92568E-01  9.92671E-01  9.74762E-01
  23  2.37140E+01  2.32447E+01  2.32754E+01  2.37542E+01  9.38003E-01  9.39521E-01  9.23121E-01  9.23293E-01  9.39590E-01
  22  2.88697E+01  2.83033E+01  2.83357E+01  2.89118E+01  1.01755E+00  1.02031E+00  9.98799E-01  9.99066E-01  1.02043E+00
  21  3.23923E+01  3.17449E+01  3.17799E+01  3.24371E+01  9.88022E-01  9.89832E-01  9.70625E-01  9.70877E-01  9.89991E-01
  20  3.38202E+01  3.31509E+01  3.31875E+01  3.38671E+01  9.95196E-01  9.96826E-01  9.77679E-01  9.77926E-01  9.96990E-01
  19  3.43475E+01  3.36764E+01  3.37140E+01  3.43958E+01  1.00878E+00  1.01041E+00  9.90952E-01  9.91198E-01  1.01057E+00
 
  18  3.48369E+01  3.41527E+01  3.41913E+01  3.48861E+01  9.95883E-01  9.96951E-01  9.78861E-01  9.79102E-01  9.97118E-01
  17  3.50117E+01  3.43255E+01  3.43648E+01  3.50616E+01  9.95028E-01  9.95807E-01  9.78242E-01  9.78483E-01  9.95977E-01
  16  3.52284E+01  3.45335E+01  3.45734E+01  3.52789E+01  9.86902E-01  9.87294E-01  9.70708E-01  9.70947E-01  9.87466E-01
  15  3.48810E+01  3.42053E+01  3.42454E+01  3.49322E+01  1.01757E+00  1.01838E+00  1.00037E+00  1.00061E+00  1.01854E+00
  14  3.53254E+01  3.46280E+01  3.46689E+01  3.53771E+01  9.90391E-01  9.90424E-01  9.74500E-01  9.74735E-01  9.90593E-01
  13  3.53985E+01  3.46989E+01  3.47403E+01  3.54508E+01  9.91868E-01  9.91730E-01  9.76103E-01  9.76336E-01  9.91898E-01
 
  12  3.51439E+01  3.44582E+01  3.44999E+01  3.51969E+01  1.01600E+00  1.01616E+00  9.99500E-01  9.99736E-01  1.01632E+00
  11  3.55821E+01  3.48749E+01  3.49174E+01  3.56356E+01  9.89679E-01  9.89099E-01  9.74432E-01  9.74661E-01  9.89265E-01
  10  3.57445E+01  3.50313E+01  3.50744E+01  3.57986E+01  9.85126E-01  9.84243E-01  9.70206E-01  9.70433E-01  9.84413E-01
   9  3.57730E+01  3.50588E+01  3.51022E+01  3.58277E+01  9.91490E-01  9.90573E-01  9.76558E-01  9.76782E-01  9.90735E-01
   8  3.55784E+01  3.48760E+01  3.49196E+01  3.56336E+01  1.01465E+00  1.01403E+00  9.98966E-01  9.99193E-01  1.01418E+00
   7  3.60486E+01  3.53207E+01  3.53651E+01  3.61042E+01  9.85716E-01  9.84345E-01  9.71325E-01  9.71543E-01  9.84510E-01
 
   6  3.60030E+01  3.52716E+01  3.53163E+01  3.60590E+01  9.85758E-01  9.84250E-01  9.71539E-01  9.71755E-01  9.84414E-01
   5  3.51895E+01  3.44833E+01  3.45275E+01  3.52455E+01  1.01857E+00  1.01761E+00  1.00327E+00  1.00349E+00  1.01775E+00
   4  3.44549E+01  3.37458E+01  3.37896E+01  3.45100E+01  9.90960E-01  9.89404E-01  9.76949E-01  9.77168E-01  9.89552E-01
   3  3.16590E+01  3.10069E+01  3.10487E+01  3.17119E+01  9.88198E-01  9.86615E-01  9.74516E-01  9.74744E-01  9.86749E-01
   2  2.57455E+01  2.52195E+01  2.52571E+01  2.57931E+01  9.49711E-01  9.47763E-01  9.38185E-01  9.38353E-01  9.47874E-01
   1  1.48095E+01  1.45212E+01  1.45487E+01  1.48430E+01  1.02739E+00  1.02501E+00  1.04566E+00  1.04576E+00  1.02511E+00
 
 Ave   3.2563E+01   3.1916E+01   3.1955E+01   3.2613E+01   9.9354E-01   9.9339E-01   9.8057E-01   9.8079E-01   9.9353E-01
 A-O       -0.017       -0.017       -0.017       -0.017       -0.002       -0.001       -0.003       -0.003       -0.001
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  135
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 15 c1c24 HFP NOMINAL DEPLETION                                                      151.98 ppm  590.000 EFPD    

 Cycle exposure at end of this step = 590.00 EFPD                                                    

 Depletion step used = 40.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  136
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 15 c1c24 HFP NOMINAL DEPLETION                                                      151.98 ppm  590.000 EFPD    

  ITE.BOR - QPANDA Flux solution with boron search

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =   590.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    33.671 GWd/MT      Update SM/PM

    NH NQ     K-eff         Diff          Eps Max        Eps Min    Peak Source    A-O    PPM Boron
     1  1  0.9979247     0.00207534     4.4777E-02     7.0343E-03     1.46774    -0.004     -17.57    
     2  2  1.0004406    -0.00044063     6.1535E-02     9.2533E-03     1.56398    -0.034      -4.82    
     3  3  1.0001427    -0.00014274     1.6500E-02     4.5879E-03     1.53859    -0.022       6.55    
     4  4  0.9999970     0.00000297 *   1.1890E-03     3.3708E-04 *   1.53676    -0.025       4.48    
     5  5  1.0000008 *  -0.00000085 *   6.9944E-04     9.3952E-05 *   1.53784    -0.024       4.22 *  
     6  6  0.9999990 *   0.00000101 *   5.2678E-04     7.1928E-05 *   1.53703    -0.024       4.52 *  
     7  7  1.0000007 *  -0.00000071 *   2.3355E-04 *   1.6689E-05 *   1.53739    -0.024       4.32 *  

    Nodal Solution Time = 0.75 Sec

 Boron (ppm)  =    4.321
 Axial Offset =   -0.024
 Max. Node-Averaged  Peaking Factor = 1.537 in Node ( 8, 8, 4)

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  1.331  0.967  1.278  0.958  1.361  1.077  1.327  1.200   08
  9  0.967  0.939  0.858  0.833  1.039  1.356  0.996  1.182   09
 10  1.278  0.833  1.327  1.001  1.340  1.048  1.300  1.183   10
 11  0.958  0.946  1.004  1.348  0.997  0.798  0.983  1.101   11
 12  1.361  1.043  1.343  0.996  1.309  1.021  1.160          12
 13  1.077  1.358  1.051  0.799  1.021  1.291  1.128          13
 14  1.327  0.990  1.305  0.987  1.161  1.129                 14
 15  1.200  1.191  1.187  1.106                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  14,13   4,13   4,13  13,14  14,13   3,13   4,13   3,13   08
  9   4,13   3, 5  15,13   5,15   5,14   4, 5   5, 4   5, 3   09
 10   4,13  13, 3  14,13  14,13   4,13   5, 4   4, 5   5, 3   10
 11  13,14  14,13  13,14   5, 4   5, 4   1, 1   3, 5   1, 1   11
 12  14,13  14, 5  13, 4   4, 5   5, 4  13, 3  13, 3          12
 13   3,13   5, 4   4, 5   1, 1   3,13   5, 4   1, 1          13
 14   4,13   3, 5   5, 4   5, 3   3,13   1, 1                 14
 15   3,13   1, 9   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8 20.093 47.021 20.670 50.682 27.995 55.785 28.582 25.511   08
  9 47.021 46.744 66.498 35.275 55.156 28.789 55.046 24.912   09
 10 20.670 68.116 24.304 53.211 26.939 52.338 26.817 23.950   10
 11 50.682 50.851 53.241 26.677 53.924 69.638 47.010 20.791   11
 12 27.995 55.233 27.232 54.089 25.502 49.980 20.686          12
 13 55.785 28.645 52.067 69.679 49.972 23.527 19.859          13
 14 28.582 54.328 26.601 46.854 20.681 19.857                 14
 15 25.511 24.679 23.656 20.666                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  137
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 15 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  590.000 EFPD    

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1 19.231 45.379 45.161 18.948 19.509 46.402 48.511 24.447 26.487 50.762 53.797 27.327 26.306 23.395 17.049    1
  2 45.379 43.826 44.836 63.660 60.830 24.939 29.508 47.323 52.657 27.337 27.503 53.423 52.205 22.871 16.797    2
  3 45.191 44.636 45.120 60.395 57.982 28.504 33.017 47.350 52.271 26.943 27.009 52.891 51.258 22.416 16.443    3
  4 19.041 62.084 65.863 19.931 21.198 48.404 49.346 24.619 25.674 45.833 48.986 25.558 24.715 21.957 15.902    4
  5 19.669 61.219 65.498 21.378 23.037 49.933 50.587 25.005 24.657 46.662 48.737 23.833 23.345 20.598 14.750    5
  6 46.472 47.396 47.949 48.759 50.157 25.147 25.379 51.720 49.542 67.296 63.997 40.091 44.742 17.918 12.599    6
  7 48.617 49.238 49.335 49.853 50.919 25.481 25.399 51.813 49.630 66.258 62.390 41.132 44.172 14.041  9.071    7
  8 24.650 47.746 47.917 25.123 25.292 51.869 51.868 24.287 23.368 44.525 47.094 18.991 15.557                  8
  9 26.591 52.928 52.605 25.949 24.838 49.651 49.677 23.379 23.553 45.924 47.949 18.889 14.107                  9
 10 50.756 27.378 27.003 45.803 46.631 67.333 66.270 44.518 45.925 22.450 20.966 17.827 12.448                 10
 11 53.748 27.409 26.890 48.794 48.551 64.002 62.388 47.071 47.940 20.965 18.834 14.659  9.243                 11
 12 27.259 52.110 52.546 25.334 23.690 39.977 41.072 18.979 18.884 17.825 14.658                               12
 13 26.213 52.050 51.894 24.432 23.162 44.590 44.089 15.538 14.100 12.445  9.242                               13
 14 23.297 22.634 22.120 21.694 20.431 17.822 13.991                                                           14
 15 16.990 16.641 16.249 15.734 14.632 12.527  9.032                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  1.285  0.931  0.913  1.228  1.226  0.911  0.922  1.273  1.303  1.044  1.012  1.269  1.219  1.101  0.852    1
  2  0.931  0.915  0.882  0.786  0.812  0.800  0.808  1.003  1.006  1.306  1.291  0.957  0.917  1.092  0.847    2
  3  0.913  0.883  0.858  0.796  0.830  0.787  0.797  1.003  1.003  1.298  1.282  0.952  0.916  1.088  0.842    3
  4  1.229  0.796  0.769  1.203  1.231  0.931  0.942  1.273  1.285  1.015  0.977  1.242  1.200  1.082  0.832    4
  5  1.224  0.807  0.787  1.227  1.266  0.961  0.964  1.278  1.255  0.949  0.920  1.207  1.174  1.050  0.798    5
  6  0.904  0.887  0.888  0.926  0.962  1.298  1.295  0.960  0.923  0.778  0.778  0.941  0.869  0.968  0.714    6
  7  0.917  0.910  0.915  0.941  0.966  1.295  1.290  0.954  0.916  0.773  0.764  0.881  0.776  0.826  0.555    7
  8  1.273  1.001  1.002  1.276  1.279  0.960  0.954  1.257  1.237  0.953  0.888  1.087  0.927                  8
  9  1.304  1.008  1.007  1.287  1.256  0.923  0.916  1.237  1.253  0.979  0.911  1.073  0.846                  9
 10  1.044  1.308  1.300  1.017  0.951  0.779  0.774  0.954  0.980  1.229  1.161  1.013  0.754                 10
 11  1.012  1.296  1.287  0.980  0.923  0.779  0.765  0.889  0.912  1.162  1.059  0.872  0.587                 11
 12  1.273  0.959  0.946  1.248  1.211  0.944  0.883  1.089  1.074  1.014  0.872                               12
 13  1.222  0.909  0.901  1.206  1.178  0.872  0.778  0.929  0.847  0.755  0.587                               13
 14  1.102  1.096  1.092  1.086  1.055  0.972  0.829                                                           14
 15  0.853  0.848  0.844  0.835  0.801  0.717  0.557                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 1.606 in Node ( 8, 8, 4)

    Pin Power Reconstruction Time = 0.30 Sec

 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  1.285  0.922  1.227  0.914  1.288  1.028  1.246  0.977   08
  9  0.922  0.884  0.806  0.798  1.004  1.294  0.935  0.967   09
 10  1.227  0.790  1.232  0.950  1.273  0.965  1.206  0.941   10
 11  0.914  0.900  0.949  1.294  0.938  0.773  0.866  0.766   11
 12  1.288  1.005  1.275  0.938  1.246  0.933  0.983          12
 13  1.028  1.298  0.968  0.774  0.934  1.153  0.806          13
 14  1.246  0.929  1.211  0.869  0.985  0.807                 14
 15  0.977  0.970  0.944  0.769                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.128  0.979  1.144  0.966  1.097  0.959  1.090  1.146   08
  9  0.979  0.983  0.914  0.931  0.971  1.085  0.952  1.151   09
 10  1.144  0.901  1.128  0.963  1.101  0.979  1.107  1.162   10
 11  0.966  0.971  0.961  1.098  0.962  0.896  1.016  1.209   11
 12  1.097  0.969  1.099  0.961  1.097  0.992  1.171          12
 13  0.959  1.086  0.980  0.896  0.992  1.135  1.206          13
 14  1.090  0.942  1.109  1.017  1.171  1.206                 14
 15  1.146  1.152  1.163  1.209                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8 19.231 45.277 19.292 47.501 25.544 52.266 26.776 20.183   08
  9 45.277 44.604 60.717 28.992 49.900 27.198 52.444 19.632   09
 10 19.292 63.666 21.386 49.567 24.989 47.554 24.363 18.302   10
 11 47.501 48.479 49.922 25.351 50.676 64.985 42.534 13.407   11
 12 25.544 50.299 25.300 50.766 23.646 46.373 16.886          12
 13 52.266 27.170 47.445 64.998 46.363 20.804 13.544          13
 14 26.776 52.150 24.154 42.432 16.875 13.543                 14
 15 20.183 19.411 18.123 13.343                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  50.89   0.00   09
 10   0.00   0.00   0.00  48.03   0.00  46.17   0.00   0.00   10
 11   0.00  46.97  48.38   0.00  49.14   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  49.23   0.00   0.00   0.00          12
 13   0.00   0.00  46.06   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  138
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 15 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  590.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97            27.20 25C-09 sil55  ( 9,13,  )      13.34  25E-15  sil96  (15,11,  )        20.78
        6    26A        16            52.15 24L-03 24L-03 (14, 9,  )      44.60  24N-05  24N-05 ( 9, 9,  )        47.38
        8    26C        48            52.44 24J-07 24J-07 ( 9,14,  )      42.43  24L-02  24L-02 (14,11,  )        48.25
        5    25C        16            65.00 24J-03 24J-03 (13,11,  )      60.72  24K-06  24K-06 ( 9,10,  )        63.59
        9    26D         4            28.99 24N-03 24N-03 ( 9,11,  )      28.99  24N-03  24N-03 ( 9,11,  )        28.99
        7    26B        12            49.92 24J-05 24J-05 (11,10,  )      48.48  24L-05  24L-05 (11, 9,  )        49.32

     CORE              193            65.00 24J-03 24J-03 (13,11,  )      13.34  25E-15  sil96  (15,11,  )        33.67


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97            28.79 25C-09 sil55  ( 2,11,  )
        6    26A        16            54.33 24L-03 24L-03 (12, 3,  )
        8    26C        48            55.79 24P-08 24P-08 ( 1,11,  )
        5    25C        16            69.68 24J-03 24J-03 (10, 6,  )
        9    26D         4            35.27 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            53.24 24J-05 24J-05 ( 7, 5,  )

     CORE              193            69.68 24J-03 24J-03 (10, 6,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  139
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 15 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  590.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP   100.0 %                               Core Average Exposure . . . . EBAR  33.671 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp. 590.0 EFPD 14160.0 EFPH  20.648 GWd/MT
 Thermal Power . . . . . . . . CTP  3469.1 MWt                             Depletion Step Length . . . . .  9.600E+02 Hours
 Core Flow . . . . . . . . . . CWT  61666. MT/hr   135.95 Mlb/hr           Depletion =  1.400 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.63 kcal/kg 150.53 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  1.678E-04  CM-2
 Inlet Enthalpy. . . . . . .HINLET  305.99 kcal/kg 550.78 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  562.54 K       552.90 F                Search for Boron Conc.
       Coolant Average . . .TMOAVE  580.25 K       584.79 F                Peak Nodal Power (Location)     1.537 ( 8, 8, 4)
       Fuel    Average . . .TFUAVE  862.18 K      1092.25 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  581.03 K       586.19 F                  F-delta-H       1.361 (12, 8)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.400 ( 8, 8, 6)   [Off]
 Boron Conc. . . . . . . . . . BOR     4.3 ppm                               Max-3PIN        1.600 ( 8, 8, 4)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        1.606 ( 8, 8, 4)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  1.00000
 Axial Offset. . . . . . . . . A-O  -0.024                                 Total Neutron Leakage . . . . . . . . .  0.044
 Edit Ring -     1      2      3      4      5
 RPF            0.946  0.952  1.098  0.992  0.787
 KIN            0.997  0.993  1.018  1.068  1.208
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.710  0.710  0.704  0.708  0.717
 TFU (Deg K)    849.0  849.8  901.5  854.6  781.6
 TMO (Deg K)    580.1  580.2  582.7  580.9  577.4
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.47210   1.07452  15.82066   0.02288   0.66727   717.876   597.503
  23   0.91120   1.11314  24.73871   0.00000   0.66988   837.737   596.600
  22   1.01700   1.07074  29.99025   0.00000   0.67349   873.864   595.327
  21   1.06292   1.05196  33.53270   0.00000   0.67734   892.348   593.937
  20   1.05684   1.04278  34.94646   0.00000   0.68122   890.945   592.503
  19   1.03955   1.03849  35.45535   0.00000   0.68502   884.375   591.065
 
  18   1.03151   1.03914  35.92680   0.00000   0.68873   880.891   589.630
  17   1.02481   1.03832  36.09255   0.00000   0.69238   877.419   588.188
  16   1.02474   1.03885  36.30462   0.00000   0.69599   876.273   586.736
  15   1.01609   1.03755  35.96327   0.00000   0.69954   871.354   585.274
  14   1.02690   1.03923  36.40632   0.00000   0.70306   874.217   583.797
  13   1.03121   1.03810  36.48542   0.00000   0.70657   874.310   582.295
 
  12   1.02860   1.03730  36.24135   0.00000   0.71005   871.499   580.777
  11   1.04147   1.03890  36.68126   0.00000   0.71352   875.000   579.237
  10   1.04935   1.03831  36.85086   0.00000   0.71699   876.369   577.670
   9   1.05570   1.03830  36.89071   0.00000   0.72044   877.004   576.077
   8   1.06050   1.03583  36.71490   0.00000   0.72388   876.749   574.463
   7   1.08440   1.03693  37.19797   0.00000   0.72734   884.182   572.814
 
   6   1.10504   1.03715  37.17898   0.00000   0.73084   889.697   571.118
   5   1.11900   1.03816  36.40503   0.00000   0.73435   891.540   569.380
   4   1.14622   1.04683  35.69751   0.00000   0.73790   898.081   567.598
   3   1.12709   1.06636  32.90230   0.00000   0.74142   884.861   565.794
   2   1.00286   1.11227  26.88784   0.00000   0.74470   834.524   564.096
   1   0.46490   1.04710  15.65746   0.00000   0.74692   681.159   562.914
 
 Ave   1.00000   1.04984  33.67122   0.00095   0.70787   862.178   581.033
 P**2                     32.65243
 A-O  -0.02376   0.00196  -0.01712   1.00000  -0.02989     0.001     0.016
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  140
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step 15 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  590.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  3.35091E-09  1.38019E-09  7.26779E-09  1.56135E-08  1.58207E+01  6.61926E-01  2.66142E+01  7.24507E+02  0.00000E+00
  23  6.48020E-09  3.73723E-09  1.37938E-08  4.81481E-08  2.47387E+01  6.66459E-01  2.87121E+01  6.90398E+02  5.55036E+00
  22  7.24804E-09  3.96999E-09  1.60097E-08  5.10885E-08  2.99902E+01  6.70155E-01  2.94274E+01  7.26924E+02  6.54427E+00
  21  7.57846E-09  3.92058E-09  1.70904E-08  4.97713E-08  3.35327E+01  6.73942E-01  2.98735E+01  7.55558E+02  7.26889E+00
  20  7.53772E-09  3.88693E-09  1.71279E-08  4.96123E-08  3.49465E+01  6.77903E-01  2.99702E+01  7.76540E+02  7.56161E+00
  19  7.41685E-09  3.86274E-09  1.68962E-08  4.97072E-08  3.54553E+01  6.81880E-01  2.99396E+01  7.91478E+02  7.66705E+00
 
  18  7.35953E-09  3.80577E-09  1.67992E-08  4.89108E-08  3.59268E+01  6.85815E-01  2.99303E+01  8.01902E+02  7.76305E+00
  17  7.31209E-09  3.77492E-09  1.66997E-08  4.85282E-08  3.60925E+01  6.89728E-01  2.98911E+01  8.08278E+02  7.79621E+00
  16  7.31186E-09  3.74064E-09  1.67112E-08  4.79200E-08  3.63046E+01  6.93605E-01  2.98768E+01  8.12074E+02  7.83755E+00
  15  7.25283E-09  3.76935E-09  1.65468E-08  4.86403E-08  3.59633E+01  6.97453E-01  2.97805E+01  8.13231E+02  7.76578E+00
  14  7.32841E-09  3.71383E-09  1.67490E-08  4.74014E-08  3.64063E+01  7.01232E-01  2.98254E+01  8.13872E+02  7.85202E+00
  13  7.35926E-09  3.70329E-09  1.68230E-08  4.71346E-08  3.64854E+01  7.05018E-01  2.98149E+01  8.13121E+02  7.86405E+00
 
  12  7.34252E-09  3.72855E-09  1.67630E-08  4.76476E-08  3.62413E+01  7.08769E-01  2.97534E+01  8.11335E+02  7.80976E+00
  11  7.43217E-09  3.67659E-09  1.69981E-08  4.64674E-08  3.66813E+01  7.12478E-01  2.98128E+01  8.09604E+02  7.89225E+00
  10  7.48733E-09  3.65849E-09  1.71345E-08  4.60101E-08  3.68509E+01  7.16196E-01  2.98316E+01  8.06652E+02  7.92076E+00
   9  7.53326E-09  3.66036E-09  1.72406E-08  4.59350E-08  3.68907E+01  7.19902E-01  2.98355E+01  8.02537E+02  7.92335E+00
   8  7.56875E-09  3.69157E-09  1.73063E-08  4.64002E-08  3.67149E+01  7.23587E-01  2.98137E+01  7.96369E+02  7.88347E+00
   7  7.73617E-09  3.64345E-09  1.77237E-08  4.51457E-08  3.71980E+01  7.27264E-01  2.99243E+01  7.88589E+02  7.97375E+00
 
   6  7.88269E-09  3.64896E-09  1.80525E-08  4.48655E-08  3.71790E+01  7.30965E-01  2.99723E+01  7.76878E+02  7.96387E+00
   5  7.98428E-09  3.71807E-09  1.82106E-08  4.56928E-08  3.64050E+01  7.34643E-01  2.99173E+01  7.59942E+02  7.80443E+00
   4  8.17382E-09  3.69374E-09  1.85569E-08  4.46349E-08  3.56975E+01  7.38280E-01  2.99110E+01  7.38034E+02  7.65850E+00
   3  8.03185E-09  3.70538E-09  1.79284E-08  4.45639E-08  3.29023E+01  7.41799E-01  2.95431E+01  7.08505E+02  7.10458E+00
   2  7.13078E-09  3.57790E-09  1.52689E-08  4.33080E-08  2.68878E+01  7.44950E-01  2.85956E+01  6.71754E+02  5.97639E+00
   1  3.30434E-09  1.35556E-09  7.10475E-09  1.54282E-08  1.56575E+01  7.47011E-01  2.59413E+01  7.27787E+02  0.00000E+00
 
 Ave   7.1310E-09   3.5427E-09   1.6117E-08   4.4524E-08   3.3671E+01   7.0629E-01   2.9438E+01   7.7191E+02   7.0740E+00
 A-O       -0.024        0.018       -0.025        0.034       -0.017       -0.032        0.001        0.007       -0.015
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  1.59176E+01  1.55679E+01  1.55945E+01  1.59528E+01  9.71583E-01  9.69737E-01  9.86192E-01  9.86291E-01  9.69805E-01
  23  2.49774E+01  2.44467E+01  2.44770E+01  2.50171E+01  9.35993E-01  9.38046E-01  9.20401E-01  9.20561E-01  9.38104E-01
  22  3.02872E+01  2.96586E+01  2.96904E+01  3.03288E+01  1.01743E+00  1.02080E+00  9.97955E-01  9.98204E-01  1.02091E+00
  21  3.38797E+01  3.31673E+01  3.32017E+01  3.39240E+01  9.87583E-01  9.89970E-01  9.69545E-01  9.69781E-01  9.90113E-01
  20  3.53017E+01  3.45688E+01  3.46049E+01  3.53480E+01  9.94830E-01  9.97024E-01  9.76688E-01  9.76920E-01  9.97172E-01
  19  3.58058E+01  3.50731E+01  3.51101E+01  3.58535E+01  1.00860E+00  1.01079E+00  9.90138E-01  9.90368E-01  1.01093E+00
 
  18  3.62850E+01  3.55387E+01  3.55767E+01  3.63336E+01  9.95570E-01  9.97177E-01  9.77926E-01  9.78153E-01  9.97330E-01
  17  3.64506E+01  3.57024E+01  3.57410E+01  3.64999E+01  9.94660E-01  9.95969E-01  9.77256E-01  9.77482E-01  9.96124E-01
  16  3.66675E+01  3.59096E+01  3.59489E+01  3.67174E+01  9.86472E-01  9.87386E-01  9.69666E-01  9.69890E-01  9.87543E-01
  15  3.63069E+01  3.55701E+01  3.56096E+01  3.63576E+01  1.01753E+00  1.01888E+00  9.99675E-01  9.99902E-01  1.01901E+00
  14  3.67673E+01  3.60062E+01  3.60465E+01  3.68184E+01  9.90028E-01  9.90579E-01  9.73518E-01  9.73739E-01  9.90733E-01
  13  3.68464E+01  3.60825E+01  3.61233E+01  3.68981E+01  9.91492E-01  9.91871E-01  9.75111E-01  9.75330E-01  9.92024E-01
 
  12  3.65876E+01  3.58387E+01  3.58797E+01  3.66400E+01  1.01595E+00  1.01664E+00  9.98799E-01  9.99020E-01  1.01677E+00
  11  3.70449E+01  3.62719E+01  3.63137E+01  3.70978E+01  9.89297E-01  9.89227E-01  9.73434E-01  9.73650E-01  9.89377E-01
  10  3.72188E+01  3.64388E+01  3.64812E+01  3.72723E+01  9.84641E-01  9.84262E-01  9.69118E-01  9.69331E-01  9.84416E-01
   9  3.72564E+01  3.64750E+01  3.65178E+01  3.73105E+01  9.91146E-01  9.90736E-01  9.75602E-01  9.75813E-01  9.90883E-01
   8  3.70683E+01  3.62992E+01  3.63422E+01  3.71228E+01  1.01458E+00  1.01448E+00  9.98265E-01  9.98478E-01  1.01461E+00
   7  3.75735E+01  3.67753E+01  3.68190E+01  3.76284E+01  9.85261E-01  9.84395E-01  9.70280E-01  9.70484E-01  9.84544E-01
 
   6  3.75570E+01  3.67534E+01  3.67974E+01  3.76123E+01  9.85310E-01  9.84311E-01  9.70505E-01  9.70706E-01  9.84457E-01
   5  3.67612E+01  3.59832E+01  3.60267E+01  3.68165E+01  1.01859E+00  1.01816E+00  1.00266E+00  1.00286E+00  1.01828E+00
   4  3.60639E+01  3.52781E+01  3.53212E+01  3.61183E+01  9.90615E-01  9.89578E-01  9.75993E-01  9.76196E-01  9.89709E-01
   3  3.32371E+01  3.25072E+01  3.25484E+01  3.32893E+01  9.87752E-01  9.86690E-01  9.73424E-01  9.73637E-01  9.86806E-01
   2  2.71425E+01  2.65429E+01  2.65799E+01  2.71896E+01  9.48064E-01  9.46576E-01  9.35838E-01  9.35993E-01  9.46671E-01
   1  1.57365E+01  1.53907E+01  1.54180E+01  1.57698E+01  1.02610E+00  1.02369E+00  1.04288E+00  1.04298E+00  1.02379E+00
 
 Ave   3.4001E+01   3.3288E+01   3.3327E+01   3.4051E+01   9.9288E-01   9.9321E-01   9.7920E-01   9.7941E-01   9.9334E-01
 A-O       -0.017       -0.017       -0.017       -0.017       -0.002       -0.001       -0.003       -0.003       -0.001
 

 Memory required was 5621386 / 30000000 ( 18.74 % )

1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  141
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  2 Step  0 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  590.000 EFPD    

 CASE   2  --- Entering Input Segment ---
 == Dependent Case: 1 of Independent Case 1 Complete                                                 

           --- Reading Input Cards ---
 _____________________________________________________________________________________________________________________________
 ** Note    -   ITESRC   - ITE.SRC - POWER Search requested with KEF as target
 Input Cards:  DEP.STA, COM    , ITE.SRC, WRE    , STA    
 
 _____________________________________________________________________________________________________________________________

 Input Summary

 Relative Power. . . . . . PERCTP     100.0000 %           Fuel Assemblies . . . . . . . .      193.0000
 Relative Flow . . . . . . PERCWT     100.0000 %           Fuel Nodes (Assembly Size). . .     4632.0000
 Thermal Power . . . . . . . .CTP    3469.0854 MWt         Assembly Pitch (Cold) . . DXASSM      21.5040 cm
 Core Flow - Metric. . . . . .CWT   61666.4141 MT/hr       Node Height. . (Cold) . . . . DZ      15.2400 cm
           - English . . . . .CWT     135.9489 Mlb/hr      Core Height. . (Cold) . . .HCORE     365.7600 cm
 Rated Power Density . . . POWDEN     106.2730 kW/liter    Core Volume  . (Cold) . . . . .    32643.1484 liters
 Rated Coolant Mass Flux . FLODEN     690.9600 kg/(cm2-hr) Core Fueled Area. . . . . . . .    22311.8613 cm2
 Core Rated Thermal Power. . . .     3469.0854 MWt         Core Loading. . . . . . . . . .       99.1285 metric tons
 Core Rated Flow - Metric. . . .    61666.4141 MT/hr       Fuel Rods in Core . . . . . . .         50952
                 - English . . .      135.9489 Mlb/hr      Fuel Rods In Average Node . . .      264.0000
 Explicit Core Fraction. . . . .        0.2500             Total Fuel Rod Length in Core .    186362.047 meters
 Boundary Conditions . . . .  3                            Core Symmetry . . . . . . . . .        ROTATIONAL
                            +-+-+                          Number of BP Rods at Midplane .           752
                          3 + + +  0
                            +-+-+
                              0    0 (Diag)

1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  142
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  2 Step  0 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  590.000 EFPD    

 Option Summary
 -------------------------------------------------- File Information ------------------------------------------------------
 Model                                       Status                    Related Input Card                Defined
 -----                                       ------                    ------------------                -------
 Error Checking                              On                        ERR.CHK                           Restart File            
 Cross Sections                              CMS-Link Library          LIB                               User Input  
 Read  Restart                               Read Restart File         RES                               User Input              
 Read  Restart with Labels                   No                        RES.LAB                           Default                 
 Write Restart File                          Yes                       WRE                               User Input              
 Write Restart File with Labels              No                        WRE.LAB                           Default                 
 Write ASCII Restart File                    No                        WRI                               Default                 
 Write SUMMARY File                          Yes                       PRI.SUM                           Default                 
 PIN FILE                                    On                        PIN.FIL                           Default                 
 Write KINETICS File                         Off                       KIN.EDT                           Default                 
 Write CMS-View Data File                    OFF                       CMS.EDT                           Restart File            

 -------------------------------------------------- Calculation Information -----------------------------------------------
 Model                                       Status                    Related Input Card                Defined
 -----                                       ------                    ------------------                -------
 Depletion Units                             EFPD                      DEP.UNI                           Restart File            
 Depletion Power Shape                       Ave                       DEP.STA                           User Input              
 Fission Product Option                      EQ. I,XE, DEPL. PM,SM     DEP.FPD (Set by DEP.STA)          Restart File            
 Soluble Boron Depletion Model               Off                       DEP.BOR                           Default                 
 Soluble Boron Search                        No Boron Search           ITE.BOR                           Overridden              
 Eigenvalue Calculation                      K-effective Calc.         ITE.KEF                           Overridden              
 Automated Searches                          On                        ITE.SRC                           User Input              
 High Worth Rod Calc.                        Off                       ITE.HWR                           Default                 
 Neutronics Model                            2 Grp QPANDA -Conv. XS    ITE.SOL                           Restart File
 MOX Models                                  OFF                       ITE.MOX                           Restart File            
 Two-In-Row Convergence                      Off                       ITE.TWO                           Restart File            
 Speed Option                                On                        ITE.SPD                           Restart File            
   Speed Cross Sections                      On                        ITE.SPD                           Restart File            
   Speed Flux Iterations                     On                        ITE.SPD                           Restart File            
   Alternate Coupling Coefficients           On                        ITE.SPD                           Restart File            
 Perform Audit of Data Library               Off                       AUD.PRI                           Default                 
 Input Scan Only                             No                        STA                               Default                 
 Adjoint Calculation                         Off                       KIN.EDT                           Default                 
 Reactivity Perturb. Calc.                   Off                       PRT.COE                           Default                 
 Hydraulic Iteration Option                  On                        HYD.ITE/COR.OPE/COR.MWT           Restart File            
 Fuel Temperature Fit                        On                        SEG.TFU                           Restart File            
 Inlet Temperature Versus Power              On                        PWR.OPT                           Restart File            
 Steam Table Option                          Tabled ASME               COR.STM                           Restart File            
 Perturb Base Conditions                     Off                       SAV.BAS                           Default                 
 Use Perturbed Base Conditions               Off                       USE.BAS                           Default                 
 Grid Model by Mech. Type                    On                        GRD.EDT                           Restart File            
 Grid Model by Segment                       Off                       FUE.GRD/SEG.GRD                   Restart File            
 Control Rod Cusping Model                   On                        CRD.CSP                           Restart File            
 Automated Control Rod Calc.                 None                      CRD.ICB/OCB/TRP/                  Default     
 CRD.SET                                     Inactive                  CRD.SET                           Default                 
 Shutdown Cooling Model                      On                        FUE.SDC                           Default                 
 Print Pin Power Reconstruction Data         On                        PIN.EDT                           Restart File            
 Pin Loadings in Exposure Calculation        Off                       PIN.LOA                           Restart File            
 Integrated Pin Exposure Model               Off                       PIN.IPE                           Restart File            
 Print Statepoint Parameters                 On                        PRI.STA                           Restart File            
 Print Isotopics                             Off                       PRI.ISO                           Default                 
 Print File Trail Data                       Off                       PRI.FTD                           Restart File            
 Batch Edits                                 On                        BAT.EDT                           Restart File            

 Pinfile File Summary
 --------------------
 Pinfile Filename: PINFILE
 Pinfile Edit Options: ON                  


 Memory required was 4588030 / 30000000 ( 15.29 % )

1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  143
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  2 Step  0 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  590.000 EFPD    

 Case   2  --- Entering Neutronics Iterations ---
 ** Note    -   PINFIL   - PIN.FIL - PIN.FIL ON --Pin edits will reflect axial integration
 _____________________________________________________________________________________________________________________________

 Depletion Statepoint List =    620.000

 Cycle exposure at end of this step = 620.00 EFPD                                                    

 Depletion step used = 30.000* (0.5 beginning of step power + 0.5 end of step power)                 


 Active Iteration Levels :          ITE.LIM :       Calculation Options :
 Use Flat Power Initial Guess. . . . . . . .F       Fix Independent Variables . . .F
 Hydraulic Iteration . . . . . . . .NHMAX  40       Use Cross Section Library . . .T
 Source Iterations . . . . . . . . .NSMAX   4       Discontinuity Factors . . . . .T
 Inner Flux Iterations . . . . . . .NSIMAX  8       Isothermal Hydraulic Option . .F
 Coupling Coefficient Iterations . .NQMAX  40       Boron Search Option . . . . . .F
 Convergence - Flux   5.0E-04  Keff   5.0E-05       Pin Reconstruction Option . . .T
 Initial Keff 1.00000         Shift   3.0E-02       Neutronic Option (ITE.SOL). . .0


 Control Rod Group Edit
 ----------------------
 C.R. Group --------------         1         2         3         4         5         6         7         8         9
 C.R. Pos. Steps Withdrawn       215       226       226       226       226       226       226       226       226
 C.R. Pos. Steps Inserted          9        -2        -2        -2        -2        -2        -2        -2        -2
 Number of  Rods/Group ---         5         8         8         4         4         4         4         8         8
 Number of Steps/Group ---        45         0         0         0         0         0         0         0         0
 
 _____________________________________________________________________________________________________________________________
 Control Rod Withdrawal Map ( -- Indicates fully withdrawn to 224 Steps ( 355.600 cm) )
 CRD positions defined by CRD.BNK with no core symmetry applied by CRD.SYM
 IR/JR =    1   2   3   4   5   6   7     8     9  10  11  12  13  14  15
 
   1
   2                   --      --        --        --      --
   3                       --      --          --      --
   4           --     215                --               215      --
   5               --                                          --
   6           --              --        --        --              --
   7               --                                          --
 
   8           --      --      --       215        --      --      --
 
   9               --                                          --
  10           --              --        --        --              --
  11               --                                          --
  12           --     215                --               215      --
  13                       --      --          --      --
  14                   --      --        --        --      --
  15

 Total control rod positions withdrawn in full core =  11923
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  144
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  2 Step  0 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  620.000 EFPD    

 ITE - QPANDA Flux solution 

 1/4 Core Rot.      24 Fueled Axial Nodes       4 Nodes/Assembly               1 Radial Reflector Row
 % Power= 100.00    Power= 3469.09 MW           Pressure= 2250.0 psia          Cycle Exp =   620.000 EFPD        Equil. XE/I
 % Flow = 100.00    Inlet Temp= 552.90 F        Total Steps Inserted= 45       Core  Exp =    34.721 GWd/MT      Update SM/PM

    NH NQ    K-eff         Eps Max        Eps Min     Peak Source    A-O
     1  1  0.9944464     4.5338E-02     1.9767E-03      1.47071    -0.001
     2  2  0.9932457     1.3361E-02     2.1594E-03      1.49063    -0.012
     3  3  0.9929166     1.3174E-03     4.6961E-04 *    1.49259    -0.012
     4  4  0.9928336     3.5464E-04 *   2.3013E-04 *    1.49312    -0.013
 
           *********************************************************************************************
           Search Started on        POWER  Triggered by            NONE   Trigger Value =        0.00000
           Target Variable          KEF    Target Value           1.00000 Target Epsilon         0.00005
           Init Search Value     100.00000 Init Search Delta     -5.00000 Search Epsilon         0.50000
           Max Search Delta       10.00000 Search Bound;lower     1.00000 Search Bound;upper   110.00000
           Max. #Iter Before Search      4 Max. #Iter Between Searches  3 Damping Factor =       1.00000
           *********************************************************************************************

           Present Kef Value  =   0.992834    Next Search Value  =     95.00 % Power

     5  5  0.9937816     2.0392E-02     4.5290E-03      1.46328     0.001
     6  6  0.9941142     2.7618E-05 *   9.1970E-05 *    1.46324    -0.000
     6  7  0.9942897     3.3758E-03     6.5987E-04      1.45832     0.002

           Present Kef Value  =   0.994290    Next Search Value  =     85.00 % Power

     7  8  0.9966299     4.5133E-02     1.1018E-02      1.52725     0.039
     8  9  0.9974024     7.2431E-03     5.0688E-04      1.51627     0.038
     8 10  0.9976000     1.5846E-03     1.8218E-04 *    1.51387     0.037

           Present Kef Value  =   0.997600    Next Search Value  =     77.75 % Power

     9 11  0.9992441     4.0735E-02     6.3742E-03      1.57815     0.060
    10 12  0.9997694     6.6834E-03     3.6618E-04 *    1.56768     0.059
    10 13  1.0000543     4.5686E-03     1.0219E-03      1.57487     0.062

           Present Kef Value  =   1.000054    Next Search Value  =     77.91 % Power

    11 14  1.0001083     2.5712E-03     1.3672E-04 *    1.57893     0.063
    12 15  1.0001670     1.1093E-03     3.6412E-04 *    1.58068     0.064
    12 16  1.0001818     1.0255E-03     2.6038E-04 *    1.58231     0.065

           Present Kef Value  =   1.000182    Next Search Value  =     78.45 % Power

    13 17  1.0000824     4.0837E-03     7.1862E-04      1.57587     0.062
    14 18  1.0000473     8.6269E-04     7.6592E-06 *    1.57723     0.062

           Present Kef Value  =   1.000047    Next Search Value  =     78.60 % Power

    15 19  1.0000025     1.3206E-03     1.6734E-04 *    1.57515     0.062
    16 20  0.9999881     2.1807E-04 *   1.4618E-05 *    1.57549     0.062

           Present Kef Value  =   0.999988    Next Search Value  =     78.57 % Power

           ************************************************************************
           Search Terminated at 78.57 % Power by Satisfying Search Variable Epsilon                            
           ************************************************************************
 
    17 21  0.9999907 *   4.0252E-05 *   2.3931E-05 *    1.57556     0.062

    Nodal Solution Time = 2.04 Sec

 K-effective  = 0.999991
 Axial Offset =    0.062
 Max. Node-Averaged  Peaking Factor = 1.576 in Node (13, 9, 21)
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  145
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   78.6% Power
 Case  2 Step  0 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  620.000 EFPD    

 PIN.EDT 2PIN  - Peak Pin Power:              Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  1.313  0.948  1.263  0.947  1.365  1.077  1.340  1.214   08
  9  0.948  0.920  0.845  0.821  1.034  1.364  0.998  1.194   09
 10  1.263  0.821  1.322  0.994  1.343  1.047  1.312  1.196   10
 11  0.947  0.936  0.997  1.348  0.992  0.797  0.985  1.110   11
 12  1.365  1.039  1.347  0.992  1.312  1.022  1.168          12
 13  1.077  1.366  1.050  0.798  1.023  1.303  1.137          13
 14  1.340  0.993  1.317  0.989  1.169  1.138                 14
 15  1.214  1.201  1.200  1.115                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
 ** Caution -   PINAXIS  - PIN.EDT - Pin locations for axis-spanning assemblies may not be correct
 Warning: Pin locations for axis-spanning assemblies may not be correct    
          Run problem in full-core to obtain correct locations

 PIN.EDT 2PLO  - Peak Pin Power Location:     Assembly 2D (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8  14,13   4,13   4,13  13,15  14,13   3,13   4,13   3,13   08
  9   4,13   3, 5  15,13   9,17   5,14   4, 5   5, 4   5, 3   09
 10   4,13  13, 3  14,13  14,13   4,13   5, 4   4, 5   5, 3   10
 11  13,15  14,13  13,14  13, 4   5, 4   1, 1   3, 5   1, 1   11
 12  14,13  14, 5  13, 4   4, 5   5, 4  13, 3  13, 3          12
 13   3,13   5, 4   4, 5   1, 1   3,13   5, 4   1, 1          13
 14   4,13   3, 5   5, 4   5, 3   3,13   1, 1                 14
 15   3,13   1, 9   3, 5   1, 1                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)
 Renorm =  1.00000E+00     Axial Plane =  1
 **    8      9     10     11     12     13     14     15     **
  8 21.343 48.141 21.868 51.797 29.282 57.007 29.831 26.644   08
  9 48.141 47.775 67.440 36.247 56.390 30.061 56.247 26.038   09
 10 21.868 69.021 25.552 54.407 28.206 53.515 28.039 25.064   10
 11 51.797 51.959 54.430 27.948 55.113 70.568 48.075 21.834   11
 12 29.282 56.462 28.501 55.288 26.742 51.107 21.799          12
 13 57.007 29.930 53.247 70.611 51.101 24.735 20.922          13
 14 29.831 55.466 27.832 47.924 21.796 20.921                 14
 15 26.644 25.811 24.774 21.714                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PIN.EDT QEXP  - Average Pin Exposure (GWd/T):   Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1 20.436 46.470 46.231 20.100 20.660 47.472 49.596 25.648 27.719 51.998 54.996 28.531 27.463 24.441 17.857    1
  2 46.470 44.897 45.870 64.582 61.782 25.877 30.457 48.505 53.845 28.573 28.726 54.558 53.294 23.908 17.600    2
  3 46.262 45.671 46.126 61.330 58.957 29.429 33.955 48.533 53.457 28.172 28.223 54.019 52.346 23.448 17.241    3
  4 20.194 63.019 66.767 21.062 22.356 49.499 50.455 25.820 26.888 47.033 50.143 26.736 25.855 22.985 16.691    4
  5 20.818 62.167 66.424 22.532 24.229 51.066 51.725 26.211 25.843 47.784 49.825 24.977 24.459 21.595 15.506    5
  6 47.535 48.438 48.994 49.850 51.292 26.371 26.602 52.854 50.632 68.216 64.917 41.205 45.773 18.835 13.276    6
  7 49.696 50.309 50.412 50.962 52.059 26.704 26.618 52.942 50.712 67.172 63.294 42.175 45.091 14.822  9.595    7
  8 25.851 48.927 49.099 26.327 26.500 53.004 52.997 25.475 24.537 45.652 48.145 20.019 16.434                  8
  9 27.824 54.121 53.796 27.166 26.025 50.742 50.759 24.548 24.739 47.085 49.030 19.906 14.907                  9
 10 51.993 28.615 28.233 47.007 47.756 68.254 67.184 45.646 47.087 23.615 22.068 18.788 13.162                 10
 11 54.949 28.637 28.110 49.954 49.644 64.924 63.293 48.124 49.022 22.067 19.838 15.485  9.798                 11
 12 28.466 53.248 53.668 26.517 24.837 41.096 42.118 20.009 19.902 18.786 15.484                               12
 13 27.374 53.129 52.963 25.577 24.280 45.625 45.012 16.417 14.901 13.160  9.798                               13
 14 24.343 23.674 23.157 22.725 21.432 18.744 14.776                                                           14
 15 17.799 17.445 17.049 16.526 15.391 13.206  9.558                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 PIN.EDT QRPF  - Relative Power Fraction          Nodal 2D
 Renorm =  1.00000E+00     Axial Plane =  1
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **
  1  1.267  0.913  0.896  1.213  1.212  0.896  0.910  1.270  1.305  1.044  1.015  1.281  1.233  1.114  0.859    1
  2  0.913  0.896  0.864  0.773  0.797  0.785  0.796  0.994  1.003  1.312  1.300  0.960  0.922  1.104  0.853    2
  3  0.896  0.866  0.841  0.783  0.817  0.774  0.787  0.994  1.001  1.304  1.290  0.955  0.921  1.099  0.848    3
  4  1.214  0.783  0.758  1.192  1.222  0.919  0.932  1.272  1.287  1.014  0.978  1.252  1.213  1.094  0.838    4
  5  1.210  0.795  0.776  1.218  1.260  0.954  0.958  1.278  1.257  0.947  0.919  1.215  1.185  1.061  0.803    5
  6  0.891  0.874  0.876  0.917  0.955  1.296  1.295  0.957  0.919  0.776  0.777  0.942  0.872  0.975  0.718    6
  7  0.907  0.900  0.905  0.933  0.961  1.296  1.292  0.952  0.913  0.771  0.763  0.881  0.777  0.830  0.556    7
  8  1.271  0.994  0.996  1.276  1.280  0.957  0.952  1.260  1.240  0.952  0.888  1.093  0.930                  8
  9  1.307  1.006  1.005  1.290  1.259  0.920  0.913  1.241  1.260  0.982  0.915  1.081  0.849                  9
 10  1.045  1.314  1.307  1.017  0.950  0.777  0.772  0.953  0.982  1.239  1.172  1.022  0.758                 10
 11  1.015  1.305  1.296  0.982  0.923  0.778  0.764  0.889  0.916  1.173  1.068  0.877  0.589                 11
 12  1.284  0.963  0.950  1.259  1.220  0.945  0.884  1.095  1.082  1.023  0.878                               12
 13  1.236  0.915  0.906  1.219  1.190  0.876  0.780  0.932  0.851  0.759  0.589                               13
 14  1.115  1.108  1.104  1.098  1.066  0.980  0.833                                                           14
 15  0.860  0.855  0.851  0.842  0.807  0.722  0.559                                                           15
 **    1      2      3      4      5      6      7      8      9     10     11     12     13     14     15     **

 Max. Intra-Assembly Peaking Factor = 1.645 in Node (13, 9,22)

    Pin Power Reconstruction Time = 0.29 Sec

1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  146
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   78.6% Power
 Case  2 Step  0 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  620.000 EFPD    
 _____________________________________________________________________________________________________________________________

 PRI.STA - State Point Edits 

 PRI.STA 2RPF  - Assembly 2D Ave RPF - Relative Power Fraction                                                                     
 **    8      9     10     11     12     13     14     15     **
  8  1.267  0.904  1.212  0.901  1.288  1.030  1.259  0.987   08
  9  0.904  0.867  0.792  0.786  0.998  1.301  0.940  0.976   09
 10  1.212  0.778  1.223  0.941  1.273  0.965  1.216  0.949   10
 11  0.901  0.889  0.941  1.295  0.935  0.772  0.868  0.770   11
 12  1.288  1.000  1.276  0.935  1.250  0.934  0.988          12
 13  1.030  1.305  0.968  0.773  0.935  1.163  0.812          13
 14  1.259  0.934  1.222  0.871  0.990  0.812                 14
 15  0.987  0.979  0.953  0.773                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2KIN  - Assembly 2D Ave KINF - K-infinity
 **    8      9     10     11     12     13     14     15     **
  8  1.128  0.977  1.143  0.965  1.097  0.957  1.090  1.146   08
  9  0.977  0.982  0.913  0.929  0.970  1.086  0.950  1.150   09
 10  1.143  0.900  1.128  0.961  1.101  0.977  1.107  1.161   10
 11  0.965  0.970  0.959  1.098  0.960  0.895  1.015  1.209   11
 12  1.097  0.967  1.099  0.960  1.097  0.990  1.171          12
 13  0.957  1.086  0.978  0.895  0.990  1.134  1.206          13
 14  1.090  0.940  1.108  1.015  1.171  1.206                 14
 15  1.146  1.152  1.162  1.209                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EXP  - Assembly 2D Ave EXPOSURE  - GWD/T                                                                                 
 **    8      9     10     11     12     13     14     15     **
  8 20.436 46.358 20.443 48.575 26.760 53.484 27.959 21.110   08
  9 46.358 45.641 61.663 29.929 51.085 28.423 53.554 20.549   09
 10 20.443 64.594 22.545 50.686 26.191 48.697 25.507 19.194   10
 11 48.575 49.538 51.041 26.574 51.785 65.900 43.561 14.132   11
 12 26.760 51.486 26.505 51.875 24.825 47.478 17.816          12
 13 53.484 28.399 48.590 65.914 47.470 21.897 14.308          13
 14 27.959 53.252 25.303 43.462 17.807 14.307                 14
 15 21.110 20.331 19.019 14.071                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **

 PRI.STA 2EBP  - Assembly 2D Ave EBP  - BURNABLE POISON EXPOSURE - GWD/T                                                           
 **    8      9     10     11     12     13     14     15     **
  8   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   08
  9   0.00   0.00   0.00   0.00   0.00   0.00  51.95   0.00   09
 10   0.00   0.00   0.00  49.10   0.00  47.26   0.00   0.00   10
 11   0.00  47.98  49.45   0.00  50.20   0.00   0.00   0.00   11
 12   0.00   0.00   0.00  50.29   0.00   0.00   0.00          12
 13   0.00   0.00  47.15   0.00   0.00   0.00   0.00          13
 14   0.00   0.00   0.00   0.00   0.00   0.00                 14
 15   0.00   0.00   0.00   0.00                               15
 **   H-     G-     F-     E-     D-     C-     B-     A-     **
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  147
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   78.6% Power
 Case  2 Step  0 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  620.000 EFPD    
 ____________________________________________________________________________________________________________________________

 BAT.EDT - ON - Batch Edits


             2EXP  - EXPOSURE - GWD/T                  SCALE =    1.000E+00

              BATCH                          MAXIMUM                                MINIMUM                     AVERAGE

     Number   Name   Assemblies       2EXP  Label  Serial  Location       2EXP   Label   Serial  Location         2EXP
     ------  ------  ----------       ----- ------ ------ ----------      -----  ------  ------ ----------        -----
        1    24B        97            28.42 25C-09 sil55  ( 9,13,  )      14.07  25E-15  sil96  (15,11,  )        21.82
        6    26A        16            53.25 24L-03 24L-03 (14, 9,  )      45.64  24N-05  24N-05 ( 9, 9,  )        48.46
        8    26C        48            53.55 24J-07 24J-07 ( 9,14,  )      43.46  24L-02  24L-02 (14,11,  )        49.38
        5    25C        16            65.91 24J-03 24J-03 (13,11,  )      61.66  24K-06  24K-06 ( 9,10,  )        64.52
        9    26D         4            29.93 24N-03 24N-03 ( 9,11,  )      29.93  24N-03  24N-03 ( 9,11,  )        29.93
        7    26B        12            51.04 24J-05 24J-05 (11,10,  )      49.54  24L-05  24L-05 (11, 9,  )        50.42

     CORE              193            65.91 24J-03 24J-03 (13,11,  )      14.07  25E-15  sil96  (15,11,  )        34.72


             QXPO  -  PEAK PIN EXPOSURE                SCALE =    1.000E+00

              BATCH                          MAXIMUM

     Number   Name   Assemblies       QXPO  Label  Serial  Location
     ------  ------  ----------       ----- ------ ------ ----------
        1    24B        97            30.06 25C-09 sil55  ( 2,11,  )
        6    26A        16            55.47 24L-03 24L-03 (12, 3,  )
        8    26C        48            57.01 24P-08 24P-08 ( 1,11,  )
        5    25C        16            70.61 24J-03 24J-03 (10, 6,  )
        9    26D         4            36.25 24N-03 24N-03 ( 3, 7,  )
        7    26B        12            54.43 24J-05 24J-05 ( 7, 5,  )

     CORE              193            70.61 24J-03 24J-03 (10, 6,  )
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  148
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   78.6% Power
 Case  2 Step  0 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  620.000 EFPD    

 Output Summary                                                            Reference State Point      0  Ref Exp     0.000
                                                                           
 Relative Power. . . . . . .PERCTP    78.6 %                               Core Average Exposure . . . . EBAR  34.721 GWd/MT
 Relative Flow . . . . . .  PERCWT   100.0 %                               Cycle Exp. 620.0 EFPD 14880.0 EFPH  21.697 GWd/MT
 Thermal Power . . . . . . . . CTP  2725.5 MWt                             Depletion Step Length . . . . .  8.064E+02 Hours
 Core Flow . . . . . . . . . . CWT  61591. MT/hr   135.78 Mlb/hr           Depletion =  1.050 * (0.5 P-BOS + 0.5 P-EOS)
 Bypass Flow . . . . . . . . . CWL      0. MT/hr     0.00 Mlb/hr           Fission Product Option: Eq  Xe, (Dep Sm)
 Inlet Subcooling (PRMEAS) .SUBCOL   83.02 kcal/kg 149.43 Btu/lb           Buckling (2D-USED:3D-ACTUAL). .  1.672E-04  CM-2
 Inlet Enthalpy. . . . . . .HINLET  306.60 kcal/kg 551.88 Btu/lb           Search Eigenvalue . . . . . . .    1.00000
 Temperatures: Inlet . . . .TINLET  563.03 K       553.78 F                Search for K-effective
       Coolant Average . . .TMOAVE  577.24 K       579.37 F                Peak Nodal Power (Location)     1.576 (13, 9,21)
       Fuel    Average . . .TFUAVE  789.41 K       961.27 F                Peak Pin Powers  (Location) [PINFILE integration]
 Coolant Volume-Averaged . .TMOVOL  577.01 K       578.95 F                  F-delta-H       1.366 (13, 9)      [On]
 Core Exit Pressure  . . . . .  PR  155.14 bar     2250.0 PSIA               Max-Fxy         1.390 (12, 8, 2)   [Off]
 Boron Conc. . . . . . . . . . BOR     4.3 ppm                               Max-3PIN        1.640 (13, 9,21)   [Off]
 CRD Positions Inserted. . . NOTWT      45                                   Max-4PIN        1.645 (13, 9,22)   [Off]
 Hydraulic Iterations                                                      K-effective . . . . . . . . . . . . .  0.99999
 Axial Offset. . . . . . . . . A-O   0.062                                 Total Neutron Leakage . . . . . . . . .  0.044
 Edit Ring -     1      2      3      4      5
 RPF            0.928  0.940  1.097  0.998  0.792
 KIN            0.996  0.992  1.017  1.067  1.207
 CRD            0.004  0.000  0.000  0.002  0.000
 DEN            0.720  0.719  0.715  0.718  0.724
 TFU (Deg K)    775.2  777.2  817.9  785.4  732.2
 TMO (Deg K)    576.1  576.2  578.2  577.0  574.3
 Core Fraction  0.047  0.145  0.311  0.415  0.083

 Average Axial Distributions for State Point Variables

   K      RPF      KINF      EXPO      CRD       DEN       TFU       TMO 
 
  24   0.53014   1.07102  16.56411   0.02288   0.68499   695.860   591.084
  23   1.03658   1.11283  25.73470   0.00000   0.68720   802.565   590.231
  22   1.15388   1.07003  31.10032   0.00000   0.69027   833.274   589.029
  21   1.19710   1.05106  34.68835   0.00000   0.69355   846.955   587.727
  20   1.17947   1.04194  36.08997   0.00000   0.69682   842.882   586.399
  19   1.14831   1.03773  36.57408   0.00000   0.70000   834.115   585.086
 
  18   1.12630   1.03841  37.03015   0.00000   0.70308   827.676   583.792
  17   1.10494   1.03763  37.18156   0.00000   0.70608   821.080   582.511
  16   1.09001   1.03817  37.38596   0.00000   0.70900   816.226   581.242
  15   1.06556   1.03697  37.02768   0.00000   0.71185   808.361   579.987
  14   1.06038   1.03861  37.47360   0.00000   0.71463   806.276   578.738
  13   1.04796   1.03752  37.54858   0.00000   0.71738   801.979   577.493
 
  12   1.02814   1.03681  37.29301   0.00000   0.72006   795.494   576.258
  11   1.02291   1.03837  37.73687   0.00000   0.72269   793.388   575.029
  10   1.01245   1.03780  37.90516   0.00000   0.72528   789.703   573.804
   9   1.00061   1.03782  37.94220   0.00000   0.72782   785.570   572.583
   8   0.98746   1.03541  37.76214   0.00000   0.73032   780.937   571.372
   7   0.99161   1.03637  38.25956   0.00000   0.73278   781.223   570.158
 
   6   0.99355   1.03650  38.25211   0.00000   0.73525   780.476   568.935
   5   0.99111   1.03747  37.48407   0.00000   0.73769   777.903   567.705
   4   1.00158   1.04589  36.79585   0.00000   0.74012   778.550   566.463
   3   0.97452   1.06530  33.97701   0.00000   0.74252   768.086   565.225
   2   0.86018   1.11135  27.84056   0.00000   0.74474   736.704   564.073
   1   0.39525   1.04414  16.29554   0.00000   0.74624   640.580   563.278
 
 Ave   1.00000   1.04897  34.72109   0.00095   0.71751   789.411   577.008
 P**2                     33.56581
 A-O   0.06172   0.00193  -0.01599   1.00000  -0.02269     0.028     0.013
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  149
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   78.6% Power
 Case  2 Step  0 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  620.000 EFPD    

 Average Axial Distributions for Depletion Arguments

  K       IOD          XEN          PRO          SAM          EXP          HTM          HTF          HBO          EBP
 
  24  2.96073E-09  1.31100E-09  6.59733E-09  1.57207E-08  1.65641E+01  6.64205E-01  2.65928E+01  6.51946E+02  0.00000E+00
  23  5.79581E-09  3.51669E-09  1.25769E-08  4.78973E-08  2.57347E+01  6.69013E-01  2.86572E+01  6.11018E+02  5.71474E+00
  22  6.46315E-09  3.73434E-09  1.45360E-08  5.06891E-08  3.11003E+01  6.72459E-01  2.93577E+01  6.48867E+02  6.72402E+00
  21  6.70765E-09  3.68170E-09  1.53996E-08  4.93244E-08  3.46884E+01  6.76040E-01  2.97880E+01  6.79509E+02  7.45239E+00
  20  6.61040E-09  3.63901E-09  1.52980E-08  4.92009E-08  3.60900E+01  6.79835E-01  2.98731E+01  7.01875E+02  7.74146E+00
  19  6.43689E-09  3.60284E-09  1.49472E-08  4.93678E-08  3.65741E+01  6.83672E-01  2.98345E+01  7.17755E+02  7.84217E+00
 
  18  6.31298E-09  3.53600E-09  1.47062E-08  4.86459E-08  3.70302E+01  6.87481E-01  2.98186E+01  7.28975E+02  7.93472E+00
  17  6.19286E-09  3.49193E-09  1.44547E-08  4.83517E-08  3.71816E+01  6.91283E-01  2.97743E+01  7.35911E+02  7.96510E+00
  16  6.10891E-09  3.44479E-09  1.42916E-08  4.78322E-08  3.73860E+01  6.95053E-01  2.97548E+01  7.40215E+02  8.00465E+00
  15  5.97318E-09  3.45151E-09  1.39733E-08  4.86702E-08  3.70277E+01  6.98802E-01  2.96550E+01  7.41663E+02  7.93059E+00
  14  5.94298E-09  3.38583E-09  1.39529E-08  4.75151E-08  3.74736E+01  7.02478E-01  2.96934E+01  7.42903E+02  8.01633E+00
  13  5.87331E-09  3.35829E-09  1.38186E-08  4.73513E-08  3.75486E+01  7.06162E-01  2.96775E+01  7.42627E+02  8.02746E+00
 
  12  5.76339E-09  3.35952E-09  1.35698E-08  4.79902E-08  3.72930E+01  7.09812E-01  2.96112E+01  7.41264E+02  7.97164E+00
  11  5.73295E-09  3.29699E-09  1.35506E-08  4.68946E-08  3.77369E+01  7.13414E-01  2.96628E+01  7.40262E+02  8.05377E+00
  10  5.67394E-09  3.26246E-09  1.34484E-08  4.65399E-08  3.79052E+01  7.17026E-01  2.96744E+01  7.37960E+02  8.08164E+00
   9  5.60822E-09  3.24469E-09  1.33231E-08  4.65787E-08  3.79422E+01  7.20625E-01  2.96715E+01  7.34455E+02  8.08363E+00
   8  5.53560E-09  3.25070E-09  1.31691E-08  4.71786E-08  3.77621E+01  7.24203E-01  2.96435E+01  7.28783E+02  8.04338E+00
   7  5.55771E-09  3.19704E-09  1.32796E-08  4.60073E-08  3.82596E+01  7.27770E-01  2.97441E+01  7.21706E+02  8.13490E+00
 
   6  5.56865E-09  3.18798E-09  1.33325E-08  4.58386E-08  3.82521E+01  7.31362E-01  2.97840E+01  7.10373E+02  8.12670E+00
   5  5.55632E-09  3.23010E-09  1.32796E-08  4.68194E-08  3.74841E+01  7.34933E-01  2.97239E+01  6.93348E+02  7.96927E+00
   4  5.61283E-09  3.20325E-09  1.33846E-08  4.58468E-08  3.67958E+01  7.38463E-01  2.97112E+01  6.71223E+02  7.82665E+00
   3  5.45803E-09  3.19469E-09  1.28276E-08  4.59109E-08  3.39770E+01  7.41876E-01  2.93482E+01  6.40768E+02  7.27153E+00
   2  4.80852E-09  3.04569E-09  1.08681E-08  4.47603E-08  2.78406E+01  7.44928E-01  2.84257E+01  6.03349E+02  6.12799E+00
   1  2.20907E-09  1.18553E-09  5.04556E-09  1.61240E-08  1.62955E+01  7.46943E-01  2.58877E+01  6.66758E+02  0.00000E+00
 
 Ave   5.6027E-09   3.2005E-09   1.3068E-08   4.4877E-08   3.4721E+01   7.0741E-01   2.9307E+01   7.0140E+02   7.2307E+00
 A-O        0.062        0.046        0.049        0.022       -0.016       -0.031        0.002        0.003       -0.014
 

  K       EY-          EX+          EY+          EX-          HIS          HY-          HX+          HY+          HX-
 
  24  1.66819E+01  1.62852E+01  1.63116E+01  1.67169E+01  9.68599E-01  9.66723E-01  9.82593E-01  9.82688E-01  9.66787E-01
  23  2.60014E+01  2.54205E+01  2.54504E+01  2.60406E+01  9.34905E-01  9.37211E-01  9.19188E-01  9.19338E-01  9.37260E-01
  22  3.14265E+01  3.07474E+01  3.07789E+01  3.14677E+01  1.01761E+00  1.02129E+00  9.97998E-01  9.98233E-01  1.02139E+00
  21  3.50659E+01  3.43013E+01  3.43352E+01  3.51098E+01  9.87465E-01  9.90148E-01  9.69318E-01  9.69540E-01  9.90280E-01
  20  3.64752E+01  3.56916E+01  3.57272E+01  3.65211E+01  9.94759E-01  9.97247E-01  9.76499E-01  9.76717E-01  9.97385E-01
  19  3.69536E+01  3.61719E+01  3.62086E+01  3.70008E+01  1.00867E+00  1.01116E+00  9.90070E-01  9.90288E-01  1.01129E+00
 
  18  3.74173E+01  3.66222E+01  3.66597E+01  3.74654E+01  9.95520E-01  9.97419E-01  9.77735E-01  9.77949E-01  9.97560E-01
  17  3.75683E+01  3.67717E+01  3.68099E+01  3.76172E+01  9.94562E-01  9.96162E-01  9.77011E-01  9.77227E-01  9.96306E-01
  16  3.77776E+01  3.69711E+01  3.70100E+01  3.78271E+01  9.86316E-01  9.87517E-01  9.69360E-01  9.69572E-01  9.87662E-01
  15  3.73992E+01  3.66155E+01  3.66545E+01  3.74494E+01  1.01767E+00  1.01932E+00  9.99642E-01  9.99856E-01  1.01944E+00
  14  3.78632E+01  3.70537E+01  3.70936E+01  3.79139E+01  9.89919E-01  9.90757E-01  9.73245E-01  9.73455E-01  9.90899E-01
  13  3.79382E+01  3.71259E+01  3.71662E+01  3.79894E+01  9.91368E-01  9.92036E-01  9.74821E-01  9.75029E-01  9.92177E-01
 
  12  3.76672E+01  3.68712E+01  3.69117E+01  3.77191E+01  1.01607E+00  1.01705E+00  9.98731E-01  9.98941E-01  1.01718E+00
  11  3.81291E+01  3.73076E+01  3.73490E+01  3.81815E+01  9.89163E-01  9.89382E-01  9.73129E-01  9.73333E-01  9.89521E-01
  10  3.83019E+01  3.74731E+01  3.75150E+01  3.83549E+01  9.84425E-01  9.84333E-01  9.68734E-01  9.68936E-01  9.84476E-01
   9  3.83366E+01  3.75065E+01  3.75488E+01  3.83901E+01  9.91023E-01  9.90905E-01  9.75303E-01  9.75503E-01  9.91040E-01
   8  3.81436E+01  3.73270E+01  3.73694E+01  3.81977E+01  1.01466E+00  1.01486E+00  9.98155E-01  9.98355E-01  1.01498E+00
   7  3.86643E+01  3.78164E+01  3.78596E+01  3.87187E+01  9.85058E-01  9.84485E-01  9.69908E-01  9.70100E-01  9.84623E-01
 
   6  3.86598E+01  3.78057E+01  3.78491E+01  3.87146E+01  9.85106E-01  9.84403E-01  9.70133E-01  9.70323E-01  9.84538E-01
   5  3.78696E+01  3.70416E+01  3.70846E+01  3.79243E+01  1.01873E+00  1.01861E+00  1.00260E+00  1.00279E+00  1.01872E+00
   4  3.71930E+01  3.63542E+01  3.63968E+01  3.72468E+01  9.90480E-01  9.89748E-01  9.75677E-01  9.75869E-01  9.89867E-01
   3  3.43424E+01  3.35590E+01  3.35997E+01  3.43941E+01  9.87536E-01  9.86779E-01  9.73008E-01  9.73208E-01  9.86882E-01
   2  2.81235E+01  2.74728E+01  2.75095E+01  2.81700E+01  9.47092E-01  9.45874E-01  9.34628E-01  9.34774E-01  9.45958E-01
   1  1.63923E+01  1.60063E+01  1.60335E+01  1.64254E+01  1.02536E+00  1.02291E+00  1.04133E+00  1.04142E+00  1.02301E+00
 
 Ave   3.5080E+01   3.4317E+01   3.4355E+01   3.5129E+01   9.9259E-01   9.9318E-01   9.7870E-01   9.7889E-01   9.9330E-01
 A-O       -0.016       -0.016       -0.016       -0.016       -0.002       -0.001       -0.003       -0.003       -0.001
 
 _____________________________________________________________________________________________________________________________

 WRE - Writing to restart file c1c25eoc.res
       at exposure =     620.0000 EFPD    
 ** Note    -   NOWRITE  - WRE - ITE.SRC card in input stream not written to restart file
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  150
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   78.6% Power
 Case  2 Step  0 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  620.000 EFPD    

 Memory required was 5554826 / 30000000 ( 18.52 % )

1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  151
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow   78.6% Power
 Case  3 Step  0 c1c24 HFP NOMINAL DEPLETION                                                        4.32 ppm  620.000 EFPD    

 CASE   3  --- Entering Input Segment ---
 == Dependent Case: 2 of Independent Case 1 Complete                                                 

           --- Reading Input Cards ---
 _____________________________________________________________________________________________________________________________
 Input Cards:  END    
 Input Cards:  

 Memory Cleared by END Card
           --- Reading Input Cards ---
 _____________________________________________________________________________________________________________________________
 _____________________________________________________________________________________________________________________________

 S u m m a r y   o f   E r r o r s / W a r n i n g s / C a u t i o n s   a n d   N o t e s          

 Label   Times   Degree     Where      Info  
 -----   -----   ------     -----      ----  

 SYMGRP G    1   WARNING    RES STEP   not quarter rotational
 SYMGRP E    1   WARNING    RES STEP   not quarter rotational
 BOR         7   WARNING    COR.BOR    Abnormally High : 3.055E+03 PPM > 2.800E+03 PPM (Upper Range Value)
 SYMGRP C    1   WARNING    RES STEP   not quarter rotational
 SYMGRP F    1   WARNING    RES STEP   not quarter rotational
 SYMGRP B    1   WARNING    RES STEP   not quarter rotational
 DEPCYC      1   WARNING    DEP.CYC    DEP.CYC card overrides starting or ending date from restart file for cycle: 25
 SYMGRP A    1   WARNING    RES STEP   not quarter rotational
 SYMGRP D    1   WARNING    RES STEP   not quarter rotational
 PINAXIS    17   CAUTION    PIN.EDT    Pin locations for axis-spanning assemblies may not be correct
 CONVERGE    1   NOTE       ITE.EXT    Problem is not converging as expected
 ITEBOR      1   NOTE       ITE.BOR    Critical boron search requested by input
 FLATPOW     1   NOTE       ITE.LIM    Flat power distribution used as initial guess
 PINFIL      2   NOTE       PIN.FIL    PIN.FIL ON --Pin edits will reflect axial integration
 XPORST      1   NOTE       DEP.CYC    DEP.CYC card has reset cycle exposure from: 537.000 to: 0.000 for cycle: 25
 NOWRITE     1   NOTE       WRE        ITE.SRC card in input stream not written to restart file
 ITESRC      1   NOTE       ITE.SRC    POWER Search requested with KEF as target
 EXPOSURE    2   NOTE       ERR.CHK    2D exp. in symmetry check are nodal averages --Loading not taken into account

 Errors can be permitted by listing the error's label on card 'ERR.CHK' 

 Present Error Checker Related Input:
      'ERR.DAT'    0.2500E-01/
      'ERR.CHK'/
      'ERR.SYM' 'ROT' 2/
      'ERR.SUP'/



1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  152
 Run: RUN NAME  ** Project: PROJECT NAME             ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0 READING INPUT                                                                      0.00 ppm    0.000 GWd/MT  


 S u m m a r y   o f   F i l e   A c t i v i t y

 == Read Restart File: c1c24eoc.res at Exp= 537.000 EFPD
 ==      Created by S-3 Version: 1.00.02  at 15:44:35 on 18/01/12
 == Read Library XS File: ./cms.pwr-all2.lib
 ==      Created by CMS-Link version: 1.00.02 at 16:58:33 on 2018/01/07
 == Wrote Pinfile: PINFILE at Exp= 0.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 25.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 50.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 75.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 100.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 125.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 150.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 200.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 250.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 300.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 350.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 400.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 450.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 500.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 550.000 EFPD
 == Wrote Pinfile: PINFILE at Exp= 590.000 EFPD
 == Title: c1c24 HFP NOMINAL DEPLETION
 == Dependent Case: 1 of Independent Case 1 Complete
 
 == Wrote Pinfile: PINFILE at Exp= 620.000 EFPD
 == Wrote Restart File: c1c25eoc.res at EXP= 620.000 EFPD %POWER= 78.566 %FLOW= 100.000
 == Title: c1c24 HFP NOMINAL DEPLETION
 == Dependent Case: 2 of Independent Case 1 Complete
 


 P W R  S u m m a r y   o f   S t e a d y - S t a t e   S I M U L A T E - 3   V e r s i o n  1 . 0 0 . 0 2                        

 Summary File Name = SUMMARY

  Case     Step             Bor  - - - - - Peak Powers - - - - -    Dens  Power  Flow  CRD   Pres  Inlet  Core     Title 
    Step   Exp    K-eff  NQ ppm   AX / K   A-O    Rad  Node  3PIN   g/cc    %     %    Pos   psia  T (F)  Exp     Exp   Set

   1  0     0.0  1.00000  9 2874 1.33/13  0.017  1.85  2.54  2.66  0.708  100.0 100.0    45  2250  552.9 13.024   0.00    0 
   1  1    25.0  1.00000  6 2629 1.29/13  0.009  1.78  2.38  2.49  0.708  100.0 100.0    45  2250  552.9 13.899   0.00    0 
   1  2    50.0  1.00000  6 2490 1.26/13  0.005  1.73  2.25  2.36  0.708  100.0 100.0    45  2250  552.9 14.773   0.00    0 
   1  3    75.0  1.00000  6 2351 1.22/13  0.001  1.68  2.12  2.23  0.708  100.0 100.0    45  2250  552.9 15.648   0.00    0 
   1  4   100.0  1.00000  6 2211 1.20/13 -0.002  1.63  2.02  2.12  0.708  100.0 100.0    45  2250  552.9 16.523   0.00    0 
 
   1  5   125.0  1.00000  6 2074 1.17/10 -0.005  1.59  1.95  2.05  0.708  100.0 100.0    45  2250  552.9 17.398   0.00    0 
   1  6   150.0  1.00000  6 1939 1.16/ 7 -0.008  1.55  1.89  1.98  0.708  100.0 100.0    45  2250  552.9 18.273   0.00    0 
   1  7   200.0  1.00000  6 1679 1.15/ 6 -0.013  1.50  1.80  1.89  0.708  100.0 100.0    45  2250  552.9 20.023   0.00    0 
   1  8   250.0  1.00000  6 1431 1.15/ 6 -0.017  1.47  1.74  1.82  0.708  100.0 100.0    45  2250  552.9 21.773   0.00    0 
   1  9   300.0  1.00000  6 1196 1.15/ 6 -0.020  1.43  1.68  1.75  0.708  100.0 100.0    45  2250  552.9 23.522   0.00    0 
 
   1 10   350.0  1.00000  6  970 1.14/ 6 -0.022  1.40  1.64  1.71  0.708  100.0 100.0    45  2250  552.9 25.272   0.00    0 
   1 11   400.0  1.00000  6  752 1.15/ 4 -0.023  1.37  1.61  1.68  0.708  100.0 100.0    45  2250  552.9 27.022   0.00    0 
   1 12   450.0  1.00000  6  544 1.15/ 4 -0.024  1.35  1.59  1.65  0.708  100.0 100.0    45  2250  552.9 28.772   0.00    0 
   1 13   500.0  1.00000  6  344 1.15/ 4 -0.025  1.33  1.56  1.62  0.708  100.0 100.0    45  2250  552.9 30.522   0.00    0 
   1 14   550.0  1.00000  6  152 1.15/ 4 -0.025  1.31  1.53  1.60  0.708  100.0 100.0    45  2250  552.9 32.271   0.00    0 
 
   1 15   590.0  1.00000  7    4 1.15/ 4 -0.024  1.30  1.54  1.60  0.708  100.0 100.0    45  2250  552.9 33.671   0.00    0 
   2  0   620.0  0.99999 21    4 1.20/21  0.062  1.30  1.58  1.64  0.718   78.6 100.0    45  2250  553.8 34.721   0.00    0 
 
1S I M U L A T E - 3 ** Studsvik CMS Steady-State 3-D Reactor Simulator ** 1.00.02 _BASE   INT Created 12/05/08 ** Page  153
 Run: NOM DEPL  ** Project: C1C24 SIM3 FFCD          ** Run Time: 19.18.08.** Date: 2018/01/12    100.0% Flow  100.0% Power
 Case  1 Step  0                                                                                    0.00 ppm    0.000 GWd/MT  

                         Report on CPU Time in Seconds By Subroutine

                                                                                     Milli-Sec
                         Subroutine         CPU Time     % of Total         Calls     per Call
                         ----------         --------     ----------         -----     --------
                         SIGNEW                 8.38          44.64        6084.0         1.38
                         PINS                   4.90          26.14          17.0       288.51
                         SWEEP                  2.70          14.39         121.0        22.32
                         TWOGRP                 1.88          10.00         121.0        15.51
                         NEWGRD                 0.32           1.72        3450.0         0.09
                         PARTB                  0.19           1.01          17.0        11.16
                         HYDPWR                 0.13           0.67         117.0         1.07
                         SIGCAL                 0.07           0.40         117.0         0.64
                         SIMULA                 0.06           0.33           1.0        61.84
                         QPANDA                 0.06           0.30         121.0         0.46
                         PINEDT                 0.04           0.20          17.0         2.23
                         SIGPIN                 0.03           0.15        2264.0         0.01
                         RODMUV                 0.01           0.04         117.0         0.07
                         GRDFRC                 0.00           0.00           1.0         0.00

 Total CPU Time       18.799 Seconds

 Start Time/Date  19:18:08  18/01/12
 End   Time/Date  19:18:28  18/01/12

 Elapsed Real Time    20.000 Seconds
 CPU Utilization      93.99%

 Maximum Container Usage:   5621386 words
`;